/* eslint-disable no-undef */
/* eslint-disable @typescript-eslint/no-var-requires */

const path = require('path');
const nodeExternals = require('webpack-node-externals');

__dirname = path.join(__dirname, "/");

const inputDir = __dirname + 'src/';

module.exports = {
    entry: path.join(inputDir, 'app.ts'),
    target : 'node',
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
                exclude: [
                    /node_modules/
                ]
            },
            { test: /\.sql$/, loader: 'ignore-loader' }
        ],
    },
    resolve: {
        extensions: [".tsx", ".ts"]
    },
    externals: [nodeExternals()]
};