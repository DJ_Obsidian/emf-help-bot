import * as Discord from 'discord.js'
import * as Path from 'path';
import { AwardNames } from './award-names';
import { DbContext, DbContextProvider } from './db/context';
import { IAward } from './db/entities/award';
import Logger from './logger';

export class Awards {

    directory = './imgs';

    medals: IAward[];

    constructor(private dbContextProvider: DbContextProvider, private logger: Logger) {
    }

    async loadMedals(db: DbContext): Promise<void> {
        this.medals = await db.awardRepo.getMedals();
    }

    awardMedalResponse(userId: string, medal: string, description: string): Promise<unknown> {
        medal = medal.toLowerCase();
        if (this.medals.findIndex(m => m.name === medal) === -1) {
            return Promise.resolve(null);
        }

        const embed = new Discord.MessageEmbed();
        embed.setTitle(':military_medal: Награда');
        embed.addField('От имени командования эскадрильи EMF, почетно награждается Командир:', `<@${userId}>`);
 
        const medalpic = this.medals.find(m => m.name === medal).file;

        embed.setImage("attachment://" + medalpic);
        embed.addField('Тип:', medal);
        embed.addField('Описание', description);
        return Promise.resolve({
            files: [new Discord.MessageAttachment(Path.join(this.directory, medalpic))],
            embed: embed
        });
    }

    async addUserAward(userid: string, medal: string, description: string): Promise<void> {
        await this.dbContextProvider.execute(async db => {
            await db.userRepo.insert({
                userid: userid,
                usernametag: '',
            });

            const medalId = this.medals.find(m => m.name === medal).id;
            await db.awardRepo.insertAwardUser({
                award_id: medalId,
                userid: userid,
                description: description,
                created: new Date(),
            });
            this.logger.log(`Added medal ${medal}(${AwardNames[medalId]}) for user ${userid}`);
        });
    }

    async getAwardsMessage(userid: string, userName: string, db: DbContext): Promise<unknown> {
        const userMedals = await db.awardRepo.getUserMedalCounts(userid);
        let message = "";
        if (userMedals.length > 0) {
            message += "```Награды " + userName + "\n";
            userMedals.forEach(medal => message += `${this.medals.find(x => x.id === medal.id).name}: ${medal.count}\n`);
        } else {
            message += "```Наград нет.\n"
        }

        message += "Сообщение будет удалено через 2 минуты\n```";
        return message;
    }
}