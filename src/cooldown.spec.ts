import { Cooldown } from "./cooldown";

describe("Cooldown", () => {

    let cooldown: Cooldown;

    beforeAll(done => {
        cooldown = new Cooldown(0.3);
        cooldown.deleteCallback = () => done();
    });

    it("should invoke delete callback", () => {
        // if this executes that means it works.
    });
});