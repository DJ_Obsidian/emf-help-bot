import { CommandValues } from "./command-values";

export interface ICooldown {
    userid?: string;
    command?: CommandValues;
    seconds: number;
    created?: Date;
    callback?: () => void;
}

export class Cooldown implements ICooldown {
    
    private _timeoutId: NodeJS.Timeout;

    command?: CommandValues;

    userid?: string;

    created: Date;

    deleteCallback: () => void = () => { return; }; 

    constructor(public seconds: number) {
        this.created = new Date();
        this._timeoutId = setTimeout(() => {
            this.expire();
        }, 1000 * seconds);
    }

    expire(): void {
        clearTimeout(this._timeoutId);
        this.deleteCallback();
    }
}