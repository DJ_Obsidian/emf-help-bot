import { BgsClient } from "./bgs-client";
import EmfBot from "./bot";
import { HttpClientHelper } from "./client-helper";
import { CommandProcessor } from "./command-processor";
import { CooldownManager } from "./cooldown-manager";
import { createPool, DbContextProvider, MigratorDbContextProvider } from "./db/context";
import { client, DiscordServer } from "./discord-server";
import FileHelper from "./file-helper";
import Logger from "./logger";
import { Migrator } from "./db/meegrator";
import { TaskProcessor } from "./task-processor";
import LongCooldownManager from "./long-cooldown-manager";
import { Arks } from "./arks";
import Helpers from "./helpers";
import { TaskScheduler } from "./task-scheduler";
import { Awards } from "./awards";
import { SettingsHelper } from "./settings-helper";
import { MusicPlayer } from "./music/music-player";
import { YoutubeStreamer } from "./music/youtube-streamer";
import { MusicAutoDisconnectTimedInvoker } from "./music/music-auto-disconnect-invoker";
import RssFeedGetter from "./rss/rss-feed-getter";
import { RssFeedFilter } from "./rss/rss-feed-filter";

const fileHelper = new FileHelper();
const settingsHelper = new SettingsHelper(fileHelper);
const logger = new Logger(fileHelper, settingsHelper);
createPool(settingsHelper.settings.db, logger);
const httpClientHelper = new HttpClientHelper();
const dbContextProvider = new DbContextProvider(logger);
const taskScheduler = new TaskScheduler(fileHelper, logger);
const taskProcessor = new TaskProcessor(dbContextProvider, fileHelper, taskScheduler, logger);
const bgsClient = new BgsClient(httpClientHelper, fileHelper, logger, settingsHelper);
const discordServer = new DiscordServer(client, dbContextProvider, logger, settingsHelper);
const cooldownManager = new CooldownManager(logger);
const longCooldownManager = new LongCooldownManager(dbContextProvider, taskProcessor, logger);

const migratorDbContextProvider = new MigratorDbContextProvider(logger);
const migrator = new Migrator(migratorDbContextProvider, fileHelper, logger);

const awards = new Awards(dbContextProvider, logger);
const arks = new Arks(dbContextProvider, fileHelper, logger);

const musicPlayer = new MusicPlayer(new YoutubeStreamer(), dbContextProvider, logger);
const musicAutoDisconnectInvoker = new MusicAutoDisconnectTimedInvoker(musicPlayer, settingsHelper);
const rss = new RssFeedGetter(new RssFeedFilter(fileHelper, logger), logger);

const bot = new EmfBot(
    discordServer,
    new CommandProcessor(),
    taskScheduler,
    taskProcessor,
    bgsClient,
    cooldownManager,
    dbContextProvider,
    longCooldownManager,
    awards,
    arks,
    logger,
    settingsHelper,
    fileHelper,
    musicPlayer,
    musicAutoDisconnectInvoker,
    rss);

if (!Helpers.isProduction) {
    logger.log('<<<<<<<<<BOT RUNNING IN DEVELOPMENT MODE>>>>>>>>>');
}

function restartProcesses(): void {
    bot.restartProcesses().then(() => logger.log('Restarted processes.'));
}

process.on('unhandledRejection', (error: any) => {
    logger.error(error.stack || error, 'unhandledRejection');
    restartProcesses();
});

process.on('uncaughtException', (error, origin) => {
    logger.error('uncaughtException: ' + error.toString());
    logger.error(error.stack);
    if (origin) {
        logger.error('Origin: ' + origin.toString());
    }

    restartProcesses();
});

process.on('exit', (code) => {
    logger.error(`Shutdown: ${code}`);
});

Helpers.inSequence([
    () => migrator.run(), // Run any pending migrations
    () => arks.importData(), // Arks - import file if specified
]).then(() => {
    // Start server
    bot.start()
        .then(() => {
            logger.log("Bot started.");
        })
        .catch(err => {
            logger.error(err, `Failed to start`);
            process.exit(1);
        });
}).catch(err => {
    logger.error(err);
    process.exit(1);
});