import { MigratorDbContextProvider, DbContext } from "./context";
import { IMigration } from "./entities/migration";
import FileHelper from "../file-helper";
import Helpers from "../helpers";
import Logger from "../logger";

interface IValidation {
    file: string;
    migration: string;
    counter: number;
}

export class Migrator {

    private _migrationsDir = './migrations';

    private _errorMsg = `Found invalid files. Remove these and add correct .sql files. 
        Filenames must be in the form of XXXX-migration-<NAME>.sql where XXXX is the number that must be in ascending order with no numbers skipped, 
        no duplicates and the overall length must not exceed 50 characters.`;

    constructor(private dbContextProvider: MigratorDbContextProvider, private fileHelper: FileHelper, private logger: Logger) {
    }

    async run(): Promise<void> {
        if (!this.fileHelper.exists(this._migrationsDir)) {
            return;
        }

        const files = await this.getMigrationFiles();
        if (files.invalid.length > 0) {
            throw new Error(this._errorMsg);
        }

        if (files.migrations.length > 0) {
            this.logger.log(`Found ${files.migrations.length} migration files.`);

            const db = await this.dbContextProvider.create();
            try {
                const current = await db.loadMigrations();
                const validFiles = this.validateMigrationFiles(files.migrations, current);
                if (validFiles.length > 0) {
                    await this.runMigrations(validFiles, db);
                }

                if (Helpers.isProduction) {
                    // Delete files when on live server
                    for (const file of files.migrations) {
                        this.fileHelper.deleteFile(this._migrationsDir + '/' + file);
                    }

                    for (const file of files.invalid) {
                        this.fileHelper.deleteFile(this._migrationsDir + '/' + file);
                    }
                }
            } finally {
                db.destroy();
            }
        } else {
            this.logger.log(`No migration files found.`)
        }
    }

    private async runMigrations(validations: IValidation[], db: DbContext): Promise<void> {
        this.logger.log("Starting transaction...");
        await db.beginTransaction();

        try {
            for (const validation of Helpers.sortAscending(validations, x => x.counter)) {
                const migrationQuery = this.loadMigrationFile(validation.file);
                this.logger.log(`Running migration ${validation.file}...`);
                await db.execute(migrationQuery);
                this.logger.log(`Migration ${validation.file} done.`);
                await db.execute(`insert into __migrations (id, name, date) values (${validation.counter}, '${validation.migration}', NOW())`);
            }
    
            this.logger.log("Committing transaction...");
            await db.commitTransaction();
            this.logger.log("Transction committed.");
        } catch (error) {
            this.logger.error(error);
            this.logger.log("Rolling back transaction...");
            await db.rollbackTransaction();
            this.logger.log("Transaction rolledback.");

            throw error;
        }
    }

    validateMigrationFiles(files: string[], currentMigrations: IMigration[]): IValidation[] {

        function getNumberString(n: number): string {
            if (n < 10) {
                return "000" + n.toString();
            } else if (n < 100) {
                return "00" + n.toString();
            } else if (n < 1000) {
                return "0" + n.toString();
            } else {
                return n.toString();
            }
        }

        if (files.findIndex(x => x.length > 50) !== -1) {
            this.logger.error('Files with too long names found');
            return [];
        }

        const fileNames = files.map(f => f.replace('.sql', ''));
        const newFileNames = fileNames.filter(x => currentMigrations.findIndex(m => m.name === x) === -1);
        const migrations = Helpers.sortDescending(currentMigrations, x => x.id);
        let lastMigration: IMigration;
        
        if (migrations.length > 0) lastMigration = migrations[0];
        
        let start = 0;
        if (lastMigration) {
            start = lastMigration.id;
        }

        const result: IValidation[] = [];
        for (let i = 0; i < newFileNames.length; i++) {
            // Expected file names
            const migrationCounter = getNumberString(start + i + 1);
            const migration = newFileNames.find(x => new RegExp(`^(${migrationCounter}-migration-)[a-zA-Z0-9]{1,}$`, 'gi').test(x));
            if (!migration) {
                this.logger.error(`Expected file ${migrationCounter}-migration-<name> but got nothing!`);
                return [];
            }

            result.push({
                counter: parseInt(migrationCounter),
                migration: migration,
                file: migration + '.sql',
            });
        }

        return result;
    }

    async getMigrationFiles(): Promise<{ migrations: string[], invalid: string[] }> {
        const migrations: string[] = [];
        const dir = await this.fileHelper.directory(this._migrationsDir);
        const invalidFiles: string[] = [];
        for (let i = 0; i < dir.files.length; i++) {
            if (/^([0-9]{4}-migration-)[a-zA-Z0-9]{1,}(\.sql)$/.test(dir.files[i])) {
                migrations.push(dir.files[i]);
            } else {
                invalidFiles.push(dir.files[i]);
            }
        }
    
        return {
            migrations: Helpers.sortAscending(migrations, x => x),
            invalid: invalidFiles,
        };
    }

    loadMigrationFile(fileName: string): string {
        return this.fileHelper.readFile(this._migrationsDir + '/' + fileName);
    }
}
