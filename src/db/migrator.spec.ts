import { getFileHelperFake, getDbContextProviderFake, getMigratorDbContextProviderFake, getDbContextFake, getLoggerFake } from "../common-spec/common.spec";
import { Migrator } from "./meegrator";


describe("Migrator", () => {

    process.env.NODE_ENV = "dev";

    describe("getMigrationFiles", () => {

        [
            {
                files: [
                    '0001-migration-mig1.sql',
                    '0002-migration-mig2.sql',
                    '0003-migration-mig3.sql',
                    '0004-migration-mig4.sql',
                ],
                expectedMigrations: [
                    '0001-migration-mig1.sql',
                    '0002-migration-mig2.sql',
                    '0003-migration-mig3.sql',
                    '0004-migration-mig4.sql',
                ],
                expectedInvalid: []
            },
            {
                files: [
                    '0001-migration-mig1.sql',
                    '0002-migration2-mig2.sql',
                ],
                expectedMigrations: [
                    '0001-migration-mig1.sql',
                ],
                expectedInvalid: [
                    '0002-migration2-mig2.sql',
                ]
            }
        ].forEach(x => {
            const fileHelperFake = getFileHelperFake();
            const migrator = new Migrator(getDbContextProviderFake(), fileHelperFake, getLoggerFake());
            
            let directorySpy: jasmine.Spy;
            let result: any;

            beforeAll(async done => {
                directorySpy = spyOn(fileHelperFake, 'directory').and.resolveTo({files: x.files});
                result = await migrator.getMigrationFiles();
                done();
            });

            it("should return expected files", () => {
                expect(result.migrations).toEqual(x.expectedMigrations);
                expect(result.invalid).toEqual(x.expectedInvalid);
            });
        });

    });

    describe("validateMigrationFiles", () => {

        [
            {
                files: [
                    '0001-migration-mig1.sql',
                    '0002-migration-mig2.sql',
                    '0003-migration-mig3.sql',
                    '0004-migration-mig4.sql',
                ],
                migrations: [],
                expected: [
                    {counter:1,migration:'0001-migration-mig1',file:'0001-migration-mig1.sql'},
                    {counter:2,migration:'0002-migration-mig2',file:'0002-migration-mig2.sql'},
                    {counter:3,migration:'0003-migration-mig3',file:'0003-migration-mig3.sql'},
                    {counter:4,migration:'0004-migration-mig4',file:'0004-migration-mig4.sql'},
                ],
            },
            {
                files: [
                    '0003-migration-mig3.sql',
                    '0004-migration-mig4.sql',
                ],
                migrations: [
                    { id: 1, name: '0001-migration-mig1', date: new Date() },
                    { id: 2, name: '0002-migration-mig2', date: new Date() },
                ],
                expected: [
                    {counter:3,migration:'0003-migration-mig3',file:'0003-migration-mig3.sql'},
                    {counter:4,migration:'0004-migration-mig4',file:'0004-migration-mig4.sql'},
                ],
            },
            {
                files: [
                    '0001-migration-mig1.sql',
                    '0002-migration-mig2.sql',
                    '0003-migration-mig3.sql',
                    '0004-migration-mig4.sql',
                ],
                migrations: [
                    { id: 1, name: '0001-migration-mig1', date: new Date() },
                    { id: 2, name: '0002-migration-mig2', date: new Date() },
                ],
                expected: [
                    {counter:3,migration:'0003-migration-mig3',file:'0003-migration-mig3.sql'},
                    {counter:4,migration:'0004-migration-mig4',file:'0004-migration-mig4.sql'},    
                ],
            },
            {
                files: [
                    '0003-migration-mig3.sql',
                    '0004-migration-mig4.sql',
                ],
                migrations: [
                    { id: 1, name: '0001-migration-mig1', date: new Date() },
                    { id: 2, name: '0002-migration-mig2', date: new Date() },
                    { id: 3, name: '0003-migration-mig3', date: new Date() },
                ],
                expected: [
                    {counter:4,migration:'0004-migration-mig4',file:'0004-migration-mig4.sql'},
                ],
            },
            {
                files: [
                    '0004-migration-mig3.sql',
                    '0004-migration-mig4.sql',
                ],
                migrations: [
                    { id: 1, name: '0001-migration-mig1', date: new Date() },
                    { id: 2, name: '0002-migration-mig2', date: new Date() },
                    { id: 3, name: '0003-migration-mig3', date: new Date() },
                ],
                expected: [],
            },
            {
                files: [
                    '0002-migration-initial.sql',
                    '0003-migration-mig3.sql',
                ],
                migrations: [],
                expected: [],
            },
            {
                files: [
                    'gibberish.sql',
                ],
                migrations: [],
                expected: [],
            },
            {
                files: [
                    '0001-initial.sql',
                ],
                migrations: [],
                expected: [],
            },
        ].forEach(x => {
            const migrator = new Migrator(getDbContextProviderFake(), getFileHelperFake(), getLoggerFake());

            it(`should validate and return ${x.expected} for ${x.files.join(', ')} with existing migrations ${x.migrations.map(m => m.name).join(', ')}`, () => {
                expect(migrator.validateMigrationFiles(x.files, x.migrations)).toEqual(x.expected);
            });
        });

    });

    describe("run", () => {

        describe("when migration files exist", () => {

            describe("and no migrations have been added so far", () => {

                const dbContextProviderFake = getMigratorDbContextProviderFake();
                const dbContextFake = getDbContextFake();
                const fileHelperFake = getFileHelperFake();

                let existsSpy: jasmine.Spy;
                let directorySpy: jasmine.Spy;
                let createSpy: jasmine.Spy;
                let loadMigrationsSpy: jasmine.Spy;
                let beginTransactionSpy: jasmine.Spy;
                let readFileSpy: jasmine.Spy;
                let executeSpy: jasmine.Spy;
                let deleteFileSpy: jasmine.Spy;
                let commitTransactionSpy: jasmine.Spy;
                let destroySpy: jasmine.Spy;

                beforeAll(async done => {
                    existsSpy = spyOn(fileHelperFake, 'exists').and.returnValue(true);
                    directorySpy = spyOn(fileHelperFake, 'directory')
                        .and.resolveTo({
                            files: [
                                '0001-migration-something.sql',
                                '0002-migration-somethingelse.sql',
                            ]
                        });
                    createSpy = spyOn(dbContextProviderFake, 'create').and.resolveTo(dbContextFake);
                    loadMigrationsSpy = spyOn(dbContextFake, 'loadMigrations').and.resolveTo([]);
                    beginTransactionSpy = spyOn(dbContextFake, 'beginTransaction').and.callThrough().and.resolveTo();
                    readFileSpy = spyOn(fileHelperFake, 'readFile').and.callThrough().and.returnValues('some sql', 'some other sql');
                    executeSpy = spyOn(dbContextFake, 'execute').and.callThrough().and.resolveTo();
                    deleteFileSpy = spyOn(fileHelperFake, 'deleteFile').and.callThrough();
                    commitTransactionSpy = spyOn(dbContextFake, 'commitTransaction').and.callThrough().and.resolveTo();
                    destroySpy = spyOn(dbContextFake, 'destroy').and.callThrough();

                    process.env.NODE_ENV = 'production';

                    const migrator = new Migrator(dbContextProviderFake, fileHelperFake, getLoggerFake());

                    // Act
                    await migrator.run();

                    done();
                });

                it("should begin transaction", () => {
                    expect(beginTransactionSpy).toHaveBeenCalledTimes(1);
                });

                it("should commit transaction", () => {
                    expect(commitTransactionSpy).toHaveBeenCalledTimes(1);
                });

                it("should call execute on DbContext", () => {
                    expect(executeSpy).toHaveBeenCalledTimes(4);
                    expect(executeSpy).toHaveBeenCalledWith('some sql');
                    expect(executeSpy).toHaveBeenCalledWith(`insert into __migrations (id, name, date) values (1, '0001-migration-something', NOW())`);
                    expect(executeSpy).toHaveBeenCalledWith('some other sql');
                    expect(executeSpy).toHaveBeenCalledWith(`insert into __migrations (id, name, date) values (2, '0002-migration-somethingelse', NOW())`);
                });

                it("should delete migration files", () => {
                    expect(deleteFileSpy).toHaveBeenCalledTimes(2);
                    expect(deleteFileSpy).toHaveBeenCalledWith('./migrations/0001-migration-something.sql');
                    expect(deleteFileSpy).toHaveBeenCalledWith('./migrations/0002-migration-somethingelse.sql');
                });

                it("should call destroy on context", () => {
                    expect(destroySpy).toHaveBeenCalledTimes(1);
                });
            });

        });

        describe("when error during migration run", () => {

            const dbContextProviderFake = getMigratorDbContextProviderFake();
            const dbContextFake = getDbContextFake();
            const fileHelperFake = getFileHelperFake();

            let existsSpy: jasmine.Spy;
            let directorySpy: jasmine.Spy;
            let createSpy: jasmine.Spy;
            let loadMigrationsSpy: jasmine.Spy;
            let beginTransactionSpy: jasmine.Spy;
            let readFileSpy: jasmine.Spy;
            let executeSpy: jasmine.Spy;
            let deleteFileSpy: jasmine.Spy;
            let commitTransactionSpy: jasmine.Spy;
            let rollbackTransactionSpy: jasmine.Spy;
            let destroySpy: jasmine.Spy;

            beforeAll(async done => {
                existsSpy = spyOn(fileHelperFake, 'exists').and.returnValue(true);
                directorySpy = spyOn(fileHelperFake, 'directory')
                    .and.resolveTo({
                        files: [
                            '0001-migration-something.sql',
                            '0002-migration-somethingelse.sql',
                        ]
                    });
                createSpy = spyOn(dbContextProviderFake, 'create').and.resolveTo(dbContextFake);
                loadMigrationsSpy = spyOn(dbContextFake, 'loadMigrations').and.resolveTo([]);
                beginTransactionSpy = spyOn(dbContextFake, 'beginTransaction').and.callThrough().and.resolveTo();
                readFileSpy = spyOn(fileHelperFake, 'readFile').and.callThrough().and.returnValues('some sql', 'some other sql');
                executeSpy = spyOn(dbContextFake, 'execute').and.callThrough().and.rejectWith('error');
                deleteFileSpy = spyOn(fileHelperFake, 'deleteFile').and.callThrough();
                commitTransactionSpy = spyOn(dbContextFake, 'commitTransaction').and.callThrough().and.resolveTo();
                rollbackTransactionSpy = spyOn(dbContextFake, 'rollbackTransaction').and.callThrough().and.resolveTo();
                destroySpy = spyOn(dbContextFake, 'destroy').and.callThrough();

                process.env.NODE_ENV = 'production';

                const migrator = new Migrator(dbContextProviderFake, fileHelperFake, getLoggerFake());

                // Act
                try {
                    await migrator.run();
                } catch (error) {
                }

                done();
            });

            it("should begin transaction", () => {
                expect(beginTransactionSpy).toHaveBeenCalledTimes(1);
            });

            it("should rollback transaction", () => {
                expect(rollbackTransactionSpy).toHaveBeenCalledTimes(1);
            });

            it("should call destroy on context", () => {
                expect(destroySpy).toHaveBeenCalledTimes(1);
            });
        });
    });
});