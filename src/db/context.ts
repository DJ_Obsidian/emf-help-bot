import * as mysql from 'mysql';
import Helpers from '../helpers';
import Logger from '../logger';
import { IConnectionSettings } from '../settings';
import { IMigration } from './entities/migration';
import AccessLevelRepository from './repos/access-level-repository';
import ArkRepository from './repos/ark-repository';
import AwardRepository from './repos/award-repository';
import ChannelRepository from './repos/channel-repository';
import CooldownRepository from './repos/cooldown-repository';
import PlaylistItemRepository from './repos/playlistitem-repository';
import RoleRepository from './repos/role-repository';
import UserRepository from './repos/user-repository';

export interface IDbConnectionInfo {
    connections: number;
    active: number;
}

let emfDbConnPool: mysql.Pool = null;
let connection: IConnectionSettings = null;
export let databaseName = null;

const emfDbConnectionInfo: IDbConnectionInfo = {
    connections: 0,
    active: 0,
};

export function createPool(conn: IConnectionSettings, logger: Logger): void {
    emfDbConnPool = mysql.createPool(conn);
    databaseName = conn.database;
    connection = conn;

    emfDbConnPool.on('connection', () => {
        if (!Helpers.isProduction) {
            console.log('New DB connection established.');
        }

        emfDbConnectionInfo.connections++;
    });

    emfDbConnPool.on('acquire', () => {
        if (!Helpers.isProduction) {
            console.log('DB connection acquired.');
        }

        emfDbConnectionInfo.active++;
    });

    emfDbConnPool.on('release', () => {
        if (!Helpers.isProduction) {
            console.log('DB connection released.');
        }

        emfDbConnectionInfo.active--;
    });

    emfDbConnPool.on('error', (err) => {
        logger.error(err);
    });
}

export function getDBConnectionInfo(): IDbConnectionInfo {
    return emfDbConnectionInfo;
}

/**
 * Creates mysql connection and connects to emfbot database, returning the connection
 * instance required for making queries.
 */
function createDbConnection(): Promise<mysql.PoolConnection> {
    return new Promise((resolve, reject) => {
        emfDbConnPool.getConnection((err, connection) => {
            if (err) {
                // Failed to connect;
                reject(err);
            } else {
                // Connected successfully
                resolve(connection);
            }
        });
    });
}

export class DbConnectionProvider {

    private _isPoolConnection = false;

    constructor(private _connection?: mysql.Connection) {
        if (_connection) {
            console.log("External mySql connection provided!");
        }
    }

    async connect(): Promise<mysql.Connection> {
        if (this._connection) {
            return this._connection;
        } else {
            this._connection = await createDbConnection();
            this._isPoolConnection = true;
            if (!Helpers.isProduction) {
                console.log("Connection opened!");
            }
            return this._connection;
        }
    }

    query<TResult>(sql: any, values?: unknown): Promise<TResult> {
        return new Promise((resolve, reject) => {
            this.connect().then(conn => {
                conn.query(sql, values, (err, result) => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(result);
                    }
                });
            });
        });
    }

    beginTransaction(): Promise<void> {
        return new Promise((resolve, reject) => {
            this.connect().then(conn => conn.beginTransaction(err => {
                if (err) {
                    console.error("Failed to begin transaction:\r\n");
                    console.error(err);
                    reject(err);
                    return;
                }

                resolve();
            }));
        });
    }

    commitTransaction(): Promise<void> {
        return new Promise((resolve, reject) => {
            this.connect().then(conn => conn.commit(err => {
                if (err) {
                    console.error("Failed to commit transaction:\r\n");
                    console.error(err);
                    reject(err);
                    return;
                }

                resolve();
            }));
        });
    }

    rollbackTransaction(): Promise<void> {
        return new Promise((resolve, reject) => {
            this.connect().then(conn => conn.rollback(err => {
                if (err) {
                    console.error("Failed to rollback transaction:\r\n");
                    console.error(err);
                    reject(err);
                    return;
                }

                resolve();
            }));
        });
    }

    disconnect(): void {
        if (this._connection) {
            if (this._isPoolConnection) {
                (<mysql.PoolConnection>this._connection).release();
            } else {
                this._connection.end(err => console.log(err));
            }

            if (!Helpers.isProduction) {
                console.log('Connection closed!');
            }
        }
    }
}

export class DbContext {

    get userRepo(): UserRepository {
        return new UserRepository(this._connectionProvider);
    }

    get channelRepo(): ChannelRepository {
        return new ChannelRepository(this._connectionProvider);
    }

    get accessLevelRepo(): AccessLevelRepository {
        return new AccessLevelRepository(this._connectionProvider);
    }

    get cooldownRepo(): CooldownRepository {
        return new CooldownRepository(this._connectionProvider);
    }

    get arkRepo(): ArkRepository {
        return new ArkRepository(this._connectionProvider);
    }

    get awardRepo(): AwardRepository {
        return new AwardRepository(this._connectionProvider);
    }

    get roleRepo(): RoleRepository {
        return new RoleRepository(this._connectionProvider);
    }

    get playlistItemRepo(): PlaylistItemRepository {
        return new PlaylistItemRepository(this._connectionProvider);
    }

    constructor(private _connectionProvider: DbConnectionProvider) {
    }

    execute<TResult>(query: string): Promise<TResult> {
        return this._connectionProvider.query<TResult>(query);
    }

    beginTransaction(): Promise<void> {
        return this._connectionProvider.beginTransaction();
    }

    commitTransaction(): Promise<void> {
        return this._connectionProvider.commitTransaction();
    }

    rollbackTransaction(): Promise<void> {
        return this._connectionProvider.rollbackTransaction();
    }

    async loadMigrations(): Promise<IMigration[]> {
        const result = await this._connectionProvider.query<unknown[]>(`select * from information_schema.tables where table_name = '__migrations' limit 1`);
        if (result.length > 0) {
            return this._connectionProvider.query('select * from __migrations');
        } else {
            return [];
        }
    }

    destroy(): void {
        this._connectionProvider.disconnect(); // Closes connection
    }
}

export class DbContextProvider {

    constructor(private logger: Logger) {
    }

    async create(): Promise<DbContext> {
        return new DbContext(new DbConnectionProvider());
    }

    async execute<TResult>(callback: (db: DbContext) => Promise<TResult>): Promise<TResult> {
        let db: DbContext;
        try {
            db = await this.create();
            if (!db) {
                throw new Error("Could not connect to DB.")
            }
            
            return await callback(db);
        } catch (error) {
            this.logger.error("DB error!\r\n" + error);
            throw error;
        } finally {
            if (db) {
                db.destroy();
            }
        }
    }
}

export class MigratorDbContextProvider extends DbContextProvider {

    async create(): Promise<DbContext> {
        const conn = mysql.createConnection({
            host: connection.host,
            user: connection.user,
            password: connection.password,
            database: connection.database,
            port: connection.port,
            multipleStatements: true,
        });
        return new DbContext(new DbConnectionProvider(conn));
    }
}
