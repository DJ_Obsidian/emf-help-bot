

export interface IChannel {
    id: number;

    /**
     * Channel id as is on Discord.
     */
    channelid: string;

    description: string;
}