import { IRoleUser } from "./role-user";


export interface IUser {
    id?: number;
    userid: string;
    usernametag: string;
    banneduntil?: Date;
    arkgrd5misses?: number;
    lastpresent?: Date;
    deleted?: Date;
    roleusers?: IRoleUser[];
}