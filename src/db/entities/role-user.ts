import { IRole } from "./role";
import { IUser } from "./user";


export interface IRoleUser {
    roleid: string;
    userid: string;
    created: Date;
    user?: IUser;
    role?: IRole;
    isPersonalRole?: boolean;
}