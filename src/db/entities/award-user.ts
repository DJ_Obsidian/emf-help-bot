

export interface IAwardUser {
    userid: string;
    award_id: number;
    description: string;
    created: Date;
}