
export interface IArkProfession {
    id: number;
    name: string;
}

export interface IArkPosition {
    id: number;
    name: string;
}

export interface IArkTag {
    id: number;
    name: string;
}

export interface IArkKnight {
    id: number;
    characterid: string;
    name: string;

    /**
     * Possible values 0-5.
     */
    rarity: number;
    itemusage?: string;
    itemdesc?: string;
    description?: string;
    positionid: number;
    professionid: number;
    position?: IArkPosition;
    profession?: IArkProfession;
    taglist?: IArkTag[];
}

export interface IArkKnightTag {
    ark_knightid: number;
    ark_tagid: number;
}

export interface IArkUser {
    userid: string;
    ark_knightid: number;
    times: number;
    ark?: IArkKnight;
}