

export interface IRole {
    id?: number;
    roleid: string;
    name: string;
    color: string;
}