export interface ILongCooldown {
    id?: number;
    command?: number;
    userid: string;
    channel?: number;
    created: Date;
    hours: number;
}