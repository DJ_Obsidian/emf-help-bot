

export interface IPlaylistItem {
    id?: number;
    row_num?: number;
    userid: string;
    title: string;
    duration: number;
    url: string;
    created: Date;
}