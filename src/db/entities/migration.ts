

export interface IMigration {
    id: number;
    name: string;
    date: Date;
}