import { ChannelNames } from "../../channel-names";
import { CommandValues } from "../../command-values";
import { DbConnectionProvider } from "../context";
import { ILongCooldown } from "../entities/long-cooldown";
import { BaseRepository } from "./base-repository";

export default class CooldownRepository extends BaseRepository {

    constructor(connectionProvider: DbConnectionProvider) {
        super(connectionProvider);
    }

    loadAllCooldowns(): Promise<ILongCooldown[]> {
        return this.query(
            `select * from Cooldowns cd
            join Users u on cd.user_id = u.id
            where DATE_ADD(cd.created, INTERVAL cd.hours HOUR) > NOW()`);
    }

    upsert(cooldown: ILongCooldown): Promise<void> {
        return this.query(
            `insert into Cooldowns (command, user_id, channel, created, hours) 
            select ?, u.id, ?, ?, ?
            from Users u where u.userid = ?
            on duplicate key update command=?, user_id=u.id, channel=?, created=?, hours=?`, 
            [
                cooldown.command || -1,
                cooldown.channel || -1,
                cooldown.created,
                cooldown.hours,
                cooldown.userid,
                cooldown.command || -1,
                cooldown.channel || -1,
                cooldown.created,
                cooldown.hours,
            ]);
    }

    async get(userid: string, command?: CommandValues, channel?: ChannelNames): Promise<ILongCooldown> {
        const r = await this.query(
            `select * from Cooldowns c 
            join Users u on u.id = c.user_id
            where u.userid = ? and c.command = ? and c.channel = ?`, [userid, command || -1, channel || -1]);
        return r[0];
    }
}