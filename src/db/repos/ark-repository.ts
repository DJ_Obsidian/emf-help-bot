import Helpers from "../../helpers";
import { DbConnectionProvider } from "../context";
import { IArkKnight, IArkKnightTag, IArkPosition, IArkProfession, IArkTag, IArkUser } from "../entities/ark-knight";
import { IUser } from "../entities/user";
import { BaseRepository } from "./base-repository";


export default class ArkRepository extends BaseRepository {

    constructor(connectionProvider: DbConnectionProvider) {
        super(connectionProvider);
    }

    insertProfessions(professions: IArkProfession[]): Promise<void> {
        const r = Helpers.getKeyValues(professions);
        return this.query(`insert ignore into ArkProfessions (${r.keys.join(',')}) values ?`, [r.values]);
    }

    deleteProfessions(): Promise<void> {
        return this.query('delete from ArkProfessions where id > 0');
    }

    getProfessions(skip: number, take: number): Promise<IArkProfession[]> {
        return this.query('select * from ArkProfessions LIMIT ?, ?', [skip, take]);
    }

    insertPositions(positions: IArkPosition[]): Promise<void> {
        const r = Helpers.getKeyValues(positions);
        return this.query(`insert ignore into ArkPositions (${r.keys.join(',')}) values ?`, [r.values]);
    }

    deletePositions(): Promise<void> {
        return this.query('delete from ArkPositions where id > 0');
    }

    getPositions(skip: number, take: number): Promise<IArkPosition[]> {
        return this.query('select * from ArkPositions LIMIT ?, ?', [skip, take]);
    }

    insertTags(tags: IArkTag[]): Promise<void> {
        const r = Helpers.getKeyValues(tags);
        return this.query(`insert ignore into ArkTags (${r.keys.join(',')}) values ?`, [r.values]);
    }

    deleteTags(): Promise<void> {
        return this.query('delete from ArkTags where id > 0');
    }

    insertArkKnights(arkKnights: IArkKnight[]): Promise<void> {
        const r = Helpers.getKeyValues(arkKnights);
        return this.query(`insert ignore into ArkKnights (${r.keys.join(',')}) values ?`, [r.values]);
    }

    async getArkKnightIdsByGrade(grade: number): Promise<number[]> {
        const r: { id: number }[] = await this.query('select id from ArkKnights where rarity = ?', grade);
        return r.map(x => x.id);
    }

    async getAllArkKnightNames(minGrade: number): Promise<string[]> {
        const r: { name: string }[] = await this.query('select name from ArkKnights where rarity >= ?', minGrade);
        return r.map(x => x.name);
    }

    async getAll(): Promise<IArkKnight[]> {
        return await this.query('select * from ArkKnights order by id');
    }

    async getAllProfessions(): Promise<IArkProfession[]> {
        return await this.query('select * from ArkProfessions order by id');
    }

    private async getArkKnightByProperty<T>(property: string, value: T): Promise<IArkKnight> {
        const options = {
            sql: `select distinct * from ArkKnights ark
                join ArkPositions position on ark.positionid = position.id
                join ArkProfessions profession on ark.professionid = profession.id
                left join ArkKnightTags arkTag on ark.id = arkTag.ark_knightid
                left join ArkTags tag on arkTag.ark_tagid = tag.id
                where ark.${property} = ?`,
            nestTables: true,
        };

        const r: { 
            ark: IArkKnight,
            position: IArkPosition,
            profession: IArkProfession,
            arkTag: IArkKnightTag,
            tag: IArkTag,
        }[] = await this.query(options, value);

        if (r.length > 0) {
            const ark = r[0].ark;
            ark.position = r[0].position;
            ark.profession = r[0].profession;
            ark.taglist = r.filter(a => a.tag && a.tag.id && a.tag.name).map(a => a.tag)
            return ark;
        } else {
            return null;
        }
    }

    getArkKnightByName(name: string): Promise<IArkKnight> {
        return this.getArkKnightByProperty("name", name);
    }

    getArkKnight(id: number): Promise<IArkKnight> {
        return this.getArkKnightByProperty("id", id);
    }

    async getUserArkKnightByName(userid: string, name: string): Promise<IArkKnight> {
        const options = {
            sql: `select distinct * from ArkKnights ark
                join ArkUsers au on ark.id = au.ark_knightid
                join Users u on u.id = au.user_id
                join ArkPositions position on ark.positionid = position.id
                join ArkProfessions profession on ark.professionid = profession.id
                left join ArkKnightTags arkTag on ark.id = arkTag.ark_knightid
                left join ArkTags tag on arkTag.ark_tagid = tag.id
                where u.userid = ? and ark.name = ?`,
            nestTables: true,
        };

        const r: { 
            ark: IArkKnight,
            position: IArkPosition,
            profession: IArkProfession,
            arkTag: IArkKnightTag,
            tag: IArkTag,
        }[] = await this.query(options, [userid, name]);

        if (r.length > 0) {
            const ark = r[0].ark;
            ark.position = r[0].position;
            ark.profession = r[0].profession;
            ark.taglist = r.filter(a => a.tag && a.tag.id && a.tag.name).map(a => a.tag)
            return ark;
        } else {
            return null;
        }
    }

    deleteArkKnights(): Promise<void> {
        return this.query('delete from ArkKnights where id > 0');
    }

    insertArkKnightTags(arkKnightTags: IArkKnightTag[]): Promise<void> {
        const r = Helpers.getKeyValues(arkKnightTags);
        return this.query(`insert ignore into ArkKnightTags (${r.keys.join(',')}) values ?`, [r.values]);
    }

    deleteArkKnightTags(): Promise<void> {
        return this.query('delete from ArkKnightTags where ark_knightid > 0');
    }

    insertArkUser(arkUser: IArkUser): Promise<void> {
        return this.query(
            `insert into ArkUsers (ark_knightid, user_id, times)
            select ?, u.id, ?
            from Users u where u.userid = ?`,
            [
                arkUser.ark_knightid,
                arkUser.times,
                arkUser.userid
            ]);
    }

    async getArkUser(userid: string, ark_knightid: number): Promise<IArkUser> {
        const options = {
            sql: `select * from ArkUsers au
            join ArkKnights ark on ark.id = au.ark_knightid
            join Users u on u.id = au.user_id
            where u.userid = ? and au.ark_knightid = ?`,
            nestTables: true,
        };
        const r: {
            au: IArkUser,
            ark: IArkKnight,
        }[] = await this.query(options, [userid, ark_knightid]);

        if (r.length > 0) {
            const arkUser = <IArkUser>r[0].au;
            arkUser.ark = r[0].ark;
            return arkUser;
        } else {
            return null;
        }
    }

    updateArkUserTimes(userid: string, ark_knightid: number, times: number): Promise<void> {
        return this.query(
            `update ArkUsers au
            join Users u on u.id = au.user_id
            set au.times = ? where u.userid = ? and au.ark_knightid = ?`,
            [times, userid, ark_knightid]);
    }

    async getUserArks(userid: string, grade?: number): Promise<IArkUser[]> {
        const options = {
            sql: `select * from ArkUsers au
            join Users u on u.id = au.user_id
            join ArkKnights ark on ark.id = au.ark_knightid
            where u.userid = ? and au.times > 0`,
            nestTables: true,
        };

        if (grade) {
            options.sql += ` and ark.rarity = ${grade}`;
        }

        const r: {
            au: IArkUser,
            u: IUser,
            ark: IArkKnight,
        }[] = await this.query(options, userid);

        r.forEach(x => x.au.ark = x.ark);
        r.forEach(x => x.au.userid = x.u.userid)
        return r.map(x => x.au);
    }

    /**
     * Update user's 5 grade ark knight miss count.
     * @param userid 
     * @param missCount 
     */
    async updateUsers5grdMissCount(userid: string, missCount: number): Promise<void> {
        await this.query(`update Users set arkgrd5misses = ? where userid = ?`, [missCount, userid]);
    }
}