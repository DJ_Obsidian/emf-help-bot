import Helpers from "../../helpers";
import { DbConnectionProvider } from "../context";
import { IRole } from "../entities/role";
import { IRoleUser } from "../entities/role-user";
import { IUser } from "../entities/user";
import { BaseRepository } from "./base-repository";


export default class RoleRepository extends BaseRepository {

    constructor(connectionProvider: DbConnectionProvider) {
        super(connectionProvider);
    }

    async getById(roleid: string): Promise<IRole> {
        const r = await this.query(`select * from Roles where roleid = ?`, roleid);
        return r[0];
    }

    insert(role: IRole): Promise<void> {
        return this.query('insert ignore into Roles set ?', role);
    }

    delete(roleid: string): Promise<void> {
        return this.query(`delete from Roles where roleid = ?`, roleid);
    }

    update(role: IRole): Promise<void> {
        return this.query(`update Roles set name = ? where roleid = ?`, [role.name, role.roleid]);
    }

    insertMany(roles: IRole[]): Promise<void> {
        const r = Helpers.getKeyValues(roles);
        return this.query(`insert ignore into Roles (${r.keys.join(',')}) values ?`, [r.values]);
    }

    insertRoleUser(roleUser: IRoleUser): Promise<void> {
        return this.query(
            `insert into RoleUsers (role_id, user_id, created, personal_role_id)
            select 
                (select id from Roles where roleid = ?),
                (select id from Users where userid = ?),
                ?,
                (${roleUser.isPersonalRole === true ? 'select id from Roles where roleid = ?' : 'NULL'})`,
            [
                roleUser.roleid,
                roleUser.userid,
                roleUser.created,
                roleUser.roleid,
            ]);
    }

    insertManyRoleUsers(roleUsers: IRoleUser[]): Promise<void> {
        if (roleUsers.length > 0) {
            return this.query(
                `insert ignore into RoleUsers (role_id, user_id, created, personal_role_id)
                values
                    ${roleUsers.map(roleUser => 
                        `(
                            (select id from Roles where roleid = '${roleUser.roleid}'),
                            (select id from Users where userid = '${roleUser.userid}'), 
                            ?,
                            (${roleUser.isPersonalRole === true ? `select id from Roles where roleid = '${roleUser.roleid}'` : 'NULL'})
                        )`
                    ).join(',')}`,
                    roleUsers.map(roleUser => roleUser.created)
            );
        } else {
            return Promise.resolve();
        }
    }

    deleteManyRoleUsers(roleUsers: {userid: string, roleid: string}[]): Promise<void> {
        return this.query(`
            delete ru from RoleUsers ru
            join Users u on ru.user_id = u.id
            join Roles r on ru.role_id = r.id
            where (u.userid, r.roleid) in (
                ${roleUsers.map(ru => `('${ru.userid}','${ru.roleid}')`).join(',')}
            )`
        );
    }

    getUserRoles(userid: string): Promise<IRole[]> {
        return this.query(`
            select r.id, r.roleid, r.name, r.color from Roles r
            join RoleUsers ru on r.id = ru.role_id
            join Users u on ru.user_id = u.id
            where u.userid = ?
        `, userid);
    }

    async getRoleUsersForUser(userid: string): Promise<IRoleUser[]> {
        const options = {
            sql: `select distinct * from RoleUsers ru
            join Roles r on ru.role_id = r.id
            join Users u on ru.user_id = u.id
            where u.userid = ?`,
            nestTables: true,
        };

        const result: {
            r: IRole,
            ru: {user_id: number, role_id: number, created: Date, personal_role_id: number},
            u: IUser,
        }[] = await this.query(options, userid);

        const roleUsers: IRoleUser[] = result.map(x => <IRoleUser>{
            userid: x.u.userid,
            roleid: x.r.roleid,
            role: x.r,
            user: x.u,
            created: x.ru.created,
            isPersonalRole: !!x.ru.personal_role_id,
        });

        return roleUsers;
    }

    async getUserPersonalRole(userid: string): Promise<IRole> {
        const options = {
            sql: `select * from RoleUsers ru
            join Users u on ru.user_id = u.id
            join Roles r on ru.role_id = r.id
            where u.userid = ? and ru.personal_role_id is NOT NULL`,
            nestTables: true,
        };

        const result: {
            r: IRole,
        }[] = await this.query(options, userid);

        if (result.length > 0) {
            return result[0].r;
        } else {
            return null;
        }
    }
}