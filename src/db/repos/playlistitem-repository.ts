import { DbConnectionProvider } from "../context";
import { IPlaylistItem } from "../entities/playlist-item";
import { BaseRepository } from "./base-repository";


export default class PlaylistItemRepository extends BaseRepository {
    
    constructor(connectionProvider: DbConnectionProvider) {
        super(connectionProvider);
    }
    
    async insert(playlist: IPlaylistItem): Promise<IPlaylistItem> {
        await this.query(
            `insert into PlaylistItems (user_id, title, duration, url, created)
            select 
                (select id from Users where userid = ?),
                ?,
                ?,
                ?,
                ?`,
            [
                playlist.userid,
                playlist.title,
                playlist.duration,
                playlist.url,
                playlist.created,
            ]);
        return playlist;
    }

    async getPlaylistSize(userid: string): Promise<number> {
        const result = await this.query(`select count(*) as count from PlaylistItems p0 join Users u0 on p0.user_id = u0.id where u0.userid = ?`, userid);
        return result[0]["count"];
    }

    getPlaylist(userid: string): Promise<IPlaylistItem[]> {
        return this.query(`
            select p.id, ROW_NUMBER() OVER (order by p.created) row_num, u.userid, p.title, p.duration, p.created, p.url from PlaylistItems p
            join Users u on p.user_id = u.id
            where u.userid = ?
            order by p.created
        `, userid);
    }

    async getItemIdByRowNumber(userid: string, row: number): Promise<number | null> {
        const r = await this.query(`
            with items as
                (select p.id, ROW_NUMBER() OVER (order by p.created) as row_num from PlaylistItems p
                    join Users u on p.user_id = u.id
                    where u.userid = ?)
                select * from items where items.row_num = ?`, [userid, row]);
        return r ? r[0].id : null;
    }

    deleteItem(id: number): Promise<unknown> {
        return this.query(`delete from PlaylistItems where id = ?`, id);
    }

    delete(userid: string): Promise<unknown> {
        return this.query(`
            delete p from PlaylistItems p
            join Users u on p.user_id = u.id
            where u.userid = ?
        `, userid)
    }
}