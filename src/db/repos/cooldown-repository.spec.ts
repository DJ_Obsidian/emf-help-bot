import { ChannelNames } from "../../channel-names";
import { CommandValues } from "../../command-values";
import { DbTestHarness, getLoggerFake, loadDbSettings } from "../../common-spec/common.spec";
import Helpers from "../../helpers";
import { DbContext, DbContextProvider } from "../context";
import { ILongCooldown } from "../entities/long-cooldown";
import { IUser } from "../entities/user";

loadDbSettings();

describe("CooldownRepository", () => {

    beforeAll((done: DoneFn) => {
        DbTestHarness.reinit().then(() => done());
    });

    describe("loadAllCooldowns", () => {

        const expectedUser1: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };
        const expectedUser2: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };
        const expectedUser3: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };

        const expectedCooldown1: ILongCooldown = {
            channel: ChannelNames.general,
            command: CommandValues.pook,
            userid: expectedUser1.userid,
            hours: 10,
            created: new Date(),
        };

        const expectedCooldown2: ILongCooldown = {
            channel: ChannelNames.general,
            command: CommandValues.award,
            userid: expectedUser2.userid,
            hours: 1,
            created: new Date(),
        };

        const expectedCooldown3: ILongCooldown = {
            channel: ChannelNames.general,
            command: CommandValues.help,
            userid: expectedUser3.userid,
            hours: 1,
            created: Helpers.date(new Date()).addHours(-1.1),
        };

        let actualCooldowns: ILongCooldown[];
        
        let context: DbContext;

        beforeAll(async done => {
            context = await new DbContextProvider(getLoggerFake()).create();
            await context.userRepo.insert(expectedUser1);
            await context.userRepo.insert(expectedUser2);
            await context.userRepo.insert(expectedUser3);
            await context.cooldownRepo.upsert(expectedCooldown1);
            await context.cooldownRepo.upsert(expectedCooldown2);
            await context.cooldownRepo.upsert(expectedCooldown3);

            actualCooldowns = await context.cooldownRepo.loadAllCooldowns();

            done();
        });

        it("should load cooldowns", () => {
            expect(actualCooldowns.findIndex(x => x.userid === expectedCooldown1.userid)).not.toEqual(-1);
            expect(actualCooldowns.findIndex(x => x.userid === expectedCooldown2.userid)).not.toEqual(-1);
            expect(actualCooldowns.findIndex(x => x.userid === expectedCooldown3.userid)).toEqual(-1);
        });

    });

    describe("upsert", () => {

        [
            { userid: Helpers.snowflake(), channel: ChannelNames.admin, command: CommandValues.o7 },
            { userid: Helpers.snowflake(), channel: -1, command: -1 },
            { userid: Helpers.snowflake(), channel: -1, command: CommandValues.prune },
            { userid: Helpers.snowflake(), channel: ChannelNames.admin, command: -1 },
        ].forEach(x => {
            describe("when adding entry for the first time", () => {
    
                const expectedUser: IUser = {
                    userid: x.userid,
                    usernametag: Helpers.snowflake(),
                };
        
                const expectedCooldown: ILongCooldown = {
                    channel: x.channel,
                    command: x.command,
                    userid: expectedUser.userid,
                    hours: 10,
                    created: new Date(),
                };
        
                let actualCooldown: ILongCooldown;
        
                let context: DbContext;
        
                beforeAll(async done => {
                    context = await new DbContextProvider(getLoggerFake()).create();
                    await context.userRepo.insert(expectedUser);
                    await context.cooldownRepo.upsert(expectedCooldown);
        
                    actualCooldown = await context.cooldownRepo.get(
                        x.userid,
                        x.command,
                        x.channel);
        
                    done();
                });
        
                afterAll(() => context.destroy());
        
                it(`should add ${Object.entries(x).map(e => `${e[0]}:${e[1]}`).join(', ')} successfully`, () => {
                    expect(actualCooldown).toBeTruthy();
                    expect(actualCooldown.userid).toEqual(expectedUser.userid);
                    expect(actualCooldown.channel).toEqual(expectedCooldown.channel);
                    expect(actualCooldown.command).toEqual(expectedCooldown.command);
                    expect(actualCooldown.created).toBeTruthy();
                    expect(actualCooldown.hours).toEqual(actualCooldown.hours);
                });
            });
        });

        [
            { userid: Helpers.snowflake(), channel: ChannelNames.admin, command: CommandValues.o7 },
            { userid: Helpers.snowflake(), channel: -1, command: -1 },
            { userid: Helpers.snowflake(), channel: -1, command: CommandValues.prune },
            { userid: Helpers.snowflake(), channel: ChannelNames.admin, command: -1 },
        ].forEach(x => {
            describe("when adding entry for the second time", () => {
    
                const expectedUser: IUser = {
                    userid: x.userid,
                    usernametag: Helpers.snowflake(),
                };
        
                const expectedCooldown: ILongCooldown = {
                    channel: x.channel,
                    command: x.command,
                    userid: expectedUser.userid,
                    hours: 10,
                    created: new Date(),
                };

                const expectedCreatedDate = Helpers.date(new Date()).addDays(100);
                const expectedHours = 100;
        
                let actualCooldown: ILongCooldown;
                let actualCooldowns: ILongCooldown[];
        
                let context: DbContext;
        
                beforeAll(async done => {
                    context = await new DbContextProvider(getLoggerFake()).create();
                    await context.userRepo.insert(expectedUser);
                    await context.cooldownRepo.upsert(expectedCooldown);

                    expectedCooldown.created = expectedCreatedDate;
                    expectedCooldown.hours = expectedHours;
                    await context.cooldownRepo.upsert(expectedCooldown);
        
                    actualCooldowns = await context.execute(
                        `select * from Cooldowns c 
                        join Users u on u.id = c.user_id
                        where u.userid = '${x.userid}' and c.command = ${x.command} and c.channel = ${x.channel}`);
                    actualCooldown = actualCooldowns[0];
        
                    done();
                });
        
                afterAll(() => context.destroy());
        
                it(`should add ${Object.entries(x).map(e => `${e[0]}:${e[1]}`).join(', ')} successfully`, () => {
                    expect(actualCooldown).toBeTruthy();
                    expect(actualCooldown.userid).toEqual(expectedUser.userid);
                    expect(actualCooldown.channel).toEqual(expectedCooldown.channel);
                    expect(actualCooldown.command).toEqual(expectedCooldown.command);
                    expect(actualCooldown.created.getDate()).toEqual(expectedCreatedDate.getDate());
                    expect(actualCooldown.created.getHours()).toEqual(expectedCreatedDate.getHours());
                    expect(actualCooldown.created.getFullYear()).toEqual(expectedCreatedDate.getFullYear());
                    expect(actualCooldown.hours).toEqual(expectedHours);
                });

                it("should not add a duplicate", () => {
                    expect(actualCooldowns.length).toEqual(1);
                });
            });
        });

    });
});