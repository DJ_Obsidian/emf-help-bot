import { DbConnectionProvider } from '../context';
import { IChannel } from '../entities/channel';
import { BaseRepository } from "./base-repository";

export default class ChannelRepository extends BaseRepository {
    
    constructor(connectionProvider: DbConnectionProvider) {
        super(connectionProvider);
    }

    getAll(): Promise<IChannel[]> {
        return this.query(`select * from Channels`);
    }
}