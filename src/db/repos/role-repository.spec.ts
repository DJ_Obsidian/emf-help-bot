import { createTestRole, DbTestHarness, getLoggerFake } from "../../common-spec/common.spec";
import Helpers from "../../helpers";
import { DbContext, DbContextProvider } from "../context";
import { IRole } from "../entities/role";
import { IRoleUser } from "../entities/role-user";
import { IUser } from "../entities/user";


describe("RoleRepository", () => {

    beforeAll((done: DoneFn) => {
        DbTestHarness.reinit().then(() => done());
    });

    describe("delete", () => {
        const expectedRole1: IRole = createTestRole();

        let dbContext: DbContext;

        let actualRole: IRole;

        beforeAll(async done => {
            dbContext = await new DbContextProvider(getLoggerFake()).create();
    
            await dbContext.roleRepo.insert(expectedRole1);

            const role = await dbContext.roleRepo.getById(expectedRole1.roleid);
            
            // Act
            await dbContext.roleRepo.delete(role.roleid);
    
            actualRole = await dbContext.roleRepo.getById(expectedRole1.roleid);

            done();
        });

        it("should delete role " + expectedRole1.roleid, () => {
            expect(actualRole).toBeFalsy();
        });
    });

    describe("update", () => {
        const expectedRole1: IRole = createTestRole();

        const expectedRoleName = Helpers.snowflake();

        let dbContext: DbContext;

        let actualRole: IRole;

        beforeAll(async done => {
            dbContext = await new DbContextProvider(getLoggerFake()).create();
    
            await dbContext.roleRepo.insert(expectedRole1);

            // Act
            await dbContext.roleRepo.update({
                roleid: expectedRole1.roleid,
                name: expectedRoleName,
                color: "",
            });
            
            actualRole = await dbContext.roleRepo.getById(expectedRole1.roleid);

            done();
        });

        it("should update role name", () => {
            expect(actualRole.name).toEqual(expectedRoleName);
        });
    });

    describe("insertMany", () => {
        const expectedRole1: IRole = createTestRole();
        const expectedRole2: IRole = createTestRole();

        let dbContext: DbContext;

        let actualRole1: IRole;
        let actualRole2: IRole;

        beforeAll(async done => {
            dbContext = await new DbContextProvider(getLoggerFake()).create();
    
            // Act
            await dbContext.roleRepo.insertMany([expectedRole1, expectedRole2]);
    
            actualRole1 = await dbContext.roleRepo.getById(expectedRole1.roleid);
            actualRole2 = await dbContext.roleRepo.getById(expectedRole2.roleid);
    
            done();
        });

        it("should insert all roles", () => {
            expect(actualRole1).toBeTruthy();
            expect(actualRole2).toBeTruthy();
            expect(actualRole1.roleid).toEqual(expectedRole1.roleid);
            expect(actualRole2.roleid).toEqual(expectedRole2.roleid);
            expect(actualRole1.name).toEqual(expectedRole1.name);
            expect(actualRole2.name).toEqual(expectedRole2.name);
        });

        afterAll(() => {
            if (dbContext) {
                dbContext.destroy();
            }
        });
    });

    describe("insertRoleUser", () => {
        const expectedUser: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };
        const expectedRole: IRole = createTestRole();
        const expectedRoleUser: IRoleUser = {
            userid: expectedUser.userid,
            roleid: expectedRole.roleid,
            created: new Date(),
        };
    
        let dbContext: DbContext;
    
        let actualUsers: IUser[];
        let actualRoles: IRole[];
        let actualRoleUsers: {
            role_id: number,
            user_id: number,
            created: Date,
        }[];
    
        beforeAll(async done => {
            dbContext = await new DbContextProvider(getLoggerFake()).create();
    
            await dbContext.userRepo.insert(expectedUser);
            await dbContext.roleRepo.insert(expectedRole);
    
            // Act
            await dbContext.roleRepo.insertRoleUser(expectedRoleUser);
            
            actualUsers = await dbContext.execute(`select * from Users where userid = '${expectedUser.userid}'`);
            actualRoles = await dbContext.execute(`select * from Roles where roleid = '${expectedRole.roleid}'`);
            actualRoleUsers = await dbContext.execute(
                `select * from RoleUsers
                where role_id = ${actualRoles[0].id} and user_id = ${actualUsers[0].id}`);
    
            done();
        });
    
        it("should add RoleUser", () => {
            expect(actualRoleUsers.length).toEqual(1);
            expect(actualRoleUsers[0].role_id).toEqual(actualRoles[0].id);
            expect(actualRoleUsers[0].user_id).toEqual(actualUsers[0].id);
            expect(actualRoleUsers[0].created).toBeTruthy();
        });
    
        afterAll(() => {
            if (dbContext) {
                dbContext.destroy();
            }
        });
    });

    describe("insertManyRoleUsers", () => {
        const expectedUser: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };
        const expectedRole1: IRole = createTestRole();
        const expectedRole2: IRole = createTestRole();
        const expectedRole3: IRole = createTestRole();
        const expectedRoleUser1: IRoleUser = {
            userid: expectedUser.userid,
            roleid: expectedRole1.roleid,
            created: new Date(),
        };
        const expectedRoleUser2: IRoleUser = {
            userid: expectedUser.userid,
            roleid: expectedRole2.roleid,
            created: new Date(),
        };
        const expectedRoleUser3: IRoleUser = {
            userid: expectedUser.userid,
            roleid: expectedRole3.roleid,
            created: new Date(),
        };
    
        let dbContext: DbContext;
    
        let actualUsers: IUser[];
        let actualRoles: IRole[];
        let actualRoleUsers: {
            role_id: number,
            user_id: number,
            created: Date,
        }[];
    
        beforeAll(async done => {
            dbContext = await new DbContextProvider(getLoggerFake()).create();
    
            await dbContext.userRepo.insert(expectedUser);
            await dbContext.roleRepo.insert(expectedRole1);
            await dbContext.roleRepo.insert(expectedRole2);
            await dbContext.roleRepo.insert(expectedRole3);
    
            // Act
            await dbContext.roleRepo.insertManyRoleUsers([
                expectedRoleUser1,
                expectedRoleUser2,
                expectedRoleUser3,
            ]);
            
            actualUsers = await dbContext.execute(`select * from Users where userid = '${expectedUser.userid}'`);
            actualRoles = await dbContext.execute(`select * from Roles where roleid in ('${expectedRole1.roleid}','${expectedRole2.roleid}','${expectedRole3.roleid}')`);
            actualRoleUsers = await dbContext.execute(
                `select * from RoleUsers
                where role_id in (${actualRoles[0].id},${actualRoles[1].id},${actualRoles[2].id}) and user_id = ${actualUsers[0].id}`);
    
            done();
        });
    
        it("should add RoleUser", () => {
            expect(actualRoleUsers.length).toEqual(3);
            expect(actualRoles.length).toEqual(3);
            expect(actualRoleUsers[0].user_id).toEqual(actualUsers[0].id);
            expect(actualRoleUsers[1].user_id).toEqual(actualUsers[0].id);
            expect(actualRoleUsers[2].user_id).toEqual(actualUsers[0].id);
            expect(actualRoleUsers[0].created).toBeTruthy();
            expect(actualRoleUsers[1].created).toBeTruthy();
            expect(actualRoleUsers[2].created).toBeTruthy();
            
            expect(actualRoleUsers.findIndex(x => x.role_id == actualRoles[0].id)).not.toEqual(-1);
            expect(actualRoleUsers.findIndex(x => x.role_id == actualRoles[1].id)).not.toEqual(-1);
            expect(actualRoleUsers.findIndex(x => x.role_id == actualRoles[2].id)).not.toEqual(-1);
        });
    
        afterAll(() => {
            if (dbContext) {
                dbContext.destroy();
            }
        });
    });

    describe("deleteManyRoleUsers", () => {

        const expectedUser: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };
        const expectedRole1: IRole = createTestRole();
        const expectedRole2: IRole = createTestRole();
        const expectedRole3: IRole = {
            name: Helpers.snowflake(),
            roleid: Helpers.snowflake(),
            color: "",
        };
        const expectedRoleUser1: IRoleUser = {
            userid: expectedUser.userid,
            roleid: expectedRole1.roleid,
            created: new Date(),
        };
        const expectedRoleUser2: IRoleUser = {
            userid: expectedUser.userid,
            roleid: expectedRole2.roleid,
            created: new Date(),
        };
        const expectedRoleUser3: IRoleUser = {
            userid: expectedUser.userid,
            roleid: expectedRole3.roleid,
            created: new Date(),
        };
    
        let dbContext: DbContext;
    
        let actualUsers: IUser[];
        let actualRoles: IRole[];
        let actualRoleUsers: {
            role_id: number,
            user_id: number,
            created: Date,
        }[];

        beforeAll(async done => {
            dbContext = await new DbContextProvider(getLoggerFake()).create();
    
            await dbContext.userRepo.insert(expectedUser);
            await dbContext.roleRepo.insert(expectedRole1);
            await dbContext.roleRepo.insert(expectedRole2);
            await dbContext.roleRepo.insert(expectedRole3);
    
            // Act
            await dbContext.roleRepo.insertManyRoleUsers([
                expectedRoleUser1,
                expectedRoleUser2,
                expectedRoleUser3,
            ]);
            
            actualUsers = await dbContext.execute(`select * from Users where userid = '${expectedUser.userid}'`);
            actualRoles = await dbContext.execute(`select * from Roles where roleid in ('${expectedRole1.roleid}','${expectedRole2.roleid}','${expectedRole3.roleid}')`);
            actualRoleUsers = await dbContext.execute(`select * from RoleUsers where user_id = ${actualUsers[0].id}`);
            
            if (actualRoleUsers.length !== 3) {
                throw new Error();
            }
            
            await dbContext.roleRepo.deleteManyRoleUsers([
                { roleid: expectedRole1.roleid, userid: expectedUser.userid },
                { roleid: expectedRole2.roleid, userid: expectedUser.userid },
                { roleid: expectedRole3.roleid, userid: expectedUser.userid },
                { roleid: expectedRole3.roleid, userid: expectedUser.userid },
            ]);

            await dbContext.roleRepo.deleteManyRoleUsers([
                { roleid: 'unknown', userid: 'unknown' },
            ]);
            
            actualRoleUsers = await dbContext.execute(`select * from RoleUsers where user_id = ${actualUsers[0].id}`);
    
            done();
        });

        it("should delete RoleUser", () => {
            expect(actualRoleUsers.length).toEqual(0);
        });

        afterAll(() => {
            if (dbContext) {
                dbContext.destroy();
            }
        });
    });

    describe("getUserRoles", () => {

        const expectedUser1: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };
        const expectedUser2: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };
        const expectedRole1: IRole = createTestRole();
        const expectedRole2: IRole = createTestRole();
        const expectedRole3: IRole = createTestRole();
        const expectedRole4: IRole = createTestRole();
        const expectedRoleUser1: IRoleUser = {
            userid: expectedUser1.userid,
            roleid: expectedRole1.roleid,
            created: new Date(),
        };
        const expectedRoleUser2: IRoleUser = {
            userid: expectedUser1.userid,
            roleid: expectedRole2.roleid,
            created: new Date(),
        };
        const expectedRoleUser3: IRoleUser = {
            userid: expectedUser1.userid,
            roleid: expectedRole3.roleid,
            created: new Date(),
        };
        const expectedRoleUser4: IRoleUser = {
            userid: expectedUser2.userid,
            roleid: expectedRole1.roleid,
            created: new Date(),
        };
        const expectedRoleUser5: IRoleUser = {
            userid: expectedUser2.userid,
            roleid: expectedRole4.roleid,
            created: new Date(),
        };
    
        let dbContext: DbContext;

        let actualRoles: IRole[];

        beforeAll(async done => {
            dbContext = await new DbContextProvider(getLoggerFake()).create();
            await dbContext.userRepo.insert(expectedUser1);
            await dbContext.userRepo.insert(expectedUser2);
            await dbContext.roleRepo.insert(expectedRole1);
            await dbContext.roleRepo.insert(expectedRole2);
            await dbContext.roleRepo.insert(expectedRole3);
            await dbContext.roleRepo.insert(expectedRole4);
    
            await dbContext.roleRepo.insertManyRoleUsers([
                expectedRoleUser1,
                expectedRoleUser2,
                expectedRoleUser3,
                expectedRoleUser4,
                expectedRoleUser5,
            ]);

            // Act
            actualRoles = await dbContext.roleRepo.getUserRoles(expectedUser1.userid);

            done();
        });

        it("should return correct user roles", () => {
            expect(actualRoles.length).toEqual(3);
            expect(actualRoles.findIndex(x => x.roleid === expectedRole1.roleid)).not.toEqual(-1);
            expect(actualRoles.findIndex(x => x.roleid === expectedRole2.roleid)).not.toEqual(-1);
            expect(actualRoles.findIndex(x => x.roleid === expectedRole3.roleid)).not.toEqual(-1);
        });

        afterAll(() => {
            if (dbContext) {
                dbContext.destroy();
            }
        });
    });

    describe("getRoleUsersForUser", () => {

        let dbContext: DbContext;

        const actualRole1: IRole = createTestRole();
        const actualRole2: IRole = createTestRole();
        const actualRole3: IRole = createTestRole();
        const actualRole4: IRole = createTestRole();

        const actualUser1: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.guid(),
        };
        const actualUser2: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.guid(),
        };

        let actualRoleUsers: IRoleUser[];

        beforeAll(async done => {
            dbContext = await new DbContextProvider(getLoggerFake()).create();

            await dbContext.roleRepo.insert(actualRole1);
            await dbContext.roleRepo.insert(actualRole2);
            await dbContext.roleRepo.insert(actualRole3);
            await dbContext.roleRepo.insert(actualRole4);
            await dbContext.userRepo.insert(actualUser1);
            await dbContext.userRepo.insert(actualUser2);
            await dbContext.roleRepo.insertManyRoleUsers([
                {
                    userid: actualUser1.userid,
                    roleid: actualRole1.roleid,
                    created: new Date(),
                },
                {
                    userid: actualUser1.userid,
                    roleid: actualRole2.roleid,
                    created: new Date(),
                },
                {
                    userid: actualUser1.userid,
                    roleid: actualRole3.roleid,
                    created: new Date(),
                },
                {
                    userid: actualUser1.userid,
                    roleid: actualRole4.roleid,
                    created: new Date(),
                },
                {
                    userid: actualUser2.userid,
                    roleid: actualRole1.roleid,
                    created: new Date(),
                },
                {
                    userid: actualUser2.userid,
                    roleid: actualRole2.roleid,
                    created: new Date(),
                },
            ]);

            actualRoleUsers = await dbContext.roleRepo.getRoleUsersForUser(actualUser1.userid);

            done();
        });

        it("should return correct RoleUsers", () => {
            expect(actualRoleUsers.length).toEqual(4);
            actualRoleUsers.forEach(x => expect(x.userid).toEqual(actualUser1.userid));
            expect(actualRoleUsers.findIndex(x => x.roleid === actualRole1.roleid)).not.toEqual(-1);
            expect(actualRoleUsers.findIndex(x => x.roleid === actualRole2.roleid)).not.toEqual(-1);
            expect(actualRoleUsers.findIndex(x => x.roleid === actualRole3.roleid)).not.toEqual(-1);
            expect(actualRoleUsers.findIndex(x => x.roleid === actualRole4.roleid)).not.toEqual(-1);
        });

        afterAll(() => {
            if (dbContext) {
                dbContext.destroy();
            }
        });
    });

    describe("getUserPersonalRole", () => {
        let dbContext: DbContext;

        const expectedRole1: IRole = createTestRole();
        const expectedRole2: IRole = createTestRole();
        const expectedRole3: IRole = createTestRole();

        const actualUser1: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.guid(),
        };

        let actualRole: IRole;

        beforeAll(async done => {
            dbContext = await new DbContextProvider(getLoggerFake()).create();

            await dbContext.roleRepo.insert(expectedRole1);
            await dbContext.roleRepo.insert(expectedRole2);
            await dbContext.userRepo.insert(actualUser1);
            await dbContext.roleRepo.insertManyRoleUsers([
                {
                    userid: actualUser1.userid,
                    roleid: expectedRole1.roleid,
                    created: new Date(),
                    isPersonalRole: false,
                },
                {
                    userid: actualUser1.userid,
                    roleid: expectedRole2.roleid,
                    created: new Date(),
                    isPersonalRole: true,
                },
                {
                    userid: actualUser1.userid,
                    roleid: expectedRole3.roleid,
                    created: new Date(),
                },
            ]);

            actualRole = await dbContext.roleRepo.getUserPersonalRole(actualUser1.userid);

            done();
        });

        it("should return correct personal Role", () => {
            expect(actualRole).toBeTruthy();
            expect(actualRole.roleid).toEqual(expectedRole2.roleid);
        });

        afterAll(() => {
            if (dbContext) {
                dbContext.destroy();
            }
        });
    });
});