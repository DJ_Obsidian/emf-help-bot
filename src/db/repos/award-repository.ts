import { DbConnectionProvider } from "../context";
import { IAward } from "../entities/award";
import { IAwardUser } from "../entities/award-user";
import { BaseRepository } from "./base-repository";


export default class AwardRepository extends BaseRepository {
    
    constructor(connectionProvider: DbConnectionProvider) {
        super(connectionProvider);
    }

    insertAwardUser(awardUser: IAwardUser): Promise<void> {
        return this.query(
            `insert into AwardUsers (award_id, user_id, description, created)
            select ?, u.id, ?, ?
            from Users u where u.userid = ?`,
            [
                awardUser.award_id,
                awardUser.description,
                awardUser.created,
                awardUser.userid,
            ]);
    }

    getUserMedalCounts(userid: string): Promise<{ id: number, count: number }[]> {
        return this.query<{ id: number, count: number }[]>(
            `select a.id, count(a.id) as count from AwardUsers au
            join Awards a on au.award_id = a.id
            join Users u on au.user_id = u.id
            where u.userid = ?
            group by a.id`,
            userid);
    }

    getMedals(): Promise<IAward[]> {
        return this.query('select * from Awards');
    }
}