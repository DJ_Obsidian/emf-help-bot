import { DbTestHarness, getLoggerFake, loadDbSettings } from "../../common-spec/common.spec";
import { DbContext, DbContextProvider } from "../context";
import { IChannel } from "../entities/channel";

loadDbSettings();

describe("ChannelRepository", () => {

    beforeAll((done: DoneFn) => {
        DbTestHarness.reinit().then(() => done());
    });

    describe("getAll", () => {

        let channels: IChannel[];
        let dbContext: DbContext;

        beforeAll(async done => {
            dbContext = await new DbContextProvider(getLoggerFake()).create();
            
            channels = await dbContext.channelRepo.getAll();
            done();
        });

        it("should return all channels", () => {
            expect(channels.findIndex(x => x.id === 1)).not.toEqual(-1);
            expect(channels.findIndex(x => x.id === 2)).not.toEqual(-1);
            expect(channels.findIndex(x => x.id === 3)).not.toEqual(-1);
            expect(channels.findIndex(x => x.id === 4)).not.toEqual(-1);
            expect(channels.findIndex(x => x.id === 5)).not.toEqual(-1);
            expect(channels.find(x => x.id === 1).channelid).toBeTruthy();
            expect(channels.find(x => x.id === 2).channelid).toBeTruthy();
            expect(channels.find(x => x.id === 3).channelid).toBeTruthy();
            expect(channels.find(x => x.id === 4).channelid).toBeTruthy();
            expect(channels.find(x => x.id === 5).channelid).toBeTruthy();
        });

        afterAll(() => {
            if (dbContext) {
                dbContext.destroy();
            }
        })
    });

});