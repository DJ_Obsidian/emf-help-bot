import { DbTestHarness, getDbContextProvider, loadDbSettings } from "../../common-spec/common.spec";
import Helpers from "../../helpers";
import { DbContext } from "../context";
import { IPlaylistItem } from "../entities/playlist-item";
import { IUser } from "../entities/user";


loadDbSettings();

describe("PlaylistItemRepository", () => {

    beforeAll((done: DoneFn) => {
        DbTestHarness.reinit().then(() => done());
    });

    describe("insert, getPlaylist, getPlaylistSize", () => {

        const expectedUser: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
            lastpresent: new Date(2020, 1, 1, 1, 1, 1),
        };

        let context: DbContext;

        const expectedPlaylistItem: IPlaylistItem = {
            userid: expectedUser.userid,
            url: "http",
            title: "Some title of a song",
            duration: 1000,
            created: new Date(),
        };

        let actualPlaylistItems: IPlaylistItem[];
        let actualCount: number;

        beforeAll(async done => {
            context = await getDbContextProvider().create();
            await context.userRepo.insert(expectedUser);

            // Act
            await context.playlistItemRepo.insert(expectedPlaylistItem);

            actualCount = await context.playlistItemRepo.getPlaylistSize(expectedUser.userid);

            actualPlaylistItems = await context.playlistItemRepo.getPlaylist(expectedUser.userid);

            done();
        });

        it("should return size of one", () => {
            expect(actualCount).toEqual(1);
        });

        it("should create playlist item", () => {
            expect(actualPlaylistItems.length).toEqual(1);
            expect(actualPlaylistItems[0].userid).toEqual(expectedUser.userid);
            expect(actualPlaylistItems[0].duration).toEqual(expectedPlaylistItem.duration);
            expect(actualPlaylistItems[0].title).toEqual(expectedPlaylistItem.title);
            expect(actualPlaylistItems[0].url).toEqual(expectedPlaylistItem.url);
            expect(actualPlaylistItems[0].id).toBeGreaterThan(0);
            expect(actualPlaylistItems[0].row_num).toEqual(1);
        });

        afterAll(() => {
            if (context) {
                context.destroy();
            }
        });
    });

    describe("deleteItem", () => {
        const expectedUser: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
            lastpresent: new Date(2020, 1, 1, 1, 1, 1),
        };

        let context: DbContext;

        const expectedPlaylistItem1: IPlaylistItem = {
            userid: expectedUser.userid,
            url: "http",
            title: "1",
            duration: 1000,
            created: Helpers.date(new Date()).addSeconds(-5),
        };
        const expectedPlaylistItem2: IPlaylistItem = {
            userid: expectedUser.userid,
            url: "http",
            title: "2",
            duration: 1000,
            created: Helpers.date(new Date()).addSeconds(-4),
        };
        const expectedPlaylistItem3: IPlaylistItem = {
            userid: expectedUser.userid,
            url: "http",
            title: "3",
            duration: 1000,
            created: Helpers.date(new Date()).addSeconds(-3),
        };
        const expectedPlaylistItem4: IPlaylistItem = {
            userid: expectedUser.userid,
            url: "http",
            title: "4",
            duration: 1000,
            created: Helpers.date(new Date()).addSeconds(-2),
        };

        let actualPlaylistItems: IPlaylistItem[];

        beforeAll(async done => {
            context = await getDbContextProvider().create();
            await context.userRepo.insert(expectedUser);

            await context.playlistItemRepo.insert(expectedPlaylistItem1);
            await context.playlistItemRepo.insert(expectedPlaylistItem2);
            await context.playlistItemRepo.insert(expectedPlaylistItem3);
            await context.playlistItemRepo.insert(expectedPlaylistItem4);

            const playlist = Helpers.sortAscending(await context.playlistItemRepo.getPlaylist(expectedUser.userid), x => x.row_num);
            const playlistItem2Id = await context.playlistItemRepo.getItemIdByRowNumber(expectedUser.userid, playlist.find(x => x.title === "1").row_num);
            const playlistItem1Id = await context.playlistItemRepo.getItemIdByRowNumber(expectedUser.userid, playlist.find(x => x.title === "4").row_num);

            // Act
            await context.playlistItemRepo.deleteItem(playlistItem1Id);
            await context.playlistItemRepo.deleteItem(playlistItem2Id);

            actualPlaylistItems = await context.playlistItemRepo.getPlaylist(expectedUser.userid);

            done();
        });

        it("should delete correct items", () => {
            expect(actualPlaylistItems.length).toEqual(2);
            expect(actualPlaylistItems.find(x => x.title === "2")).toBeTruthy();
            expect(actualPlaylistItems.find(x => x.title === "3")).toBeTruthy();
        });

        afterAll(() => {
            if (context) {
                context.destroy();
            }
        });
    });

    [
        {deleteByDeletingUser: true},
        {deleteByDeletingUser: false},
    ].forEach(x => {
        describe("delete" + (x.deleteByDeletingUser ? " by deleting the user" : ""), () => {
            const expectedUser: IUser = {
                userid: Helpers.snowflake(),
                usernametag: Helpers.snowflake(),
                lastpresent: new Date(2020, 1, 1, 1, 1, 1),
            };
    
            let context: DbContext;
    
            const expectedPlaylistItem1: IPlaylistItem = {
                userid: expectedUser.userid,
                url: "http",
                title: "1",
                duration: 1000,
                created: new Date(),
            };
            const expectedPlaylistItem2: IPlaylistItem = {
                userid: expectedUser.userid,
                url: "http",
                title: "2",
                duration: 1000,
                created: new Date(),
            };
            const expectedPlaylistItem3: IPlaylistItem = {
                userid: expectedUser.userid,
                url: "http",
                title: "3",
                duration: 1000,
                created: new Date(),
            };
            const expectedPlaylistItem4: IPlaylistItem = {
                userid: expectedUser.userid,
                url: "http",
                title: "4",
                duration: 1000,
                created: new Date(),
            };
    
            let actualPlaylistItems1: IPlaylistItem[];
            let actualPlaylistItems2: IPlaylistItem[];
    
            beforeAll(async done => {
                context = await getDbContextProvider().create();
                await context.userRepo.insert(expectedUser);
    
                await context.playlistItemRepo.insert(expectedPlaylistItem1);
                await context.playlistItemRepo.insert(expectedPlaylistItem2);
                await context.playlistItemRepo.insert(expectedPlaylistItem3);
                await context.playlistItemRepo.insert(expectedPlaylistItem4);
    
                actualPlaylistItems1 = await context.playlistItemRepo.getPlaylist(expectedUser.userid);
    
                // Act
                if (x.deleteByDeletingUser) {
                    await context.userRepo.delete(expectedUser.userid);
                } else {
                    await context.playlistItemRepo.delete(expectedUser.userid);
                }
    
                actualPlaylistItems2 = await context.playlistItemRepo.getPlaylist(expectedUser.userid);
    
                done();
            });
    
            it("should create items first", () => {
                expect(actualPlaylistItems1.length).toEqual(4);
            });
    
            it("should delete items", () => {
                expect(actualPlaylistItems2.length).toEqual(0);
            });
    
            afterAll(() => {
                if (context) {
                    context.destroy();
                }
            });
        });
    });
});