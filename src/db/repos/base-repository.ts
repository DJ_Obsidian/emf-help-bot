import { DbConnectionProvider } from '../context';

export class BaseRepository {

    constructor(protected connectionProvider: DbConnectionProvider) {
    }

    query<T>(sql: unknown, values?: unknown): Promise<T> {
        return this.connectionProvider.query(sql, values);
    }

    async insertEntity<T>(entity: T, name: string, ignore = false): Promise<T> {
        await this.query(`insert ${ignore ? 'ignore' : ''} into ${name} set ?`, entity);
        return entity;
    }
}