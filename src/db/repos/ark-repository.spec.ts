import { IArkData } from "../../arks";
import { DbTestHarness, getLoggerFake, loadDbSettings } from "../../common-spec/common.spec";
import Helpers from "../../helpers";
import { DbContext, DbContextProvider } from "../context";
import { IArkKnight, IArkProfession, IArkUser } from "../entities/ark-knight";
import { IUser } from "../entities/user";

loadDbSettings();

function getDbContextProvider(): DbContextProvider {
    return new DbContextProvider(getLoggerFake());
}

describe("ArkRepository", () => {

    beforeAll((done: DoneFn) => {
        DbTestHarness.reinit().then(() => done());
    });

    let arkData: IArkData = <any>{};

    beforeAll(done => {
        const dbContextProvider = new DbContextProvider(getLoggerFake());
        dbContextProvider.execute(async db => {
            arkData.arkKnights = await db.arkRepo.getAll();
            arkData.professions = await db.arkRepo.getAllProfessions();
            done();
        })
    });

    describe("insertProfessions", () => {

        let dbContext: DbContext;

        let actualProfessions: IArkProfession[];

        beforeAll(async done => {
            dbContext = await getDbContextProvider().create();

            // Act
            await dbContext.arkRepo.insertProfessions(arkData.professions);

            actualProfessions = await dbContext.arkRepo.getProfessions(0, 100);
            done();
        });

        afterAll(() => dbContext.destroy());

        it('should add professions', () => {
            expect(actualProfessions.length).toEqual(arkData.professions.length);
        });

    });

    describe("getArkKnightIdsByGrade", () => {

        let dbContext: DbContext;

        let grade5ArkIds: number[];        

        beforeAll(async done => {
            dbContext = await getDbContextProvider().create();

            grade5ArkIds = await dbContext.arkRepo.getArkKnightIdsByGrade(5);
            done();
        });

        it("should return ark ids", () => {
            expect(grade5ArkIds.length).toBeGreaterThan(0);
            grade5ArkIds.forEach(id => expect(id).toBeGreaterThan(0));
        });

        afterAll(() => dbContext.destroy());
    });

    describe("updateArkUserTimes", () => {
        let dbContext: DbContext;

        const expectedUser: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };

        const expectedArkUser: IArkUser = {
            userid: expectedUser.userid,
            ark_knightid: 1,
            times: 1,
        };

        let actualArkUser: IArkUser;

        beforeAll(async done => {
            dbContext = await getDbContextProvider().create();
            await dbContext.userRepo.insert(expectedUser);
            await dbContext.arkRepo.insertArkUser(expectedArkUser);

            await dbContext.arkRepo.updateArkUserTimes(expectedUser.userid, 1, 1000);

            actualArkUser = await dbContext.arkRepo.getArkUser(expectedUser.userid, 1);
            done();
        });

        it("should update times", () => {
            expect(actualArkUser.times).toEqual(1000);
        });

        afterAll(() => dbContext.destroy());
    });

    describe("getArkKnight", () => {

        describe("when ark knight has tags", () => {
            let dbContext: DbContext;
    
            let ark: IArkKnight;        
    
            beforeAll(async done => {
                dbContext = await getDbContextProvider().create();
    
                const ids = await dbContext.arkRepo.getArkKnightIdsByGrade(5);
    
                ark = await dbContext.arkRepo.getArkKnight(ids[0]);
                done();
            });
    
            it("should return ark", () => {
                expect(ark).toBeTruthy();
            });
    
            it("should return ark's position", () => {
                expect(ark.position).toBeTruthy();
            });
    
            it("should return ark's profession", () => {
                expect(ark.profession).toBeTruthy();
            });
    
            it("should return ark's tags", () => {
                expect(ark.taglist).toBeTruthy();
                expect(ark.taglist.length).toBeGreaterThan(0);
            });
    
            afterAll(() => dbContext.destroy());
        });

    });

    describe("getArkUser", () => {

        let dbContext: DbContext;

        const expectedUser: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };

        const expectedArkUser: IArkUser = {
            userid: expectedUser.userid,
            ark_knightid: 1,
            times: 1,
        };

        let actualArkUser: IArkUser;

        beforeAll(async done => {
            dbContext = await getDbContextProvider().create();

            await dbContext.userRepo.insert(expectedUser);
            await dbContext.arkRepo.insertArkUser(expectedArkUser);

            // Act
            actualArkUser = await dbContext.arkRepo.getArkUser(expectedArkUser.userid, 1);

            done();
        });

        afterAll(() => dbContext.destroy());

        it("should return arkUser", () => {
            expect(actualArkUser).toBeTruthy();
        });

        it("should return ark as well", () => {
            expect(actualArkUser.ark).toBeTruthy();
        });
    });

    describe("getUserArks", () => {

        let dbContext: DbContext;

        const expectedUser: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };

        const expectedArkUser: IArkUser = {
            userid: expectedUser.userid,
            ark_knightid: 1,
            times: 1,
        };

        let actualArkUsers: IArkUser[];

        beforeAll(async done => {
            dbContext = await getDbContextProvider().create();

            await dbContext.userRepo.insert(expectedUser);
            await dbContext.arkRepo.insertArkUser(expectedArkUser);

            // Act
            actualArkUsers = await dbContext.arkRepo.getUserArks(expectedArkUser.userid);

            done();
        });

        afterAll(() => dbContext.destroy());

        it("should return arkUser", () => {
            expect(actualArkUsers.length).toBeGreaterThan(0);
        });

        it("should return ark as well", () => {
            expect(actualArkUsers[0].ark).toBeTruthy();
        });
    });

    describe("getUserArkKnightByName", () => {

        let dbContext: DbContext;

        const expectedUser: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };

        let expectedArkUser1: IArkUser;
        let expectedArk: IArkKnight;

        let actualResult: IArkKnight;

        beforeAll(async done => {
            expectedArkUser1 = {
                userid: expectedUser.userid,
                ark_knightid: arkData.arkKnights.filter(x => x.rarity === 5 && /^char/.test(x.characterid))[0].id,
                times: 2,
            };
            
            dbContext = await getDbContextProvider().create();
            
            await dbContext.userRepo.insert(expectedUser);
            await dbContext.arkRepo.insertArkUser(expectedArkUser1);
            expectedArk = await dbContext.arkRepo.getArkKnight(expectedArkUser1.ark_knightid);

            // Act
            actualResult = await dbContext.arkRepo.getUserArkKnightByName(expectedUser.userid, expectedArk.name);

            done();
        });

        it("should return ark knight", () => {
            expect(actualResult).toBeTruthy();
        });

        it("should return ark's position", () => {
            expect(actualResult.position).toBeTruthy();
        });

        it("should return ark's profession", () => {
            expect(actualResult.profession).toBeTruthy();
        });

        it("should return ark's tags", () => {
            expect(actualResult.taglist).toBeTruthy();
            expect(actualResult.taglist.length).toBeGreaterThan(0);
        });

        afterAll(() => dbContext.destroy());
    });

    describe("getAllArkKnightNames", () => {
        let dbContext: DbContext;

        let actualResult: string[];

        beforeAll(async done => {
            dbContext = await getDbContextProvider().create();
            
            // Act
            actualResult = await dbContext.arkRepo.getAllArkKnightNames(1);

            done();
        });

        it("should return ark names", () => {
            expect(actualResult.length).toBeGreaterThan(0);
        });
    });


    describe("updateUsers5grdMissCount", () => {
        let dbContext: DbContext;

        const expectedUser: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.guid(),
        };

        let actualUser: IUser;

        beforeAll(async done => {
            dbContext = await getDbContextProvider().create();
            await dbContext.userRepo.insert(expectedUser);

            // Act
            await dbContext.arkRepo.updateUsers5grdMissCount(expectedUser.userid, 23);

            actualUser = await dbContext.userRepo.getById(expectedUser.userid);

            done();
        });

        it("should update the value", () => {
            expect(actualUser.arkgrd5misses).toEqual(23);
        });
    });
});