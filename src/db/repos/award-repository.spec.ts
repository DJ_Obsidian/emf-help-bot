import { AwardNames } from "../../award-names";
import { DbTestHarness, getLoggerFake, loadDbSettings } from "../../common-spec/common.spec";
import Helpers from "../../helpers";
import { DbContext, DbContextProvider } from "../context";
import { IAwardUser } from "../entities/award-user";
import { IUser } from "../entities/user";

loadDbSettings();

describe("AwardRepository", () => {

    beforeAll((done: DoneFn) => {
        DbTestHarness.reinit().then(() => done());
    });

    describe("insertAwardUser", () => {
        const expectedUser: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };
        const expectedAwardUser: IAwardUser = {
            award_id: AwardNames.valor,
            userid: expectedUser.userid,
            description: 'some text',
            created: new Date(),
        };
    
        let dbContext: DbContext;
    
        let actualUsers: IUser[];
        let actualAwardUsers: {
            award_id: number,
            user_id: number,
            description: string,
            created: Date,
        }[];
    
        beforeAll(async done => {
            dbContext = await new DbContextProvider(getLoggerFake()).create();
    
            await dbContext.userRepo.insert(expectedUser);
    
            // Act
            await dbContext.awardRepo.insertAwardUser(expectedAwardUser);
            
            actualUsers = await dbContext.execute(`select * from Users where userid = '${expectedUser.userid}'`);
            actualAwardUsers = await dbContext.execute(
                `select * from AwardUsers au
                where au.award_id = ${AwardNames.valor} and au.user_id = ${actualUsers[0].id}`);
    
            done();
        });
    
        it("should add AwardUser", () => {
            expect(actualAwardUsers.length).toEqual(1);
            expect(actualAwardUsers[0].award_id).toEqual(AwardNames.valor);
            expect(actualAwardUsers[0].user_id).toBeGreaterThan(0);
            expect(actualAwardUsers[0].created).toBeTruthy();
            expect(actualAwardUsers[0].description).toEqual('some text');
        });
    
        afterAll(() => {
            if (dbContext) {
                dbContext.destroy();
            }
        });
    });

    describe("getUserMedalCounts", () => {

        const expectedUser: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };

        const expectedAwardUser1: IAwardUser = {
            award_id: AwardNames.medal,
            userid: expectedUser.userid,
            description: 'some text',
            created: new Date(),
        };
        const expectedAwardUser2: IAwardUser = {
            award_id: AwardNames.medal1,
            userid: expectedUser.userid,
            description: 'some text',
            created: new Date(),
        };
        const expectedAwardUser3: IAwardUser = {
            award_id: AwardNames.medal2,
            userid: expectedUser.userid,
            description: 'some text',
            created: new Date(),
        };
        const expectedAwardUser4: IAwardUser = {
            award_id: AwardNames.medal3,
            userid: expectedUser.userid,
            description: 'some text',
            created: new Date(),
        };
        const expectedAwardUser5: IAwardUser = {
            award_id: AwardNames.protection,
            userid: expectedUser.userid,
            description: 'some text',
            created: new Date(),
        };
        const expectedAwardUser6: IAwardUser = {
            award_id: AwardNames.valor,
            userid: expectedUser.userid,
            description: 'some text',
            created: new Date(),
        };
        const expectedAwardUser7: IAwardUser = {
            award_id: AwardNames.victory,
            userid: expectedUser.userid,
            description: 'some text',
            created: new Date(),
        };

        let dbContext: DbContext;

        let expectedResult: { id: number, count: number }[];

        beforeAll(async done => {
            dbContext = await new DbContextProvider(getLoggerFake()).create();
            await dbContext.userRepo.insert(expectedUser);
            await dbContext.awardRepo.insertAwardUser(expectedAwardUser1);
            await dbContext.awardRepo.insertAwardUser(expectedAwardUser2);
            await dbContext.awardRepo.insertAwardUser(expectedAwardUser3);
            await dbContext.awardRepo.insertAwardUser(expectedAwardUser4);
            await dbContext.awardRepo.insertAwardUser(expectedAwardUser5);
            await dbContext.awardRepo.insertAwardUser(expectedAwardUser6);
            await dbContext.awardRepo.insertAwardUser(expectedAwardUser6);
            await dbContext.awardRepo.insertAwardUser(expectedAwardUser7);

            // Act
            expectedResult = await dbContext.awardRepo.getUserMedalCounts(expectedUser.userid);

            done();
        });

        it("should return medals", () => {
            expect(expectedResult.length).toEqual(7);
            expect(expectedResult.find(x => x.id === AwardNames.medal).count).toEqual(1);
            expect(expectedResult.find(x => x.id === AwardNames.medal1).count).toEqual(1);
            expect(expectedResult.find(x => x.id === AwardNames.medal2).count).toEqual(1);
            expect(expectedResult.find(x => x.id === AwardNames.medal3).count).toEqual(1);
            expect(expectedResult.find(x => x.id === AwardNames.protection).count).toEqual(1);
            expect(expectedResult.find(x => x.id === AwardNames.valor).count).toEqual(2);
            expect(expectedResult.find(x => x.id === AwardNames.victory).count).toEqual(1);
        });

        afterAll(() => {
            if (dbContext) {
                dbContext.destroy();
            }
        });
    });

});