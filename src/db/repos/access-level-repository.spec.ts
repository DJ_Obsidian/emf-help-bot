import { DbTestHarness, getLoggerFake, loadDbSettings } from "../../common-spec/common.spec";
import { DbContext, DbContextProvider } from "../context";
import { IAccessLevel } from "../entities/access-level";

loadDbSettings();

describe("AccessLevelRepository", () => {

    beforeAll((done: DoneFn) => {
        DbTestHarness.reinit().then(() => done());
    });

    describe("getAll", () => {

        let accessLevels: IAccessLevel[];
        let dbContext: DbContext;

        beforeAll(async done => {
            dbContext = await new DbContextProvider(getLoggerFake()).create();
            
            accessLevels = await dbContext.accessLevelRepo.getAll();
            done();
        });

        it("should return all channels", () => {
            expect(accessLevels.findIndex(x => x.id === 1)).not.toEqual(-1);
            expect(accessLevels.findIndex(x => x.id === 2)).not.toEqual(-1);
            expect(accessLevels.findIndex(x => x.id === 3)).not.toEqual(-1);
            expect(accessLevels.find(x => x.id === 1).roleid).toBeTruthy();
            expect(accessLevels.find(x => x.id === 2).roleid).toBeTruthy();
            expect(accessLevels.find(x => x.id === 3).roleid).toBeTruthy();
        });

        afterAll(() => {
            if (dbContext) {
                dbContext.destroy();
            }
        });
    });

});