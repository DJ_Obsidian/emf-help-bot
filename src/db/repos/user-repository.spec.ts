import { ChannelNames } from "../../channel-names";
import { CommandValues } from "../../command-values";
import { createTestRole, DbTestHarness, getLoggerFake, loadDbSettings } from "../../common-spec/common.spec";
import Helpers from "../../helpers";
import { DbContext, DbContextProvider } from "../context";
import { IArkUser } from "../entities/ark-knight";
import { ILongCooldown } from "../entities/long-cooldown";
import { IRole } from "../entities/role";
import { IRoleUser } from "../entities/role-user";
import { IUser } from "../entities/user";

loadDbSettings();

function getDbContextProvider(): DbContextProvider {
    return new DbContextProvider(getLoggerFake());
}

describe("UserRepository", () => {

    beforeAll((done: DoneFn) => {
        DbTestHarness.reinit().then(() => done());
    });

    describe("insert", () => {

        describe("when inserting a new user", () => {
            const expectedUser: IUser = {
                userid: Helpers.snowflake(),
                usernametag: Helpers.snowflake(),
                lastpresent: new Date(2020, 1, 1, 1, 1, 1),
            };
    
            let context: DbContext;
            let actualUser: IUser;
    
            beforeAll(async done => {
                context = await getDbContextProvider().create();
                await context.userRepo.insert(expectedUser);
                actualUser = await context.userRepo.getById(expectedUser.userid);
                done();
            });
    
            it('should insert user', () => {
                expect(actualUser).toBeTruthy();
            });

            it('should have correct lastpresent value', () => {
                expect(actualUser.lastpresent).toEqual(expectedUser.lastpresent);
            })
    
            afterAll(() => {
                if (context) {
                    context.destroy();
                }
            });
        });

        describe("when inserting a user with a duplicate userid", () => {
            const expectedUser: IUser = {
                userid: Helpers.snowflake(),
                usernametag: Helpers.snowflake(),
            };
    
            let context: DbContext;
            let actualUsers: IUser[];
    
            beforeAll(async done => {
                context = await getDbContextProvider().create();
                await context.userRepo.insert(expectedUser);
                await context.userRepo.insert({
                    userid: expectedUser.userid,
                    usernametag: 'something',
                });

                actualUsers = await context.execute(`select * from Users where userid = '${expectedUser.userid}'`);
                done();
            });
    
            it('should not insert a user with duplicate userid', () => {
                expect(actualUsers.length).toEqual(1);
            });

            it("should not update the previously added user entry", () => {
                expect(actualUsers[0].usernametag).toEqual(expectedUser.usernametag);
            });
    
            afterAll(() => {
                if (context) {
                    context.destroy();
                }
            });
        });
    });

    describe("update", () => {
        const expectedUser: IUser = {
            usernametag: Helpers.snowflake(),
            userid: Helpers.snowflake(),
        };

        const expectedUserName = Helpers.snowflake();

        let dbContext: DbContext;

        let actualUser: IUser;

        beforeAll(async done => {
            dbContext = await new DbContextProvider(getLoggerFake()).create();
    
            await dbContext.userRepo.insert(expectedUser);

            // Act
            await dbContext.userRepo.update({
                usernametag: expectedUserName,
                userid: expectedUser.userid,
            });
            
            actualUser = await dbContext.userRepo.getById(expectedUser.userid);

            done();
        });

        it("should update user's usernametag", () => {
            expect(actualUser.usernametag).toEqual(expectedUserName);
        });
    });

    describe('getById', () => {
        const expectedUser: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };
        let actualUser: IUser;
        let context: DbContext;

        beforeAll(async done => {
            context = await getDbContextProvider().create();
            await context.userRepo.insert(expectedUser);

            actualUser = await context.userRepo.getById(expectedUser.userid);

            done();
        });

        it('should return user', () => {
            expect(actualUser.userid).toEqual(expectedUser.userid);
            expect(actualUser.usernametag).toEqual(expectedUser.usernametag);
        });

        afterAll(() => {
            if (context) {
                context.destroy();
            }
        });
    });

    describe('exists', () => {

        describe("if user actually exists", () => {
            const expectedUser: IUser = {
                userid: Helpers.snowflake(),
                usernametag: Helpers.snowflake(),
            };
            let context: DbContext;
            let actualResult: boolean;
    
            beforeAll(async done => {
                context = await getDbContextProvider().create();
                await context.userRepo.insert(expectedUser);
    
                actualResult = await context.userRepo.exists(expectedUser.userid);
    
                done();
            });
    
            it('should return true', () => {
                expect(actualResult).toEqual(true);
            });
    
            afterAll(() => {
                if (context) {
                    context.destroy();
                }
            });
        });

        describe("if user doesn't exist", () => {
            const expectedUser: IUser = {
                userid: Helpers.snowflake(),
                usernametag: Helpers.snowflake(),
            };
            let context: DbContext;
            let actualResult: boolean;
    
            beforeAll(async done => {
                context = await getDbContextProvider().create();
    
                actualResult = await context.userRepo.exists("0");
    
                done();
            });
    
            it('should return false', () => {
                expect(actualResult).toEqual(false);
            });
    
            afterAll(() => {
                if (context) {
                    context.destroy();
                }
            });
        });
    });

    describe('getAll', () => {
        const expectedUser1: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };
        const expectedUser2: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };
        const expectedUser3: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };
        let actualUsers: IUser[];
        let context: DbContext;

        beforeAll(async done => {
            context = await getDbContextProvider().create();
            await context.userRepo.insert(expectedUser1);
            await context.userRepo.insert(expectedUser2);
            await context.userRepo.insert(expectedUser3);

            actualUsers = await context.userRepo.getAll(0, 2);

            done();
        });

        it('should return user 2nd and 3rd users', () => {
            expect(actualUsers.length).toEqual(2);
            expect(actualUsers[0].userid).not.toEqual(actualUsers[1].userid);
        });

        afterAll(() => {
            if (context) {
                context.destroy();
            }
        });
    });

    describe('getBannedUsers', () => {
        const expectedUser1: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };
        const expectedUser2: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };
        let actualUsers: IUser[];
        let context: DbContext;

        beforeAll(async done => {
            context = await getDbContextProvider().create();
            await context.userRepo.insert(expectedUser1);
            await context.userRepo.insert(expectedUser2);
            await context.userRepo.query('update Users set banneduntil = ? where userid = ?', [Helpers.date(new Date()).addSeconds(10), expectedUser2.userid]);
            await context.userRepo.banUser(expectedUser1.userid, Helpers.date(new Date()).addSeconds(-10));

            actualUsers = await context.userRepo.getExpiredBannedUsers(0, 10000);

            done();
        });

        it('should return a banned user that has expired', () => {
            expect(actualUsers.findIndex(x => x.userid === expectedUser1.userid)).not.toEqual(-1);
        });

        it('should not return user that has not yet expired', () => {
            expect(actualUsers.findIndex(x => x.userid === expectedUser2.userid)).toEqual(-1);
        })

        afterAll(() => {
            if (context) {
                context.destroy();
            }
        });
    });

    describe('banUser', () => {
        const expectedUser: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };
        let actualUser: IUser;
        let context: DbContext;

        let expectedDate = Helpers.utcDate();

        beforeAll(async done => {
            context = await getDbContextProvider().create();
            await context.userRepo.insert(expectedUser);

            await context.userRepo.banUser(expectedUser.userid, expectedDate);

            actualUser = await context.userRepo.getById(expectedUser.userid);

            done();
        });

        it('should update user banneduntil', () => {
            expect(actualUser.banneduntil).toEqual(expectedDate);
        });

        afterAll(() => {
            if (context) {
                context.destroy();
            }
        });
    });

    describe('unbanUser', () => {
        const expectedUser: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };
        let actualUser: IUser;
        let context: DbContext;

        beforeAll(async done => {
            context = await getDbContextProvider().create();
            await context.userRepo.insert(expectedUser);

            await context.userRepo.banUser(expectedUser.userid, new Date());
            await context.userRepo.unbanUser(expectedUser.userid);

            actualUser = await context.userRepo.getById(expectedUser.userid);

            done();
        });

        it('should set user"s banneduntil to null', () => {
            expect(actualUser.banneduntil).toBeFalsy();
        });

        afterAll(() => {
            if (context) {
                context.destroy();
            }
        });
    });

    describe("delete", () => {

        describe("if user exists", () => {
            const expectedUser: IUser = {
                userid: Helpers.snowflake(),
                usernametag: Helpers.snowflake(),
            };
    
            let context: DbContext;
            let actualUser: IUser;
            let result: any;
    
            beforeAll(async done => {
                context = await getDbContextProvider().create();
                await context.userRepo.insert(expectedUser);
    
                actualUser = await context.userRepo.getById(expectedUser.userid);
    
                // Act
                result = await context.userRepo.delete(actualUser.userid);
                actualUser = await context.userRepo.getById(expectedUser.userid);
    
                done();
            });
    
            it('should delete user', () => {
                expect(result.affectedRows).toEqual(1);
                expect(actualUser).toBeFalsy();
            });
    
            afterAll(() => {
                if (context) {
                    context.destroy();
                }
            });
        });

        describe("if user doesn't exist", () => {
            let context: DbContext;

            let result: any;

            beforeAll(async done => {
                context = await getDbContextProvider().create();
    
                // Act
                result = await context.userRepo.delete('some unknown id');
    
                done();
            });
    
            it('should just succeed', () => {
                expect(result.affectedRows).toEqual(0);
            });
    
            afterAll(() => {
                if (context) {
                    context.destroy();
                }
            });
        });
    });

    describe("updatePresence", () => {
        let context: DbContext;
        let user: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };

        let expectedPresence = new Date(2020, 1, 1, 1, 1, 1);

        let actualUser: IUser;

        beforeAll(async done => {
            context = await getDbContextProvider().create();
            await context.userRepo.insert(user);

            // Act
            await context.userRepo.updatePresence(user.userid, expectedPresence);

            actualUser = await context.userRepo.getById(user.userid);
            done();
        });

        it('should update value', () => {
            expect(actualUser.lastpresent).toEqual(expectedPresence);
        });

        afterAll(() => {
            if (context) {
                context.destroy();
            }
        });
    });

    describe("softDeleteUser", () => {
        let context: DbContext;
        let user: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };

        let expectedDeleted = new Date(2020, 1, 1, 1, 1, 1);

        let actualUser: IUser;

        beforeAll(async done => {
            context = await getDbContextProvider().create();
            await context.userRepo.insert(user);

            // Act
            await context.userRepo.softDeleteUser(user.userid, expectedDeleted);

            actualUser = await context.userRepo.getById(user.userid);
            done();
        });

        it('should update value deleted', () => {
            expect(actualUser.deleted).toEqual(expectedDeleted);
        });

        afterAll(() => {
            if (context) {
                context.destroy();
            }
        });
    });

    describe("unsoftDeleteUser", () => {
        let context: DbContext;
        let user: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
            deleted: new Date(),
        };

        let actualUser: IUser;

        beforeAll(async done => {
            context = await getDbContextProvider().create();
            await context.userRepo.insert(user);

            // Act
            await context.userRepo.unSoftDeleteUser(user.userid);

            actualUser = await context.userRepo.getById(user.userid);
            done();
        });

        it('should update value deleted to NULL', () => {
            expect(actualUser.deleted).toBeFalsy();
        });

        afterAll(() => {
            if (context) {
                context.destroy();
            }
        });
    });

    describe("getInactiveUsers", () => {

        let context: DbContext;
        const role1: IRole = createTestRole();
        const role2: IRole = createTestRole();
        const role3: IRole = createTestRole();
        const zooRole: IRole = {
            name: 'Зоопарк',
            roleid: Helpers.snowflake(),
            color: '',
        };
        const user1: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
            lastpresent: Helpers.date(new Date()).addHours(-25),
        };
        const user2: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
            lastpresent: new Date(),
        };
        const user3: IUser = { // В зоопарке
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
            lastpresent: Helpers.date(new Date()).addDays(-20),
        };
        const user1Role1: IRoleUser = {
            created: new Date(),
            roleid: role1.roleid,
            userid: user1.userid,
        };
        const user1Role2: IRoleUser = {
            created: new Date(),
            roleid: role2.roleid,
            userid: user1.userid,
        };
        const user3Role3: IRoleUser = {
            created: new Date(),
            roleid: role3.roleid,
            userid: user3.userid,
        };
        const user3ZooRole: IRoleUser = {
            created: new Date(),
            roleid: zooRole.roleid,
            userid: user3.userid,
        };

        let actualUsers: IUser[];

        beforeAll(async done => {
            context = await getDbContextProvider().create();
            await context.userRepo.insert(user1);
            await context.userRepo.insert(user2);
            await context.userRepo.insert(user3);
            await context.roleRepo.insert(role1);
            await context.roleRepo.insert(role2);
            await context.roleRepo.insert(role3);
            await context.roleRepo.insert(zooRole);
            await context.roleRepo.insertRoleUser(user1Role1);
            await context.roleRepo.insertRoleUser(user1Role2);
            await context.roleRepo.insertRoleUser(user3Role3);
            await context.roleRepo.insertRoleUser(user3ZooRole);

            // Act
            actualUsers = await context.userRepo.getInactiveUsers(1, [zooRole.roleid, 'some other role id'], 1000000);
            done();
        });

        it('should return a user', () => {
            expect(actualUsers.findIndex(u => u.userid === user1.userid)).not.toEqual(-1);
            expect(actualUsers.findIndex(u => u.userid === user2.userid)).toEqual(-1);
            expect(actualUsers.findIndex(u => u.userid === user3.userid)).toEqual(-1);
        });

        it('should return distinct users', () => {
            expect(actualUsers.filter(x => x.userid === user1.userid).length).toEqual(1);
        });

        afterAll(() => {
            if (context) {
                context.destroy();
            }
        });

    });

    describe("getUsersWithRoleAfterDays", () => {

        let context: DbContext;
        const role1: IRole = createTestRole();
        const role2: IRole = createTestRole();

        const user1: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
            lastpresent: new Date(),
        };
        const user2: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
            lastpresent: new Date(),
        };

        const user1Role1: IRoleUser = {
            created: new Date(),
            roleid: role1.roleid,
            userid: user1.userid,
        };
        const user1Role2: IRoleUser = {
            created: Helpers.date(new Date()).addHours(-23),
            roleid: role2.roleid,
            userid: user1.userid,
        };
        const user2Role1: IRoleUser = {
            created: Helpers.date(new Date()).addHours(-50),
            roleid: role1.roleid,
            userid: user2.userid,
        };
        const user2Role2: IRoleUser = {
            created: Helpers.date(new Date()).addHours(-25),
            roleid: role2.roleid,
            userid: user2.userid,
        };

        let actualUsers: IUser[];

        beforeAll(async done => {
            context = await getDbContextProvider().create();

            await context.userRepo.insert(user1);
            await context.userRepo.insert(user2);
            await context.roleRepo.insert(role1);
            await context.roleRepo.insert(role2);
            await context.roleRepo.insertRoleUser(user1Role1);
            await context.roleRepo.insertRoleUser(user1Role2);
            await context.roleRepo.insertRoleUser(user2Role1);
            await context.roleRepo.insertRoleUser(user2Role2);

            actualUsers = await context.userRepo.getUsersWithRoleAfterDays(role2.roleid, 1);

            done();
        });

        it("should return correct result", () => {
            expect(actualUsers.length).toEqual(1);
            expect(actualUsers[0].userid).toEqual(user2.userid);
            expect(actualUsers[0].usernametag).toBeTruthy();
            expect(actualUsers[0].lastpresent).toBeTruthy();
            expect(actualUsers[0].id).toBeGreaterThan(0);
        });

        afterAll(() => {
            if (context) {
                context.destroy();
            }
        });
    });

    describe("deleteManyUsers", () => {

        const user1: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };
        const role1 = createTestRole();
        const user1Role: IRoleUser = {
            userid: user1.userid,
            roleid: role1.roleid,
            created: new Date(),
        };
        const arkUser: IArkUser = {
            userid: user1.userid,
            ark_knightid: 1,
            times: 1,
        };
        const cooldown1: ILongCooldown = {
            channel: ChannelNames.admin,
            userid: user1.userid,
            command: CommandValues.avatar,
            created: new Date(),
            hours: 10,
        };

        const user2: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };
        const user3: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };

        let context: DbContext;

        let actualUser1: IUser;
        let actualUser2: IUser;
        let actualUser3: IUser;

        beforeAll(async done => {
            context = await getDbContextProvider().create();

            await context.userRepo.insert(user1);
            await context.userRepo.insert(user2);
            await context.userRepo.insert(user3);

            await context.roleRepo.insert(role1);
            await context.roleRepo.insertRoleUser(user1Role);
            await context.arkRepo.insertArkUser(arkUser);
            await context.cooldownRepo.upsert(cooldown1);

            // Act
            await context.userRepo.deleteManyUsers([user1.userid, user2.userid, user3.userid]);

            actualUser1 = await context.userRepo.getById(user1.userid);
            actualUser2 = await context.userRepo.getById(user2.userid);
            actualUser3 = await context.userRepo.getById(user3.userid);

            done();
        });

        it("should delete users", () => {
            expect(actualUser1).toBeFalsy();
            expect(actualUser2).toBeFalsy();
            expect(actualUser3).toBeFalsy();
        })

        afterAll(() => {
            if (context) {
                context.destroy();
            }
        });
    });

    describe("getDisabledUsers", () => {
        let context: DbContext;

        const user1: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
            deleted: Helpers.date(new Date()).addDays(-9),
        };
        
        const user2: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
            deleted: Helpers.date(new Date()).addDays(-11),
        };

        const user3: IUser = {
            userid: Helpers.snowflake(),
            usernametag: Helpers.snowflake(),
        };

        let actualUsers: IUser[];

        beforeAll(async done => {
            context = await getDbContextProvider().create();

            await context.userRepo.insert(user1);
            await context.userRepo.insert(user2);
            await context.userRepo.insert(user3);

            // Act
            actualUsers = await context.userRepo.getDisabledUsers(10, 5000);

            await context.userRepo.deleteManyUsers(actualUsers.map(u => u.userid));

            done();
        });

        it("should get user", () => {
            expect(actualUsers.findIndex(x => x.userid === user2.userid)).not.toEqual(-1);
            expect(actualUsers.findIndex(x => x.userid === user1.userid)).toEqual(-1);
            expect(actualUsers.findIndex(x => x.userid === user3.userid)).toEqual(-1);
        });

        afterAll(() => {
            if (context) {
                context.destroy();
            }
        });
    });
});