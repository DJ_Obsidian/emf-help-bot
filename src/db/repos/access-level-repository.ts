
import { DbConnectionProvider } from '../context';
import { IAccessLevel } from '../entities/access-level';
import { BaseRepository } from "./base-repository";

export default class AccessLevelRepository extends BaseRepository {
    
    constructor(protected connectionProvider: DbConnectionProvider) {
        super(connectionProvider);
    }

    async getByRoleId(roleid: number): Promise<IAccessLevel> {
        const result = await this.query(`select * from AccessLevels where roleid = ?`, roleid);
        return result[0];
    }

    getAll(): Promise<IAccessLevel[]> {
        return this.query(`select * from AccessLevels`);
    }

    async insert(accessLevel: IAccessLevel): Promise<number> {
        await this.query('insert into AccessLevels set ?', accessLevel);
        return accessLevel.id;
    }
}