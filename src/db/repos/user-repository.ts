import { DbConnectionProvider } from '../context';
import { IUser } from '../entities/user';
import { BaseRepository } from './base-repository';

export default class UserRepository extends BaseRepository {
    
    constructor(connectionProvider: DbConnectionProvider) {
        super(connectionProvider);
    }

    /**
     * Inserts user entry if one with the same userid does not already exist.
     * @param user 
     */
    async insert(user: IUser): Promise<IUser> {
        await this.query('insert ignore into Users set ?', user);
        return user;
    }

    update(user: IUser): Promise<void> {
        return this.query('update Users set usernametag = ? where userid = ?', [user.usernametag, user.userid]);
    }

    async getById(id: string): Promise<IUser> {
        const result = await this.query(`select * from Users where userid = ?`, id);
        return result[0];
    }

    async exists(id: string): Promise<boolean> {
        const r = await this.query(`select exists(select * from Users where userid = ?)`, id);
        return Object.entries(r[0])[0][1] === 1;
    }

    getAll(skip: number, take: number): Promise<IUser[]> {
        return this.query(`select * from Users LIMIT ?, ?`, [skip, take]);
    }

    /**
     * Get users that have been banned but have gone past the due date.
     * @param skip 
     * @param take 
     */
    getExpiredBannedUsers(skip: number, take: number): Promise<IUser[]> {
        return this.query(`select * from Users where banneduntil < NOW() LIMIT ?, ?`, [skip, take]);
    }

    banUser(id: string, banUntil: Date): Promise<unknown> {
        return this.query('update Users set banneduntil = ? where userid = ?', [banUntil, id]);
    }

    unbanUser(id: string): Promise<unknown> {
        return this.query('update Users set banneduntil = NULL where userid = ?', id);
    }

    delete(userid: string): Promise<unknown> {
        return this.query('delete from Users where userid = ?', userid);
    }

    deleteManyUsers(userIds: string[]): Promise<unknown> {
        if (userIds.length) {
            return this.query(`delete from Users where userid in (${userIds.map(x => `'${x}'`).join(',')})`);
        } else {
            return Promise.resolve();
        }
    }

    updatePresence(userid: string, lastPresent: Date): Promise<unknown> {
        return this.query('update Users set lastpresent = ? where userid = ?', [lastPresent, userid]);
    }

    softDeleteUser(userid: string, deleted: Date): Promise<unknown> {
        return this.query('update ignore Users set deleted = ? where userid = ?', [deleted, userid]);
    }

    unSoftDeleteUser(userid: string): Promise<unknown> {
        return this.query('update ignore Users set deleted = NULL where userid = ?', userid);
    }

    /**
     * Gets users that have been inactive for over the number of days specified.
     * @param daysInactive 
     * @param excludeRoleIds Roles the users should be ignored having assigned to them.
     * @param limit Result limit.
     */
    getInactiveUsers(daysInactive: number, excludeRoleIds: string[], limit = 5): Promise<IUser[]> {
        return this.query(`
            select u.id, u.userid, u.usernametag, u.lastpresent 
            from Users u 
            where not exists(select * from Roles r join RoleUsers ru on r.id = ru.role_id where ru.user_id = u.id
                and r.roleid in (${excludeRoleIds.map(r => `'${r}'`).join(',')}))
            and DATE_ADD(u.lastpresent, INTERVAL ? DAY) < NOW() and u.deleted IS NULL
            order by u.lastpresent desc limit 0, ?`,
            [daysInactive, limit]);
    }

    /**
     * Gets users that have the specified role for over the period of days specified.
     * @param roleId Role id.
     * @param days Days the role has been assigned to the user for.
     * @param limit Result limit
     */
    getUsersWithRoleAfterDays(roleId: string, days: number, limit = 5): Promise<IUser[]> {
        return this.query(`
            select u.userid, u.usernametag, u.id, u.deleted, u.lastpresent from Users u
            join RoleUsers ru on u.id = ru.user_id
            join Roles r on ru.role_id = r.id
            where r.roleid = ? and DATE_ADD(ru.created, INTERVAL ? DAY) < NOW() and u.deleted IS NULL
            order by u.lastpresent limit 0, ?
        `, [roleId, days, limit]);
    }

    getDisabledUsers(afterDays: number, limit: number): Promise<IUser[]> {
        return this.query(
            `select * from Users where deleted IS NOT NULL and DATE_ADD(deleted, INTERVAL ? DAY) < NOW() limit 0, ?`,
            [afterDays,limit]);
    }
}