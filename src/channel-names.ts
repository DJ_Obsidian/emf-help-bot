export enum ChannelNames {
    unspecified = -1,
    general = 1,
    nsfw = 2,
    serverInfo = 3,
    serverRules = 4,
    bgsInfo = 5,
    admin = 6,
    awards = 7,
    reception = 8,
    music = 9,
}