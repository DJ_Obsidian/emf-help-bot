import * as Discord from 'discord.js'
import { EventEmitter } from 'events';
import { Readable } from 'stream';
import { AccessLevels } from './access-levels';
import { ChannelNames } from './channel-names';
import { embedColor } from './colors';
import { DbContext, DbContextProvider } from './db/context';
import { IAccessLevel } from './db/entities/access-level';
import { DC } from './discord-channels';
import { IDiscordDMEvent, IDiscordDMEventOptions, IDiscordMemberAddEvent, IDiscordMemberAddEventOptions, IDiscordMemberRemoveEvent, IDiscordMemberRemoveEventOptions, IDiscordMessageEvent, IDiscordChannelMessageEventOptions, IDiscordReadyEvent, IDiscordReadyEventOptions, IDiscordTypingStartEvent, IDiscordVoiceStateUpdateEvent, IDiscordTypingStartEventOptions, IDiscordVoiceStateUpdateEventOptions, IDiscordRoleUpdateEvent, IDiscordRoleUpdateEventOptions, IDisocrdUserUpdateEvent, IDiscordUserUpdateEventOptions, ActivityType, IStreamSession } from './discord-event';
import { StreamSession } from './discord-stream-session';
import Helpers from './helpers';
import Logger from './logger';
import { SettingsHelper } from './settings-helper';

/* Discord client is a singleton for the whole app */
export const client = new Discord.Client({ ws: { intents: new Discord.Intents(Discord.Intents.ALL) }});

const READY = 'ready';
const MESSAGE = 'message';
const GUILD_MEMBER_ADD = 'guildMemberAdd';
const GUILD_MEMBER_REMOVE = 'guildMemberRemove';
const GUILD_MEMBER_UPDATE = 'guildMemberUpdate';
const TYPING_START = 'typingStart';
const VOICE_STATE_UPDATE = 'voiceStateUpdate';
const ROLE_CREATE = 'roleCreate';
const ROLE_DELETE = 'roleDelete';
const ROLE_UPDATE = 'roleUpdate';

export interface IChannel {
    id: string;
    name: string;
    type: string;
}

export interface IServer {
    guildid: string;
    name: string;
    channels: IChannel[];
}

export interface IMember {
    userid: string;
    displayName: string;
    joined: Date;
    joinedTimestamp: number;
    roles: string[];
}

export class DiscordServer extends EventEmitter {

    // Servers and their channels the bot is connected to.
    private _connectedServers: IServer[] = [];

    private _accessLevels: IAccessLevel[];

    private _readyEvent: IDiscordReadyEvent = null;
    private _messageEvent: IDiscordMessageEvent = null;
    private _dmEvent: IDiscordDMEvent = null;
    private _memberAddEvent: IDiscordMemberAddEvent = null;
    private _memberRemoveEvent: IDiscordMemberRemoveEvent = null;
    private _typingStartEvent: IDiscordTypingStartEvent = null;
    private _voiceStateUpdateEvent: IDiscordVoiceStateUpdateEvent = null;
    private _roleCreateEvent: IDiscordRoleUpdateEvent = null;
    private _roleDeleteEvent: IDiscordRoleUpdateEvent = null;
    private _roleUpdateEvent: IDiscordRoleUpdateEvent = null;
    private _userUpdateEvent: IDisocrdUserUpdateEvent = null;

    get guild(): IServer {
        return this._connectedServers[0];
    }

    get isZooKeepingTurnedOn(): boolean {
        const guild = this.client.guilds.cache.get(this.guild.guildid)
        const bot = guild.members.cache.get(this.client.user.id)
        return bot.roles.cache.has(this.settingsHelper.settings.zooKeeperRoleId);
    } 

    constructor(private client: Discord.Client, private dbContextProvider: DbContextProvider, private logger: Logger,
        private settingsHelper: SettingsHelper) {
        super();
        this.connectEventsToClient();
    }

    async start(): Promise<void> {
        await this.client.login(this.settingsHelper.settings.clientKey);
        await this.loadAccessLevels();
    }

    private getChannel<T extends Discord.Channel>(id: ChannelNames | string): T {
        if (typeof id === 'string') {
            return <T>this.client.channels.cache.get(id);
        } else {
            return <T>this.client.channels.cache.find(c => c.id === DC.getId(<ChannelNames>id));
        }
    }

    private getTextChannel(id: ChannelNames | string): Discord.TextChannel {
        const channel = this.getChannel<Discord.Channel>(id);
        if (channel.type === "text") {
            return <Discord.TextChannel>channel;
        } else {
            throw new Error('Channel is not a text channel');
        }
    }

    private getServer(): Promise<Discord.Guild> {
        return Promise.resolve(this.client.guilds.cache.get(this._connectedServers[0].guildid));
    }

    private async getMember(userid: string): Promise<Discord.GuildMember> {
        const guild = await this.getServer();
        try {
            return await guild.members.fetch(userid);
        } catch (error) {
            if (error.httpStatus === 404) {
                this.logger.error(`User ${userid} not found in Discord`);
                return null;
            } else {
                this.logger.error(error);
                throw error;
            }
        }
    }

    async getServerChannels(): Promise<{name: string, id: string}[]> {
        const guild = await this.getServer();
        return guild.channels.cache.map<{name: string, id: string}>(x => <{name: string, id: string}>{ name: x.name, id: x.id });
    }

    channelExists(id: string): boolean {
        const channel = this.getChannel(id);
        return !!channel;
    }

    sendMessage(message: unknown, id: ChannelNames | string): Promise<unknown> {
        const discordChannel = this.getChannel<Discord.TextChannel>(id);
        if (!discordChannel) {
            throw new Error(`Channel with id ${id} does not exist.`);
        }
        return discordChannel.send(message);
    }

    async sendPrivateMessage(message: unknown, userid: string): Promise<void> {
        const guild = await this.getServer();
        const user = guild.members.cache.get(userid);
        if (user) {
            try {
                await user.send(message);
            } catch (error) {
                await this.handleSendError(error);
            }
        }
    }

    async sendPMToNonMember(message: unknown, userid: string): Promise<void> {
        const user = await this.client.users.fetch(userid, false);
        if (user) {
            try {
                await user.send(message);
            } catch (error) {
                await this.handleSendError(error);
            }
        }
    }

    async getServerRoles(): Promise<{id: string, name: string, color: string}[]> {
        const guild = await this.getServer();
        return guild.roles.cache.map<{id: string, name: string, color: string}>(
            r => <{id: string, name: string, color: string}>{ id: r.id, name: r.name, color: r.hexColor })
    }

    async getServerUsers(): Promise<{id: string, name: string}[]> {
        const guild = await this.getServer();
        const members = await guild.members.fetch();
        return members.map(m => <{id: string, name: string}>{id: m.id, name: m.displayName});
    }

    async getServerUser(userid: string): Promise<{id: string, name: string}> {
        const guild = await this.getServer();
        const member = guild.members.cache.get(userid);
        return member ? {id: member.id, name: member.displayName} : null;
    }

    async getMemberRoles(userid: string): Promise<{id: string, name: string}[]> {
        const member = await this.getMember(userid);
        return member ? member.roles.cache.map(r => <{id: string, name: string}>{id: r.id, name: r.name}) : [];
    }

    async setRolesToMember(userid: string, roleids: string[]): Promise<void> {
        const member = await this.getMember(userid);
        if (member) {
            await member.roles.set(roleids);
        }
    }

    botHasRole(roleid: string): boolean {
        const guild = this.client.guilds.cache.get(this.guild.guildid)
        const bot = guild.members.cache.get(this.client.user.id)
        return bot.roles.cache.has(roleid);
    }

    async removeRolesFromMember(userid: string, rolesToRemoveids: string[]): Promise<void> {
        if (rolesToRemoveids.length > 0) {
            const member = await this.getMember(userid);
            if (member) {
                await member.roles.remove(rolesToRemoveids);
            }
        }
    }

    /**
     * Wipe channel of all its messages.
     * @param channelId 
     */
    async wipeChannel(channelId: string): Promise<void> {
        const channel = this.getTextChannel(channelId);
        const messages = await channel.messages.fetch({limit: 100}, false, true);
        while (messages.size > 0) {
            await channel.bulkDelete(messages);
        }
    }

    /**
     * Deletes the role specified by the role id. If no role found, nothing happens.
     * @param roleid 
     */
    async deleteRoleFromGuild(roleid: string): Promise<void> {
        const server = await this.getServer();
        const role = server.roles.cache.get(roleid);
        if (role) {
            await role.delete();
        }
    }

    async userHasRole(userid: string, roleid: string): Promise<boolean> {
        const member = await this.getMember(userid);
        return member ? member.roles.cache.has(roleid) : false;
    }

    async userHasAnyRoleOf(userid: string, roleids: string[]): Promise<boolean> {
        const member = await this.getMember(userid);
        if (member) {
            for (const roleid of roleids) {
                if (member.roles.cache.has(roleid)) {
                    return true;
                }
            }
        }

        return false;
    }

    private async fetchMembers(userids: string[]): Promise<Discord.Collection<string, Discord.GuildMember>> {
        const guild = await this.getServer();
        return await guild.members.fetch({ user: userids });
    }

    async userExists(userid: string): Promise<boolean> {
        const member = await this.getMember(userid);
        return !!member;
    }

    async isBot(userid: string): Promise<boolean> {
        const member = await this.getMember(userid);
        return member ? member.user.bot : false;
    }

    async isUserBanned(userid: string): Promise<boolean> {
        const guild = await this.getServer();
        if (guild.members.cache.has(userid)) {
            return false;
        }

        try {
            await guild.fetchBan(userid);
            return true;
        } catch (error) {
            return false;
        }
    }

    async getKickAuditLog(userid: string): Promise<{createdAt: Date}> {
        const guild = await this.getServer();
        const options = { limit: 25 };
        const logs = await guild.fetchAuditLogs(options);

        return logs.entries.find(v => v.action === "MEMBER_KICK" && (<Discord.User>v.target).id === userid);
    }

    getRoleIdForAccessLevel(accessLevel: AccessLevels): string {
        return this._accessLevels.find(al => al.id === accessLevel).roleid;
    }

    async getRole(roleid: string): Promise<{id: string, name: string}> {
        const guild = await this.getServer();
        const role = guild.roles.cache.get(roleid);
        return role ? {
            id: role.id,
            name: role.name,
        } : null;
    }

    async roleExists(roleid: string): Promise<boolean> {
        const guild = await this.getServer();
        return guild.roles.cache.has(roleid);
    }

    async getMembersByRole(roleid: string): Promise<{id: string, name: string}[]> {
        const guild = await this.getServer();
        const members = guild.members.cache.filter(m => !m.user.bot && m.roles.cache.has(roleid)); // Make sure to exclude bot users.
        return members.map(m => <{id: string, name: string}>{
            id: m.id,
            name: m.displayName,
        });
    }

    async getMembersByAccessLevel(accessLevel: AccessLevels): Promise<{id: string, name: string}[]> {
        const roleId = this.getRoleIdForAccessLevel(accessLevel);
        return await this.getMembersByRole(roleId);
    }

    private getMemberHighestLevel(member: Discord.GuildMember): AccessLevels {
        for (const al of Helpers.sortDescending(this._accessLevels, x => x.id)) {
            if (member.roles.cache.has(al.roleid) === true) {
                return al.id;
            }
        }

        return AccessLevels.Everyone;
    }

    async getHighestUserLevel(userid: string): Promise<AccessLevels> {
        const member = await this.getMember(userid);
        if (!member) {
            return undefined;
        }

        return this.getMemberHighestLevel(member);
    }

    async banUserPerm(userid: string, reason: string): Promise<{exists: boolean}> {
        const userToBan = await this.getMember(userid);
        if (userToBan) {
            await userToBan.ban({
                reason: reason,
            });

            return {exists: true};
        } else {
            return {exists: false};
        }
    }

    async kickUser(userid: string, reason?: string): Promise<{exists: boolean}> {
        const userToKick = await this.getMember(userid);
        if (userToKick) {
            await userToKick.kick(reason);
            return {exists: true};
        } else {
            return {exists: false};
        }
    }

    async setBotActivity(text: string, activity: ActivityType): Promise<unknown> {
        return await this.client.user.setActivity(text, { type: activity });
    }

    // Have bot attempt to join the specified channel. Will return existing voice connection if already joined
    private async joinChannel(channel: Discord.VoiceChannel): Promise<Discord.VoiceConnection> {
        if (channel.joinable) {
            // Check if bot has already joined this channel
            if (channel.members.has(this.client.user.id)) {
                return this.client.voice.connections.find(c => c.channel.id === channel.id);
            }

            const connection = await channel.join();
            connection.on("disconnect", () => this.emit("botvoicedisconnect", connection.channel.id));
            return connection;
        }

        throw new Error(`Channel ${channel.name} (${channel.id}) not joinable`);
    }

    /**
     * Have the bot leave the voice channel.
     * @param channelId 
     */
    private leaveChannel(channelId: string): void {
        const connection = this.client.voice.connections.find(c => c.channel.id === channelId);
        if (connection) {
            connection.disconnect();
        }
    }

    onReady(callback: (args: IDiscordReadyEventOptions, db: DbContext) => Promise<void>): void {
        this._readyEvent = {
            callback: callback,
        };
    }

    onGuildMemberAdd(callback: (args: IDiscordMemberAddEventOptions, db: DbContext) => Promise<void>): void {
        this._memberAddEvent = {
            callback: callback,
        };
    }

    onGuildMemberRemove(callback: (args: IDiscordMemberRemoveEventOptions, db: DbContext) => Promise<void>): void {
        this._memberRemoveEvent = {
            callback: callback,
        };
    }

    onMessage(callback: (args: IDiscordChannelMessageEventOptions, db: DbContext) => Promise<void>): void {
        this._messageEvent = {
            callback: callback,
        };
    }

    onDM(callback: (args: IDiscordDMEventOptions, db: DbContext) => Promise<void>): void {
        this._dmEvent = {
            callback: callback,
        };
    }

    onTypingStart(callback: (args: IDiscordTypingStartEventOptions) => Promise<void>): void {
        this._typingStartEvent = {
            callback: callback,
        };
    }

    onVoiceStatusUpdate(callback: (args: IDiscordVoiceStateUpdateEventOptions) => Promise<void>): void {
        this._voiceStateUpdateEvent = {
            callback: callback,
        };
    }

    onRoleCreate(callback: (args: IDiscordRoleUpdateEventOptions) => Promise<void>): void {
        this._roleCreateEvent = {
            callback: callback,
        };
    }

    onRoleDelete(callback: (args: IDiscordRoleUpdateEventOptions) => Promise<void>): void {
        this._roleDeleteEvent = {
            callback: callback,
        };
    }

    onRoleUpdate(callback: (args: IDiscordRoleUpdateEventOptions) => Promise<void>): void {
        this._roleUpdateEvent = {
            callback: callback,
        };
    }

    onUserUpdate(callback: (args: IDiscordUserUpdateEventOptions) => Promise<void>): void {
        this._userUpdateEvent = {
            callback: callback,
        };
    }

    async unbanUser(userid: string): Promise<void> {
        const user = this.client.guilds.cache.get(this.guild.guildid).members.cache.get(userid);
        if (user) {
            await user.roles.remove(
                this._accessLevels.find(x => x.id === AccessLevels.Banned).roleid);
        }
    }

    private async loadAccessLevels(): Promise<void> {
        return this.dbContextProvider.execute(async db => {
            this._accessLevels = await db.accessLevelRepo.getAll();
            if (this._accessLevels.length === 0) {
                throw new Error('Failed to load main roles (access levels).');
            }
    
            this.logger.log("Access levels:");
            this._accessLevels.forEach(c => this.logger.log(`${AccessLevels[c.id]} - ${c.roleid}`));
        });
    }

    private connectEventsToClient(): void {
        this.client.on('shardError', error => {
            this.logger.error(error, 'Discord shardError');
       });

        // On ready
        this.client.on(READY, () => this.ready());

        // On message received
        this.client.on(MESSAGE, (message: Discord.Message) => this.message(message));

        // On new member added
        this.client.on(GUILD_MEMBER_ADD, (member: Discord.GuildMember) => this.guildMemberAdd(member));

        // On member removed/kicked
        this.client.on(GUILD_MEMBER_REMOVE, (member: Discord.GuildMember) => this.guildMemberRemove(member));

        // On member typing in a channel
        this.client.on(TYPING_START, (channel: Discord.Channel, user: Discord.User) => this.typingStart(channel, user));

        // On member voice state updated
        this.client.on(VOICE_STATE_UPDATE, (oldState: Discord.VoiceState, newState: Discord.VoiceState) => this.voiceStateUpdate(oldState, newState));

        // On guild role update
        this.client.on(ROLE_CREATE, (role: Discord.Role) => this.roleCreate(role));
        this.client.on(ROLE_DELETE, (role: Discord.Role) => this.roleDelete(role));
        this.client.on(ROLE_UPDATE, (oldRole: Discord.Role, newRole: Discord.Role) => this.roleUpdate(oldRole, newRole));

        // On guild member update
        this.client.on(GUILD_MEMBER_UPDATE, (oldUser: Discord.GuildMember, newUser: Discord.GuildMember) => this.guildMemberUpdate(oldUser, newUser));
    }

    ready(): void {
        this.showServerInfo(); // For testing

        this.dbContextProvider.create().then(db => {
            this._readyEvent.callback({
                setActivity: (text: string) => this.client.user.setActivity(text),
            }, 
            db)
            .then()
            .catch(err => {
                // Any error on ready means trouble. Better shut down.
                this.logger.error(`${READY}: ${err.toString()}`);
                process.exit(1);
            })
            .finally(() => db.destroy());
        });
    }

    guildMemberAdd(member: Discord.GuildMember): void {
        this.activityNotify();
        this.dbContextProvider.create().then(db => {
            this._memberAddEvent.callback({
                memberId: member.id,
                userTag: member.user.tag,
                addRoleToMember: (...roles: any[]): Promise<unknown> => member.roles.add(roles),
            },
            db)
            .then()
            .catch(err => this.handleEventError(GUILD_MEMBER_ADD, err))
            .finally(() => db.destroy());
        });
    }

    guildMemberRemove(member: Discord.GuildMember): void {
        this.activityNotify();
        this.dbContextProvider.create().then(db => {
            this._memberRemoveEvent.callback({
                memberId: member.id,
                memberName: member.displayName,
            },
            db)
            .then()
            .catch(err => this.handleEventError(GUILD_MEMBER_REMOVE, err))
            .finally(() => db.destroy());
        });
    }

    dm(message: Discord.Message): void {
        const logger = this.logger;
        function sendPrivateMessage(message: unknown, user: Discord.User): Promise<void> {
            return new Promise<void>((resolve) => user.send(message).then(() => resolve()).catch(err => {
                logger.error(err)
                resolve();
            }));
        }

        this.dbContextProvider.create().then(db => {
            this._dmEvent.callback({
                authorId: message.author.id,
                authorDisplayName: message.author.username,
                message: message.content,
                getHighestUserLevel: (): Promise<AccessLevels> => {
                    return this.getHighestUserLevel(message.author.id);
                },        
                send: (msg: unknown): Promise<void> => {
                    return sendPrivateMessage(msg, message.author);
                },
                dm: (msg: unknown): Promise<void> => {
                    return sendPrivateMessage(msg, message.author);
                },
                deleteMessage: (afterSeconds?: number): Promise<unknown> => {
                    if (afterSeconds) {
                        return message.delete({ timeout: afterSeconds * 1000 }).catch(err => err);
                    } else {
                        return message.delete().catch(err => err);
                    }
                },
            },
            db)
            .then()
            .catch(err => this.handleEventError(`${MESSAGE} (DM)`, err))
            .finally(() => db.destroy());
        });
    }

    message(message: Discord.Message): void {
        this.activityNotify();

        if (message.author == this.client.user) { // Prevent bot from responding to its own messages
            return
        }

        if (message.channel.type === "dm") { // When message is sent directly to bot
            this.dm(message);
            return;
        }

        if (!message.member) {
            // This message has come from someone (or something) that's not a member of this server
            return;
        }

        const accessLevels = this._accessLevels;
        const logger = this.logger;
        const voiceChannelId = message.member.voice.channel ? message.member.voice.channel.id : null;
        const voiceChannelName = message.member.voice.channel ? message.member.voice.channel.name : null;
        this.dbContextProvider.create().then(db => {
            this._messageEvent.callback(
            {
                authorId: message.author.id,
                authorDisplayName: message.member.displayName,
                memberId: message.member.id,
                message: message.content,
                channelId: message.channel.id,
                serverId: message.guild.id,
                guildName: message.guild.name,
                voiceChannelId: voiceChannelId,
                voiceChannelName: voiceChannelName,

                setActivity: (text: string, activityType: ActivityType): Promise<unknown> => this.client.user.setActivity(
                    text, {
                        type: activityType,
                    }),
                getHighestUserLevel: (): AccessLevels => {
                    return this.getMemberHighestLevel(message.member);
                },
                send: (msg: unknown, deleteAfterSeconds?: number): Promise<void> => {
                    return new Promise<void>((resolve, reject) => {
                        message.channel.send(msg).then((msg) => {
                            if (deleteAfterSeconds) {
                                msg.delete({ timeout: deleteAfterSeconds * 1000 })
                                    .then(() => resolve()).catch(err => {
                                        logger.error(err);
                                        resolve();
                                    });
                            } else {
                                resolve();
                            }
                        }).catch(err => {
                            logger.error(err)
                            reject(err);
                        });
                    });
                },
                dm: (msg: unknown): Promise<void> => {
                    return new Promise<void>((resolve, reject) => message.member.send(msg).then(() => resolve()).catch(err => {
                        logger.error(err);
                        resolve(); // TODO: The recipient may have the bot blocked
                    }));
                },
                deleteMessage: (afterSeconds?: number): Promise<unknown> => {
                    if (afterSeconds) {
                        return message.delete({ timeout: afterSeconds * 1000 }).catch(err => logger.error(err));
                    } else {
                        return message.delete().catch(err => logger.error(err));
                    }
                },
                mentions: (): { userid: string, usernametag: string }[] => {
                    return message.mentions.users.map<{ userid: string, usernametag: string }>(
                        x => <{ userid: string, usernametag: string }>{ userid: x.id, usernametag: x.tag });
                },
                getRoles: (): { roleid: string, name: string }[] => {
                    return message.member.roles.cache
                        .filter(r => message.member.guild.roles.cache.has(r.id) && r.name !== '@everyone')
                        .map<{ roleid: string, name: string }>(
                            x => <{ roleid: string, name: string }>{ roleid: x.id, name: x.name });
                },
                getRoleByName: (name: string): { roleid: string, name: string } => {
                    name = name.toLowerCase();
                    const role = message.guild.roles.cache.find(r => r.name.toLowerCase().includes(name));
                    if (role) {
                        return {
                            roleid: role.id,
                            name: role.name,
                        };
                    } else {
                        return null;
                    }
                },
                hasRole: (roleId: string): boolean => {
                    return message.member.roles.cache.has(roleId);
                },
                isAdmin: (): boolean => {
                    return message.member.roles.cache.has(this.getRoleIdForAccessLevel(AccessLevels.Admin));
                },
                createRole: async (name: string, position?: number, mentionable?: boolean): Promise<void> => {
                    const role = await message.guild.roles.create({
                        data: {
                            name: name,
                            mentionable: mentionable,
                            position: position,
                        },
                    });
                    await message.member.roles.add(role);
                },
                addRole: async (roleid: string): Promise<void> => {
                    const role = message.guild.roles.cache.get(roleid);
                    if (role) {
                        await message.member.roles.add(role);
                    }
                },
                removeRole: async (roleid: string): Promise<void> => {
                    await message.member.roles.remove(roleid);
                },
                changeRoleColor: async (roleid: string, hexColor: string): Promise<void> => {
                    const role = message.member.roles.cache.get(roleid);
                    if (role) {
                        await role.setColor(hexColor);
                    }
                },
                changeRoleName: async (roleid: string, name: string): Promise<void> => {
                    const role = message.member.roles.cache.get(roleid);
                    if (role) {
                        await role.setName(name);
                    }
                },
                member: (userid: string): IMember => {
                    const member = message.guild.members.cache.get(userid);
                    if (member) {
                        return {
                            userid: member.id,
                            displayName: member.displayName,
                            joined: member.joinedAt,
                            joinedTimestamp: member.joinedTimestamp,
                            roles: member.roles.cache.map(r => r.id),
                        };
                    } else {
                        return null;
                    }
                },
                randomMember: (): { displayName: string, userid: string } => {
                    const member = message.guild.members.cache.random();
                    if (member) {
                        return {
                            displayName: member.displayName,
                            userid: member.id,
                        };
                    } else {
                        return null;
                    }
                },
                members: (callback: (m: unknown) => boolean): Promise<IMember[]> => {
                    const members = message.guild.members.cache.filter(m => callback(m));
                    return Promise.resolve(members.map<IMember>(
                        m => <IMember>{
                            userid: m.id,
                            displayName: m.displayName,
                            joined: m.joinedAt,
                            joinedTimestamp: m.joinedTimestamp
                        }));
                },
                ban: async (): Promise<string> => {
                    const userToBan = message.mentions.users.first();
                    await message.guild.members.cache.get(userToBan.id).roles.add(
                        accessLevels.find(x => x.id === AccessLevels.Banned).roleid);
                    return userToBan.id;
                },
                unban: async (): Promise<string> => {
                    const userToUnan = message.mentions.users.first();
                    await message.guild.members.cache.get(userToUnan.id).roles.remove(
                        accessLevels.find(x => x.id === AccessLevels.Banned).roleid);
                    return userToUnan.id;
                },
                isPermBannable: (userid: string): boolean => {
                    const member = message.guild.members.cache.get(userid);
                    if (member) {
                        return member.bannable;
                    } else {
                        return false;
                    }
                },
                banPerm: async (reason?: string, days?: number): Promise<void> => {
                    const userToBan = message.mentions.users.first();
                    await message.guild.members.cache.get(userToBan.id).ban({
                        reason: reason,
                        days: days,
                    });
                },
                unbanPerm: async (): Promise<void> => {
                    const userToUnban = message.mentions.users.first();
                    await message.guild.members.unban(userToUnban.id);
                },
                isBanned: (userId: string): boolean => {
                    const user = message.guild.members.cache.get(userId);
                    if (user) {
                        return user.roles.cache.has(
                            accessLevels.find(x => x.id === AccessLevels.Banned).roleid);
                    } else {
                        return false;
                    }
                },
                kick: async (userid: string, reason?: string): Promise<void> => {
                    const member = message.guild.members.cache.get(userid);
                    if (member) {
                        await member.kick(reason);
                    }
                },
                kickable: (userid: string): boolean => {
                    const member = message.guild.members.cache.get(userid);
                    if (member) {
                        return member.kickable;
                    } else {
                        return false;
                    }
                },
                deleteMessages: async (n: number): Promise<void> => {
                    if (message.channel.type === "text") {
                        const textChannel = <Discord.TextChannel>message.channel;
                        const messages = await textChannel.messages.fetch({
                            limit: n
                        }, false, true);
    
                        if (messages.size > 1) {
                            await textChannel.bulkDelete(messages);
                        }
                    }
                },
                getAvatar: (memberId?: string): Promise<unknown> => {
                    let userTag: string;
                    let avatar: string;
                    if (memberId) {
                        const member = message.guild.members.cache.get(memberId);
                        if (member) {
                            userTag = member.user.tag;
                            avatar = member.user.avatarURL();
                        } else {
                            Promise.resolve(null);
                        }
                    } else {
                        userTag = message.author.tag;
                        avatar = message.author.avatarURL();
                    }

                    const embed = new Discord.MessageEmbed()
                        .setColor(embedColor)
                        .setTitle(userTag)
                        .setURL(avatar)
                        .setImage(avatar)
                        .setFooter("Сообщение пропадет через 2 минуты");

                    return Promise.resolve(embed);
                },
                like: (): Promise<unknown> => {
                    return message.react('👍');
                },
                error: (): Promise<unknown> => {
                    return message.react('❌');
                },
                stream: async (readable: Readable): Promise<IStreamSession> => {
                    const channel = this.getChannel(voiceChannelId);
                    if (channel.type !== "voice") {
                        throw new Error('Not a voice channel');
                    }

                    const connection = await this.joinChannel(<Discord.VoiceChannel>channel);
                    connection.voice.setDeaf(true);
                    return new StreamSession(connection, readable);
                },
                disconnect: (): Promise<void> => {
                    this.leaveChannel(voiceChannelId);
                    return Promise.resolve();
                },
                temp: { // Unused command.
                    getAwardMedalsFromMessages: async (): Promise<{ medal: string, userid: string, description: string }[]> => {
                        const channel = this.getChannel<Discord.TextChannel>(ChannelNames.awards);
                        const messages = await channel.messages.fetch({ limit: 100 });
                        return messages.array().filter(msg => {
                            const embed = msg.embeds.length > 0 ? msg.embeds[0] : null;
                            if (!embed) {
                                return false;
                            }

                            const medal = embed.fields.length > 1 ? embed.fields[1].value : null;
                            if (!medal) {
                                return false;
                            }

                            const medals = [
                                {
                                    medalType: "орден",
                                },
                                {
                                    medalType: "медаль1",
                                },
                                {
                                    medalType: "медаль2",
                                },
                                {
                                    medalType: "медаль3",
                                },
                                {
                                    medalType: "защита",
                                },
                                {
                                    medalType: "отвага",
                                },
                                {
                                    medalType: "победа",
                                },
                                {
                                    medalType: "охота",
                                },
                            ];

                            if (medals.findIndex(m => m.medalType === medal) !== -1) {
                                return true;
                            } else {
                                return false;
                            }
                        }).map(msg => <{ medal: string, userid: string, description: string }>{
                            userid: msg.embeds[0]['fields'][0]['value'].replace(/[^0-9]/g, ""),
                            medal: msg.embeds[0]['fields'][1]['value'],
                            description: msg.embeds[0]['fields'][2]['value'],
                        });
                    },
                },
            },
            db)
            .then()
            .catch(err => this.handleEventError(MESSAGE, err))
            .finally(() => db.destroy());
        });
    }

    typingStart(channel: Discord.Channel, user: Discord.User): void {
        this.activityNotify();
        this._typingStartEvent.callback({
            channelId: channel.id,
            memberId: user.id,
            userNameTag: user.username,
        })
        .then()
        .catch(err => this.handleEventError(TYPING_START, err));
    }

    voiceStateUpdate(oldState: Discord.VoiceState, newState: Discord.VoiceState): void {
        this.activityNotify();
        this._voiceStateUpdateEvent.callback({
            memberId: newState.member ? newState.member.id : null,
            channelId: newState.channel ? newState.channel.id : null,
            speaking: newState.speaking || false,
            memberName: newState.member ? newState.member.displayName : null,
        })
        .then()
        .catch(err => this.handleEventError(VOICE_STATE_UPDATE, err));

        // Check if Miku is alone in channel
        if (!newState.channelID && oldState.channel) {
            // User left channel
            this.checkBotAloneInChannel(oldState.channel.id);
        }
    }

    private checkBotAloneInChannel(channelId: string): boolean {
        const channel = this.getChannel(channelId);
        if (channel && channel.type === "voice") {
            const connection = this.client.voice.connections.find(x => x.channel.id === channelId);
            if (connection) {
                const voiceChannel = (<Discord.VoiceChannel>channel);
                const otherConnectedMembers = voiceChannel.members.filter(x => x.id !== this.client.user.id);
                if (Array.from(otherConnectedMembers).length === 0) {
                    this.logger.log(`Everyone left channel ${voiceChannel.name} (${voiceChannel.id} and now Miku is all alone (((`);
                    this.emit("botaloneinchannel", channel.id);
                    return true;
                }
            }
        }

        return false;
    }

    private activityNotify(): void {
        this.emit("activity");
    }

    roleCreate(role: Discord.Role): void {
        this._roleCreateEvent.callback({
            id: role.id,
            name: role.name,
            color: role.hexColor,
        })
        .then()
        .catch(err => this.handleEventError(ROLE_CREATE, err));
    }

    roleDelete(role: Discord.Role): void {
        this._roleDeleteEvent.callback({
            id: role.id,
            name: role.name,
            color: role.hexColor,
        })
        .then()
        .catch(err => this.handleEventError(ROLE_DELETE, err));
    }

    roleUpdate(oldRole: Discord.Role, newRole: Discord.Role): void {
        this._roleUpdateEvent.callback({
            id: newRole.id,
            name: newRole.name,
            color: newRole.hexColor,
            oldRole: {id: oldRole.id, name: oldRole.name, color: oldRole.hexColor},
        })
        .then()
        .catch(err => this.handleEventError(ROLE_UPDATE, err));
    }

    guildMemberUpdate(oldUser: Discord.GuildMember, newUser: Discord.GuildMember): void {
        this.activityNotify();
        this._userUpdateEvent.callback({
            id: newUser.id,
            name: newUser.displayName,
            rolesAdded: newUser.roles.cache.filter(r => !oldUser.roles.cache.has(r.id)).map<{id: string, name: string}>(
                r => <{id: string, name: string}>{id: r.id, name: r.name}),
            rolesRemoved: oldUser.roles.cache.filter(r => !newUser.roles.cache.has(r.id)).map<{id: string, name: string}>(
                r => <{id: string, name: string}>{id: r.id, name: r.name}),
            oldUser: {
                id: oldUser.id,
                name: oldUser.displayName,
            },
        })
        .then()
        .catch(err => this.handleEventError(GUILD_MEMBER_UPDATE, err));
    }

    private handleEventError(eventName: string, err: unknown) {
        this.logger.error(`${eventName}: ${(err ? err.toString() : 'unknown')}`);
    }

    private handleSendError(error: Discord.DiscordAPIError): Promise<void> {
        this.logger.error(error);
        // if (error.httpStatus !== 404) {
        //     throw error;
        // }

        return Promise.resolve();
    }

    private showServerInfo(): void {
        const members: IMember[] = [];
        const guildRoles: { roleid: string, name: string }[] = [];
        this.client.guilds.cache.forEach((guild: Discord.Guild) => {
    
            const server: IServer = {
                guildid: guild.id,
                name: guild.name,
                channels: [],
            };
    
            // Channels
            guild.channels.cache.forEach((c: Discord.GuildChannel) => {
                server.channels.push({
                    id: c.id,
                    name: c.name,
                    type: c.type,
                });
                this.logger.log(` -- ${c.name} (${c.type}) - ${c.id}`);
            });

            // Guild roles
            guild.roles.cache.forEach((role: Discord.Role) => {
                guildRoles.push({
                    roleid: role.id,
                    name: role.name,
                });
            });

            if (!Helpers.isProduction) { // Do not show on live
                // Members
                guild.members.cache.forEach((member: Discord.GuildMember) => {
                    members.push({
                        userid: member.id,
                        displayName: member.displayName,
                        roles: member.roles.cache.map(r => r.id),
                        joined: member.joinedAt,
                        joinedTimestamp: member.joinedTimestamp,
                    });
                });
            }

            this._connectedServers.push(server);
        });
    
        this.logger.log("Servers:");
        this._connectedServers.forEach(s => {
            this.logger.log(" - " + s.name);
            this.logger.log("Server roles:");
            guildRoles.forEach(role => this.logger.log(`${role.name} - ${role.roleid}`));
    
            if (!Helpers.isProduction) {
                this.logger.log('\r\n');
                this.logger.log('Members:');
                members.forEach(member => this.logger.log(`${member.displayName}, ${member.userid} - role: ${member.roles.map(r => r).join(", ")}`));
            }
        });
    }
}
