import { getDbContextProviderFake, getDbContextFake, getFileHelperFake, getTaskSchedulerFake, getLoggerFake } from "./common-spec/common.spec";
import { DbContext } from "./db/context";
import { IUser } from "./db/entities/user";
import { IProcessTask, TaskProcessor } from "./task-processor";
import { TaskNames } from "./task-names";


describe("TaskProcessor", () => {

    describe("addUser", () => {

        const dbContextProviderFake: any = getDbContextProviderFake();
        const dbContextFake: any = getDbContextFake();
        const fileHelperFake: any = getFileHelperFake();
        const loggerFake: any = getLoggerFake();
        const taskProcessor = new TaskProcessor(dbContextProviderFake, fileHelperFake, getTaskSchedulerFake(), loggerFake);

        const user: IUser = {
            userid: "1",
            usernametag: "name tag",
        };

        let prepareDirectorySpy: jasmine.Spy;
        let writeToFileSpy: jasmine.Spy;
        let directorySpy: jasmine.Spy;
        let readFileSpy: jasmine.Spy;
        let createSpy: jasmine.Spy;
        let destroySpy: jasmine.Spy;
        let deleteFileSpy: jasmine.Spy;

        let taskFile: string;
        let json: string;

        const expectedTaskId = "task1";

        let actualTask: IProcessTask;
        let actualDb: DbContext;

        beforeAll(async done => {
            prepareDirectorySpy = spyOn(fileHelperFake, 'prepareDirectory').and.callThrough();
            writeToFileSpy = spyOn(fileHelperFake, 'writeToFile').and.callThrough()
                .and.callFake((file: string, content: string) => {
                    taskFile = file;
                    json = content;
                });
            directorySpy = spyOn(fileHelperFake, 'directory').and.callThrough()
                .and.callFake(() => {
                    return new Promise((resolve, _) => resolve({ files: [ `1604702516176bot-task_${expectedTaskId}.json` ] }));
                });
            readFileSpy = spyOn(fileHelperFake, 'readFile').and.callThrough()
                .and.callFake(() => json);
            createSpy = spyOn(dbContextProviderFake, 'create').and.callThrough()
                .and.returnValue(dbContextFake);
            destroySpy = spyOn(dbContextFake, 'destroy').and.callThrough();
            deleteFileSpy = spyOn(fileHelperFake, 'deleteFile').and.callThrough();

            taskProcessor.onTaskProcess(async (task, db) => {
                actualTask = task;
                actualDb = db;
            });

            // Act
            taskProcessor.addTask(<IProcessTask>{
                name: TaskNames.addUser,
                data: user,
            },
            expectedTaskId);
            await taskProcessor.processPendingTasks();

            done();
        });

        it("should call prepare directory", () => {
            expect(prepareDirectorySpy).toHaveBeenCalledWith("./temp");
            expect(prepareDirectorySpy).toHaveBeenCalledTimes(2);
        });

        it("should call writeToFile", () => {
            expect(writeToFileSpy).toHaveBeenCalledTimes(1);
            expect(taskFile).toBeTruthy();
            expect(/\.\/temp\/[0-9]{13}bot-task_/.test(taskFile)).toBeTrue();
        });

        it("should call readFile", () => {
            expect(readFileSpy).toHaveBeenCalledTimes(1);
        });

        it("should call event", () => {
            expect(actualTask.data.userid).toEqual(user.userid);
            expect(actualTask.name).toEqual(TaskNames.addUser);
            expect(actualDb).toBeTruthy();
        });

        it("should call deleteFile", () => {
            expect(deleteFileSpy).toHaveBeenCalledTimes(1);
        });

        it("should call destroy on DbContext", () => {
            expect(destroySpy).toHaveBeenCalledTimes(1);
        });
    });
});