

export interface ISyncUsersTaskObject {
    userid: string;
    deleted: {userid: string, name: string}[];
    finished: boolean;
}