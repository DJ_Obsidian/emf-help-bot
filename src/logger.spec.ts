import { getFileHelperFake, getSettingsHelperFake } from "./common-spec/common.spec";
import Helpers from "./helpers";
import Logger from "./logger";
import * as p from "path";


describe("logger", () => {

    const logFake = jasmine.createSpyObj<{
        setLevel: () => void;
        trace: () => void;
        error: () => void;
    }>(["setLevel", "trace", "error"]);

    describe("deleteOldFiles", () => {
        [
            { fileDate: new Date(), expectedToBeDeleted: false },
            { fileDate: Helpers.date(new Date()).addDays(-1), expectedToBeDeleted: false },
            { fileDate: Helpers.date(new Date()).addDays(-2), expectedToBeDeleted: false },
            { fileDate: Helpers.date(new Date()).addDays(-3), expectedToBeDeleted: false },
            { fileDate: Helpers.date(new Date()).addDays(-4), expectedToBeDeleted: false },
            { fileDate: Helpers.date(new Date()).addDays(-5), expectedToBeDeleted: false },
            { fileDate: Helpers.date(new Date()).addDays(-6), expectedToBeDeleted: true },
            { fileDate: Helpers.date(new Date()).addDays(-7), expectedToBeDeleted: true },
        ].forEach(x => {
            describe(`when a file is ${x.fileDate}`, () => {

                const settingsHelper = getSettingsHelperFake();
                settingsHelper.settings = {
                    logFileLifeDays: 5,
                };
    
                const fileHelper = getFileHelperFake();
                const logger = new Logger(fileHelper, settingsHelper, logFake);
        
                let prepareDirectorySpy: jasmine.Spy;
                let directorySpy: jasmine.Spy;
                let deleteFileSpy: jasmine.Spy;
        
                beforeAll(async done => {
                    
                    prepareDirectorySpy = spyOn(fileHelper, 'prepareDirectory').and.callThrough();
                    directorySpy = spyOn(fileHelper, 'directory').and.callThrough()
                        .and.resolveTo({
                            files: [
                                `botlog-${x.fileDate.getFullYear()}.${x.fileDate.getMonth() + 1 < 10 ? "0" : ""}${x.fileDate.getMonth() + 1}.${x.fileDate.getDate() < 10 ? "0" : ""}${x.fileDate.getDate()}.log`
                            ],
                        });
                    deleteFileSpy = spyOn(fileHelper, 'deleteFile').and.callThrough();
        
                    // Act
                    await logger.deleteOldFiles();
                    done();
                });
        
                it("should call prepareDirectory", () => {
                    expect(prepareDirectorySpy).toHaveBeenCalledTimes(1);
                });

                it("should call directory once", () => {
                    expect(directorySpy).toHaveBeenCalledTimes(1);
                })

                if (x.expectedToBeDeleted) {
                    it("should delete file", () => {
                        expect(deleteFileSpy).toHaveBeenCalledOnceWith(
                            p.join("logs", `botlog-${x.fileDate.getFullYear()}.${x.fileDate.getMonth() + 1 < 10 ? "0" : ""}${x.fileDate.getMonth() + 1}.${x.fileDate.getDate() < 10 ? "0" : ""}${x.fileDate.getDate()}.log`)
                        );
                    });
                } else {
                    it("should not delete file", () => {
                        expect(deleteFileSpy).not.toHaveBeenCalled();
                    })
                }
            });
        });
    });
});