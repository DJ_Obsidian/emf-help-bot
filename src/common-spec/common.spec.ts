/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { EventEmitter } from "events";
import { Arks } from "../arks";
import { ICommand } from "../command";
import { ICooldown } from "../cooldown";
import { createPool, DbContext, DbContextProvider, MigratorDbContextProvider } from "../db/context";
import { ILongCooldown } from "../db/entities/long-cooldown";
import { IRole } from "../db/entities/role";
import { IUser } from "../db/entities/user";
import { Migrator } from "../db/meegrator";
import FileHelper from "../file-helper";
import Helpers from "../helpers";
import Logger from "../logger";
import { SettingsHelper } from "../settings-helper";
import { IProcessTask } from "../task-processor";

if (!Helpers.isProduction) {
    console.log('<<<<<<<<<BOT RUNNING IN DEVELOPMENT MODE>>>>>>>>>');
}

export function getServerFake(): any {
    return {
        sendMessage: function (msg: string, channel: string): Promise<void> { return null; },
        getChannel: function (n: any): any { return null; },
        getServerUsers: function (): any { return null; },
        getMemberRoles: function (): any { return null; },
        onReady: function (): any { return null; },
        onGuildMemberAdd: function (): any { return null; },
        onGuildMemberRemove: function (): any { return null; },
        onMessage: function (): any { return null; },
        onDM: function (): any { return null; },
        unbanUser: (userid: string): Promise<any> => { return null; },
        onTypingStart: (): Promise<any> => { return null; },
        onVoiceStatusUpdate: (): Promise<any> => { return null; },
        onRoleCreate: (): Promise<any> => { return null; },
        onRoleDelete: (): Promise<any> => { return null; },
        onRoleUpdate: (): Promise<any> => { return null; },
        onUserUpdate: (): Promise<any> => { return null; },
        getRoleIdForAccessLevel: (): any => { return null; },
        userHasRole: (): Promise<any> => { return null; },
        setRolesToMember: (): Promise<any> => { return null; },
        getRole: (): Promise<any> => { return null; },
        removeRolesFromMember: (): Promise<any> => { return null; },
        banUserPerm: (): Promise<any> => { return null; },
        userHasAnyRoleOf: (): Promise<any> => { return null; },
        isBot: (): Promise<boolean> => { return null; },
        getMembersByAccessLevel: (): Promise<any> => { return null; },
        userExists: (): Promise<boolean> => { return null; },
        getMembersByRole: (): Promise<boolean> => { return null; },
        kickUser: (): Promise<unknown> => { return null },
        on: (): void => {},
        isUserBanned: (): void => {},
        getKickAuditLog: (): void => {},
        isZooKeepingTurnedOn: true,
    };
}

export function getDbContextFake(): any {
    return {
        userRepo: {
            getById: function (id: string): Promise<IUser> { return null; },
            insert: function (user: IUser): Promise<string> { return null; },
            update: function (user: IUser): Promise<string> { return null; },
            delete: function (): Promise<void> { return null; },
            unbanUser: (userid: string): Promise<any> => { return null; },
            getExpiredBannedUsers: (skip: number, take: number): Promise<any> => { return null; },
            getInactiveUsers: (): Promise<any> => { return null; },
            getUsersWithRoleAfterDays: (): Promise<any> => { return null; },
        },
        cooldownRepo: {
            loadAllCooldowns: (): Promise<ILongCooldown[]> => { return null; },
        },
        roleRepo: {
            insert: function (): Promise<void> { return null; },
            insertManyRoleUsers: function (): Promise<void> { return null; },
            deleteManyRoleUsers: function (): Promise<void> { return null; },
            getUserRoles: function (): Promise<void> { return null; },
            getUserPersonalRole: function (): Promise<void> { return null; },
        },
        arkRepo: {
            getUserArks: function (): Promise<void> { return null; },
            getAllArkKnightNames: function (): Promise<void> { return null; },
        },
        execute: (sql: string): Promise<any> => { return null; },
        destroy: (): void => { return; },
        beginTransaction: (): Promise<any> => { return null; },
        commitTransaction: (): Promise<any> => { return null; },
        rollbackTransaction: (): Promise<any> => { return null; },
        loadMigrations: (): Promise<any> => { return null; },
    }
}

export function getLoggerFake(): any {
    return {
        error: (t: string): void => { },
        log: (t: string): void => { },
    }
}

export function getTaskProcessorFake(): any {
    return {
        addTask: function (task: IProcessTask): void { return null; },
        onTaskProcess(callback: (task: IProcessTask, db: DbContext) => Promise<any>): void { return; }
    }
}

export function getTaskSchedulerFake(options?: {
    lastActivitySecondsAgo: number,
}): any {
    return {
        addTask: function (task: any): void { },
        start: function(): void { },
        stop: function(): Promise<void> { return null; },
        lastActivitySecondsAgo: options?.lastActivitySecondsAgo,
    };
}

export function getBgsClientFake(): any {
    return {};
}

export function getFileHelperFake(): any {
    return {
        writeToFile: (file: string, content: string): void => { return; },
        prepareDirectory: (dir: string): void => { return; },
        directory: (dir: string): Promise<any> => { return null },
        readFile: (file: string): string => { return null },
        deleteFile: (file: string): void => { return; },
        exists: (path: string): boolean => { return false; },
        readFileAsync: () => {},
    }
}

export function getDbContextProviderFake(): any {
    return {
        create: (): Promise<DbContext> => { return null; },
        execute: (db: DbContext): Promise<any> => { return null; },
    }
}

export function getMigratorDbContextProviderFake(): any {
    return {
        create: (): Promise<DbContext> => { return null; },
    }
}

export function getHttpClientHelperFake(): any {
    return {
        get: (request: any): Promise<any> => { return null; },
    };
}

export function getCommandProcFake(): any {
    return {
        parse: (msg: string): ICommand => { return null; },
    };
}

export function getCooldownManagerFake(): any {
    return {
        has: (userid: string, id: any): boolean => { return false },
        add: (cooldown: ICooldown): void => { return; },
    };
}

export function getDiscordClientFake(author?: any): any {
    return {
        client: {
            user: author || {},
        },
        on: (event: string, callback: any): void => { return; },
    };
}

export function getArksFake(): any {
    return {
        isArkNameValid: (text: string): boolean => { return null; },
        myArkKnightByName: (): Promise<any> => { return null },
        myArkKnightsMessage: (): Promise<any> => { return null },
    };
}

export function getAwardsFake(): any {
    return {};
}

export function getSettingsHelperFake(): any {
    return {
        settings: {},
    };
}

export function loadDbSettings(): void {
    const settingsHelper = new SettingsHelper(new FileHelper());
    createPool(settingsHelper.settings.db, getLoggerFake());
}

export function createTestRole(): IRole {
    return {
        name: Helpers.snowflake(),
        roleid: Helpers.snowflake(),
        color: "",
    };
}

export function getDbContextProvider(): DbContextProvider {
    return new DbContextProvider(getLoggerFake());
}

export function getMusicPlayerFake(): any {
    return {
        on: (evt: string, callback: () => void): void => {},
        disconnect: (): void => {},
        play: (): void => {},
    };
}

export function getMusicAutoDisconnectInvoker(): any {
    return {};
}

export class DbTestHarness extends EventEmitter {

    private static _instance: DbTestHarness;

    private _pending = false;
    private _complete = false;

    private _migratorDbContextProvider: MigratorDbContextProvider;
    private _migrator: Migrator;

    private _settingsHelper: SettingsHelper;
    private _arks: Arks;

    private constructor() {
        super();
        const logger = jasmine.createSpyObj<Logger>([
            "log",
            "error",
        ]);
        logger.error.and.callFake(error => console.error(error));
        logger.log.and.callFake(text => console.log(text));
        this._migratorDbContextProvider = new MigratorDbContextProvider(logger);
        const fileHelper = new FileHelper();
        this._settingsHelper = new SettingsHelper(fileHelper);
        this._migrator = new Migrator(this._migratorDbContextProvider, fileHelper, logger);
        this._arks = new Arks(this._migratorDbContextProvider, fileHelper, logger);
    }

    // Delete database then create a new one and initiate it.
    private init(): Promise<void> {
        if (this._complete) {
            return Promise.resolve();
        }            

        if (!this._pending) {
            this._pending = true;
            return new Promise((resolve, reject) => {
                Helpers.inSequence([
                    () => this._migratorDbContextProvider.execute(async (db: DbContext) => {
                        // Drop db and create a new one
                        console.log(`dropping database ${this._settingsHelper.settings.db.database}...`);
                        await db.execute(`drop database ${this._settingsHelper.settings.db.database}`);
                        console.log(`creating a new database ${this._settingsHelper.settings.db.database}...`);
                        await db.execute(`create database ${this._settingsHelper.settings.db.database}`);
                        console.log(`finished initiating database ${this._settingsHelper.settings.db.database}.`);
                    }),
                    () => this._migrator.run(), // Run migrations
                    () => this._arks.importData(), // Import Arknights
                ]).then(() => {
                    this._complete = true;
                    this._pending = false;
                    this.emit("complete");
                    resolve();
                }).catch(err => {
                    this.emit("error", err);
                    reject(err);
                });
            })
        } else {
            // There is already an instance running - await it.
            return new Promise((resolve, reject) => {
                this.on("error", (err) => reject(err));
                this.on("complete", () => resolve());
            });
        }
    }

    private static getInstance(): DbTestHarness {
        if (!DbTestHarness._instance) {
            DbTestHarness._instance = new DbTestHarness();
        }

        return DbTestHarness._instance;
    }

    static reinit(): Promise<void> {
        const instance = DbTestHarness.getInstance();
        return instance.init();
    }
}
