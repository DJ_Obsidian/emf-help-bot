import { CommandValues } from "./command-values";
import { ICooldown } from "./cooldown";
import { CooldownManager } from "./cooldown-manager";
import { DbContext, DbContextProvider } from "./db/context";
import Helpers from "./helpers";
import Logger from "./logger";
import { TaskNames } from "./task-names";
import { TaskProcessor } from "./task-processor";

export default class LongCooldownManager extends CooldownManager {

    constructor(private dbContextProvider: DbContextProvider, private taskProcessor: TaskProcessor, logger: Logger) {
        super(logger);
    }

    add(cooldown: ICooldown): { cooldown: ICooldown, delete: () => void } {
        if (!cooldown.userid) {
            throw new Error("userid is missing");
        }

        const r = super.add(cooldown);

        this.taskProcessor.addTask({
            name: TaskNames.addLongCooldown,
            data: <ICooldown>{
                userid: r.cooldown.userid,
                command: r.cooldown.command,
                seconds: r.cooldown.seconds,
                created: r.cooldown.created,
            },
        });

        return r;
    }

    getSecondsLeft(userid: string, command?: CommandValues): number {
        const cooldown = this.get(userid, command);
        if (cooldown) {
            const seconds = Helpers.date(new Date()).diffSeconds(cooldown.created);
            return cooldown.seconds - seconds;
        } else {
            return 0;
        }
    }

    /**
     * Must be run on Ready.
     */
    async loadCooldowns(): Promise<void> {
        this.logger.log("Loading cooldowns...");
        super.clear();
        let dbContext: DbContext;
        try {
            dbContext = await this.dbContextProvider.create();
            const cooldowns = await dbContext.cooldownRepo.loadAllCooldowns();
            for (const cooldown of cooldowns) {
                let seconds: number = Helpers.date(Date.now()).diffSeconds(cooldown.created);
                if (seconds > 0) {
                    seconds = cooldown.hours * 60 * 60 - seconds;
                    super.add({
                        userid: cooldown.userid,
                        seconds: seconds,
                        created: cooldown.created,
                        command: cooldown.command,
                    });
                }
            }

            this.trace('Cooldowns');
        } finally {
            dbContext.destroy();
        }
    }

    addToDb(cooldown: ICooldown): Promise<void> {
        return this.dbContextProvider.execute(async db => {
            const exists = await db.userRepo.exists(cooldown.userid);
            if (!exists) {
                await db.userRepo.insert({
                    userid: cooldown.userid,
                    usernametag: '',
                });
            }

            await db.cooldownRepo.upsert({
                created: cooldown.created,
                hours: cooldown.seconds / (60 * 60),
                userid: cooldown.userid,
                command: cooldown.command,
            });
        });
    }
}