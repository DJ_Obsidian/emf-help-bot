
export enum AccessLevels {
    Banned = -1,
    Everyone = 0,
    Leader = 1, // Leaders и ниже
    Admin = 2, // Admins и ниже 
    Fuhrer = 3, // Только фюрер
}
