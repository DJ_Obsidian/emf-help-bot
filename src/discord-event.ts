import { Readable } from "stream";
import { AccessLevels } from "./access-levels";
import { DbContext } from "./db/context";

export const activityTypes = ['PLAYING', 'STREAMING', 'LISTENING', 'WATCHING', 'COMPETING'] as const;
export type ActivityType = typeof activityTypes[number];

export interface IDiscordEventOptions {
    memberId: string;
}

export interface IDiscordMessageEventOptions {
    
    authorId: string;

    authorDisplayName: string;

    /**
     * Sends text to the channel where the message arrived from.
     */
    send: (message: unknown, deleteAfterSeconds?: number) => Promise<void>;

    /**
     * Sends text to user's private channel.
     */
    dm: (msg: unknown) => Promise<void>;

    /**
     * Deletes this message, optionally after a specified number of seconds.
     */
    deleteMessage: (afterSeconds?: number) => Promise<unknown>;
}

export interface IDiscordEvent {
    callback: (options: any, db: DbContext) => Promise<unknown>;
}

export interface IDiscordReadyEventOptions {
    setActivity: (text: string) => Promise<unknown>;
}

export interface IDiscordReadyEvent extends IDiscordEvent {
    callback: (options: IDiscordReadyEventOptions, db: DbContext) => Promise<void>;
}

export interface IStreamable {

    /**
     * Attempts to join to the voice channel the user is currently in and stream audio.
     */
    stream: (readable: Readable) => Promise<IStreamSession>;

    /**
     * Returns the id of a channel if it is a voice channel, otherwise null.
     */
    voiceChannelId: string;

    /**
     * Returns the name of a channel if it is a voice channel, otherwise null.
     */
    voiceChannelName: string;

    /**
     * Disconnects the bot from the voice channel.
     */
    disconnect: () => Promise<void>;
}

export interface IDiscordChannelMessageEventOptions extends IDiscordMessageEventOptions, IDiscordEventOptions, IStreamable {
    authorId: string;

    channelId: string;

    serverId: string;

    guildName: string;

    setActivity: (text: string, activityType: ActivityType) => Promise<unknown>;

    /**
     * Gets this message user's highest level within the EMF discord server.
     */
    getHighestUserLevel: () => AccessLevels;

    /**
     * Received message.
     */
    message: string;

    /**
     * Gets user of members mentioned in this message.
     */
    mentions: () => { userid: string, usernametag: string }[];

    /**
     * Gets user's roles within the guild.
     */
    getRoles: () => { roleid: string, name: string}[];

    getRoleByName: (name: string) => { roleid: string, name: string };

    /**
     * Checks if user has the role within the guild.
     */
    hasRole: (roleId: string) => boolean;

    /**
     * Checks if user is admin.
     */
    isAdmin: () => boolean;

    /**
     * Creates new role for the current user.
     */
    createRole: (name: string, position?: number, mentionable?: boolean) => Promise<void>;

    /**
     * Adds a role to the current user.
     */
    addRole: (roleid: string) => Promise<void>;

    /**
     * Removes role from the current user.
     */
    removeRole: (roleid: string) => Promise<void>;

    /**
     * Updates the color of the role belonging to the user. hexColor must be in hex form beginning with a hash.
     */
    changeRoleColor: (roleid: string, hexColor: string) => Promise<void>;

    /**
     * Updates the name of the role belonging to the user.
     */
    changeRoleName: (roleid: string, name: string) => Promise<void>

    randomMember: () => { displayName: string, userid: string },

    member: (userid: string) => { displayName: string, joined: any, joinedTimestamp: number },

    members: (callback: (m: any) => boolean) => Promise<{ userid: string, displayName: string, joined: any, joinedTimestamp: number }[]>;

    /**
     * Банит упомянутого на время (ставит роль юзеру роль banned).
     * Returns userid of the banned.
     */
    ban: () => Promise<string>;

    /**
     * Разбанивает упомянутого (снимает роль юзеру banned).
     * Returns userid of the unbanned.
     */
    unban: () => Promise<string>;

    /**
     * Checks if this user is banned.
     */
    isBanned: (userId: string) => boolean;

    isPermBannable: (userid: string) => boolean;

    banPerm: (reason?: string, days?: number) => Promise<void>;

    unbanPerm: () => Promise<void>;

    /**
     * Kicks this user from the server.
     */
    kick: (userid: string, reason?: string) => Promise<void>;

    kickable: (userid: string) => boolean;

    /**
     * Specify the number of last messages to delete.
     */
    deleteMessages: (limit: number) => Promise<void>;

    getAvatar: (memberId?: string) => Promise<unknown>;

    /**
     * Likes current message.
     */
    like: () => Promise<unknown>;

    /**
     * Tags current message with a red cross indicating en error.
     */
    error: () => Promise<unknown>;

    // Temporary method
    temp: { getAwardMedalsFromMessages: () => Promise<{ medal: string, userid: string, description: string }[]> },
}

export interface IDiscordDMEventOptions extends IDiscordMessageEventOptions {
    authorId: string;
    message: string;

    /**
     * Gets this message user's highest level within the EMF discord server.
     */
    getHighestUserLevel: () => Promise<AccessLevels>;
}

export interface IDiscordMessageEvent extends IDiscordEvent {
    callback: (options: IDiscordChannelMessageEventOptions, db: DbContext) => Promise<void>;
}

export interface IDiscordDMEvent extends IDiscordEvent {
    callback: (options: IDiscordDMEventOptions, db: DbContext) => Promise<void>;
}

export interface IDiscordMemberAddEventOptions extends IDiscordEventOptions {
    userTag: string;
    addRoleToMember: (role: string) => Promise<unknown>;
}

export interface IDiscordMemberRemoveEventOptions extends IDiscordEventOptions {
    memberName: string;
}

export interface IDiscordTypingStartEventOptions extends IDiscordEventOptions {
    channelId: string;
    userNameTag: string;
}

export interface IDiscordVoiceStateUpdateEventOptions extends IDiscordEventOptions {
    channelId: string;
    speaking: boolean;
    memberName: string;
}

export interface IDiscordRoleUpdateEventOptions {
    id: string;
    name: string;
    color: string;
    oldRole?: {id: string, name: string, color: string};
}

export interface IDiscordUserUpdateEventOptions {
    id: string;
    name: string;
    rolesAdded: {id: string, name: string}[],
    rolesRemoved: {id: string, name: string}[],
    oldUser?: {id: string, name: string};
}

export interface IDiscordMemberAddEvent extends IDiscordEvent {
    callback: (options: IDiscordMemberAddEventOptions, db: DbContext) => Promise<void>;
}

export interface IDiscordMemberRemoveEvent extends IDiscordEvent {
    callback: (options: IDiscordMemberRemoveEventOptions, db: DbContext) => Promise<void>;
}

export interface IDiscordTypingStartEvent extends IDiscordEvent {
    callback: (options: IDiscordTypingStartEventOptions) => Promise<unknown>;
}

export interface IDiscordVoiceStateUpdateEvent extends IDiscordEvent {
    callback: (options: IDiscordVoiceStateUpdateEventOptions) => Promise<unknown>;
}

export interface IDiscordRoleUpdateEvent extends IDiscordEvent {
    callback: (options: IDiscordRoleUpdateEventOptions) => Promise<void>;
}

export interface IDisocrdUserUpdateEvent extends IDiscordEvent {
    callback: (options: IDiscordUserUpdateEventOptions) => Promise<void>;
}

export interface IStreamSession {
    pause: () => Promise<void>;
    resume: () => Promise<void>;
    end: () => Promise<void>;
    destroy: () => void;
    on: (event: "finish" | "start" | "error" | "pause" | "resume" | "end" | "disconnect", callback: (...args: unknown[]) => void) => void;
}