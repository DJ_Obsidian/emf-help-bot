import * as Discord from 'discord.js'
import { HttpClientHelper } from './client-helper';
import { embedColor } from './colors';
import FileHelper from './file-helper';
import Helpers from './helpers';
import Logger from './logger';
import { SettingsHelper } from './settings-helper';

export interface IBsgTick {
    _id: string;
    time: string;
    updated_at: string;
    __v: number;
}

export class BgsClient {

    private _tickFile = 'bgs-tick.json';

    constructor(private httpClientHelper: HttpClientHelper, private fileHelper: FileHelper,
        private logger: Logger, private settingsHelper: SettingsHelper) {
    }

    async getBgsTickInfo(): Promise<IBsgTick> {
        const response = await this.httpClientHelper.get<IBsgTick[]>(this.settingsHelper.settings.eliteBgsApiUrl);
        if (response && response.statusCode === 200 && response.data && response.data.length > 0) {
            return response.data[0];
        } else {
            this.logger.log('BGS https status: ' + response.statusCode);
        }

        return null;
    }

    /**
     * Gets fresh tick from the internet if exists.
     */
    async getNewBgsTickInfo(): Promise<IBsgTick> {
        const currentTick = await this.getBgsTickInfo();
        const lastTick = this.getLastTick();
        
        if (currentTick) {
            this.fileHelper.writeToFile(this._tickFile, JSON.stringify(currentTick));
            if (lastTick && Helpers.date(Helpers.date(lastTick.time).datePart()).equals(Helpers.date(currentTick.time).datePart())) {
                return null; // Old tick
            }
        }

        return currentTick;  
    }

    getTickNotificationMessage(tick: IBsgTick): unknown {
        return new Discord.MessageEmbed()
            .setColor(embedColor)
            .setTitle('Оповещение о тике!')
            .setURL(this.settingsHelper.settings.eliteBgsUrl)
            .addField('Последний тик', new Date(tick.time).toLocaleString('ru-RU', { hour12: false }));
    }

    private getLastTick(): IBsgTick {
        try {
            const json = this.fileHelper.readFile(this._tickFile);
            return JSON.parse(json);
        } catch (error) {
            return null;
        }
    }

}