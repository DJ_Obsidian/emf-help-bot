import * as Discord from 'discord.js'
import { embedColor } from './colors';
import { DbContext, DbContextProvider } from "./db/context";
import { IArkKnight, IArkKnightTag, IArkPosition, IArkProfession, IArkTag } from "./db/entities/ark-knight";
import FileHelper from "./file-helper";
import Helpers from "./helpers";
import Logger from './logger';

export interface IArkData {
    arkKnights: IArkKnight[],
    professions: IArkProfession[],
    positions: IArkPosition[],
    tags: IArkTag[],
    arkKnightTags: IArkKnightTag[],
}

interface IUserDrop {
    userid: string;
    dropId: number;
    arkgrd5misses: number;
}

export class Arks {

    private _dir = './arks';
    private _filename = 'character_table.json';

    private _arkNames: string[];

    lowestGrade = 2;
    highestGrade = 5;

    get total(): number {
        return this._arkNames.length;
    }

    constructor(private dbContextProvider: DbContextProvider, private fileHelper: FileHelper, private logger: Logger) {
    }

    async load(db: DbContext): Promise<void> {
        this._arkNames = await db.arkRepo.getAllArkKnightNames(this.lowestGrade);
    }

    getRandomGrade(grade5misses: number): number {
        const yourLuck = Math.abs(Helpers.getRndInteger(0, 10000) - 5000) * 2;
        let charGrade = this.lowestGrade;

        const dropRates = [
            { grade: 5, prop: 2 },
            { grade: 4, prop: 8 },
            { grade: 3, prop: 50 },
            { grade: 2, prop: 40 },
        ];

        if (grade5misses > 50) {
            dropRates.find(x => x.grade === 5).prop += (grade5misses - 50) * 2;
        }

        let lower = 0;
        for (let i = 0; i < dropRates.length; i++) {
            const previous = dropRates[i - 1];
            const current = dropRates[i];
            lower =+ i > 0 ? previous.prop * 100 : 0;
            const upper = current.prop * 100;
            if (lower < yourLuck && yourLuck <= lower + upper) {
                charGrade = dropRates[i].grade;
                break;
            }
        }

        return charGrade;
    }

    async rollMessage(userid: string, userDisplayName: string, db: DbContext): Promise<{message: unknown, drop: IUserDrop, arkName: string}> {
        let user = await db.userRepo.getById(userid);
        if (!user) {
            user = await db.userRepo.insert({
                userid: userid,
                usernametag: userDisplayName,
            });
        }

        const charGrade = this.getRandomGrade(user.arkgrd5misses);

        const possibleDrops = await db.arkRepo.getArkKnightIdsByGrade(charGrade);

        const dropId = possibleDrops[Helpers.getRndInteger(0, possibleDrops.length - 1)];
        const drop = await db.arkRepo.getArkKnight(dropId);
        if (!drop) {
            throw new Error(`Could not get ark character from DB with id ${dropId}. Character grade:${charGrade}`);
        }

        const message = this.getArkMessage(drop);
        message.setFooter(`Requested by ${userDisplayName}`);

        return {
            message: message,
            arkName: drop.name,
            drop: <IUserDrop>{
                userid: userid,
                dropId: dropId,
                arkgrd5misses: charGrade === this.highestGrade ? 0 : (user.arkgrd5misses ? user.arkgrd5misses + 1 : 1),
            },
        };
    }

    async viewArkMessageName(arkName: string, db: DbContext): Promise<unknown> {
        const ark = await db.arkRepo.getArkKnightByName(arkName);
        if (ark) {
            return this.getArkMessage(ark);
        } else {
            return null;
        }
    }

    getArkMessage(ark: IArkKnight): Discord.MessageEmbed {
        const message = new Discord.MessageEmbed()
            .setColor(embedColor)
            .setImage(`https://aceship.github.io/AN-EN-Tags/img/characters/${ark.characterid}_1.png`)
            .setURL(`https://aceship.github.io/AN-EN-Tags/akhrchars.html?opname=${encodeURI(ark.name)}`)
            .setTitle(ark.name)
            .setDescription(ark.rarity < this.highestGrade ? ":star:".repeat(ark.rarity + 1) : ":star2:".repeat(this.highestGrade + 1));
        
        message.addField('Description', ark.itemusage + '\n' + ark.itemdesc);
        message.addField('Position', ark.position.name, true);

        if (ark.taglist && ark.taglist.length > 0) {
            message.addField('Tags', ark.taglist.map(t => t.name).join(", "), true)
        }
        if (ark.description) {
            message.addField('Traits', ark.description.replace(/<[^>]*>/g, ""));
        }

        switch(ark.profession.name) {
            case "TANK":
                message.setThumbnail("https://aceship.github.io/AN-EN-Tags/img/classes/class_defender.png");
                break;
            case "SPECIAL":
                message.setThumbnail("https://aceship.github.io/AN-EN-Tags/img/classes/class_specialist.png");
                break;
            case "MEDIC":
                message.setThumbnail("https://aceship.github.io/AN-EN-Tags/img/classes/class_medic.png");
                break;
            case "SUPPORT":
                message.setThumbnail("https://aceship.github.io/AN-EN-Tags/img/classes/class_supporter.png");
                break;
            case "CASTER":
                message.setThumbnail("https://aceship.github.io/AN-EN-Tags/img/classes/class_caster.png");
                break;
            case "SNIPER":
                message.setThumbnail("https://aceship.github.io/AN-EN-Tags/img/classes/class_sniper.png");
                break;
            case "WARRIOR":
                message.setThumbnail("https://aceship.github.io/AN-EN-Tags/img/classes/class_guard.png");
                break;
            case "PIONEER":
                message.setThumbnail("https://aceship.github.io/AN-EN-Tags/img/classes/class_vanguard.png");
                break;
            default:
                this.logger.error(`Profession ${ark.profession.name} is unexpected.`);
                break;
        }

        return message;
    }

    async updateDropInfo(info: IUserDrop): Promise<void> {
        await this.dbContextProvider.execute(async db => {
            await db.userRepo.insert({
                userid: info.userid,
                usernametag: '',
            });

            let arkUser = await db.arkRepo.getArkUser(info.userid, info.dropId);
            if (!arkUser) {
                arkUser = {
                    ark_knightid: info.dropId,
                    userid: info.userid,
                    times: 0,
                };
                await db.arkRepo.insertArkUser(arkUser);
            }
            
            await db.arkRepo.updateUsers5grdMissCount(info.userid, info.arkgrd5misses);
            await db.arkRepo.updateArkUserTimes(info.userid, info.dropId, arkUser.times + 1);
        });
    }

    async myArkKnightsMessage(userid: string, userDisplayName: string, db: DbContext): Promise<unknown> {

        const arkUsers = await db.arkRepo.getUserArks(userid);

        const levels = {
            5: [],
            4: [],
            3: [],
            2: [],
            1: [],
            0: [],
        };

        for (const arkUser of Helpers.sortDescending(arkUsers, x => x.ark.rarity)) {
            if (arkUser.ark.rarity >= this.lowestGrade && arkUser.ark.rarity <= this.highestGrade) {
                const item = `${arkUser.ark.name} x${arkUser.times}`;
                levels[arkUser.ark.rarity].push(item);
            }
        }

        const messages = [];
        const embed = new Discord.MessageEmbed();
        let description = '';
        
        for (const [level, items] of Object.entries(levels)) {
            if (items.length > 0) {
                const l = parseInt(level);
                const star = l === this.highestGrade ? 'star2' : 'star';
                description += `:${star}:`.repeat(l + 1) + '\n';
                description += `${items.join('\n')}\n\n`;
            }
        }

        embed.setTitle(`${userDisplayName}'s collection ${arkUsers.length}/${this.total} (${(arkUsers.length/this.total * 100).toFixed()}%)`)
            .setDescription(description)
            .setFooter(`Используй команду !майарк <имя> чтоб посмотреть на персонажа.`);
        messages.push(embed);
        
        return messages;
    }

    isArkNameValid(name: string): boolean {
        return this._arkNames.indexOf(name) !== -1;
    }

    async myArkKnightByName(userid: string, arkName: string, db: DbContext): Promise<unknown> {
        const ark = await db.arkRepo.getUserArkKnightByName(userid, arkName);
        if (!ark) {
            return null;
        }

        return this.getArkMessage(ark);
    }

    async importData(): Promise<void> {
        if (this.fileHelper.exists(`${this._dir}/${this._filename}`)) {
            try {
                const allArks = await this.dbContextProvider.execute(db => db.arkRepo.getAll());
                const data = await this.parseFile(allArks);
                await this.saveToDB(data);

                if (Helpers.isProduction) {
                    this.fileHelper.deleteFile(`${this._dir}/${this._filename}`);
                }

                this.logger.log('Finished importing file.');
            } catch (error) {
                throw new Error(`Error importing ark knights. ${error}`);
            }
        } else {
            this.logger.log('No ark knights file character_table.json found.')
            this.logger.log('Did not import file.');
        }
    }

    /**
     * Saves data into DB.
     */
    async saveToDB(data: IArkData): Promise<void> {
        const db = await this.dbContextProvider.create();
        try {
            for (const chunk of Helpers.splitArray(data.professions, 10)) {
                this.logger.log(`Inserting ${chunk.length} of Professions...`);
                await db.arkRepo.insertProfessions(chunk);
            }
            for (const chunk of Helpers.splitArray(data.positions, 10)) {
                this.logger.log(`Inserting ${chunk.length} of Positions...`);
                await db.arkRepo.insertPositions(chunk);
            }
            for (const chunk of Helpers.splitArray(data.tags, 10)) {
                this.logger.log(`Inserting ${chunk.length} of Tags...`);
                await db.arkRepo.insertTags(chunk);
            }
            // Filter out only the characters
            for (const chunk of Helpers.splitArray(data.arkKnights.filter(x => /^char/.test(x.characterid)), 100)) {
                this.logger.log(`Inserting ${chunk.length} of ArkKnights...`);
                await db.arkRepo.insertArkKnights(chunk);
            }
            for (const chunk of Helpers.splitArray(data.arkKnightTags, 100)) {
                this.logger.log(`Inserting ${chunk.length} of ArkKnightTags...`);
                await db.arkRepo.insertArkKnightTags(chunk);
            }
            await db.commitTransaction();
            this.logger.log("Data saved.");
        } catch (error) {
            await db.rollbackTransaction();
            throw new Error(`Failed to import Ark data: ${error}`);
        } finally {
            db.destroy();
        }
    }

    /**
     * Parses character file.
     */
    async parseFile(existingArks: IArkKnight[]): Promise<IArkData> {
        const file = await this.fileHelper.readFileAsync(`${this._dir}/${this._filename}`);
        const characters = JSON.parse(file);
        
        const professions = new Set<string>();
        const positions = new Set<string>();
        const tags = new Set<string>();

        for (const value of Object.values(characters)) {
            const profession = value['profession'];
            if (profession) {
                professions.add(profession);
            }

            const position = value['position'];
            if (position) {
                positions.add(position);
            }

            const tagList = value['tagList'];
            if (tagList) {
                (<string[]>tagList).forEach(tag => tags.add(tag));
            }
        }

        const profs: IArkProfession[] = [];
        let i = 0;
        professions.forEach(p => profs.push({ id: ++i, name: p }));

        i = 0;
        const poss: IArkPosition[] = [];
        positions.forEach(p => poss.push({ id: ++i, name: p }));

        i = 0;
        const tgs: IArkTag[] = [];
        tags.forEach(t => tgs.push({ id: ++i, name: t }));

        const arkKnightTags: IArkKnightTag[] = [];

        i = existingArks.length > 0 ? existingArks[existingArks.length - 1].id : 0;
        const arks: IArkKnight[] = [];
        for (const [key, value] of Object.entries(characters)) {
            if (existingArks.findIndex(x => x.characterid === key) > -1) {
                continue;
            } else {
                this.logger.log(`Found new character ${key}`);
            }

            const position = value['position'];
            if (!position) {
                this.logger.log(`${key} has no position?!`);
                continue;
            }
            const profession = value['profession'];
            if (!profession) {
                this.logger.log(`${key} has no profession?!`);
                continue;
            }

            arks.push({
                id: ++i,
                characterid: key,
                name: value['name'],
                description: value['description'],
                itemdesc: value['itemDesc'],
                itemusage: value['itemUsage'],
                rarity: parseInt(value['rarity']),
                positionid: position ? poss.find(x => x.name === position).id : undefined,
                professionid: profession ? profs.find(x => x.name === profession).id : undefined,
            });

            const tagList = <string[]>value['tagList'];
            if (tagList) {
                tagList.map(tag => arkKnightTags.push({
                    ark_knightid: i,
                    ark_tagid: tgs.find(x => x.name === tag).id,
                }));
            } else {
                this.logger.log(`${key} has no tags?!`);
            }
        }

        return {
            arkKnights: arks,
            professions: profs,
            positions: poss,
            tags: tgs,
            arkKnightTags: arkKnightTags
        };
    }
}
