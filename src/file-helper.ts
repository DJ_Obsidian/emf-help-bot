import * as fs from "fs";

export default class FileHelper {

    readFile(filename: string): string {
        return fs.readFileSync(filename, 'utf8');
    }

    readFileAsync(filename: string): Promise<string> {
        return new Promise((resolve, reject) => {
            fs.readFile(filename, { encoding: 'utf8' }, (err, data) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(data);
                }
            });
        });
    }

    writeToFile(filename: string, content: string): void {
        return fs.writeFileSync(filename, content, {
            encoding: 'utf8',
        });
    }

    writeToFileAsync(filename: string, content: string): Promise<void> {
        return new Promise((resolve, reject) => {
            fs.writeFile(filename, content, {
                encoding: 'utf8',
            }, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    }

    deleteFile(filename: string): void {
        fs.unlinkSync(filename);
    }

    appendToFileAsync(filename: string, content: string): Promise<void> {
        return new Promise((resolve, reject) => {
            fs.appendFile(
                filename,
                content, 
                { encoding: 'utf8' },
                err => {
                    if (err) {
                        reject(err);
                    } else {
                        resolve();
                    }
                });
        });
    }

    appendToFile(filename: string, content: string): void {
        fs.appendFileSync(filename, content, { encoding: 'utf8' });
    }

    directory(path: string): Promise<{ files: string[] }> {
        return new Promise((resolve, reject) => {
            fs.readdir(path, (err, files) => {
                if (err) {
                    reject(err);
                } else {
                    resolve({ files: files });
                }
            });
        });
    }

    exists(path: string): boolean {
        return fs.existsSync(path);
    }

    prepareDirectory(dir: string): void {
        if (!fs.existsSync(dir)) {
            fs.mkdirSync(dir);
        }
    }
}