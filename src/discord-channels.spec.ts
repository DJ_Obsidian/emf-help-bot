import { ChannelNames } from "./channel-names";
import { DC } from "./discord-channels";

describe("DiscordChannels", () => {

    describe("getId", () => {

        beforeAll(() => {
            DC.channels([
                { id: ChannelNames.general, channelid: "1", description: "" },
                { id: ChannelNames.nsfw, channelid: "2", description: "" },
                { id: ChannelNames.serverInfo, channelid: "3", description: "" },
                { id: ChannelNames.serverRules, channelid: "4", description: "" },
                { id: ChannelNames.bgsInfo, channelid: "5", description: "" },
                { id: ChannelNames.admin, channelid: "6", description: "" },
                { id: ChannelNames.awards, channelid: "7", description: "" },
                { id: ChannelNames.reception, channelid: "8", description: "" },
                { id: ChannelNames.music, channelid: "9", description: "" },
            ]);
        });
        [
            { name: ChannelNames.general, expected: "1" },
            { name: ChannelNames.nsfw, expected: "2" },
            { name: ChannelNames.serverInfo, expected: "3" },
            { name: ChannelNames.serverRules, expected: "4" },
            { name: ChannelNames.bgsInfo, expected: "5" },
        ].forEach(x => {
            it(`should return id ${x.expected} for ${x.name}`, () => {
                expect(DC.getId(x.name)).toEqual(x.expected);
            });
        });

    });

    describe("channels", () => {

        it("should not throw if all channel id's present", () => {
            expect(() => DC.channels([
                { id: ChannelNames.admin, channelid: "", description: "" },
                { id: ChannelNames.awards, channelid: "", description: "" },
                { id: ChannelNames.bgsInfo, channelid: "", description: "" },
                { id: ChannelNames.general, channelid: "", description: "" },
                { id: ChannelNames.nsfw, channelid: "", description: "" },
                { id: ChannelNames.serverInfo, channelid: "", description: "" },
                { id: ChannelNames.serverRules, channelid: "", description: "" },
                { id: ChannelNames.reception, channelid: "", description: "" },
                { id: ChannelNames.music, channelid: "", description: "" },
            ])).not.toThrow();
        });

        it("should throw if channel id does not exist", () => {
            expect(() => DC.channels([
                { id: ChannelNames.admin, channelid: "", description: "" },
                { id: ChannelNames.awards, channelid: "", description: "" },
                { id: ChannelNames.bgsInfo, channelid: "", description: "" },
                { id: ChannelNames.general, channelid: "", description: "" },
                { id: ChannelNames.nsfw, channelid: "", description: "" },
                { id: ChannelNames.serverInfo, channelid: "", description: "" },
                { id: ChannelNames.serverRules, channelid: "", description: "" },
            ])).toThrow(new Error("Channel reception has not been loaded."));
        });

    });

});