import * as Discord from 'discord.js'
import { EventEmitter } from "events";
import { embedColor } from '../colors';
import Logger from "../logger";
import { IRssFeed } from "./rss-feed";
import { RssFeedFilter } from "./rss-feed-filter";
import { IRssFeedItem } from "./rss-feed-item";
import { RssFeedParser } from "./rss-feed-parser";
import { IRssFeedSub } from "./rss-feed-sub";

export default class RssFeedGetter extends EventEmitter {

    private readonly subs: IRssFeedSub[] = [];

    constructor(private filter: RssFeedFilter, private logger: Logger) {
        super();
    }

    add(sub: IRssFeedSub): void {
        sub.name = sub.name.toLowerCase();
        if (this.subs.find(s => s.name === sub.name)) {
            throw new Error(`Sub with ${sub.name} already exists.`);
        }

        this.subs.push(sub);
        this.on(sub.name, (feed: IRssFeed) => sub.on(feed, sub));
    }

    async getFeeds(): Promise<void> {
        for (const sub of this.subs) {
            await this.getFeed(sub);
        }
    }

    static composeMessage(item: IRssFeedItem, feed: IRssFeed): unknown {
        let message = new Discord.MessageEmbed()
            .setColor(embedColor)
            .setTitle(item.title)
            .setThumbnail(feed.logo)
            .setURL(item.link);

        if (item.image) {
            message = message.setImage(item.image);
        }

        return message;
    }

    private async getFeed(sub: IRssFeedSub): Promise<void> {
        const parser: RssFeedParser = sub.parser ? sub.parser : new RssFeedParser();
        const feed = await parser.parse(sub);
        if (!feed) {
            return;
        }

        const filtered = await this.filter.sieve(feed.items, sub);

        if (filtered.length > 0) {
            this.logger.log(`New feed from ${sub.name}:\n${filtered.map(f => `  (${f.id})${f.date}-${f.title}`).join('\n')}`);

            filtered.forEach(f => f.confirm = () => this.filter.setItem(f, sub));

            const emitted: IRssFeed = {
                title: feed.title,
                logo: feed.logo,
                items: filtered,
            };
            this.emit(sub.name, emitted);
        } else {
            this.logger.log(`No new feed from ${sub.name}`);
        }
    }
}
