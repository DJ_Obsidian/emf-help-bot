

export interface IRssFeedSettings {
    everyMinutes: number;
    roleid: string;
    feeds: {
        url: string;
        name: string;
        channelId: string;
        category: string;
        n?: number;
    }[]
}
