import FileHelper from "../file-helper";
import { IRssFeedItem } from "./rss-feed-item";
import * as path from "path";
import Logger from "../logger";
import Helpers from "../helpers";
import { IRssFeedSub } from "./rss-feed-sub";

export class RssFeedFilter {

    dir = './rss';

    constructor(private fileHelper: FileHelper, private logger: Logger) {
    }

    async sieve(items: IRssFeedItem[], sub: IRssFeedSub): Promise<IRssFeedItem[]> {
        const cache = await this.loadRssFromFile(sub.name);

        let sorted = Helpers.sortDescending(items, x => new Date(x.date));
        if (sub.category) {
            sub.category = sub.category.toLowerCase();
            sorted = sorted.filter(x => x.category === sub.category);
        }
        
        const picked: IRssFeedItem[] = [];
        // Pick item not present in cache
        let i = 0;
        const n = sub.n && sub.n > 0 ? sub.n : 1;
        for (const item of sorted) {
            if (i++ >= n) {
                break;
            }

            if (!cache || cache.items.findIndex(f => f.id === item.id) === -1) {
                // Item is new
                picked.push(item);
            }
        }

        return picked;
    }

    async setItem(item: IRssFeedItem, sub: IRssFeedSub): Promise<void> {
        let cache = await this.loadRssFromFile(sub.name);
        if (!cache) {
            cache = {
                items: [],
            };
        }

        if (cache.items.length > 25) {
            // trim cache
            cache.items.splice(0, 1);
        }

        delete item["confirm"];
        cache.items.push(item);

        try {
            await this.fileHelper.writeToFileAsync(this.getFilePath(sub.name), JSON.stringify(cache));
        } catch (error) {
            this.logger.error(`Failed to save rss feed item ${item.title} (${item.id}) in cache`);
        }
    }

    private getFilePath(name: string): string {
        return path.join(this.dir, `${name}.json`);
    }
    
    private async loadRssFromFile(name: string): Promise<{items: IRssFeedItem[]}> {
        this.fileHelper.prepareDirectory(this.dir);
        let rss: { items: IRssFeedItem[] };
        try {
            if (this.fileHelper.exists(this.getFilePath(name))) {
                const rssFile = await this.fileHelper.readFileAsync(this.getFilePath(name));
                rss = JSON.parse(rssFile);
            }
        } catch (error) {
            this.logger.error(error, "Error when reading rss.json file.");
        }

        return rss;
    }
}
