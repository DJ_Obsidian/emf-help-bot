import { IRssFeed } from "./rss-feed";
import { RssFeedParser } from "./rss-feed-parser";

export interface IRssFeedSub {
    name: string;
    channelId: string;
    url: string;
    category: string;
    parser?: RssFeedParser;
    n?: number;
    on: (feed: IRssFeed, sub: IRssFeedSub) => void,
}
