import * as XmlParser from "fast-xml-parser";
// import HtmlParser from 'node-html-parser';
import * as he from "he";
import { HttpClientHelper } from "../client-helper";
import { IRssFeed } from "./rss-feed";
import { IRssFeedItem } from "./rss-feed-item";
import { IRssFeedSub } from "./rss-feed-sub";

export class RssFeedParser {

    private readonly options = {
        attributeNamePrefix : "@_",
        attrNodeName: "attr", //default is 'false'
        textNodeName : "#text",
        ignoreAttributes : false,
        ignoreNameSpace : false,
        allowBooleanAttributes : false,
        parseNodeValue : true,
        parseAttributeValue : false,
        trimValues: true,
        cdataTagName: "__cdata", //default is 'false'
        cdataPositionChar: "\\c",
        parseTrueNumberOnly: false,
        arrayMode: false, //"strict"
        attrValueProcessor: val => he.decode(val, {isAttributeValue: true}),//default is a=>a
        tagValueProcessor : val => he.decode(val), //default is a=>a
        stopNodes: ["parse-me-as-string"]
    };

    private parseFeedEntry(item: any): IRssFeedItem {
        const media = item["media:thumbnail"];
        let image: string;
        if (media && media.attr) {
            image = media.attr["@_url"];
        }

        let link: string;
        if (item.link && item.link.attr) {
            link = item.link.attr["@_href"];
        }

        let content: string;
        if (item.content) {
            content = item.content["#text"];
        }

        let category: string;
        if (item.category && item.category.attr) {
            category = item.category.attr["@_term"];
        }

        return {
            id: item.id,
            title: item.title,
            content: content,
            date: item.published,
            link: link,
            image: image,
            category: category,
        };
    }

    async parse(sub: IRssFeedSub): Promise<IRssFeed> {
        const http = new HttpClientHelper();
        const response = await http.download(sub.url);
        if (response.statusCode !== 200) {
            throw new Error(`Failed to download RSS for ${sub.name}: ${response.statusCode.toString()}`);
        }

        const result = XmlParser.validate(response.data);

        let jsonObj;
        if(result === true) {
            jsonObj = XmlParser.parse(response.data, this.options);
        } else {
            throw new Error(`Error parsing RSS xml for ${sub.name}: ${JSON.stringify(result)}`);
        }

        return {
            title: jsonObj.feed.title,
            logo: jsonObj.feed.logo,
            items: jsonObj.feed.entry.map(item => this.parseFeedEntry(item)),
        };
    }
}
