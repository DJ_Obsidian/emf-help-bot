

export interface IRssFeedItem {
    id: string;
    title: string;
    content: string;
    date: string;
    link: string;
    image: string;
    category: string;
    confirm?: () => Promise<void>;
}
