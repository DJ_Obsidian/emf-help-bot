import FileHelper from "../file-helper";
import Logger from "../logger";
import { RssFeedFilter } from "./rss-feed-filter";


describe("RssFeedFilter", () => {

    describe("sieve", () => {
        let rssFeedFilter: RssFeedFilter;        
        const fileHelperSpy = jasmine.createSpyObj(
            "FileHelper",
            [
                "prepareDirectory",
                "exists",
                "readFileAsync",
            ]) as jasmine.SpyObj<FileHelper>;
        const logger: Logger = jasmine.createSpyObj("Logger", ["error"]);
        beforeEach(() => {
            rssFeedFilter = new RssFeedFilter(fileHelperSpy, logger);
        });

        [
            {n: 1, new: [{id: "1", date: new Date(2020, 1, 1).toString()}], cached: [{id: "2"}], expected: [{id: "1"}]},
            {n: 1, new: [{id: "1", date: new Date(2020, 1, 1).toString()}], cached: [{id: "1"}], expected: []},
            {n: 1, new: [
                {id: "2", date: new Date(2020, 1, 1).toString()},
                {id: "1", date: new Date(2020, 1, 2).toString()},
            ], cached: undefined, expected: [{id: "1"}]},
            {n: 2, new: [
                {id: "2", date: new Date(2020, 1, 1).toString()},
                {id: "1", date: new Date(2020, 1, 2).toString()},
            ], cached: [], expected: [{id: "1"},{id: "2"}]},
        ].forEach(t => {
            it(`should get ${JSON.stringify(t.expected)} items`, async () => {
                fileHelperSpy.prepareDirectory.and.callThrough();
                fileHelperSpy.exists.and.callThrough().and.returnValue(true);
                fileHelperSpy.readFileAsync.and.callThrough().and.resolveTo(
                    JSON.stringify(t.cached ? {
                        items: t.cached,
                    } : undefined)
                );
                const actual = await rssFeedFilter.sieve(<any[]>t.new, <any>{name: "rss", n: t.n});
    
                expect(actual.length).toEqual(t.expected.length);
                actual.forEach(item => expect(t.expected.find(x => x.id === item.id)).toBeTruthy());
            });
        });
    });
});
