import { IRssFeedItem } from "./rss-feed-item";


export interface IRssFeed {
    logo: string;
    title: string;
    items: IRssFeedItem[];
}