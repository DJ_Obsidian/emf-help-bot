import Helpers from "./helpers";


const messages = {

    greeting: function (memberId: string, serverInfoId: string, serverRulesId: string): string {
        return `<@${memberId}> , добро пожаловать в Elitach Multipurpose Forces (EMF), сквадрон и сообщество по элите! ` +
        `Мы занимаемся поддержкой нашей минорки в игре, Orange Society, а так же можем помочь с вопросами по элите. ` +
        `При входе на сервер обратиться к ORANGE LEADERS! Обязательно ознакомиться с правилами сервера в <#${serverInfoId}> и <#${serverRulesId}>` +
        `\n**Ник на сервере должен соответствовать нику в игре!**`;
    },

    insufficientPermissions: function (): string {
        return Helpers.getRandomPhrase(["Не хватает силенок", "У тебя нет прав на это", "Чекни свои привелегии"]);
    }
};

export default messages;