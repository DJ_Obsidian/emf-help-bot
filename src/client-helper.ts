import axios from 'axios';

interface IHttpResult<T> {
    statusCode?: number;
    data?: T;
}

export class HttpClientHelper {

    async get<T>(url: string): Promise<IHttpResult<T>> {
        const response = await axios.get<T>(url);
        return {
            statusCode: response.status,
            data: response.data,
        };
    }

    async download(url: string): Promise<IHttpResult<string>> {
        const response = await axios.request({
            url: url,
        });
        return {
            statusCode: response.status,
            data: response.data ? response.data as string : undefined,
        };
    }
}