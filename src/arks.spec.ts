import { Arks, IArkData } from "./arks";
import { getDbContextFake, getDbContextProviderFake, getFileHelperFake, getLoggerFake } from "./common-spec/common.spec";
import { IArkUser } from "./db/entities/ark-knight";
import FileHelper from "./file-helper";


describe("Arks", () => {

    describe("parseFile", () => {

        describe("using actual file", () => {
            let actualData: IArkData;

            beforeAll(async done => {
                const arks = new Arks(getDbContextProviderFake(), new FileHelper(), getLoggerFake());
    
                actualData = await arks.parseFile([]);
    
                done();
            });

            it("should extract arks", () => {
                expect(actualData.arkKnights.length).toBeGreaterThanOrEqual(1);
            });
    
            it("each ark should have a profession", () => {
                actualData.arkKnights.forEach(ark => expect(ark.professionid).toBeGreaterThan(0));
            });
    
            it("each ark should have a position", () => {
                actualData.arkKnights.forEach(ark => expect(ark.positionid).toBeGreaterThan(0));
            });
    
            it("should export professions", () => {
                expect(actualData.professions.length).toBeGreaterThan(0);
                actualData.professions.forEach(p => expect(parseInt(p.name)).toBeNaN());
            });
    
            it("should export positions", () => {
                expect(actualData.positions.length).toBeGreaterThan(0);
            });
    
            it("should export ark tags", () => {
                expect(actualData.arkKnightTags.length).toBeGreaterThan(0);
            });
        });

        describe("when there are existing arks in DB", () => {
            let actualData: IArkData;

            const fileHelper = getFileHelperFake();

            beforeAll(async done => {
                const arks = new Arks(getDbContextProviderFake(), fileHelper, getLoggerFake());

                spyOn(fileHelper, 'readFileAsync').and.callThrough().and.resolveTo(
                    JSON.stringify({
                        ark1: {
                            name: "ark 1",
                            profession: "pf 1",
                            position: "pos 1",
                            description: 'a',
                            itemUsage: 'b',
                            rarity: '5',
                        },
                        ark3: {
                            name: "ark 3",
                            profession: "pf 2",
                            position: "pos 1",
                            description: 'a',
                            itemUsage: 'b',
                            rarity: '5',
                        },
                        ark2: {
                            name: "ark 2",
                            profession: "pf 3",
                            position: "pos 1",
                            description: 'a',
                            itemUsage: 'b',
                            rarity: '5',
                        },
                        ark4: {
                            name: "ark 4",
                            profession: "pf 4",
                            position: "pos 1",
                            description: 'a',
                            itemUsage: 'b',
                            rarity: '5',
                        },
                    })
                )
                
                actualData = await arks.parseFile([
                    <any>{
                        characterid: 'ark1',
                        id: 1,
                    },
                    <any>{
                        characterid: 'ark2',
                        id: 2,
                    },
                ]);
    
                done();
            });

            it("should extract arks", () => {
                expect(actualData.arkKnights.length).toEqual(2);
                expect(actualData.arkKnights.findIndex(x => x.characterid === "ark3" && x.id === 3) > -1).toBeTrue();
                expect(actualData.arkKnights.findIndex(x => x.characterid === "ark4" && x.id === 4) > -1).toBeTrue();
            });
    
            it("each ark should have a profession", () => {
                actualData.arkKnights.forEach(ark => expect(ark.professionid).toBeGreaterThan(0));
            });
    
            it("each ark should have a position", () => {
                actualData.arkKnights.forEach(ark => expect(ark.positionid).toBeGreaterThan(0));
            });
    
            it("should export professions", () => {
                expect(actualData.professions.length).toBeGreaterThan(0);
                actualData.professions.forEach(p => expect(parseInt(p.name)).toBeNaN());
            });
    
            it("should export positions", () => {
                expect(actualData.positions.length).toBeGreaterThan(0);
            });
        });

    });

    describe("getRandomGrade", () => {
        const arks = new Arks(<any>{}, <any>{}, <any>{});

        const total = 5000;
        [0, 50, 60, 70, 80, 90, 100].forEach(misses => {
            let grade6misses = misses;
            describe(`test for ${total} drops`, () => {
                let i = 0;
                
                const drops = {
                    5: 0,
                    4: 0,
                    3: 0,
                    2: 0,
                };
                while (i++ < total) {
                    const grade = arks.getRandomGrade(grade6misses);
                    drops[grade]++;
                }
    
                for (const [grade, count] of Object.entries(drops)) {
                    it(`grade ${grade} result ${count} - ${Math.round((count/total * 100))}% when ${grade6misses} misses`, () => {
                        if (misses > 50) {
                            expect(count).toBeGreaterThanOrEqual(0);
                        } else {
                            expect(count).toBeGreaterThan(0);
                        }
                    });
                }
            });
        });
    });

    describe("myArkKnightsMessage", () => {

        const db = getDbContextFake();

        const arkUsers: IArkUser[] = [
            {
                ark_knightid: 1,
                times: 100,
                userid: '1',
                ark: {
                    name: "ark1",
                    rarity: 2,
                    characterid: '1',
                    id: 1,
                    positionid: 1,
                    professionid: 1,
                },
            },
            {
                ark_knightid: 2,
                times: 100,
                userid: '1',
                ark: {
                    name: "ark2",
                    rarity: 3,
                    characterid: '2',
                    id: 2,
                    positionid: 1,
                    professionid: 1,
                },
            },
            {
                ark_knightid: 3,
                times: 100,
                userid: '1',
                ark: {
                    name: "ark3",
                    rarity: 4,
                    characterid: '1',
                    id: 3,
                    positionid: 1,
                    professionid: 1,
                },
            },
            {
                ark_knightid: 4,
                times: 100,
                userid: '1',
                ark: {
                    name: "ark4",
                    rarity: 5,
                    characterid: '1',
                    id: 4,
                    positionid: 1,
                    professionid: 1,
                },
            },
        ];

        let actual: any;

        beforeAll(async done => {
            spyOn(db.arkRepo, 'getUserArks').and.callThrough().and.resolveTo(arkUsers);
            spyOn(db.arkRepo, 'getAllArkKnightNames').and.callThrough().and.resolveTo(['ark1','ark2','ark3','ark4']);

            const arks = new Arks(getDbContextProviderFake(), new FileHelper(), getLoggerFake());
            await arks.load(db);

            // Act
            actual = await arks.myArkKnightsMessage("1", "name", db);

            done();
        });

        it("should return message", () => {
            expect(actual.length).toBeTruthy();
        });
    });
});