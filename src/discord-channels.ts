import { ChannelNames } from "./channel-names";
import { IChannel } from "./db/entities/channel";

let channels: IChannel[] = [];

export const DC = {
    channels: (c: IChannel[]): void => {
        // Check that all Channel id's have been loaded.
        const channelIds = Object.keys(ChannelNames).filter(x => !isNaN(Number(x)))
            .map<ChannelNames>(x => parseInt(x)).filter(x => x !== -1);

        for (const name of channelIds) {
            if (c.findIndex(x => x.id === name) === -1) {
                throw new Error(`Channel ${ChannelNames[name]} has not been loaded.`);
            }
        }

        channels = c;
    },

    getId: (name: ChannelNames): string => {
        const c = channels.find(c => c.id === name);
        if (!c) {
            return "";
        }

        return c.channelid;
    },

    getName: (id: string): ChannelNames => {
        const c = channels.find(c => c.channelid === id);
        if (!c) {
            return null;
        } else {
            return  c.id;
        }
    }
}