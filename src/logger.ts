import FileHelper from "./file-helper";
import * as path from "path";
import Helpers from "./helpers";
import { SettingsHelper } from "./settings-helper";
import * as logger from 'simple-node-logger';

interface IL {
    setLevel: () => void;
    trace: () => void;
    error: () => void;
}

export default class Logger {
    
    dir = './logs';

    private opts = {
        logDirectory: this.dir,
        fileNamePattern: 'botlog-<date>.log',
        timestampFormat:'YYYY-MM-DD HH:mm:ss.SSS',
        dateFormat:'YYYY.MM.DD'
    };
    
    private _log: any;
    
    constructor(private fileHelper: FileHelper, private settingsHelper: SettingsHelper, l: IL = undefined) {
        this._log = !l ? logger.createRollingFileLogger(this.opts) : l;
        this._log.setLevel('all');
        this.fileHelper.prepareDirectory(this.dir);
    }

    log(text: string): void {
        console.log(text);
        this._log.trace(text);
    }

    error(error: string | Object, message: string = null): void {
        if (message) {
            console.error(message, error);
        } else {
            console.error(error);
        }
        if (typeof error === 'string') {
            if (message) {
                this._log.error(message + ": " + error);
            } else {
                this._log.error(error);
            }
        } else {
            if (message) {
                this._log.error(message + ": " + JSON.stringify(error));
            } else {
                this._log.error(JSON.stringify(error));
            }
            if (error['stack']) {
                this._log.error(error['stack']);
            }
        }
    }

    async deleteOldFiles(): Promise<void> {
        this.fileHelper.prepareDirectory(this.dir);
        const dir = await this.fileHelper.directory(this.dir);
        if (dir && dir.files) {
            for (const file of dir.files) {
                const rgx = /^botlog-([0-9]{4}.[0-9]{2}.[0-9]{2}).log$/.exec(file);
                if (rgx && rgx.length === 2) {
                    const dateParts = rgx[1].split('.').map(s => parseInt(s));
                    const logDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);
                    const now = new Date().setHours(0, 0, 0, 0);
                    if (Helpers.date(now).diffHours(logDate) <= this.settingsHelper.settings.logFileLifeDays * 24) {
                        continue;
                    }
                }

                this.fileHelper.deleteFile(path.join(this.dir, file));
            }
        }
    }
}