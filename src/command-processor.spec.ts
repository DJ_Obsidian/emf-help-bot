import { AccessLevels } from "./access-levels";
import { IPersonalRoleArgs, ISetActivityArgs } from "./command-args";
import { CommandProcessor } from "./command-processor";
import { CommandValues } from "./command-values";


describe("CommandProcessor", () => {

    describe("parse", () => {

        const cmd = new CommandProcessor();
        [
            { command: "!help", expected: CommandValues.help, expectedLevel: AccessLevels.Everyone },
            { command: "!ger", expected: CommandValues.pook, expectedLevel: AccessLevels.Everyone },
            { command: "!ark", expected: CommandValues.ark, expectedLevel: AccessLevels.Everyone },
            { command: "!myark", expected: CommandValues.myark, expectedLevel: AccessLevels.Everyone },
            { command: "!o7", expected: CommandValues.o7, expectedLevel: AccessLevels.Everyone },
            { command: "!awards", expected: CommandValues.awards, expectedLevel: AccessLevels.Everyone },
            { command: "!award", expected: CommandValues.award, expectedLevel: AccessLevels.Leader },
            { command: "!ban", expected: CommandValues.ban, expectedLevel: AccessLevels.Admin },
            { command: "!unban", expected: CommandValues.unban, expectedLevel: AccessLevels.Admin },
            { command: "!bgs", expected: CommandValues.bgs, expectedLevel: AccessLevels.Admin },

            { command: "!помощь", expected: CommandValues.help, expectedLevel: AccessLevels.Everyone },
            { command: "!пук", expected: CommandValues.pook, expectedLevel: AccessLevels.Everyone },
            { command: "!арк", expected: CommandValues.ark, expectedLevel: AccessLevels.Everyone },
            { command: "!название Моя персональная роль", expected: CommandValues.personalRoleName, expectedLevel: AccessLevels.Everyone, text: 'Моя персональная роль' },

        ].forEach((c: any) => {
            const command = cmd.parse(c.command);
            it(`should find command ${c.expected} for text ${c.command}`, () => {
                expect(command.id).toEqual(c.expected);
            });

            it('command should have level', () => {
                expect(command.level).toEqual(c.expectedLevel);
            });          
        });
    });

    describe("parse with arguments", () => {

        const cmd = new CommandProcessor();
        [
            { command: "!myark", expected: CommandValues.myark, expectedText: "" },
            { command: "!арк", expected: CommandValues.ark, expectedText: "" },
            // TODO: add the rest

        ].forEach((c: any) => {
            it(`should find command ${c.expected} for text ${c.command} with args`, () => {
                const command = cmd.parse(c.command);
                expect(command.id).toEqual(c.expected);
                expect(command.text).toEqual(c.expectedText);
            });
        });
    });

    describe("helpResponse", () => {

        [
            { level: null, expected: 13 },
            { level: AccessLevels.Everyone, expected: 13 },
            { level: AccessLevels.Leader, expected: 17 },
            { level: AccessLevels.Admin, expected: 22 },
            { level: AccessLevels.Everyone, tag: "music", expected: 8 }
        ]
        .forEach((x: any) => {
            it(`should return ${x.expected} for ${AccessLevels[x.level]}`, () => {
                const response = new CommandProcessor().helpResponse(x.level, x.tag);
                expect(response.commands.length).toEqual(x.expected);
            });
        });
    });

    describe("parseNewfags", () => {

        [
            { text: '!newfags 10', expected: {days: 10} },
            { text: '!whateva 10', expected: {days: 10} },
            { text: '!whateva', expected: null },
            { text: '!whateva b', expected: null },
            { text: '!whateva 12 2', expected: null },
        ].forEach(x => {
            it(`should parse ${x.text} and return correct number of days`, () => {
                expect(CommandProcessor.parseNewfags(x.text)).toEqual(x.expected);
            });
        })
    });

    describe("parseBan", () => {

        [
            { text: '!ban <@!319151213679476737> 5', expectedArgs: {hours: 5,isPerm:false} },
            { text: '!бан <@!319251213679476737> 5', expectedArgs: {hours: 5,isPerm:false} },
            { text: '!бан <@!319251213679476737> 0.5', expectedArgs: {hours: 0.5,isPerm:false} },
            { text: '!бан <@!319251213679476737>', expectedArgs: null },
            { text: '!бан чувак 5', expectedArgs: null },
            { text: '!бан <@!319251213679476737> a', expectedArgs: null },
            { text: '!бан perm <@!319251213679476737>', expectedArgs: {isPerm: true, reason: undefined} },
            { text: '!ban perm <@!319251213679476737> aaaaa <@!319251213679476737> aaaa', expectedArgs: {isPerm: true, reason: 'aaaaa <@!319251213679476737> aaaa'} },
            { text: '!бан perm', expectedArgs: null },
        ].forEach(x => {
            let args = [];
            if (x.expectedArgs) {
                for (const [key, value] of Object.entries(x.expectedArgs)) {
                    args.push(`${key}: ${value}`);
                }
            } else {
                args.push('null');
            }
            it(`should return ${args.join(', ')} for ${x.text}`, () => {
                expect(CommandProcessor.parseBan(x.text)).toEqual(x.expectedArgs);
            });
        });

    });

    describe("parseKick", () => {

        [
            { text: '!kick <@!319251213679476737> good reason!', expectedArgs: {reason: 'good reason!'} },
            { text: '!kick <@!319251213679476737> good reason!', expectedArgs: {reason: 'good reason!'} },
            { text: '!кик <@!319251213679476737> good reason!', expectedArgs: {reason: 'good reason!'} },
            { text: '!kick        <@!319251213679476737>         good reason!', expectedArgs: {reason: 'good reason!'} },
            { text: '!kick good reason!', expectedArgs: null },
            { text: '!kik <@!319251213679476737> good reason!', expectedArgs: {reason: 'good reason!'} },
            { text: '!abcdwad <@!319251213679476737> <@!319251213679476737> мешает!', expectedArgs: {reason: '<@!319251213679476737> мешает!'} },
            { text: '!kick <@!319251213679476737>', expectedArgs: null },
        ].forEach(x => {

            it(`should return ${!x.expectedArgs ? 'null' : x.expectedArgs.reason} for ${x.text}`, () => {
                expect(CommandProcessor.parseKick(x.text)).toEqual(x.expectedArgs);
            });
        });

    });

    describe("parseSetActivity", () => {
        [
            { text: "!activity listening Some text.", expected: { activity: 'LISTENING', message: 'Some text.' }},
            { text: "!activity LISTENING Some text.", expected: { activity: 'LISTENING', message: 'Some text.' }},
            { text: "!activity watching Some text.", expected: { activity: 'WATCHING', message: 'Some text.' }},
            { text: "!activity WATCHING Some text.", expected: { activity: 'WATCHING', message: 'Some text.' }},
            { text: "!activity streaming Some text.", expected: { activity: 'STREAMING', message: 'Some text.' }},
            { text: "!activity STREAMING Some text.", expected: { activity: 'STREAMING', message: 'Some text.' }},
            { text: "!activity DOING Some text.", expected: null},
            { text: "STREAMING Some text.", expected: null},
            { text: "!activity STREAMING", expected: null},
            { text: "!activity Some text.", expected: null},
        ].forEach(x => {
            it(`should parse ${x.text} into ${x.expected ? JSON.stringify(x.expected) : 'null'}`, () => {
                expect(CommandProcessor.parseSetActivity(x.text)).toEqual(<ISetActivityArgs>x.expected);
            });
        });
    });
});