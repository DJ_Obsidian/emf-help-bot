import { CommandValues } from "./command-values";
import { getLoggerFake } from "./common-spec/common.spec";
import { ICooldown } from "./cooldown";
import { CooldownManager } from "./cooldown-manager";

function getCooldownManager(): CooldownManager {
    return new CooldownManager(getLoggerFake());
}

describe("CooldownManager", () => {

    describe("add", () => {

        const kd = getCooldownManager();

        beforeAll(done => {

            kd.add({
                userid: '1',
                seconds: 0.1,
                callback: () => done(),
            });
        });

        it("should remove cooldown after certain time", () => {
            expect(kd.has('1')).toBeFalse();
        });

    });

    describe("delete", () => {

        const kd = getCooldownManager();
        let cooldown: any;

        beforeAll(done => {

            cooldown = kd.add({
                userid: '1',
                seconds: 1000,
            });

            kd.add({
                userid: '2',
                seconds: 1000,
            });

            cooldown.delete();
            done();
        });

        it("should remove the cooldown", () => {
            expect(kd.has('1')).toBeFalse();
        });

        it("should not delete the other cooldown", () => {
            expect(kd.has('2')).toBeTrue();
        });

    });

    [
        { c: { userid: '1', seconds: 500000, }, has: { userid: '1' }, expected: true },
        { c: { userid: '1', seconds: 500000, }, has: { userid: '1', command: CommandValues.o7 }, expected: false },
        { c: { userid: '1', command: CommandValues.o7, seconds: 500000, }, has: { userid: '1', command: CommandValues.o7 }, expected: true },
        { c: { userid: '2', command: CommandValues.o7, seconds: 500000, }, has: { userid: '1', command: CommandValues.o7 }, expected: false },
        { c: { userid: '1', command: CommandValues.o7, seconds: 500000, }, has: { userid: '1' }, expected: false },
        { c: { command: CommandValues.o7, seconds: 500000, }, has: { command: CommandValues.o7 }, expected: true },
        { c: { command: CommandValues.o7, seconds: 500000, }, has: { command: CommandValues.o7, userid: null }, expected: true },
        { c: { userid: "1", seconds: 500000, }, has: { command: undefined, userid: "1" }, expected: true },
    ]
    .forEach(x => {
        describe("has", () => {

            const kd = getCooldownManager();
    
            beforeAll(done => {
                kd.add(<ICooldown>x.c);
                done();
            });

            it(`should return ${x.expected}`, () => {
                expect(kd.has(x.has.userid, x.has.command)).toEqual(x.expected);
            });

            afterAll(() => kd.dispose());
        });
    });

});