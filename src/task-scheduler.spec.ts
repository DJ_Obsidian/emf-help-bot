import { getFileHelperFake, getLoggerFake } from "./common-spec/common.spec";
import Helpers from "./helpers";
import { TaskScheduler } from "./task-scheduler";
import * as p from "path";

describe("TaskScheduler", () => {

    describe("when task is due", () => {

        const taskScheduler = new TaskScheduler(getFileHelperFake(), getLoggerFake());
        taskScheduler.delay = 10;
        let executed = 0;

        beforeAll(async done => {
            taskScheduler.addTask({
                name: "task 1",
                data: 1,
                everySeconds: 0.05,
                execute: () => {
                    executed++;
                    return Promise.resolve();
                },
            });
            taskScheduler.onTick = () => executed === 0;
            taskScheduler.on("stop", () => done());

            taskScheduler.start();
        });

        it("should execute the task", () => {
            expect(executed).toBeGreaterThan(0);
        });

        it("should stop", () => {
            expect(taskScheduler.stopped).toEqual(true);
        });

        it("should set executed datetime in task", () => {
            expect(taskScheduler.tasks[0].executed).toBeTruthy();
        });
    });

    describe("when task is not due", () => {
        const taskScheduler = new TaskScheduler(getFileHelperFake(), getLoggerFake());
        taskScheduler.delay = 10;
        let executed = 0;

        beforeAll(async done => {
            taskScheduler.addTask({
                name: "task 1",
                data: 1,
                everySeconds: 10,
                execute: () => {
                    executed++;
                    return Promise.resolve();
                },
            });

            taskScheduler.start();
            await Helpers.wait(200);
            await taskScheduler.stop();
            done();
        });

        it("should not execute the task", () => {
            expect(executed).toEqual(0);
        });

        it("should stop", () => {
            expect(taskScheduler.stopped).toEqual(true);
        });
    });

    describe("when stopping and timeout", () => {
        const logger = getLoggerFake();
        const taskScheduler = new TaskScheduler(getFileHelperFake(), logger);
        taskScheduler.delay = 10000;
        taskScheduler.timeout = 300;

        let errorSpy: jasmine.Spy;

        beforeAll(async done => {
            errorSpy = spyOn(logger, 'error').and.callThrough();

            taskScheduler.start();
            await Helpers.wait(100);
            taskScheduler.stop().then(() => done());
        });

        it("should stop", () => {
            expect(taskScheduler.stopped).toEqual(true);
        });

        it("should stop by timing out", () => {
            expect(errorSpy).toHaveBeenCalledOnceWith("Timeout. TaskScheduler failed to stop in time.");
        });
    });

    [
        { throw: async (): Promise<void> => { throw new Error('error'); } },
        { throw: (): Promise<void> => { return Promise.reject('error'); } },
    ].forEach(x => {
        describe("when a task throws an unhandled exception/rejection", () => {
            const logger = getLoggerFake();
            const taskScheduler = new TaskScheduler(getFileHelperFake(), logger);
            taskScheduler.delay = 10;
    
            let loggerErrorSpy: jasmine.Spy;
    
            beforeAll(async done => {
                loggerErrorSpy = spyOn(logger, 'error').and.callThrough();
    
                taskScheduler.addTask({
                    name: "task 1",
                    data: 1,
                    execute: async () => {
                        await x.throw();
                    },
                },
                false);
                taskScheduler.onTick = () => false;
                taskScheduler.on("stop", () => done());
    
                taskScheduler.start();
            });
    
            it("should stop", () => {
                expect(taskScheduler.stopped).toEqual(true);
            });
    
            it("should log error", () => {
                expect(loggerErrorSpy).toHaveBeenCalledTimes(1);
            });
    
            it("should set executed datetime in task", () => {
                expect(taskScheduler.tasks[0].executed).toBeTruthy();
            });
        });
    });

    describe("load", () => {
        const fileHelper = getFileHelperFake();
        const taskScheduler = new TaskScheduler(fileHelper, getLoggerFake());

        let directorySpy: jasmine.Spy;
        let readFileSpy: jasmine.Spy;

        const dir = {
            files: [
                'task 1',
                'task 2',
            ],
        };

        beforeAll(async done => {

            taskScheduler.addTask({
                name: 'task 1',
                everyHours: 1,
            });

            directorySpy = spyOn(fileHelper, 'directory').and.callThrough()
                .and.resolveTo(dir);
            readFileSpy = spyOn(fileHelper, 'readFile').and.callThrough()
                .and.returnValues(
                    '{ "name": "task 1", "executed": "2021-03-17T21:11:31.716Z", "everyHours": 2 }',
                    '{ "name": "task 2", "executed": "2021-03-17T21:11:31.716Z" }',
                );

            // Act
            await taskScheduler.load();

            done();
        });

        it("should add only one task", () => {
            expect(taskScheduler.tasks.length).toEqual(2);
            expect(taskScheduler.tasks.find(t => t.name == 'task 2')).toBeTruthy();
        });

        it("should update the other task", () => {
            expect(taskScheduler.tasks.length).toEqual(2);
            expect(taskScheduler.tasks.find(t => t.name == 'task 1').everyHours).toEqual(2);
        });

        it("should call directory with correct dir name", () => {
            expect(directorySpy).toHaveBeenCalledOnceWith("./schedules");
        });

        it("should call readFile twice", () => {
            expect(readFileSpy).toHaveBeenCalledTimes(2);
            expect(readFileSpy).toHaveBeenCalledWith(p.join("schedules", "task 1"));
            expect(readFileSpy).toHaveBeenCalledWith(p.join("schedules", "task 2"));
        });
    });
});