import { DbContext, DbContextProvider } from "./db/context";
import FileHelper from "./file-helper";
import Helpers from "./helpers";
import Logger from "./logger";
import { TaskNames } from "./task-names";
import { TaskScheduler } from "./task-scheduler";

export interface IProcessTask {
    id?: string;
    name: TaskNames;
    data?: any;
    error?: any;
    attempts?: number;
    nextAttempt?: Date;
}

export class TaskProcessor {

    dir = './temp';

    private _taskProcessEvent: (task: IProcessTask, db: DbContext) => Promise<void>;

    constructor(private dbContextProvider: DbContextProvider, private fileHelper: FileHelper,
        private taskScheduler: TaskScheduler, private logger: Logger) {
        this.taskScheduler.addTask({
            data: this,
            execute: (taskProcessor: TaskProcessor) => { return taskProcessor.processPendingTasks(); },
            everySeconds: 2,
            name: 'TaskProcessor',
        });
    }

    addTask(task: IProcessTask, id: string = null): void {
        this.fileHelper.prepareDirectory(this.dir);
        if (!id) {
            id = Helpers.guid(true);
        }

        task.id = id;
        this.fileHelper.writeToFile(`${this.dir}/${Date.now()}bot-task_${id}.json`, JSON.stringify(task));
    }

    async processPendingTasks(): Promise<void> {
        if (!this._taskProcessEvent) {
            throw new Error('onTaskProcess event not set.');
        }

        this.fileHelper.prepareDirectory(this.dir);

        // Load a task
        const dir = await this.fileHelper.directory(this.dir);
        if (dir && dir.files) {
            // Pick the valid files and sort them in ascending order
            for (const file of Helpers.sortAscending(dir.files.filter(f => /^[0-9]{13}bot-task_/.exec(f)), f => f)) {
                this.logger.log(`Processing file ${file}...`);
                const filePath = `${this.dir}/${file}`;
                const fileContent = this.fileHelper.readFile(filePath);
                const task: IProcessTask = JSON.parse(fileContent);
                this.logger.log(`Task ${TaskNames[task.name]}:`);
                this.logger.log(fileContent);
                const dbContext = await this.dbContextProvider.create();
                try {
                    await this._taskProcessEvent(task, dbContext);
                    this.fileHelper.deleteFile(filePath);
                } catch (error) {
                    task.error = error;
                    if (task.attempts) {
                        task.attempts++;
                    } else {
                        task.attempts = 1;
                    }

                    this.logger.error(`Error processing task ${TaskNames[task.name]}.\r\n${error}`);
                    if (task.attempts < 25) {
                        this.fileHelper.writeToFile(filePath, JSON.stringify(task));
                    } else {
                        this.fileHelper.deleteFile(filePath);
                        this.fileHelper.writeToFile(`${this.dir}/dead_${file}`, JSON.stringify(task));
                    }

                    await Helpers.wait(1000 * 60 * 2);
                } finally {
                    dbContext.destroy();
                }

                await Helpers.wait(500);
            }
        }
    }

    onTaskProcess(callback: (task: IProcessTask, db: DbContext) => Promise<void>): void {
        this._taskProcessEvent = callback;
    }
}