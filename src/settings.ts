import { IRssFeedSettings } from "./rss/rss-feed-settings";

export interface IConnectionSettings {
    host: string;
    user: string;
    password: string;
    database: string;
    connectionLimit: number;
    port: number;
    charset: 'utf8mb4',
}

export interface ISettings {

    /**
     * Discord BOT key.
     */
    clientKey: string;

    db: IConnectionSettings;

    /**
     * Number of days log files are allowed to stay before
     * being deleted.
     */
    logFileLifeDays: number;

    /**
     * Роль Зоопарк.
     */
    zooRoleId: string;

    /**
     * Zoo keeper role id. The bot must have this role assigned
     * to be able to send people to the zoo and ban them.
     */
    zooKeeperRoleId: string;

    /**
     * Users with these roles are exempt from being banned or sent to the zoo.
     */
    zooExemptionRoleIds: string[];

    allowedInactivityDays: number;
    allowedZooInactivityDays: number;

    /**
     * Elite BGS API http url.
     */
    eliteBgsApiUrl: string;

    eliteBgsUrl: string;

    /**
     * Number of days after which to delete disabled users.
     */
    disabledUsersLifeDays: number;

    /**
     * The number indicating the position of the personal role when created.
     */
    personalRoleStartingPos: number;

    botReservedRoleNames: string[];

    /**
     * Role id of Officer.
     */
    officerRoleId: string;

    osRoleId: string;

    strangerRoleId: string;

    rssSettings: IRssFeedSettings;
}