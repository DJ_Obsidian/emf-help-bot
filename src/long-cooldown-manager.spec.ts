import { CommandValues } from "./command-values";
import { getDbContextProviderFake, getDbContextFake, getTaskProcessorFake, getLoggerFake } from "./common-spec/common.spec";
import { ICooldown } from "./cooldown";
import Helpers from "./helpers";
import LongCooldownManager from "./long-cooldown-manager";
import { TaskNames } from "./task-names";


describe("LongCooldownManager", () => {


    describe("loadCooldowns", () => {

        const dbProviderFake = getDbContextProviderFake();
        const dbContextFake = getDbContextFake();
        const taskProcessorFake = getTaskProcessorFake();
        
        let createSpy: jasmine.Spy;
        let loadAllCooldownsSpy: jasmine.Spy;
        let addTaskSpy: jasmine.Spy;

        let actualCooldown: ICooldown;
        let destroySpy: jasmine.Spy;

        beforeAll(async done => {
            createSpy = spyOn(dbProviderFake, 'create').and.resolveTo(dbContextFake);
            loadAllCooldownsSpy = spyOn(dbContextFake.cooldownRepo, 'loadAllCooldowns').and.resolveTo([
                {
                    userid: '1',
                    created: Helpers.date(new Date()).addHours(-12),
                    hours: 24,
                }
            ]);
            addTaskSpy = spyOn(taskProcessorFake, 'addTask').and.callThrough();
            destroySpy = spyOn(dbContextFake, 'destroy').and.callThrough();
            
            const cdManager = new LongCooldownManager(dbProviderFake, taskProcessorFake, getLoggerFake());
            await cdManager.loadCooldowns();

            actualCooldown = cdManager.get('1');

            done();
        });

        it("should add a cooldown", () => {
            expect(12 * 60 * 60 - actualCooldown.seconds).toBeLessThan(1000);
        });

        it("should call destroy on DbContext", () => {
            expect(destroySpy).toHaveBeenCalledTimes(1);
        });

        it("should not add task", () => {
            expect(addTaskSpy).not.toHaveBeenCalled();
        });

    });

    describe("add", () => {

        const taskManager = getTaskProcessorFake();
        const longCooldownManager = new LongCooldownManager(getDbContextProviderFake(), taskManager, getLoggerFake());

        let addTaskSpy: jasmine.Spy;

        let actualCooldown: ICooldown;
        let actualTaskName: TaskNames;

        beforeAll(() => {
            addTaskSpy = spyOn(taskManager, 'addTask').and.callThrough().and.callFake(task => {
                actualCooldown = task.data;
                actualTaskName = task.name;
            });

            // Act
            const r = longCooldownManager.add({
                seconds: 10000,
                command: CommandValues.o7,
                userid: '1',
            });

            r.delete();
        });

        it("should add task", () => {
            expect(actualTaskName).toEqual(TaskNames.addLongCooldown);
            expect(actualCooldown).toBeTruthy();
            expect(actualCooldown.created).toBeTruthy();
            expect(actualCooldown.seconds).toEqual(10000);
            expect(actualCooldown.userid).toEqual('1');
            expect(actualCooldown.command).toEqual(CommandValues.o7);
        });
    });

    describe("getSecondsLeft", () => {
        let actualSeconds: number;

        beforeAll(() => {

            const longCooldownManager = new LongCooldownManager(<any>{}, getTaskProcessorFake(), getLoggerFake());
            longCooldownManager.add({
                userid: "1",
                seconds: 1,
            });

            // Act
            actualSeconds = longCooldownManager.getSecondsLeft("1");
        });

        it("should return correct number of seconds left", () => {
            expect(10 - actualSeconds).toBeLessThan(1000);
        });

    });

});