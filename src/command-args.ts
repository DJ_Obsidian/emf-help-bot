import { ActivityType } from "./discord-event";

export interface INewfagArgs {
    days: number;
}

export interface IPersonalRoleArgs {
    name: string;
    color: string;
    delete: boolean;
}

export interface IBanArgs {
    isPerm: boolean;
    hours?: number;
    reason?: string;
}

export interface IKickArgs {
    reason: string;
}

export interface IAwardArgs {
    medal: string;
    description: string;
}

export interface ISayArgs {
    channelid: string;
    message: string;
}

export interface ISetActivityArgs {
    activity: ActivityType;
    message: string;
}

export interface IPlayArgs {
    url?: string;
    playPl?: boolean;
    text?: string;
}

export interface IPlaylistArgs {
    url?: string;
    deleteIndex?: number;
    clear?: boolean;
    show?: boolean;
}
