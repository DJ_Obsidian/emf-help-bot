import { CommandValues } from "./command-values";
import { Cooldown, ICooldown } from "./cooldown";
import Logger from "./logger";

export class CooldownManager {

    private _cooldowns: Cooldown[] = [];

    get count(): number {
        return this._cooldowns.length;
    }

    get cooldowns(): ICooldown[] {
        return this._cooldowns;
    }

    constructor(protected logger: Logger) {
    }

    add(cooldown: ICooldown): { cooldown: ICooldown, delete: () => void } {
        const list = this._cooldowns;
        const cd = new Cooldown(cooldown.seconds);
        cd.userid = cooldown.userid;
        cd.command = cooldown.command;
        cd.created = cooldown.created || new Date();
        list.push(cd);
        cd.deleteCallback = () => {
            list.splice(list.indexOf(cd), 1);
            if (cooldown.callback) {
                try {
                    cooldown.callback();
                } catch (error) {
                    console.error('Error executing a cooldown task: ' + error);
                }
            }
        };
        return {
            cooldown: cd,
            delete: cd.deleteCallback,
        };
    }

    has(userid: string, command?: CommandValues): boolean {
        if (userid) {
            return this._cooldowns.findIndex(c => c.userid === userid && c.command === command) !== -1;
        } else {
            return this._cooldowns.findIndex(c => c.command === command) !== -1;
        }
    }

    get(userid: string, command?: CommandValues): ICooldown {
        if (userid) {
            return this._cooldowns.find(c => c.userid === userid && c.command === command);
        } else {
            return this._cooldowns.find(c => c.command === command);
        }
    }

    clear(): void {
        this._cooldowns.forEach(c => c.expire());
        this._cooldowns = [];
    }

    trace(title: string): void {
        this.logger.log(title);
        this.logger.log(`   Count ${this.cooldowns.length}${this.cooldowns.length > 0 ? ':' : '.'}`);
        this.cooldowns.forEach(cooldown => {
            this.logger.log(`   User - ${cooldown.userid || 'null'}; command - ${CommandValues[cooldown.command] || 'null'}; created - ${cooldown.created}; seconds - ${cooldown.seconds}`);
        });
    }

    dispose(): void {
        this._cooldowns.forEach(c => c.expire());
    }
}