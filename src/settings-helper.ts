import FileHelper from "./file-helper";
import { ISettings } from "./settings";
import * as Path from 'path';

export class SettingsHelper {
    private _settngsFile = 'settings.json';

    private _settings: ISettings = null;

    get settings(): ISettings {
        return this._settings;
    }

    constructor(private fileHelper: FileHelper) {
        this.load();
    }

    private load(): void {
        const file = this.fileHelper.readFile(Path.join(this._settngsFile));
        this._settings = JSON.parse(file);
    }
}