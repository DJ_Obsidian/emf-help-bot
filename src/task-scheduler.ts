import FileHelper from "./file-helper";
import Helpers from "./helpers";
import Logger from "./logger";
import { TaskNames } from "./task-names";
import * as path from "path";
import { IProcessTask } from "./task-processor";
import { EventEmitter } from "events";

export interface IScheduledTask {
    name: string;
    everySeconds?: number;
    everyHours?: number;
    executed?: Date;
    data?: unknown;
    task?: TaskNames
    execute?: (data: unknown, task?: TaskNames) => Promise<void>;
}

export class TaskScheduler extends EventEmitter {

    dir = './schedules';
    private _running = false;
    private _tasks: IScheduledTask[] = [];

    private _filesLoaded: Date;

    onTaskExecute: (task: IProcessTask) => Promise<void>;

    private _lastActivity = new Date();

    get lastActivity(): Date {
        return this._lastActivity;
    }

    get lastActivitySecondsAgo(): number {
        return Math.abs(Helpers.date(new Date()).diffMillis(this._lastActivity) / 1000);
    }

    stopped = false;

    delay = 1000;

    /**
     * Timeout in milliseconds to wait at most before stopping
     * after stop has been called.
     */
    timeout = 60 * 1000;

    onTick?: () => boolean;

    get tasks(): IScheduledTask[] {
        return this._tasks;
    }

    constructor(protected fileHelper: FileHelper, protected logger: Logger) {
        super();
        fileHelper.prepareDirectory(this.dir);
    }

    start(): void {
        if (this._running) {
            return;
        }

        this._lastActivity = new Date()
        this._running = true;
        this.stopped = false;
        this.run();
        this.logger.log("TaskScheduler has started.");
    }

    stop(): Promise<void> {
        this._running = false;
        this.logger.log("Stopping TaskScheduler...");
        return new Promise(resolve => {
            if (this.stopped) {
                resolve();
            } else {
                const timeoutId = setTimeout(() => {
                    this.logger.error("Timeout. TaskScheduler failed to stop in time.");
                    this.stopped = true;
                    resolve();
                }, this.timeout);
                this.on("stop", () => {
                    clearTimeout(timeoutId);
                    resolve();
                });
            }
        });
    }

    addTask(task: IScheduledTask, startOnNextTick = true): void {
        if (!task.name || task.name.trim().length === 0) {
            throw new Error('Task name is missing.');
        }

        if (this._tasks.findIndex(x => x.name === task.name) === -1) {
            task.executed = startOnNextTick ? new Date() : undefined;
            this._tasks.push(task);

            if (task.task && !task.execute) {
                task.execute = undefined;
                this.saveFile(task);
            }
        }
    }

    private saveFile(task: IScheduledTask): void {
        this.fileHelper.prepareDirectory(this.dir);
        this.fileHelper.writeToFile(path.join(this.dir, `${task.name}.json`), JSON.stringify(task));
    }

    async load(): Promise<void> {
        if (this._filesLoaded) {
            return;
        }

        const dir = await this.fileHelper.directory(this.dir);
        if (dir && dir.files) {
            for (const file of dir.files) {
                const fileContent = this.fileHelper.readFile(path.join(this.dir, file));
                const task: IScheduledTask = JSON.parse(fileContent);
                task.executed = new Date(task.executed); // JSON parse does not parse dates as you would want it to.
                const index = this._tasks.findIndex(x => x.name === task.name);
                // Loaded files replace those loaded by default in the beginning.
                if (index < 0) {
                    this._tasks.push(task);
                } else {
                    this._tasks[index] = task;
                }
            }
        }

        this._filesLoaded = new Date();
    }

    private next(): void {
        if (this.onTick && this.onTick() === false) {
            this._running = false;
        }

        this.run();
    }

    private run(): void {
        if (!this._running) {
            this.stopped = true;
            this.emit("stop");

            return;
        }

        setImmediate(() => {
            this._lastActivity = new Date();
            this.executeTasks().then(() => this.next());
        });
    }

    private async executeTask(task: IScheduledTask): Promise<void> {
        try {
            if (task.execute) {
                await task.execute(task.data, task.task);
            } else {
                if (this.onTaskExecute) {
                    await this.onTaskExecute({
                        name: task.task,
                        data: task.data,
                    });
                }
            }

        } catch (error) {
            this.logger.error(`Task ${task.name} threw an error: ${error.message}.\n${error.stack || ''}`);
        } finally {
            task.executed = new Date();
            if (task.task && !task.execute) {
                this.saveFile(task);    
            }
        }
    }

    private async executeTasks(): Promise<void> {
        for (const task of this._tasks) {
            const period = (task.everySeconds || 0) + ((task.everyHours || 0) * 3600);
            if (!task.executed || (period > 0 && Helpers.date(new Date()).diffSeconds(task.executed) > period)) {
                await this.executeTask(task);
            }
        }

        await Helpers.wait(this.delay);
    }
}
