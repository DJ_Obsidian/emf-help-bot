import { AccessLevels } from "./access-levels";
import { IAccessLevel } from "./db/entities/access-level";
import Helpers from "./helpers";

describe("removeDuplicateSpaces", () => {
    [
        { 
            text: 'this is a sentence',
            expected: 'this is a sentence',
        },
        { 
            text: ' this  is     a     sentence     ',
            expected: ' this is a sentence ',
        },
        { 
            text: ' thisisasentence   ',
            expected: ' thisisasentence ',
        },
        { 
            text: ' ',
            expected: ' ',
        },
        { 
            text: '  ',
            expected: ' ',
        },
        { 
            text: '',
            expected: '',
        },
    ].forEach(t => {
        it(`should remove duplicate spaces from ${t.text}`, () => {
            expect(Helpers.removeDuplicateSpaces(t.text)).toEqual(t.expected);
        });
    })
});

describe("getRandomPhrase", () => {

    const phrases = ['1', '2', '3', '4'];

    it("should get random phrase", () => {
        const selected: string[] = [];
        for (let i = 0; i < 1000; i++) {
            const phrase = Helpers.getRandomPhrase(phrases);
            if (selected.indexOf(phrase) === -1) {
                selected.push(phrase);
            }
        }

        expect(selected.length).toEqual(4);
    });
});

describe("sortDescending", () => {

    const accessLevels: IAccessLevel[] = [
        { id: AccessLevels.Everyone, roleid: Helpers.guid() },
        { id: AccessLevels.Leader, roleid: Helpers.guid() },
        { id: AccessLevels.Admin, roleid: Helpers.guid() },
        { id: AccessLevels.Fuhrer, roleid: Helpers.guid() },
    ];

    it("should sort in descending order", () => {
        const sorted = Helpers.sortDescending(accessLevels, x => x.id);
        expect(sorted[0].id).toEqual(AccessLevels.Fuhrer);
        expect(sorted[1].id).toEqual(AccessLevels.Admin);
        expect(sorted[2].id).toEqual(AccessLevels.Leader);
        expect(sorted[3].id).toEqual(AccessLevels.Everyone);
    });

    it("should return empty array if sorting empty array", () => {
        expect(Helpers.sortDescending([], () => null)).toBeTruthy();
    });

});

describe("sortAscending", () => {

    const accessLevels: IAccessLevel[] = [
        { id: AccessLevels.Fuhrer, roleid: Helpers.guid() },
        { id: AccessLevels.Admin, roleid: Helpers.guid() },
        { id: AccessLevels.Leader, roleid: Helpers.guid() },
        { id: AccessLevels.Everyone, roleid: Helpers.guid() },
    ];

    it("should sort in ascending order", () => {
        const sorted = Helpers.sortAscending(accessLevels, x => x.id);
        expect(sorted[3].id).toEqual(AccessLevels.Fuhrer);
        expect(sorted[2].id).toEqual(AccessLevels.Admin);
        expect(sorted[1].id).toEqual(AccessLevels.Leader);
        expect(sorted[0].id).toEqual(AccessLevels.Everyone);
    });

    it("should return empty array if sorting empty array", () => {
        expect(Helpers.sortAscending([], _ => null)).toBeTruthy();
    });

});

describe("showflake", () => {

    it("should create a random number string with 18 digits", () => {
        for (let i = 0; i < 100; i++) {
            expect(Helpers.snowflake().length).toEqual(18);
        }
    });
});

describe("date", () => {

    describe("equals", () => {
        [
            { this: new Date(2020, 1, 1, 18, 0, 0), other: new Date(2020, 1, 1, 18, 0, 0), expected: true },
            { this: new Date(2020, 1, 1, 18, 0, 1), other: new Date(2020, 1, 1, 18, 0, 0), expected: false },
        ].forEach(x => {
            it(`${x.this} should ${x.expected ? '': 'not '}${x.other}`, () => {
                expect(Helpers.date(x.this).equals(x.other)).toEqual(x.expected);
            });
        });
    });

    describe("addHours", () => {

        [
            { initial: new Date(2020, 1, 1, 18, 0, 0), expected: new Date(2020, 1, 2, 4, 0, 0), hours: 10 },
            { initial: new Date(2020, 1, 1, 18, 0, 0), expected: new Date(2020, 1, 1, 18, 30, 0), hours: 0.5 },
            { initial: new Date(2020, 1, 1, 0, 0, 0), expected: new Date(2020, 1, 2, 0, 0, 0), hours: 24 },
            { initial: new Date(2020, 1, 1, 10, 0, 0), expected: new Date(2020, 1, 1, 0, 0, 0), hours: -10 },
            { initial: '2020-11-07T16:46:47.000Z', expected: new Date(2020, 10, 7, 6, 46, 47), hours: -10 },
        ].forEach(x => {
            it(`should add ${x.hours} hours to date ${x.initial}`, () => {
                const newDt = Helpers.date(<string>x.initial).addHours(x.hours);
                expect(newDt).toEqual(x.expected);
            });
        });
    });

    describe("addDays", () => {
        [
            { initial: new Date(2020, 1, 1, 18, 0, 0), expected: new Date(2020, 1, 11, 18, 0, 0), days: 10 },
            { initial: new Date(2020, 1, 1, 0, 0, 0), expected: new Date(2020, 1, 1, 12, 0, 0), days: 0.5 },
        ].forEach(x => {
            it(`should add ${x.days} hours to date ${x.initial} and get ${x.expected}`, () => {
                const newDt = Helpers.date(x.initial).addDays(x.days);
                expect(newDt).toEqual(x.expected);
            });
        });
    });

    describe("addSeconds", () => {
        [
            { initial: new Date(2020, 1, 1, 1, 0, 0), expected: new Date(2020, 1, 1, 1, 0, 59), seconds: 59 },
        ].forEach(x => {
            it(`should add ${x.seconds} seconds to date ${x.initial} and get ${x.expected}`, () => {
                const newDt = Helpers.date(x.initial).addSeconds(x.seconds);
                expect(newDt).toEqual(x.expected);
            });
        });
    });

    describe("getDate", () => {
        it("should get only date without time", () => {
            expect(Helpers.date(new Date(2020, 5, 6, 12, 50, 50, 700)).datePart().getTime())
                .toEqual(new Date(2020, 5, 6, 0, 0, 0, 0).getTime());
        });
    });

    describe("diffSeconds", () => {
        [
            { current: new Date(2020, 0, 1, 0, 0, 0, 0), before: new Date(2020, 0, 0, 0, 0, 0, 0), expected: 24 * 60 * 60 },
            { current: new Date(2020, 0, 0, 0, 0, 0, 0), before: new Date(2020, 0, 1, 0, 0, 0, 0), expected: -24 * 60 * 60 },
            { current: new Date(2020, 0, 0, 0, 0, 0, 0), before: new Date(2020, 0, 1, 0, 0, 0, 0), expected: -24 * 60 * 60 },
            { current: new Date(2020, 0, 0, 15, 3, 25), before: new Date(2020, 0, 0, 13, 3, 25), expected: 2 * 60 * 60 },
        ].forEach(x => {
            it(`should return the difference of ${x.expected} seconds between ${x.current} and ${x.before}`, () => {
                expect(Helpers.date(x.current).diffSeconds(x.before)).toEqual(x.expected);
            });
        });   
    });

    describe("diffMillis", () => {
        [
            { current: new Date(2020, 0, 1, 0, 0, 0, 0), before: new Date(2020, 0, 0, 0, 0, 0, 0), expected: 24 * 60 * 60 * 1000 },
            { current: new Date(2020, 0, 0, 0, 0, 0, 0), before: new Date(2020, 0, 1, 0, 0, 0, 0), expected: -24 * 60 * 60 * 1000 },
        ].forEach(x => {
            it(`should return the difference of ${x.expected} milliseconds between ${x.current} and ${x.before}`, () => {
                expect(Helpers.date(x.current).diffMillis(x.before)).toEqual(x.expected);
            });
        });        
    });

    describe("diffHours", () => {
        [
            { current: new Date(2020, 0, 1, 0, 0, 0, 0), before: new Date(2020, 0, 0, 0, 0, 0, 0), expected: 24 },
            { current: new Date(2020, 0, 1, 0, 0, 0, 0), before: new Date(2020, 0, 0, 12, 0, 0, 0), expected: 12 },
            { current: new Date(2020, 0, 0, 13, 0, 0, 0), before: new Date(2020, 0, 0, 12, 0, 0, 0), expected: 1 },
        ].forEach(x => {
            it(`should return the difference of ${x.expected} hours between ${x.current} and ${x.before}`, () => {
                expect(Helpers.date(x.current).diffHours(x.before)).toEqual(x.expected);
            });
        });        
    });
});

describe("removeItem", () => {

    it("should remove item from list", () => {
        const array = [1, 2, 3, 4];
        const r = Helpers.removeItem(array, c => c === 2);
        expect(array).toEqual([1, 3, 4]);
        expect(r).toEqual(2);
    });
});

describe("formatDuration", () => {

    [
        { durationMillis: 1000 * 60 * 60, expected: '1 час' },
        { durationMillis: 1000 * 60 * 60 * 2, expected: '2 часа' },
        { durationMillis: 1000 * 60 * 60 * 10, expected: '10 часов' },
        { durationMillis: 1000 * 60, expected: '1 минута' },
        { durationMillis: 1000 * 60 * 2, expected: '2 минуты' },
        { durationMillis: 1000 * 60 * 5, expected: '5 минут' },
        { durationMillis: (1000 * 60 * 60) + (60 * 1000 * 2), expected: '1 час 2 минуты' },
    ].forEach(x => {
        it(`should format ${x.durationMillis} as ${x.expected}`, () => {
            expect(Helpers.formatDuration(x.durationMillis)).toEqual(x.expected);
        });
    })

});

describe("splitArray", () => {

    [
        { array: [1, 2, 3, 4, 5], size: 2, expected: [[1, 2], [3, 4], [5]] },
        { array: [1, 2, 3, 4, 5], size: 1, expected: [[1], [2], [3], [4], [5]] },
        { array: [1, 2, 3, 4, 5], size: 5, expected: [[1, 2, 3, 4, 5]] },
        { array: [1], size: 1, expected: [[1]] },
        { array: [1], size: 5, expected: [[1]] },
        { array: [1], size: 5, expected: [[1]] },
        { array: [1, 2, 3, 4, 5], size: 0, expected: [[1, 2, 3, 4, 5]] },
        { array: [], size: 10, expected: [] },
    ].forEach(x => {
        it(`should split [${x.array.join(',')}] into [${x.expected.map(i => `[${i.join(',')}]`).join(',')}] of size ${x.size}`, () => {
            expect(Helpers.splitArray(x.array, x.size)).toEqual(x.expected);
        });
    });
});

describe("distinct", () => {
    [
        { array: [1,2,3,3,4,4,5], expected: [1,2,3,4,5]},
        { array: [], expected: []},
        { array: ['1','2','3','3','4','4','5'], expected: ['1','2','3','4','5']},
    ].forEach(x => {
        it(`should return [${x.array.join(',')}] as distinct [${x.expected.join(',')}]`, () => {
            expect(Helpers.distinct(<any[]>x.array)).toEqual(x.expected);
        });
    });
});

describe("shuffle", () => {
    [
        { array: [1,2,3,4,5,6,7,8,9] },
        { array: [1] },
        { array: [] },
    ].forEach(x => {
        it(`should return [${x.array.join(',')}] as shuffled array of size ${x.array.length}`, () => {
            const original = x.array.slice();
            Helpers.shuffle(x.array);
            expect(x.array.length).toEqual(original.length);
            x.array.forEach(i => original.indexOf(i) > -1);
        });
    });
});

describe("sliceAround", () => {
    [
        { array: [1,2,3,4,5,6,7,8,9], select: 4, leftOffset: 2, rightOffset: 4, expected: [2,3,4,5,6,7,8] },
        { array: [1,2,3,4,5,6,7,8,9], select: 4, leftOffset: 20, rightOffset: 40, expected: [1,2,3,4,5,6,7,8,9] },
        { array: [4], select: 4, leftOffset: 20, rightOffset: 40, expected: [4] },
        { array: [2,4,6], select: 4, leftOffset: 1, rightOffset: 1, expected: [2,4,6] },
        { array: [2,4,6], select: 4, leftOffset: 0, rightOffset: 0, expected: [2,4,6] },
        { array: [2,4,6,7,8,9], select: 1, leftOffset: 1, rightOffset: 1, expected: [2,4,6,7,8,9] },
        { array: [], select: 1, leftOffset: 1, rightOffset: 1, expected: [] },
    ].forEach(x => {
        it(`should return [${x.array.join(',')}] as sliced array [${x.expected.join(',')}]`, () => {
            const result = Helpers.sliceAround(x.array, i => i == x.select, x.leftOffset, x.rightOffset);
            expect(result).toEqual(x.expected);
        });
    })
});