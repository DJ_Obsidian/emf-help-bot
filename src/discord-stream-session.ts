import * as Discord from 'discord.js'
import { EventEmitter } from 'events';
import { Readable } from "stream";
import { IStreamSession } from "./discord-event";


export class StreamSession extends EventEmitter implements IStreamSession {

    private _dispatcher: Discord.StreamDispatcher;

    constructor(private _connection: Discord.VoiceConnection, private _readable: Readable) {
        super();

        this._dispatcher = this._connection.play(this._readable, { seek: 0, volume: 1 });
        this._dispatcher
            .on("start", () => this.emit("start"))
            .on("finish", () => this.emit("finish"))
            .on("error", error => this.emit("error", error));
    }

    pause(): Promise<void> {
        if (!this._dispatcher.paused) {
            this._dispatcher.pause();
            this.emit("pause");
        }
        return Promise.resolve();
    }
    
    resume(): Promise<void> {
        if (this._dispatcher.paused) {
            this._dispatcher.resume();
            this.emit("resume");
        }
        return Promise.resolve();
    }
    
    end(): Promise<void> {
        this._dispatcher.destroy()
        this.emit("end");
        return Promise.resolve();
    }

    destroy(): void {
        this._dispatcher.destroy();
    }
}