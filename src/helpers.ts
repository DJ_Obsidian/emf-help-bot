import { v4 as uuidv4 } from 'uuid';
import { DateTime } from 'luxon';

interface IDateOptions {
    /**
     * Checks if current date equals another.
     */
    equals: (another: Date) => boolean,

    /**
     * Gets only the date part without time.
     */
    datePart: () => Date,

    addHours: (hours: number) => Date,
    addDays: (days: number) => Date,
    addSeconds: (seconds: number) => Date,
    addMillis: (seconds: number) => Date,
    // TODO: добавь сюда addMinutes, addSeconds, etc по надобности.

    toRULocalDate: () => string,

    /**
     * Differnce in seconds between this date and the specified one.
     */
    diffSeconds: (date: Date) => number,

    diffHours: (date: Date) => number,

    /**
     * Differnce in milliseconds between this date and the specified one.
     */
    diffMillis: (date: Date) => number,
}

interface IKeyValues {
    keys: string[],
    values: string[][],
}

export default class Helpers {

    static removeDuplicateSpaces(text: string): string {
        return text.replace(/\s+/g, ' ');
    }
    
    static guid(noHashes = false): string {
        if (noHashes) {
            return uuidv4().replace(/-/g, '');
        } else {
            return uuidv4();
        }
    }

    static snowflake(): string {
        return Math.floor(Math.pow(10, 17) + Math.random() * 9 * Math.pow(10, 17)).toString();
    }
    
    static getRndInteger(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    static doYouFeelLucky(probability: number): boolean {
        const val = Helpers.getRndInteger(0,100);
        return val < probability;
    }

    static getRandomPhrase(phrases: string[]): string {
        return phrases[Math.floor(Math.random() * phrases.length)];
    }

    static sortDescending<T>(items: T[], selector: (a: T) => unknown): T[] {
        return items.sort((a, b) => selector(a) > selector(b) ? -1 : 1);
    }

    static sortAscending<T>(items: T[], selector: (a: T) => unknown): T[] {
        return items.sort((a, b) => selector(a) < selector(b) ? -1 : 1);
    }

    static distinct<T>(items: T[]): T[] {
        return Array.from(new Set(items));
    }

    static utcDate(): Date {
        const date = new Date(); 
        const now_utc =  Date.UTC(
            date.getUTCFullYear(), 
            date.getUTCMonth(),
            date.getUTCDate(),
            date.getUTCHours(),
            date.getUTCMinutes(),
            date.getUTCSeconds());

        return new Date(now_utc);
    }

    static date(dt: number): IDateOptions;
    static date(dt: string): IDateOptions;
    static date(dt: Date): IDateOptions;
    static date(dt: unknown): IDateOptions {
        let d: Date;
        let dateTime: DateTime;
        if (typeof dt === 'string') {
            d = new Date(dt);
            dateTime = DateTime.fromISO(<string>dt);
        } else if (typeof dt === 'number') {
            dateTime = DateTime.fromMillis(+dt);
        } else {
            dateTime = DateTime.fromJSDate(<Date>dt);
            d = <Date>dt;
        }

        return {
            equals: (another: Date): boolean => {
                return dateTime.equals(DateTime.fromJSDate(another));
            },
            datePart: () => dateTime.startOf("day").toJSDate(),
            addHours: (hours: number): Date => {
                return dateTime.plus({hours: hours}).toJSDate();
            },
            addSeconds: (seconds: number): Date => {
                return dateTime.plus({seconds: seconds}).toJSDate();
            },
            addMillis: (millis: number): Date => {
                return dateTime.plus({milliseconds: millis}).toJSDate();
            },
            addDays: (days: number): Date => {
                return dateTime.plus({days: days}).toJSDate();
            },
            toRULocalDate: (): string => {
                return d.toLocaleDateString(
                    'ru-RU',
                    { year: 'numeric', month: 'long', day: 'numeric' });
            },
            diffSeconds: (date: Date): number => {
                return dateTime.diff(DateTime.fromJSDate(date), ["seconds"]).seconds;
            },
            diffHours: (date: Date): number => {
                return dateTime.diff(DateTime.fromJSDate(date), ["hours"]).hours;
            },
            diffMillis: (date: Date): number => {
                return dateTime.diff(DateTime.fromJSDate(date), ["milliseconds"]).milliseconds;
            },
        }
    }

    static removeItem<T>(array: T[], selector: (x: T) => boolean): T {
        const index = array.findIndex(item => selector(item));
        if (index > -1) {
            const removed = array.splice(index, 1);
            if (removed.length > 0) {
                return removed[0];
            }
        }

        return undefined;
    }

    static get isProduction(): boolean {
        return process.env.NODE_ENV === 'production';
    }

    static formatDuration(durationMillis: number): string {
        function __getTimeString(number: number, variants: { one: string, two: string, many: string }) {
            const hun = number % 100;
            const dec = number % 10;
    
            if (hun >= 11 && hun <= 14 || dec === 0) {
                return variants.many; // x[11,14], x0
            }
            if (dec === 1) {
                return variants.one; // x1
            }
            if (dec <= 4) {
                return variants.two; // x[2,4]
            }
            return variants.many; // x[5,9]
        }
    
        function _getTimeString(number: number, variants: { one: string, two: string, many: string }) {
            if (!Number.isInteger(number)) {
                return '';
            }
            return `${number} ${__getTimeString(number, variants)}`;
        }
    
        function getMinutesString(number: number) {
            return _getTimeString(number, {
                one: 'минута',
                two: 'минуты',
                many: 'минут'
            });
        }
    
        function getHoursString(number: number) {
            return _getTimeString(number, {
                one: 'час',
                two: 'часа',
                many: 'часов'
            });
        }
    
        let hours = Math.floor(durationMillis / 1000 / 60 / 60);
        let minutes = Math.round(durationMillis / 1000 / 60 % 60);
        if (minutes === 60) {
            hours += 1;
            minutes = 0;
        }

        let result: string;
        if (hours > 0 && minutes > 0) {
            result = `${getHoursString(hours)} ${getMinutesString(minutes)}`;
        } else if (hours > 0) {
            result = `${getHoursString(hours)}`;
        } else {
            result = `${getMinutesString(minutes)}`;
        }

        return result;
    }

    static getKeyValues<T>(entries: T[]): IKeyValues {
        const keys: string[] = [];
        const values: string[][] = [];

        if (entries.length > 0) {
            for (const [key] of Object.entries(entries[0])) {
                keys.push(key);
            }
        }
        for (const entry of entries) {
            values.push(keys.map(x => entry[x]));
        }

        return {
            keys: keys,
            values: values,
        };
    }

    static splitArray<T>(array: T[], size: number): T[][] {
        const chunks: T[][] = [];

        if (size > 0) {
            let chunk:T[] = [];
            for(let i = 0; i < array.length; i++) {
                chunk.push(array[i]);
                if ((i + 1) % size === 0) {
                    chunks.push(chunk);
                    chunk = [];
                }
            }
    
            if (chunk.length > 0) {
                chunks.push(chunk);
            }
        } else {
            chunks.push(array);
        }

        return chunks;
    }

    /**
     * Executes promises in sequence.
     * @param executions 
     */
    static inSequence(executions: {(): Promise<void> }[]): Promise<void> {
        return executions.reduce(
            (prev, curr) => new Promise(
                (resolve, reject) => prev.then(() => resolve(curr())).catch(err => reject(err))), Promise.resolve());
    }

    /**
     * Waits.
     * @param millis milliseconds to wait
     */
    static wait(millis: number): Promise<void> {
        return new Promise(resolve => setTimeout(() => resolve(), millis));
    }

    /**
     * Shuffles the specified array.
     * @param array
     */
    static shuffle(array: unknown[]): void {
        for (let i = array.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            const temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }
    }

    /**
     * Slice a portion from the specified array, bounded by the left & right offsets from the
     * item whose index is governed by the selector function.
     */
    static sliceAround<T>(array: T[], selector: (x: T) => boolean, leftOffset?: number, rightOffset?: number): T[] {
        const centreIndex = array.findIndex(i => selector(i));
        if (centreIndex === -1) {
            return array;
        }

        let startIndex: number;
        let endIndex: number;
        if (leftOffset && leftOffset > 0) {
            startIndex = centreIndex - leftOffset;
            if (startIndex < 0) {
                startIndex = 0;
            }
        }

        if (rightOffset && rightOffset > 0) {
            endIndex = centreIndex + rightOffset + 1;
        }

        return array.slice(startIndex, endIndex);
    }
}
