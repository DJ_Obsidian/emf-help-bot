import * as Discord from 'discord.js'
import { embedColor } from './colors';

function pressFMessage(): Discord.MessageEmbed {
    return new Discord.MessageEmbed()
        .setColor(embedColor)
        .setImage('https://pbs.twimg.com/media/D-5sUKNXYAA5K9l.jpg');
}

export function getPressFResponse(senderDisplayName: string, recipientUserId?: string): unknown {
    const embed = pressFMessage();

    if(!recipientUserId) {
        embed.setDescription(`**${senderDisplayName}** заплатил увожение. o7`);
    } else {
        embed.setDescription(`**${senderDisplayName}** заплатил увожение за <@${recipientUserId}>. o7`);
    }

    return embed;
}

export function getGenericPressFResponse(message: string): unknown {
    const embed = pressFMessage();
    embed.setDescription(message);
    return embed;
}