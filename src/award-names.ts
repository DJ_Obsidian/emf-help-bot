

export enum AwardNames {
    medal = 1,
    medal1,
    medal2,
    medal3,
    protection,
    valor,
    victory,
    hunt,
}