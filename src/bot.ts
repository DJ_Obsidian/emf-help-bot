import { AccessLevels } from "./access-levels";
import { Arks } from "./arks";
import { AwardNames } from "./award-names";
import { Awards } from "./awards";
import { BgsClient } from "./bgs-client";
import { ChannelNames } from "./channel-names";
import { IBanCommand, ICommand, IKickCommand, INewFagCommand } from "./command";
import { ISetActivityArgs } from "./command-args";
import { CommandProcessor } from "./command-processor";
import { CommandValues } from "./command-values";
import { Cooldown, ICooldown } from "./cooldown";
import { CooldownManager } from "./cooldown-manager";
import { DbContext, DbContextProvider, getDBConnectionInfo } from "./db/context";
import { IChannel } from "./db/entities/channel";
import { IRole } from "./db/entities/role";
import { IRoleUser } from "./db/entities/role-user";
import { IUser } from "./db/entities/user";
import { DC } from "./discord-channels";
import { IDiscordChannelMessageEventOptions, IDiscordDMEventOptions, IDiscordMemberAddEventOptions, IDiscordMemberRemoveEventOptions, IDiscordMessageEventOptions, IDiscordReadyEventOptions, IDiscordRoleUpdateEventOptions, IDiscordTypingStartEventOptions, IDiscordUserUpdateEventOptions, IDiscordVoiceStateUpdateEventOptions } from "./discord-event";
import { DiscordServer } from "./discord-server";
import FileHelper from "./file-helper";
import Helpers from "./helpers";
import Logger from "./logger";
import LongCooldownManager from "./long-cooldown-manager";
import messages from "./messages";
import { MusicAutoDisconnectTimedInvoker } from "./music/music-auto-disconnect-invoker";
import { ISessionItem } from "./music/music-play-session";
import { MusicPlayer } from "./music/music-player";
import { getGenericPressFResponse } from "./press-f-response";
import { IRssFeed } from "./rss/rss-feed";
import RssFeedGetter from "./rss/rss-feed-getter";
import { IRssFeedSub } from "./rss/rss-feed-sub";
import { SettingsHelper } from "./settings-helper";
import { TaskNames } from "./task-names";
import { ISyncUsersTaskObject } from "./task-objects";
import { IProcessTask, TaskProcessor } from "./task-processor";
import { TaskScheduler } from "./task-scheduler";

export default class EmfBot {

    private _activityCheckCooldowns = new Map<string, ICooldown>();

    private _commandCooldownTime = 5; // 5 sec.
    private _maxBanTime = 24 * 31; // Maximum ban time in hours // TODO - make configurable
    
    private _tickFrequency = 60 * 10; // 10 minutes.

    private _restartingProcesses = false;
    
    pressFCoolDownTime = 30 * 60; // 30 mins
    saluteCoolDownTime = 30 * 60; // 30 mins
    shortMsgDeleteTime = 10; // Seconds // TODO - make configurable
    defaultActivity = "BGS Autism";

    constructor(public server: DiscordServer,
        public commandProc: CommandProcessor,
        public taskScheduler: TaskScheduler,
        public taskProcessor: TaskProcessor,
        public bgsClient: BgsClient,
        public cooldownManager: CooldownManager,
        public dbContextProvider: DbContextProvider,
        public longCooldownManager: LongCooldownManager,
        public awards: Awards,
        public arks: Arks,
        public logger: Logger,
        public settingsHelper: SettingsHelper,
        public fileHelper: FileHelper,
        public musicPlayer: MusicPlayer,
        private madi: MusicAutoDisconnectTimedInvoker,
        private rss: RssFeedGetter) {

        // Set up ready event
        server.onReady((args, db) => this.ready(args, db));

        // Set up member add event - gets invoked when a new member enters Discord server for the first time.
        server.onGuildMemberAdd((args) => this.guildMemberAdd(args));

        // Set up member remove event -gets invoked when a member leaves or is kicked.
        server.onGuildMemberRemove((args) => this.guildMemberRemove(args));
        
        // Hook up messaging
        server.onMessage((args, db) => this.message(args, db));
        server.onDM((args, db) => this.dm(args, db));

        // Hook member state changes
        server.onTypingStart(args => this.typingStart(args));
        server.onVoiceStatusUpdate(args => this.voiceStatusUpdate(args));

        server.onRoleCreate(args => this.roleCreate(args));
        server.onRoleDelete(args => this.roleDelete(args));
        server.onRoleUpdate(args => this.roleUpdate(args));

        server.onUserUpdate(args => this.userUpdate(args));

        // Hook up task processor event
        this.initMainTaskProcessor();

        // Music player events
        this.musicPlayer.on("start", (s: ISessionItem) => this.onPlayItemStart(s));
        this.musicPlayer.on("end", () => this.onPlaySessionEnd());
        this.musicPlayer.on("queue", (s: ISessionItem) => this.onItemQueued(s));
        this.server.on("botvoicedisconnect", (channelId: string) => {
            this.logger.log(`Voice disconnect from channel ${channelId}`);
            this.musicPlayer.endSession(channelId, true);
        });
        this.server.on("botaloneinchannel", (channelId: string) => {
            this.onBotAloneInChannel(channelId).catch(error => this.logger.error(error));
        });

        // RSS
        this.configureRssFeed();
    }

    initMainTaskProcessor(): void {
        this.taskProcessor.onTaskProcess((task, db) => this.processTask(task, db));
    }

    async setUpScheduledTasks(): Promise<void> {
        //await this.taskScheduler.load(); // TODO:
        this.taskScheduler.onTaskExecute = (task: IProcessTask) => this.processTask(task, null);
        this.taskScheduler.addTask({
            name: 'tick',
            data: this,
            everySeconds: this._tickFrequency,
            execute: (bot: EmfBot) => { return bot.onTick(); },
        });
        this.taskScheduler.addTask({
            name: 'UnbanUsers',
            data: this,
            everySeconds: 120,
            execute: (bot: EmfBot) => { return bot.unbanPending(); },
        });
        this.taskScheduler.addTask({
            name: 'SendMembersToZoo',
            data: this,
            everySeconds: 120,
            execute: (bot: EmfBot) => { return bot.sendMembersToTheZoo(); },
        });
        this.taskScheduler.addTask({
            name: 'RemoveZooMembers',
            data: this,
            everySeconds: 180,
            execute: (bot: EmfBot) => { return bot.removeZooMembersFromServerTask(); },
        });
        this.taskScheduler.addTask({
            name: 'CleanUpLogFiles',
            data: this.logger,
            everySeconds: 60 * 60,
            execute: (logger: Logger) => { return logger.deleteOldFiles(); },
        });
        this.taskScheduler.addTask({
            name: 'Stats',
            data: this,
            everySeconds: 60 * 10,
            execute: (bot: EmfBot) => { 
                bot.stats();
                return Promise.resolve();
            },
        });
        this.taskScheduler.addTask({
            name: 'CleanUsers',
            data: this,
            everySeconds: 60,
            execute: (bot: EmfBot) => { return bot.deleteDisabledUsers(); },
        });
        this.taskScheduler.addTask({
            name: 'CheckMADI',
            data: this,
            everySeconds: 60,
            execute: (bot: EmfBot) => Promise.resolve(bot.madiStatus()),
        });
        this.taskScheduler.addTask({
            name: 'MusicStatus',
            data: this,
            everySeconds: 60,
            execute: (bot: EmfBot) => Promise.resolve(bot.musicPlayerStatus()),
        });
        if (this.settingsHelper.settings.rssSettings) {
            const everyMinutes = this.settingsHelper.settings.rssSettings.everyMinutes;
            this.taskScheduler.addTask({
                name: "RSS",
                data: this,
                everySeconds: everyMinutes ? everyMinutes * 60 : 3600,
                execute: (bot: EmfBot) => {
                    if (this.server.botHasRole(this.settingsHelper.settings.rssSettings.roleid)) {
                        return bot.rss.getFeeds();
                    }

                    return Promise.resolve();
                },
            });
        }

        // this.taskScheduler.addTask({
        //     name: 'WipeReception',
        //     everyHours: this.settingsHelper.settings.wipeReceptionEveryHours,
        //     task: TaskNames.wipeReception,
        // });
    }

    async start(): Promise<void> {
        await this.server.start();
    }

    async ready(args: IDiscordReadyEventOptions, db: DbContext): Promise<void> {
        await args.setActivity(this.defaultActivity);
        this.logger.log("<<<<<<Connected to Discord>>>>>>");

        // Load channels from DB
        const channels = await db.channelRepo.getAll();
        if (channels.length === 0) {
            throw new Error('Failed to load channels from DB. Shutting down.');
        }

        DC.channels(channels);

        this.logger.log(`Channels loaded from DB:\n${channels.map(c => `    ${ChannelNames[c.id]} - ${c.channelid}`).join('\n')}`);
        await this.verifyChannels(channels);

        const tables = await db.execute<unknown[]>("show tables");
        this.logger.log(`DB tables:\n${tables.map(m => `    ${Object.entries(m)[0][1]}`).join('\n')}`);

        // Long cooldowns
        await this.longCooldownManager.loadCooldowns();
        this.logger.log(`Loaded ${this.longCooldownManager.count} long cooldowns.`);

        // Medals
        await this.awards.loadMedals(db);
        this.logger.log(`Loaded medals:\n${this.awards.medals.map(m => `    ${AwardNames[m.id]}: ${m.name} - ${m.file}`).join('\n')}`);

        // Arks
        await this.arks.load(db);

        // Loads tasks with long intervals
        await this.setUpScheduledTasks();

        
        // Start main loop
        this.server.on("activity", () => this.checkHeartbeat());
        this.taskScheduler.start();
    }

    private async verifyChannels(channels: IChannel[]): Promise<void> {
        this.logger.log('Verifying existence of loaded channels on the server.');
        for (const channel of channels.filter(c => c.id !== ChannelNames.unspecified)) {
            if (!this.server.channelExists(channel.channelid)) {
                throw new Error(`Channel ${ChannelNames[channel.id]} - ${channel.channelid} does not exist on Discord.`);
            }
        }
    }

    private isUserTooQuick(options: IDiscordMessageEventOptions): boolean {
        const tooQuickPhrases = ["Слишком часто просишь.", "Ты такой быстрый. ^-^", "Помедленее семпай. ^-^", "Speed restrictions are currently in place. Please comply."];
        if (this.cooldownManager.has(options.authorId)) {
            options.send(Helpers.getRandomPhrase(tooQuickPhrases), this.shortMsgDeleteTime);
            return true;
        } else {
            this.cooldownManager.add({ userid: options.authorId, seconds: this._commandCooldownTime });
            return false;
        }
    }

    async message(options: IDiscordChannelMessageEventOptions, db: DbContext): Promise<void> {
        this.addPresenseUpdateTask(options.memberId, options.authorDisplayName);

        const command: ICommand = this.commandProc.parse(options.message);
        if (command) {
            if (this.isUserTooQuick(options)) {
                return;
            }

            const userLog = `${options.authorDisplayName} (${options.authorId})`;
            this.logger.log(`${userLog} invoked a command ${CommandValues[command.id]}.`);

            // Check if user is allowed to use this command
            // Check if user is OS
            if (command.osOnly === true && this.settingsHelper.settings.osRoleId &&
                !options.hasRole(this.settingsHelper.settings.osRoleId)) {
                this.logger.log(`${CommandValues[command.id]} is OS-only and ${userLog} is not an OS member so cannot use the command.`);
                options.deleteMessage(10);
                options.send(messages.insufficientPermissions(), this.shortMsgDeleteTime);
                return;
            }

            // Check if user's level is high enough to use this command
            const userLevel = options.getHighestUserLevel();
            if (command.level > userLevel) {
                this.logger.log(`${userLog} does not have sufficient level to use ${CommandValues[command.id]}.`);
                // Нельзя!!!
                options.deleteMessage(10);
                options.send(messages.insufficientPermissions(), this.shortMsgDeleteTime);
                return;
            }

            command.userLevel = userLevel;

            // Check if this command is allowed in this channel
            if (command.channel) {
                const channels = Array.isArray(command.channel) ? <ChannelNames[]>command.channel : [<ChannelNames>command.channel];
                if (channels.indexOf(DC.getName(options.channelId)) === -1) {
                    options.send(`Команда работает только в ${channels.map(c => `<#${DC.getId(c)}>`).join(', ')}`, this.shortMsgDeleteTime);
                    options.deleteMessage(10);
                    return;
                }
            }

            if (command.execute) {
                if (command.args) {
                    await command.execute(this, options, db);
                } else {
                    if (command.instructions) {
                        await options.send(command.instructions, this.shortMsgDeleteTime);
                    } else {
                        await options.send(`Неправильное введение команды. Используй !помощь, чтоб посмотреть, как пользоваться.`, this.shortMsgDeleteTime)
                    }
                }
                return;
            } 

            switch (command.id) {
                case CommandValues.importAwards:
                    {
                        await this.importExistingMedals(options);
                    }
                    break;
                case CommandValues.newfags:
                    {
                        await this.newfags(<INewFagCommand>command, options);
                    }
                    break;
                case CommandValues.prune:
                    {
                        await this.prune(command, options);
                    }
                    break;
                case CommandValues.ban: // БАН
                    {
                        await this.ban(<IBanCommand>command, options, db);
                    }
                    break;
                case CommandValues.unban: // РАЗБАН
                    {
                        await this.unban(options);
                    }
                    break;
                case CommandValues.kick:
                    {
                        await this.kick(<IKickCommand>command, options);
                    }
                    break;
                case CommandValues.bgs:
                    {
                        await this.bgs(command, options);
                    }
                    break;
                case CommandValues.tick:
                    {
                        await this.processTick(options);
                    }
                    break;
                case CommandValues.member:
                    {
                        await this.member(options);
                    }
                    break;
                case CommandValues.syncUsers:
                    {
                        await this.syncUsers(options);
                    }
                    break;
                case CommandValues.syncRoles:
                    {
                        await this.syncRoles(options);
                    }
                    break;
                case CommandValues.syncUserRoles:
                    {
                        await this.syncUserRoles(options);
                    }
                    break;
                case CommandValues.roleinfo:
                    {
                        await this.showRoleInfo(command, options);
                    }
                    break;
                case CommandValues.serverinfo:
                    {
                        await this.showServerInfo(options);
                    }
                    break;
                case CommandValues.setBotActivity:
                    {
                        await this.setBotActivity(command, options);
                    }
                    break;
                case CommandValues.wipeReception:
                    {
                        await this.wipeReception(options);
                    }
                    break;
                case CommandValues.zoo:
                    {
                        await this.zoo(command, options);
                    }
                    break;
                case CommandValues.os:
                    {
                        await this.os(command, options);
                    }
                    break;
                default:
                    options.deleteMessage(this.shortMsgDeleteTime);
                    options.send('Еще не сделано', this.shortMsgDeleteTime);
                    break;
            }

            return;
        }
    }

    async dm(options: IDiscordDMEventOptions, db: DbContext): Promise<void> {
        // TODO:

        // if (this.isUserTooQuick(options)) {
        //     return;
        // }

        // const command: ICommand = this.commandProc.parse(options.message);
        // if (command) {
        //     // Check if user is allowed to use this command.
        //     const userLevel = await options.getHighestUserLevel();
        //     if (!userLevel || command.level > userLevel) {
        //         return;
        //     }

        //     if (command.execute) {
        //         if (command.args) {
        //             await command.execute(this, options, db);
        //         } else {
        //             await options.send(`Неправельное введение команды. Используй !помощь, чтоб посмотреть, как пользоваться.`)
        //         }
        //         return;
        //     } 
        // }
    }

    async guildMemberAdd(options: IDiscordMemberAddEventOptions): Promise<void> {
        this.taskProcessor.addTask({
            name: TaskNames.guildMemberAdd,
            data: {
                userid: options.memberId,
                usernametag: options.userTag,
            },
        });
    }

    async guildMemberAddTask(memberid: string, usernametag: string, db: DbContext): Promise<void> {
        if (await this.server.isBot(memberid)) {
            this.logger.log(`Member ${memberid} - ${usernametag} is actually a bot. No user added.`);
            return;
        }

        const user = await db.userRepo.getById(memberid);
        if (!user) {
            // New user
            await this.server.sendMessage(
                messages.greeting(memberid, DC.getId(ChannelNames.serverInfo), DC.getId(ChannelNames.serverRules)),
                ChannelNames.reception);

            this.taskProcessor.addTask({
                name: TaskNames.addUser,
                data: <IUser>{
                    userid: memberid,
                    usernametag: usernametag,
                },
            });

            this.taskProcessor.addTask({
                name: TaskNames.addStrangerRoleToNewUser,
                data: <IUser>{
                    userid: memberid,
                    usernametag: usernametag,
                },
            });
        } else {
            const returnedPhrases = [' не смог покинуть EMF', ', добро прожаловать, снова!', ', ну и где ты пропадал?', ' зашёл. Oh shit, here we go again'];
            const response = `<@${memberid}>${Helpers.getRandomPhrase(returnedPhrases)}`;
            await this.server.sendMessage(response, ChannelNames.reception);
            
            // Unsoftdelete user
            this.taskProcessor.addTask({
                name: TaskNames.unsoftDeleteUser,
                data: {
                    userid: memberid,
                },
            });

            this.taskProcessor.addTask({
                name: TaskNames.readdRolesToMember,
                data: {
                    userid: memberid,
                }
            });
        }
    }

    async readdRolesToMemberTask(userid: string, db: DbContext): Promise<void> {
        // Remove zoo role from db first.
        await db.roleRepo.deleteManyRoleUsers([{ userid: userid, roleid: this.settingsHelper.settings.zooRoleId }]);

        const roles = await db.roleRepo.getUserRoles(userid);
        await this.server.setRolesToMember(userid, roles.map(r => r.roleid));
    }

    async addStangerRoleToNewUser(user: {userid: string; usernametag: string}): Promise<void> {
        if (this.settingsHelper.settings.strangerRoleId) {
            if (this.server.roleExists(this.settingsHelper.settings.strangerRoleId)) {
                this.logger.log(`Adding stanger role (${this.settingsHelper.settings.strangerRoleId}) to user ${user.usernametag} (${user.userid})`);
                this.server.setRolesToMember(user.userid, [this.settingsHelper.settings.strangerRoleId]);
            } else {
                this.logger.log(`Stanger role with id ${this.settingsHelper.settings.strangerRoleId} does not exist.`);
            }
        }
    }

    async guildMemberRemove(options: IDiscordMemberRemoveEventOptions): Promise<void> {
        this.taskProcessor.addTask({
            name: TaskNames.softDeleteUser,
            data: {
                userid: options.memberId,
                usernametag: options.memberName
            },
        });

        // Post a message F in the chat for this member if they have left
        const isBanned = await this.server.isUserBanned(options.memberId);
        if (isBanned) {
            this.logger.log(`${options.memberName}: ${options.memberId} is banned.`);
            return;
        }
        
        try {
            const kickAuditLog = await this.server.getKickAuditLog(options.memberId);
            if (!kickAuditLog || Helpers.date(kickAuditLog.createdAt).addSeconds(60) < new Date()) {
                this.logger.log(`${options.memberName}: ${options.memberId} has left on their own\n     Audit log ${kickAuditLog ? kickAuditLog.createdAt.toString() : ''}.`);
                const message: unknown = getGenericPressFResponse(`${options.memberName} ушел с сервера. Помяним.`);
                await this.server.sendMessage(message, ChannelNames.general);
            }
        } catch (error) {
            this.logger.error(error);
        }
    }

    private addPresenseUpdateTask(memberId: string, memberName: string): void {
        if (!this._activityCheckCooldowns.has(memberId)) {
            const cooldowns = this._activityCheckCooldowns;
            const cooldown = new Cooldown(60 * 60 * 24); // 24 hours
            cooldown.deleteCallback = () => cooldowns.delete(memberId);
            cooldowns.set(memberId, cooldown);
            this.taskProcessor.addTask({
                name: TaskNames.updateUserPresence,
                data: {
                    userid: memberId,
                    usernametag: memberName,
                },
            });
        }
    }

    async sendMembersToTheZoo(): Promise<void> {
        if (!await this.server.getRole(this.settingsHelper.settings.zooRoleId) 
            || !await this.server.getRole(this.settingsHelper.settings.zooKeeperRoleId)) {
            return; // No roles configured. No point in continuing.
        }

        if (!this.server.isZooKeepingTurnedOn) {
            return;
        }

        return this.dbContextProvider.execute(async db => {
            let users = await db.userRepo.getInactiveUsers(
                this.settingsHelper.settings.allowedInactivityDays,
                [
                    this.settingsHelper.settings.zooRoleId,
                    this.settingsHelper.settings.zooKeeperRoleId,
                    this.server.getRoleIdForAccessLevel(AccessLevels.Leader),
                    this.server.getRoleIdForAccessLevel(AccessLevels.Admin),
                    this.server.getRoleIdForAccessLevel(AccessLevels.Fuhrer),
                ].concat(this.settingsHelper.settings.zooExemptionRoleIds));

            const noBots = []; // Exclude bots
            for (const user of users) {
                const isBot = await this.server.isBot(user.userid);
                if (!isBot) {
                    noBots.push(user);
                }

                if (!await this.server.userExists(user.userid)) {
                    this.logger.log(`User ${user.userid} - ${user.usernametag} does not exist on server. Deleting from DB...`);
                    await db.userRepo.delete(user.userid);
                }
            }

            users = noBots;

            if (users.length > 0) {
                this.logger.log(`Found ${users.length} users to send to the zoo.`);
            }

            for (const user of users) {
                const hasRole = await this.server.userHasRole(user.userid, this.settingsHelper.settings.zooRoleId);
                if (!hasRole) {
                    this.logger.log(`Adding role ${this.settingsHelper.settings.zooRoleId} to user ${user.userid}: ${user.usernametag}`);
                    const memberRoles = await this.server.getMemberRoles(user.userid);
                    await this.server.removeRolesFromMember(user.userid, memberRoles.map(r => r.id));
                    await this.server.setRolesToMember(user.userid, [this.settingsHelper.settings.zooRoleId]);
                } else {
                    this.logger.log(`User ${user.userid}: ${user.usernametag} has already a zoo role ${this.settingsHelper.settings.zooRoleId} assigned. Updating DB.`);
                    await db.roleRepo.insertManyRoleUsers([{ userid: user.userid, roleid: this.settingsHelper.settings.zooRoleId, created: new Date() }]);
                }
            }
        });
    }

    async updateUserPresenceTask(userid: string, username: string, db: DbContext): Promise<void> {
        if (await this.server.isBot(userid)) {
            this.logger.log(`Member ${userid} - ${username} is actually a bot. No presence updated.`);
            return;
        }

        await db.userRepo.insert({
            userid: userid,
            usernametag: username,
        });
        await db.userRepo.updatePresence(userid, new Date());
    }

    async softDeleteUserTask(userid: string, username: string, db: DbContext): Promise<void> {
        this.logger.log(`User ${username} has been deactivated.`);
        await db.userRepo.softDeleteUser(userid, new Date());
    }

    /**
     * Multi-step task that gets zoo members and then removes them one by one from this server.
     * Sends report to admin.
     * @param users 
     */
    async removeZooMembersFromServerTask(users: {user: IUser, isRemoved: boolean}[] = []): Promise<void> {
        if (!this.server.isZooKeepingTurnedOn) {
            return;
        }

        if (users.length > 0) {
            // 2) Remove member one at a time from server. Perform this step as many times as there are members due.
            const notRemovedYet = users.filter(u => !u.isRemoved);
            if (notRemovedYet.length > 0) {
                const user = notRemovedYet[0];

                // Check just in case that the user still has zoo roleid
                if (!await this.server.userHasRole(user.user.userid, this.settingsHelper.settings.zooRoleId)) {
                    // User does not have zoo role in Discord anymore. Update DB and terminate the process.
                    this.logger.log(`User ${user.user.userid} appears to still have zoo role in Discord. Update DB.`);
                    await this.dbContextProvider.execute(db => db.roleRepo.deleteManyRoleUsers([
                        { userid: user.user.userid, roleid: this.settingsHelper.settings.zooRoleId }
                    ]));

                    return;
                }

                // Kick user
                const r = await this.server.kickUser(user.user.userid, `Был удален за долгое присутсвие в зоопарке.`);
                if (!r.exists) {
                    this.taskProcessor.addTask({
                        name: TaskNames.deleteUser,
                        data: {
                            userid: user.user.userid,
                        },
                    });
                } else {
                    await this.memberKicked(user.user);
                }

                this.logger.log(`Member ${user.user.userid}-${user.user.usernametag} has been kicked.`);
                user.isRemoved = true;
                this.taskProcessor.addTask({
                    name: TaskNames.removeZooUsers,
                    data: {
                        users: users,
                    },
                });
            } else {
                // 3) Send report to members with the specific role.
                const membersToSend = await this.server.getMembersByRole(this.settingsHelper.settings.zooKeeperRoleId);
                for (const userid of membersToSend.map(m => m.id)) {
                    this.taskProcessor.addTask({
                        name: TaskNames.sendRemovedUsersReport,
                        data: {
                            users: users.map(u => u.user),
                            recipientid: userid,
                        }
                    });
                }
            }
        } else {
            // 1) Get members that need to be banned.
            await this.dbContextProvider.execute(async db => {
                const users = await db.userRepo.getUsersWithRoleAfterDays(
                    this.settingsHelper.settings.zooRoleId,
                    this.settingsHelper.settings.allowedZooInactivityDays);
                
                if (users.length > 0) {
                    this.logger.log(`Found ${users.length} to kick.`);
                    this.taskProcessor.addTask({
                        name: TaskNames.removeZooUsers,
                        data: {
                            users: users.map(u => <{user: IUser, isRemoved: boolean}>{user: u, isRemoved: false}),
                        },
                    });
                }
            });
        }
    }

    private async sendKickedMemberNotificationTask(user: {userid: string; usernametag: string}): Promise<void> {
        await this.server.sendPMToNonMember(
            `Уведомление! ${user.usernametag} в связи с тем, что вы не отреагировали на сообщение о роли "ЗООПАРК", очень жаль, нам пришлось попрощаться с вами. `
            + 'У вас есть возможность вернуться снова на сервер по ссылке - https://discord.gg/mVr9gBv ...  Ждем Вас снова у нас в гостях!',
            user.userid
        );
        this.logger.log(`Sent a notification to ${user.usernametag} (${user.userid}) that they have been kicked.`);
    }

    async sendReportOfRemovedUsersTask(users: IUser[], recipientid: string): Promise<void> {
        if (users.length > 0) {
            const message = `Кикнула вот ${users.length === 1 ? 'этого' : 'этих'}:\n${users.map(u => `- ${u.usernametag}`).join('\n')}`;
            await this.server.sendPrivateMessage(message, recipientid);
        }
    }

    async typingStart(options: IDiscordTypingStartEventOptions): Promise<void> {
        const enterPhrases = ["нажми энтер уже", "ты что там сочинение пишешь?"];
        // TODO
    }

    async voiceStatusUpdate(options: IDiscordVoiceStateUpdateEventOptions): Promise<void> {
        this.addPresenseUpdateTask(options.memberId, options.memberName);
    }

    roleCreate(options: IDiscordRoleUpdateEventOptions): Promise<void> {
        this.logger.log(`Adding ${TaskNames[TaskNames.roleCreate]} task for role - ${options.id}: ${options.name} (${options.color})`);
        this.taskProcessor.addTask({
            name: TaskNames.roleCreate,
            data: {
                role: {id: options.id, name: options.name, color: options.color},
            },
        });
        return Promise.resolve();
    }

    async roleCreateTask(role: {id: string, name: string, color: string}, db: DbContext): Promise<void> {
        await db.roleRepo.insert({
            roleid: role.id,
            name: role.name,
            color: role.color,
        });
    }

    roleDelete(options: IDiscordRoleUpdateEventOptions): Promise<void> {
        this.logger.log(`Adding ${TaskNames[TaskNames.roleDelete]} task for role- ${options.id}: ${options.name}`);
        this.taskProcessor.addTask({
            name: TaskNames.roleDelete,
            data: {
                role: {id: options.id, name: options.name},
            },
        });
        return Promise.resolve();
    }
    
    roleUpdate(options: IDiscordRoleUpdateEventOptions): Promise<void> {
        if (options.oldRole && options.id === options.oldRole.id && options.name === options.oldRole.name
            && options.color === options.oldRole.color) {
            return Promise.resolve();
        }
        
        this.logger.log(`Adding ${TaskNames[TaskNames.roleUpdate]} task for role - ${options.id}: ${options.name} (${options.color})`);
        this.taskProcessor.addTask({
            name: TaskNames.roleUpdate,
            data: {
                role: {id: options.id, name: options.name, color: options.color},
            },
        });
        return Promise.resolve();
    }

    async roleUpdateTask(role: {id: string, name: string, color: string}, db: DbContext): Promise<void> {
        await db.roleRepo.update({
            roleid: role.id,
            name: role.name,
            color: role.color,
        });
    }

    userUpdate(options: IDiscordUserUpdateEventOptions): Promise<void> {
        this.logger.log(`Adding ${TaskNames[TaskNames.userUpdate]} task for user - ${options.id}: ${options.name}`);
        this.taskProcessor.addTask({
            name: TaskNames.userUpdate,
            data: {
                user: {id: options.id, name: options.name},
                rolesAdded: options.rolesAdded,
                rolesRemoved: options.rolesRemoved,
            },
        });
        return Promise.resolve();
    }

    async userUpdateTask(user: IUser, rolesAdded: IRole[], rolesRemoved: IRole[], db: DbContext): Promise<void> {
        if (await this.server.isBot(user.userid)) {
            this.logger.log(`Member ${user.userid} - ${user.usernametag} is actually a bot. No update made.`);
            return;
        }

        await db.userRepo.insert(user);
        await db.userRepo.update(user);

        // Process roles added
        if (rolesAdded.length > 0) {
            const leaderRoles = [
                this.server.getRoleIdForAccessLevel(AccessLevels.Leader),
                this.server.getRoleIdForAccessLevel(AccessLevels.Admin),
                this.server.getRoleIdForAccessLevel(AccessLevels.Fuhrer),
                this.settingsHelper.settings.officerRoleId,
            ];

            let zooRoleBeingAdded = false;

            if (rolesAdded.findIndex(r => r.roleid === this.settingsHelper.settings.zooRoleId) !== -1) {
                this.logger.log(`User ${user.userid}: ${user.usernametag} has had the role ${this.settingsHelper.settings.zooRoleId} (zoo) added to them.`)
                // One of the roles added is zoo roleid, check if it is allowed to be added
                const isAdmin = await this.server.userHasAnyRoleOf(user.userid, leaderRoles);
                if (isAdmin) {
                    this.logger.log(`But user ${user.userid}: ${user.usernametag} has a high access level. Zoo role is not allowed to be added.`);
                    await this.server.removeRolesFromMember(user.userid, [this.settingsHelper.settings.zooRoleId]);

                    Helpers.removeItem(rolesAdded, x => x.roleid === this.settingsHelper.settings.zooRoleId);
                } else {
                    zooRoleBeingAdded = true;
                }
            }

            for (const addedRole of rolesAdded) {
                if (leaderRoles.indexOf(addedRole.roleid) !== -1) {
                    zooRoleBeingAdded = false;
                    this.logger.log(`User ${user.userid}: ${user.usernametag} cannot be in the zoo.`);
                    // This member cannot be in the zoo, so remove the zoo role if they have one.
                    await this.server.removeRolesFromMember(user.userid, [this.settingsHelper.settings.zooRoleId]);
                    break;
                }
            }

            const roleUsers = rolesAdded.map<IRoleUser>(r => <IRoleUser>{
                roleid: r.roleid,
                userid: user.userid,
                role: r,
                created: new Date(),
            });

            for (const roleUser of roleUsers) {
                if (roleUser.role.name === user.usernametag) {
                    // If role name is the same as user name then it has to be a personal role.
                    roleUser.isPersonalRole = true;
                    break;
                }
            }

            await db.roleRepo.insertManyRoleUsers(roleUsers);

            if (zooRoleBeingAdded) {
                // Zoo role has been added to a normal user, notify them about this
                await this.zooMemberRoleAssigned(user);
            }
        }

        // Process roles removed
        if (rolesRemoved.length > 0) {
            const personalRole = await db.roleRepo.getUserPersonalRole(user.userid);
            if (personalRole && rolesRemoved.findIndex(x => x.roleid === personalRole.roleid) !== -1) {
                // One of the roles removed is a personal role. Delete the role from the server
                this.logger.log(`One of the roles removed is a personal role ${personalRole.name} for user ${user.userid} (${user.usernametag}). Deleting this role from Guild.`);
                await this.server.deleteRoleFromGuild(personalRole.roleid);
            }

            await db.roleRepo.deleteManyRoleUsers(rolesRemoved.map<{userid: string;roleid: string}>(r => <{userid: string, roleid: string}>{
                userid: user.userid,
                roleid: r.roleid,
            }));

            if (rolesRemoved.findIndex(r => r.roleid === this.settingsHelper.settings.zooRoleId) !== -1) {
                this.logger.log(`User ${user.userid}: ${user.usernametag} has had the role ${this.settingsHelper.settings.zooRoleId} (zoo) removed from them.`);
                // Сняли зоопарк, обновляем дату присутствия
                this.taskProcessor.addTask({
                    name: TaskNames.updateUserPresence,
                    data: {
                        userid: user.userid,
                        usernametag: user.usernametag,
                    },
                });
            }
        }
    }

    // Gets invoked when a zoo role is assigned to a user.
    private zooMemberRoleAssigned(user: IUser): Promise<void> {
        this.taskProcessor.addTask({
            name: TaskNames.notifyAboutZoo,
            data: <IUser>{
                userid: user.userid,
                usernametag: user.usernametag,
            },
        });

        return Promise.resolve();
    }

    // Gets invoked when bot kicks the user
    private memberKicked(user: IUser): Promise<void> {
        // Send the user a notification about their being kicked
        this.taskProcessor.addTask({
            name: TaskNames.notifyAboutKick,
            data: <IUser>{
                userid: user.userid,
                usernametag: user.usernametag,
            },
        });

        return Promise.resolve();
    }

    private async sendZooMemberNotificationTask(user: {userid: string; usernametag: string}): Promise<void> {
        await this.server.sendPrivateMessage(
            `Уведомление! ${user.usernametag} вас переместили в ЗООПАРК в связи с тем, что вы не учавствовали долго в жизни нашей эскадрильи и не выходили на связь в чате и войсе на нашем сервере. `
            + `У вас еще есть ${this.settingsHelper.settings.allowedZooInactivityDays} дней на размышление и восстановление статуса на сервере. Ситуацию с ролью можно изменить, связавшись с администрацией.`,
            user.userid);

        this.logger.log(`Sent a notification to ${user.usernametag} (${user.userid}) that they have been placed into Zoo.`);
    }

    async ban(command: IBanCommand, options: IDiscordChannelMessageEventOptions, db: DbContext): Promise<void> {
        if (command.args) {
            const mentions = options.mentions();
            if (mentions.length === 1) {
                // Check if the user has already been banned.
                let user = await db.userRepo.getById(mentions[0].userid);
                if (!user) {
                    // User not in DB? Add them.
                    user = await db.userRepo.insert({
                        userid: mentions[0].userid,
                        usernametag: mentions[0].usernametag,
                    });
                }

                if (!command.args.isPerm) {
                    const hours = +command.args.hours;
                    if (isNaN(hours)) {
                        options.send('Ты что бака?', this.shortMsgDeleteTime);
                    } else if (hours > this._maxBanTime) {
                        options.send('Ты что его на всю жизнь хочешь забанить???', this.shortMsgDeleteTime);
                    } else {
                        const banUntil = Helpers.date(Helpers.utcDate()).addHours(hours);
                        if (user.banneduntil && user.banneduntil > banUntil) {
                            options.send(`Да успокойся ты, <@${mentions[0].userid}> уже в бане.`, this.shortMsgDeleteTime);
                        } else {
                            const userid = await options.ban();
                            options.send(`<@${userid}> забанен на ${hours} часов.`, this.shortMsgDeleteTime);
                            this.taskProcessor.addTask({
                                name: TaskNames.banUser,
                                data: {
                                    userid: userid, // User id of the member to ban
                                    banUntil: banUntil,
                                },
                            });
                        }
                    }
                } else {
                    if (options.isPermBannable(user.userid)) {
                        await options.banPerm(command.args.reason);
                        await options.send(`<@${user.userid}> забанен навсегда в ${options.guildName}`);
                    } else {
                        options.send("Не банится!", this.shortMsgDeleteTime); 
                    }
                }
            } else {
                options.send(`По одному давай!`, this.shortMsgDeleteTime);
            }
        } else {
            options.send(`Чего хочешь? 
                    Не понимаю. Напиши сначала кого, потом на сколько (в часах). 
                    Либо используй аргумент perm для бана на сервере.`,
                this.shortMsgDeleteTime);
        }

        options.deleteMessage(this.shortMsgDeleteTime);
    }

    async unban(options: IDiscordChannelMessageEventOptions): Promise<void> {
        const mentions = options.mentions();
        if (mentions.length === 1) {
            if (options.isBanned(mentions[0].userid)) {
                await options.unban();
                options.send(`<@${mentions[0].userid}> разбанен.`, this.shortMsgDeleteTime);

                this.taskProcessor.addTask({
                    name: TaskNames.unBanUser,
                    data: {
                        userid: mentions[0].userid,
                    },
                });
            } else {
                options.send(`<@${mentions[0].userid}> не был забанен.`, this.shortMsgDeleteTime);
            }
        } else {
            options.send('Чего хочешь? Не понимаю. Напиши кого разбанить то.', this.shortMsgDeleteTime);
        }

        options.deleteMessage(this.shortMsgDeleteTime);
    }

    async unbanPending(): Promise<void> {
        await this.dbContextProvider.execute(async db => {
            let users: IUser[];
            do {
                users = await db.userRepo.getExpiredBannedUsers(0, 10);
                if (users.length > 0) {
                    this.logger.log(`Found ${users.length} members to unban.`);
                }

                for (const user of users) {
                    this.logger.log(`Unbanning ${user.usernametag}...`);
                    await this.server.unbanUser(user.userid);
                    await db.userRepo.unbanUser(user.userid);
                    this.logger.log(`${user.usernametag} unbanned`);
                }
            } while (users.length > 0)
        });
    }

    /**
     * Delete last N messages in channel.
     * @param command 
     * @param options 
     */
    async prune(command: ICommand, options: IDiscordChannelMessageEventOptions): Promise<void> {
        await options.deleteMessage();

        if (command.args.length === 0) {
            options.send("Как я буду вилкой-то чистить?", this.shortMsgDeleteTime);
            return;
        }

        const n = parseInt(command.args[0]);
        if (isNaN(n)) {
            options.send("Ты чё, ебан?", this.shortMsgDeleteTime);
            return;
        }

        if(+n > 100){
            options.send("Слишком многого просишь.", this.shortMsgDeleteTime);
            return;
        }

        if (this.cooldownManager.has(null, CommandValues.prune)) {
            options.send("Подожди! Ещё не закончила чистить.", this.shortMsgDeleteTime);
            return;
        }

        const cd = this.cooldownManager.add({
            seconds: 60 * 60,
            command: CommandValues.prune,
        });

        await options.deleteMessages(+n);

        cd.delete();

        options.send(`Удалила ${n} сообщений.`, this.shortMsgDeleteTime);
    }

    async kick(command: IKickCommand, options: IDiscordChannelMessageEventOptions): Promise<void> {
        const mentions = options.mentions();
        if (mentions.length > 0) {
            const mention = mentions[0];
            let reason: string;
            if (command.args) {
                reason = command.args.reason;
            }

            if (!options.kickable(mention.userid)) {
                options.send(`<@${mention.userid}> кикнуть нельзя!`, this.shortMsgDeleteTime);
                return;
            }

            if (mention.userid === options.authorId) {
                await options.kick(options.authorId, reason);
                await options.send(`<@${options.authorId}> , кикнул сам себя. Можешь не возвращаться`);
            } else {
                await options.kick(mention.userid, reason);
                await options.send(`<@${mention.userid}> , пошёл нахуй с этого сервера`);
            }
        }
    }

    // TEMP
    async importExistingMedals(options: IDiscordChannelMessageEventOptions): Promise<void> {
        if (this.cooldownManager.has(null, CommandValues.importAwards)) {
            return;
        }

        const userMedals = await options.temp.getAwardMedalsFromMessages();
        if (userMedals.length > 0) {
            for (const userMedal of userMedals) {
                this.taskProcessor.addTask({
                    name: TaskNames.addUserAward,
                    data: {
                        medal: userMedal.medal,
                        userid: userMedal.userid,
                        description: userMedal.description,
                    },
                });
            }

            options.deleteMessage();
            options.send(userMedals.map(m => `User <@${m.userid}>: ${m.medal}`).join('\n') + '\nwill be imported.', this._commandCooldownTime);
            this.cooldownManager.add({
                command: CommandValues.importAwards,
                seconds: 60 * 60 * 24 * 10,
            });
        } else {
            options.send("no medals");
        }
    }

    async newfags(command: INewFagCommand, options: IDiscordChannelMessageEventOptions): Promise<void> {
        const maxDays = 365;
        if (command.args) {
            const days = command.args.days;
            if (days && days > 0) {
                if (days <= maxDays) {
                    const after = Helpers.date(Helpers.utcDate()).addDays(-days);
                    const members = await options.members(m => m.joinedAt > after);
    
                    if (members.length === 0) {
                        options.send("За данный период новичков не приходило.", this.shortMsgDeleteTime);
                    } else {
                        let newbiesList = `\`\`\`css\nНовички за последние ${days} дней:\n`;
                        for (const member of members) {
                            newbiesList += member.displayName + "\t" 
                                + Helpers.date(member.joinedTimestamp).toRULocalDate() + '\n';
                        }
                        newbiesList += `\nА еще сообщение пропадет через 2 минуты, ня\n\`\`\``;
                        options.send(newbiesList, 120);
                    }

                    options.deleteMessage(this.shortMsgDeleteTime);
                } else {
                    options.deleteMessage(this.shortMsgDeleteTime);
                    options.send(`До ${maxDays} дней разрешается`, this.shortMsgDeleteTime);
                    return;
                }
            } else {
                options.deleteMessage(this.shortMsgDeleteTime);
                options.send("Ты чё, ебан?", this.shortMsgDeleteTime);
                return;
            }
        } else {
            options.deleteMessage(this.shortMsgDeleteTime);
            options.send('Чего хочешь? Не понимаю. Напиши за сколько дней вывести нубисов.', this.shortMsgDeleteTime);
            return;
        }
    }

    /**
     * !member command method. Shows some basic information about a guild member.
     * @param options 
     */
    async member(options: IDiscordChannelMessageEventOptions): Promise<void> {
        const mentions = options.mentions();
        if (mentions.length > 0 && mentions.length < 10) {
            options.deleteMessage(this.shortMsgDeleteTime);
            let list = '';
            for (const mention of mentions) {
                const member = options.member(mention.userid);
                const message = member.displayName + "\t" + mention.userid + "\t" + Helpers.date(member.joinedTimestamp).toRULocalDate() + '\n';
                list += message;
            }
            options.send(`\`\`\`${list}\`\`\``, 120);
        }
    }

    /**
     * Deletes all messages from channel 'reception'.
     */
    async wipeReception(options: IDiscordChannelMessageEventOptions): Promise<void> {
        options.deleteMessage(this.shortMsgDeleteTime);

        if (this.cooldownManager.has(null, CommandValues.wipeReception)) {
            options.send("КД час.", this.shortMsgDeleteTime);
            return;
        }

        this.taskProcessor.addTask({
            name: TaskNames.wipeReception,
        });

        this.cooldownManager.add({
            seconds: 3600, // 1 hour
            command: CommandValues.wipeReception,
        });

        await options.like();
    }

    async wipeReceptionTask(): Promise<void> {
        if (Helpers.isProduction) {
            await this.server.wipeChannel(DC.getId(ChannelNames.reception));
        }

        const wipedChannelResponses = ["Все! Теперь тут чисто!", "Вот поднасрали! За вами еще убирать."];
        await this.server.sendMessage(Helpers.getRandomPhrase(wipedChannelResponses), ChannelNames.reception);
    }

    async zoo(command: ICommand, options: IDiscordChannelMessageEventOptions): Promise<void> {
        if (!this.settingsHelper.settings.zooRoleId) {
            this.logger.error(`Missing role zooRoleId in settings`);
            return;
        }

        await this.assignRoleAsOfficer(command, options, this.settingsHelper.settings.zooRoleId);
    }

    async os(command: ICommand, options: IDiscordChannelMessageEventOptions): Promise<void> {
        if (!this.settingsHelper.settings.osRoleId) {
            this.logger.error(`Missing role osRoleId in settings`);
            return;
        }

        await this.assignRoleAsOfficer(command, options, this.settingsHelper.settings.osRoleId);
    }

    async assignRoleAsOfficer(command: ICommand, options: IDiscordChannelMessageEventOptions, roleId: string): Promise<void> {
        if (!this.settingsHelper.settings.officerRoleId) {
            return;
        }

        if (!command.args || options.mentions().length === 0) {
            options.deleteMessage(10);
            options.send('Неправильное использование команды. Посмотри в !помощь.', this.shortMsgDeleteTime);
            return;
        }

        // Check access
        const userRoles = options.getRoles();
        if (userRoles.findIndex(x => x.roleid === this.settingsHelper.settings.officerRoleId) !== -1 
            || options.isAdmin()) { // If user is admin, they can assign the role too.
            const mentions = options.mentions();
            for (const mention of mentions) {
                // Check if user can be assigned the role. If user is admin, they can do this themselves.
                const cannotBeAssigned = await this.server.userHasAnyRoleOf(mention.userid, [
                    this.server.getRoleIdForAccessLevel(AccessLevels.Leader),
                    this.server.getRoleIdForAccessLevel(AccessLevels.Admin),
                    this.server.getRoleIdForAccessLevel(AccessLevels.Fuhrer),
                    this.settingsHelper.settings.officerRoleId,
                ]);
                
                if (!cannotBeAssigned) {
                    if (!command.args.includes("delete")) {
                        // Add role
                        const userHasRole = await this.server.userHasRole(mention.userid, roleId);
                        if (userHasRole) {
                            options.send('Уже есть роль', this.shortMsgDeleteTime);
                        } else {
                            await this.server.setRolesToMember(mention.userid, [roleId]);
                            await options.like();
                            this.logger.log(`User ${options.authorId} (${options.authorDisplayName}) has assigned the role ${roleId} to ${mention.usernametag} (${mention.userid})`);
                        }
                    } else {
                        // Remove role
                        await this.server.removeRolesFromMember(mention.userid, [roleId]);
                        await options.like();
                        this.logger.log(`User ${options.authorId} (${options.authorDisplayName}) has removed the role ${roleId} to ${mention.usernametag} (${mention.userid})`);
                    }
                } else {
                    options.send('Превышение должностных полномочий!', this.shortMsgDeleteTime);
                }
            }
        } else {
            options.send(messages.insufficientPermissions(), this.shortMsgDeleteTime);
        }

        options.deleteMessage(this.shortMsgDeleteTime);
    }

    private onPlayItemStart(item: ISessionItem): void {
        this.server.sendMessage(this.musicPlayer.getTrackMessage(item), ChannelNames.music)
            .then(() => this.server.setBotActivity(item.info.title, "STREAMING").catch(e => this.logger.error(e)))
            .catch(e => this.logger.error(e));
    }

    private onItemQueued(item: ISessionItem): void {
        this.server.sendMessage(this.musicPlayer.getTrackQueuedMessage(item), ChannelNames.music)
            .catch(e => this.logger.error(e));
    }

    private onPlaySessionEnd(): void {
        this.server.setBotActivity(this.defaultActivity, "PLAYING").catch(e => this.logger.error(e));
    }

    // This method gets triggered when all members leave voice channel and Miku alone
    private async onBotAloneInChannel(channelId: string): Promise<void> {
        const isPlaying = await this.musicPlayer.isPlaying(channelId);
        if (isPlaying) {
            await this.server.sendMessage(`Все ушли, значит останавливаю ваш музон в <#${channelId}>.`, DC.getId(ChannelNames.music));
            await this.musicPlayer.pause(channelId);
        }
    }

    /**
     * Syncs users in DB with those in Discord.
     * @param options 
     */
    async syncUsers(options: IDiscordChannelMessageEventOptions): Promise<void> {
        this.taskProcessor.addTask({
            name: TaskNames.syncUsers,
            data: <ISyncUsersTaskObject>{
                finished: false,
                deleted: [],
                userid: options.authorId,
            },
        });

        options.deleteMessage(this.shortMsgDeleteTime);
        options.send('Syncing users...', this.shortMsgDeleteTime);
    }

    async syncUsersTask(taskObject: ISyncUsersTaskObject): Promise<void> {
        const size = 20;
        if (!taskObject.finished) {
            await this.dbContextProvider.execute(async db => {
                const users = await db.userRepo.getAll(0, size);
                const toBeDeleted = [];
                for (const user of users) {
                    const exists = await this.server.userExists(user.userid);
                    if (!exists) {
                        toBeDeleted.push(user.userid);
                        this.logger.log(`User ${user.userid} - ${user.usernametag} does not exist in Discord.`);
                        taskObject.deleted.push({
                            userid: user.userid,
                            name: user.usernametag,
                        });
                    }
                }

                if (toBeDeleted.length > 0) {
                    await db.userRepo.deleteManyUsers(toBeDeleted);
                } else {
                    taskObject.finished = true;
                }

                this.taskProcessor.addTask({
                    name: TaskNames.syncUsers,
                    data: taskObject,
                });
            });
        } else {
            await this.server.sendPrivateMessage(`Удалила ${taskObject.deleted.length} ненужных юзеров из БД.`, taskObject.userid);
        }
    }

    /**
     * Syncs roles in DB with those in Discord.
     * @param options 
     */
    async syncRoles(options: IDiscordChannelMessageEventOptions): Promise<void> {
        options.deleteMessage(this.shortMsgDeleteTime);
        if (this.cooldownManager.has(null, CommandValues.syncRoles)) {
            options.send('Cooldown in progress.', this.shortMsgDeleteTime);
            return;
        }

        const roles = await this.server.getServerRoles();
        for (const chunk of Helpers.splitArray(roles, 10)) {
            this.logger.log(`Syncing roles:\n${chunk.map(c => ` ${c.id} - ${c.name}`).join('\n')}`);
            this.taskProcessor.addTask({
                name: TaskNames.syncRoles,
                data: {
                    roles: chunk,
                },
            });
        }

        // TODO: remove roles from DB that don't exist in Discord

        this.cooldownManager.add({
            command: CommandValues.syncRoles,
            seconds: 60 * 60, // 60 mins
        });

        options.send('Syncing roles...', this.shortMsgDeleteTime);
    }

    async syncRolesTask(roles: {id: string, name: string}[], dbContext: DbContext): Promise<void> {
        await dbContext.roleRepo.insertMany(roles.map<IRole>(r => <IRole>{roleid: r.id, name: r.name}));
    }

    syncUserRoles(options: IDiscordChannelMessageEventOptions): Promise<void> {
        options.deleteMessage(this.shortMsgDeleteTime);
        if (this.cooldownManager.has(null, CommandValues.syncUserRoles)) {
            options.send('Cooldown in progress.', this.shortMsgDeleteTime);
            return;
        }

        const mentions = options.mentions();
        this.taskProcessor.addTask({
            name: TaskNames.syncUserRoles,
            data: {
                users: mentions.length > 0 ? mentions.map(m => <{id: string, name: string}>{id: m.userid, name: m.usernametag}) : null,
            },
        });

        this.cooldownManager.add({
            command: CommandValues.syncUserRoles,
            seconds: 60 * 60, // 60 mins
        });

        options.send('Syncing user roles...', this.shortMsgDeleteTime);
        return Promise.resolve();
    }

    async syncUserRolesTask(users: {id: string, name: string, roles?: {id: string, name: string}[]}[], db: DbContext): Promise<void> {
        if (!users) {
            users = await this.server.getServerUsers();
            for (const chunk of Helpers.splitArray(users, 25)) {
                this.taskProcessor.addTask({
                    name: TaskNames.syncUserRoles,
                    data: {
                        users: chunk,
                    },
                });
            }
        } else {
            for (const user of users) {
                if (!user.roles) {
                    user.roles = await this.server.getMemberRoles(user.id);
                    this.taskProcessor.addTask({
                        name: TaskNames.syncUserRoles,
                        data: {
                            users: [user],
                        },
                    });
                } else {
                    await db.userRepo.insert({
                        userid: user.id,
                        usernametag: user.name,
                    });

                    const userRolesInDb = await db.roleRepo.getUserRoles(user.id);
                    const rolesToDelete = userRolesInDb.filter(r => user.roles.findIndex(x => x.id === r.roleid) === -1);
                    if (rolesToDelete.length > 0) {
                        await db.roleRepo.deleteManyRoleUsers(
                            rolesToDelete.map(r => <{userid: string; roleid: string}>{userid: user.id, roleid: r.roleid}));
                    }

                    await db.roleRepo.insertManyRoleUsers(user.roles.map(ur => <IRoleUser>{
                        roleid: ur.id,
                        userid: user.id,
                        created: new Date(),
                    }));
                }
            }
        }
    }

    showRoleInfo(command: ICommand, options: IDiscordChannelMessageEventOptions): Promise<void> {
        options.deleteMessage(this.shortMsgDeleteTime);
        if (command.text) {
            const role = options.getRoleByName(command.text);
            if (role) {
                options.send(`Роль найдена: \`${role.name} - ${role.roleid}\``, this.shortMsgDeleteTime);
            } else {
                options.send(`\`Запрос ничего не вернул.\``, this.shortMsgDeleteTime);
            }
        }

        return Promise.resolve();
    }

    async showServerInfo(options: IDiscordChannelMessageEventOptions): Promise<void> {
        options.deleteMessage(this.shortMsgDeleteTime);
        const channels = await this.server.getServerChannels();
        const message = channels.map(x => `${x.name}: ${x.id}`).join('\n');
        await options.send(`\`${message}\``, 60);
    }

    async setBotActivity(command: ICommand, options: IDiscordChannelMessageEventOptions): Promise<void> {
        options.deleteMessage(this.shortMsgDeleteTime);
        const args = <ISetActivityArgs>command.args
        if (args) {
            await options.setActivity(args.message, args.activity);
            await options.like();
        } else {
            await options.error();
        }
    }

    async bgs(command: ICommand, options: IDiscordChannelMessageEventOptions): Promise<void> {
        const tick = await this.bgsClient.getBgsTickInfo();
        const message = this.bgsClient.getTickNotificationMessage(tick);
        options.send(message, 60);
    }

    async processTick(options: IDiscordChannelMessageEventOptions): Promise<void> {
        await this.onTick();
        await options.deleteMessage();
        options.send(`Тик обработан`, this.shortMsgDeleteTime);
    }

    /**
     * Displays fresh BGS tick info notification.
     */
    private async getFreshBgsMessageNotification(): Promise<void> {
        const latest = await this.bgsClient.getNewBgsTickInfo();
        if (latest) {
            // This is a fresh tick - display it
            const message = this.bgsClient.getTickNotificationMessage(latest);
            await this.server.sendMessage(message, ChannelNames.bgsInfo);
        }
    }

    private async processTask(task: IProcessTask, db: DbContext): Promise<void> {
        switch(task.name) {
            case TaskNames.guildMemberAdd:
                await this.guildMemberAddTask(task.data.userid, task.data.usernametag, db);
                break;
            case TaskNames.addUser:
                await db.userRepo.insert(<IUser>task.data);
                break;
            case TaskNames.banUser:
                await db.userRepo.banUser(task.data.userid, new Date(task.data.banUntil));
                break
            case TaskNames.unBanUser:
                await db.userRepo.unbanUser(task.data.userid);
                break;
            case TaskNames.deleteUser:
                await db.userRepo.delete(task.data.userid);
                break;
            case TaskNames.addLongCooldown:
                await this.longCooldownManager.addToDb({
                    userid: task.data.userid,
                    seconds: task.data.seconds,
                    command: task.data.command,
                    created: new Date(task.data.created),
                });
                break;
            case TaskNames.updateUserArkDrop:
                await this.arks.updateDropInfo(task.data);
                break;
            case TaskNames.addUserAward:
                await this.awards.addUserAward(task.data.userid, task.data.medal, task.data.description);
                break;
            case TaskNames.updateUserPresence:
                await this.updateUserPresenceTask(task.data.userid, task.data.usernametag, db);
                break;
            case TaskNames.syncRoles:
                await this.syncRolesTask(task.data.roles, db);
                break;
            case TaskNames.syncUserRoles:
                await this.syncUserRolesTask(task.data.users, db);
                break;
            case TaskNames.syncUsers:
                await this.syncUsersTask(<ISyncUsersTaskObject>task.data);
                break;
            case TaskNames.roleCreate:
                await this.roleCreateTask(task.data.role, db);
                break;
            case TaskNames.roleDelete:
                await db.roleRepo.delete(task.data.role.id);
                break;
            case TaskNames.roleUpdate:
                await this.roleUpdateTask(task.data.role, db);
                break;
            case TaskNames.userUpdate:
                await this.userUpdateTask({
                    userid: task.data.user.id,
                    usernametag: task.data.user.name,
                },
                (<{id:string,name:string}[]>task.data.rolesAdded).map(r => <IRole>{roleid: r.id, name: r.name}),
                (<{id:string,name:string}[]>task.data.rolesRemoved).map(r => <IRole>{roleid: r.id, name: r.name}),
                db);
                break;
            case TaskNames.softDeleteUser:
                await this.softDeleteUserTask(task.data.userid, task.data.usernametag, db);
                break;
            case TaskNames.unsoftDeleteUser:
                await db.userRepo.unSoftDeleteUser(task.data.userid);
                break;
            case TaskNames.removeZooUsers:
                await this.removeZooMembersFromServerTask(task.data.users);
                break;
            case TaskNames.sendRemovedUsersReport:
                await this.sendReportOfRemovedUsersTask(task.data.users, task.data.recipientid);
                break;
            case TaskNames.loadCooldowns:
                await this.longCooldownManager.loadCooldowns();
                break;
            case TaskNames.readdRolesToMember:
                await this.readdRolesToMemberTask(task.data.userid, db);
                break;
            case TaskNames.wipeReception:
                await this.wipeReceptionTask();
                break;
            case TaskNames.notifyAboutZoo:
                await this.sendZooMemberNotificationTask(<IUser>task.data);
                break;
            case TaskNames.notifyAboutKick:
                await this.sendKickedMemberNotificationTask(<IUser>task.data);
                break;
            case TaskNames.addStrangerRoleToNewUser:
                await this.addStangerRoleToNewUser(<IUser>task.data);
                break;
            default:
                this.logger.error(`No such task with name ${TaskNames[task.name]} found.`);
                break;
        }
    }

    private async onTick(): Promise<void> {
        this.logger.log(`${Helpers.utcDate()}: tick.`);

        // BGS tick
        await this.getFreshBgsMessageNotification();
    }

    async checkHeartbeat(): Promise<void> {
        if (!this._restartingProcesses && this.taskScheduler.lastActivitySecondsAgo > 60 * 10) {
            this.logger.log("It appears to be dead - resuscitate");
            this.logger.log("Administering electric shock...");
            this._restartingProcesses = true;
            await this.restartProcesses();
            this.logger.log("Revived!");
            this._restartingProcesses = false;
        }
    }

    async restartProcesses(): Promise<void> {
        this.logger.log("Restarting TaskScheduler...");
        await this.taskScheduler.stop();
        this.taskScheduler.start();
        this.logger.log("Reloading long cooldowns...");
        this.taskProcessor.addTask({
            name: TaskNames.loadCooldowns,
        });
    }

    async deleteDisabledUsers(): Promise<void> {
        if (this.settingsHelper.settings.disabledUsersLifeDays > 0) {
            await this.dbContextProvider.execute(async db => {
                const users = await db.userRepo.getDisabledUsers(this.settingsHelper.settings.disabledUsersLifeDays, 10);
    
                if (users.length > 0) {
                    this.logger.log(
                        `Found ${users.length} users to delete:\n${users.map(u => ` - ${u.usernametag}. Disabled ${u.deleted};`).join('\n')}`);
                }
    
                await db.userRepo.deleteManyUsers(users.map(u => u.userid));
            });
        }
    }

    private configureRssFeed(): void {
        const settings = this.settingsHelper.settings.rssSettings;
        if (!settings || !settings.feeds) {
            return;
        }

        if (Helpers.distinct(settings.feeds.map(f => f.name)).length !== settings.feeds.length) {
            this.logger.error('RSS: name must be unique');
            return;
        }

        if (Helpers.distinct(settings.feeds.map(f => f.url)).length !== settings.feeds.length) {
            this.logger.error('RSS: url must be unique');
            return;
        }

        for (const feedSetting of settings.feeds) {
            if (!feedSetting.name || !feedSetting.channelId || !feedSetting.url) {
                continue;    
            }

            this.rss.add({
                name: feedSetting.name,
                url: feedSetting.url,
                channelId: feedSetting.channelId,
                category: feedSetting.category,
                n: feedSetting.n,
                on: (feed: IRssFeed, sub: IRssFeedSub) => {
                    for (const item of feed.items) {
                        this.server.sendMessage(RssFeedGetter.composeMessage(item, feed), sub.channelId)
                            .then(() => item.confirm())
                            .catch(err => this.logger.error(`RSS: ${err.toString()}`));
                    }
                },
            });
        }
    }

    stats(): void {
        this.cooldownManager.trace("Cooldowns");
        this.longCooldownManager.trace("Long cooldowns");
        this.logger.log(`DB stats:\n    - connections ${getDBConnectionInfo().connections}\n    - active ${getDBConnectionInfo().active}`);

        const heapTotal = process.memoryUsage().heapTotal.toString();
        const heapUsed = process.memoryUsage().heapUsed.toString();
        const rss = process.memoryUsage().rss;
        this.logger.log(
            `Memory:
    - heap-total ${heapTotal}
    - heap-used ${heapUsed}
    - rss ${rss}
            `);
    }

    madiStatus(): void {
        const result = this.madi.toString();
        if (result) {
            this.logger.log(result);
        }
    }

    musicPlayerStatus(): void {
        const result = this.musicPlayer.toString();
        if (result) {
            this.logger.log(result);
        }
    }
}
