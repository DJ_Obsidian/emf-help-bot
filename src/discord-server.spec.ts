import { getDbContextFake, getDbContextProviderFake, getDiscordClientFake, getLoggerFake } from "./common-spec/common.spec";
import { DbContext } from "./db/context";
import { DiscordServer } from "./discord-server";

describe("DiscordServer", () => {

    describe("message", () => {

        describe("when invoked with no error", () => {
            const author: any = {
                id: 'author id'
            };
            const discordClientFake = getDiscordClientFake();
            const dbProviderFake = getDbContextProviderFake();
            const dbContextFake = getDbContextFake();
    
            const discordMessageFake: any = {
                author: author,
                member: {
                    displayName: 'name',
                    id: 'member id',
                    voice: {
                        channel: {
                            id: "voice channel id",
                        }
                    }
                },
                content: "message text",
                channel: {
                    id: 'channel id',
                    type: 'text',
                },
                guild: {
                    id: 'server id',
                    name: 'server name',
                }
            };
    
            let createSpy: jasmine.Spy;
            let onSpy: jasmine.Spy;
            let destroySpy: jasmine.Spy;
    
            let actualDbContext: any;
    
            beforeAll(done => {
                onSpy = spyOn(discordClientFake, 'on').and.callThrough();
                createSpy = spyOn(dbProviderFake, 'create').and.resolveTo(dbContextFake);
                destroySpy = spyOn(dbContextFake, 'destroy').and.callThrough();
    
                const server = new DiscordServer(discordClientFake, dbProviderFake, <any>{}, <any>{});
    
                server.onMessage((args, db) => {
                    actualDbContext = db;
                    return Promise.resolve();
                });
    
                server.message(discordMessageFake);
    
                done();
            });
    
            it("should pass in DbContext instance", () => {
                expect(actualDbContext).toBeTruthy();
            });
    
            it("should destroy dbContext", () => {
                expect(destroySpy).toHaveBeenCalledTimes(1);
            });
        });

        describe("when there is an unhandled rejection", () => {
            const author: any = {
                id: 'author id'
            };
            const discordClientFake = getDiscordClientFake();
            const dbProviderFake = getDbContextProviderFake();
            const dbContextFake = getDbContextFake();
            const loggerFake = getLoggerFake();
    
            const discordMessageFake: any = {
                author: author,
                member: {
                    displayName: 'name',
                    id: 'member id',
                    voice: {
                        channel: {
                            id: "voice channel id",
                        }
                    }
                },
                content: "message text",
                channel: {
                    id: 'channel id',
                    type: 'text',
                },
                guild: {
                    id: 'server id',
                    name: 'server name',
                },
            };
    
            let createSpy: jasmine.Spy;
            let onSpy: jasmine.Spy;
            let destroySpy: jasmine.Spy;
            let errorSpy: jasmine.Spy;
    
            let actualDbContext: DbContext;
    
            beforeAll(done => {
                onSpy = spyOn(discordClientFake, 'on').and.callThrough();
                createSpy = spyOn(dbProviderFake, 'create').and.resolveTo(dbContextFake);
                destroySpy = spyOn(dbContextFake, 'destroy').and.callThrough();
                errorSpy = spyOn(loggerFake, 'error').and.callThrough().and.resolveTo();
    
                const server = new DiscordServer(discordClientFake, dbProviderFake, loggerFake, <any>{});
    
                server.onMessage((args, db) => {
                    actualDbContext = db;
                    return Promise.reject('error');
                });
    
                server.message(discordMessageFake);
    
                done();
            });
    
            it("should pass in DbContext instance", () => {
                expect(actualDbContext).toBeTruthy();
            });

            it("should log error", () => {
                expect(errorSpy).toHaveBeenCalledTimes(1);
            });

            it("should destroy dbContext", () => {
                expect(destroySpy).toHaveBeenCalledTimes(1);
            });
        });
    });

});