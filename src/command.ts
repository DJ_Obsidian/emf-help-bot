import { AccessLevels } from "./access-levels";
import { ChannelNames } from "./channel-names";
import { IBanArgs, IKickArgs, INewfagArgs, IPersonalRoleArgs } from "./command-args";
import { CommandValues } from "./command-values";

export const tags = {
    MUSIC: "music",
    ADMIN: "admin",
}

const commandTags = [tags.MUSIC, tags.ADMIN];
export type CommandTag = typeof commandTags[number];

export interface ICommand {
    
    id: CommandValues;

    /**
     * Command names.
     */
    cmds: string[];

    description: string;
    
    /**
     * Permission level the command has.
     */
    level?: AccessLevels;

    /**
     * Indicates whether only OS members are allowed to use this command.
     */
    osOnly?: boolean;

    /**
     * Text after command name.
     */
    text?: string,

    args?: any,

    /**
     * Restricts command to these channels if specified.
     */
    channel?: ChannelNames | ChannelNames[];

    /** 
     * Hides help entirely.
    */
    hideHelp?: boolean;

    tag?: CommandTag;

    userLevel?: AccessLevels;

    instructions?: string;

    parse?: (text: string) => unknown;

    execute?: (...args: unknown[]) => Promise<void>;
}

export interface IPersonalRoleCommand extends ICommand {
    args: IPersonalRoleArgs;
}

export interface INewFagCommand extends ICommand {
    args: INewfagArgs;
}

export interface IBanCommand extends ICommand {
    args: IBanArgs;
}

export interface IKickCommand extends ICommand {
    args: IKickArgs;
}
