import { BgsClient, IBsgTick } from "./bgs-client";
import { getFileHelperFake, getHttpClientHelperFake, getLoggerFake, getSettingsHelperFake } from "./common-spec/common.spec";

describe("BgsClient", () => {

    const settingsFake = getSettingsHelperFake();
    settingsFake.settings.eliteBgsApiUrl = 'some url';
    settingsFake.settings.eliteBgsUrl = 'some url';

    describe("getNewBgsTickInfo", () => {
        
        describe("if no latest file exists", () => {
    
            const actualTickInfo = JSON.parse('[{"_id":"5fa6d4bb219db90313c5d916","time":"2020-11-07T16:46:47.000Z","updated_at":"2020-11-07T17:09:15.272Z","__v":0}]');
    
            const httpClientFake: any = getHttpClientHelperFake();
            const fileHelperFake: any = getFileHelperFake();
    
            let getSpy: jasmine.Spy;
            let writeToFileSpy: jasmine.Spy;
    
            let actualTick: IBsgTick;
    
            beforeAll(async done => {
                getSpy = spyOn(httpClientFake,'get').and.callThrough()
                    .and.returnValue({ statusCode: 200, data: actualTickInfo });
                writeToFileSpy = spyOn(fileHelperFake, 'writeToFile').and.callThrough();
                const bgsClient = new BgsClient(httpClientFake, fileHelperFake, getLoggerFake(), settingsFake);

                // Act
                actualTick = await bgsClient.getNewBgsTickInfo();
    
                done();
            });
    
            it('should return tick', () => {
                expect(actualTick).toBeTruthy();
                expect(new Date(actualTick.time)).toEqual(new Date('2020-11-07T16:46:47.000Z'));
            });

            it('should write to file', () => {
                expect(writeToFileSpy).toHaveBeenCalledTimes(1);
            });
        });

        describe("if today's tick file exists", () => {
            const actualTickInfo = JSON.parse('[{"_id":"5fa6d4bb219db90313c5d916","time":"2020-11-07T16:46:47.000Z","updated_at":"2020-11-07T17:09:15.272Z","__v":0}]');
            const todaysFile = '{"_id":"5fa6d4bb219db90313c5d916","time":"2020-11-07T10:00:00.000Z","updated_at":"2020-11-07T17:09:15.272Z","__v":0}';

            const httpClientFake: any = getHttpClientHelperFake();
            const fileHelperFake: any = getFileHelperFake();
    
            let getSpy: jasmine.Spy;
            let writeToFileSpy: jasmine.Spy;
            let readFileSpy: jasmine.Spy;

            let actualTick: IBsgTick;
    
            beforeAll(async done => {
                getSpy = spyOn(httpClientFake,'get').and.callThrough()
                    .and.returnValue({ statusCode: 200, data: actualTickInfo });

                writeToFileSpy = spyOn(fileHelperFake, 'writeToFile').and.callThrough();
                readFileSpy = spyOn(fileHelperFake, 'readFile').and.callThrough().and.returnValue(todaysFile);
                const bgsClient = new BgsClient(httpClientFake, fileHelperFake, getLoggerFake(), settingsFake);

                // Act
                actualTick = await bgsClient.getNewBgsTickInfo();
    
                done();
            });

            it('should not return tick', () => {
                expect(actualTick).toBeFalsy();
            });

            it('should write to file', () => {
                expect(writeToFileSpy).toHaveBeenCalledTimes(1);
            });
        });

        describe("if nothing came from internet", () => {
            const todaysFile = '{"_id":"5fa6d4bb219db90313c5d916","time":"2020-11-07T10:00:00.000Z","updated_at":"2020-11-07T17:09:15.272Z","__v":0}';

            const httpClientFake: any = getHttpClientHelperFake();
            const fileHelperFake: any = getFileHelperFake();
    
            let getSpy: jasmine.Spy;
            let writeToFileSpy: jasmine.Spy;
            let readFileSpy: jasmine.Spy;

            let actualTick: IBsgTick;
    
            beforeAll(async done => {
                getSpy = spyOn(httpClientFake,'get').and.callThrough()
                    .and.returnValue({ statusCode: 200, data: undefined });

                writeToFileSpy = spyOn(fileHelperFake, 'writeToFile').and.callThrough();
                readFileSpy = spyOn(fileHelperFake, 'readFile').and.callThrough().and.returnValue(todaysFile);
                const bgsClient = new BgsClient(httpClientFake, fileHelperFake, getLoggerFake(), settingsFake);

                // Act
                actualTick = await bgsClient.getNewBgsTickInfo();
    
                done();
            });

            it("should return null", () => {
                expect(actualTick).toBeFalsy;
            });

            it("should call get", () => {
                expect(getSpy).toHaveBeenCalledTimes(1);
            });

            it('should not write to file', () => {
                expect(writeToFileSpy).toHaveBeenCalledTimes(0);
            });
        });
    });
});