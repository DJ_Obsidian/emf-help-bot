
import * as Discord from 'discord.js';
import { AccessLevels } from './access-levels';
import { ChannelNames } from './channel-names';
import { embedColor } from './colors';
import { CommandTag, ICommand } from './command';
import { IBanArgs, IKickArgs, INewfagArgs, ISetActivityArgs } from './command-args';
import { CommandValues } from './command-values';
import { ArkCommand } from './commands/ark/ark.command';
import { MyArkCommand } from './commands/ark/my-ark.command';
import { ViewArkCommand } from './commands/ark/view-ark.command';
import { AwardCommand } from './commands/awards/award.command';
import { ViewAwardCommand } from './commands/awards/view-awards.command';
import { HelpCommand } from './commands/help/help.command';
import { PauseCommand } from './commands/music/pause.command';
import { PlayCommand } from './commands/music/play.command';
import { PlayingCommand } from './commands/music/playing.command';
import { PlaylistCommand } from './commands/music/playlist.command';
import { ResumeCommand } from './commands/music/resume.command';
import { ShuffleCommand } from './commands/music/shuffle.command';
import { SkipCommand } from './commands/music/skip.command';
import { StopCommand } from './commands/music/stop.command';
import { CreatePersonalRoleCommand } from './commands/personal-role/create-personal-role.command';
import { PersonalRoleColorCommand } from './commands/personal-role/personal-role-color.command';
import { PersonalRoleNameCommand } from './commands/personal-role/personal-role-name.command';
import { PMCommand } from './commands/pm.command';
import { PookCommand } from './commands/pook.command';
import { PressFCommand } from './commands/press-f.command';
import { SaluteCommand } from './commands/salute.command';
import { SayCommand } from './commands/say.command';
import { ViewAvatarCommand } from './commands/view-avatar.command';
import { DC } from './discord-channels';
import { ActivityType, activityTypes } from './discord-event';
import Helpers from './helpers';

export class CommandProcessor {

    get commands(): ICommand[] {
        return [
            new HelpCommand(),
            new ArkCommand(),
            new MyArkCommand(),
            new ViewArkCommand(),
            new PookCommand(),
            new CreatePersonalRoleCommand(),
            new PersonalRoleColorCommand(),
            new PersonalRoleNameCommand(),
           
            new PressFCommand(),
            new SaluteCommand(),
            new ViewAvatarCommand(),
            new ViewAwardCommand(),
            
            // Leader commands
            new SayCommand(),
            { 
                id: CommandValues.newfags,
                cmds: ['!новички', '!нубисы', '!ньюфаги', '!newfags'],
                description: 'Выводит имена новичков за последние N дней. Использовать: !новички <Сколько_дней>',
                level: AccessLevels.Leader,
                channel: ChannelNames.admin,
                parse: (text: string): INewfagArgs => CommandProcessor.parseNewfags(text),
            },
            new AwardCommand(),
            { 
                id: CommandValues.kick,
                cmds: ['!кик', '!kick'],
                description: 'Кикает участника с сервера, указывать причину не обязательно. Использовать: !kick <Кого> <причина>',
                level: AccessLevels.Leader,
                parse: (text: string): IKickArgs => CommandProcessor.parseKick(text),
            },

            // Music
            new PlayCommand(),
            new PauseCommand(),
            new StopCommand(),
            new ResumeCommand(),
            new SkipCommand(),
            new PlayingCommand(),
            new PlaylistCommand(),
            new ShuffleCommand(),

            // Admin commands
            { id: CommandValues.prune, cmds: ['!чисти', '!prune'], description: 'Удаляет N последних сообщений. Использовать: !чисти <Сколько>', level: AccessLevels.Admin },
            { 
                id: CommandValues.ban, 
                cmds: ['!бан', '!ban'],
                description: 'Софтбан, накидывает роль при которой невозможно писать и говорить на определенный период времени. Использовать: !бан @<Кого> <Сколько_часов>.\r\n С аргументом perm банит на сервере. Использовать: !бан perm <Кого>.',
                level: AccessLevels.Admin,
                parse: (text: string): IBanArgs => CommandProcessor.parseBan(text),
            },
            { id: CommandValues.unban, cmds: ['!разбан', '!unban'], description: 'Разбанит пользователя раньше времени. Использовать: !разбан <Кого> ', level: AccessLevels.Admin },
            
            { id: CommandValues.bgs, cmds: ['!bgs', '!бгс'], description: 'Показывает оповещение о BGS тике', level: AccessLevels.Admin, channel: ChannelNames.bgsInfo },
            { id: CommandValues.member, cmds: ['!member'], description: 'Some basic member info', level: AccessLevels.Admin, channel: ChannelNames.admin, hideHelp: true },
            {
                id: CommandValues.tick,
                cmds: ['!tick'],
                description: 'Тик',
                level: AccessLevels.Admin,
                channel: ChannelNames.admin,
                hideHelp: true,
            },
            { id: CommandValues.syncRoles, cmds: ['!syncroles'], description: 'Sync Discord server roles with DB', level: AccessLevels.Admin, channel: ChannelNames.admin, hideHelp: true },
            { id: CommandValues.syncUserRoles, cmds: ['!syncuserroles'], description: 'Sync roles in DB with those in Discord.', level: AccessLevels.Admin, channel: ChannelNames.admin, hideHelp: true },
            { id: CommandValues.syncUsers, cmds: ['!syncusers'], description: 'Sync users in DB with those in Discord.', level: AccessLevels.Admin, channel: ChannelNames.admin, hideHelp: true },

            { id: CommandValues.roleinfo, cmds: ['!roleinfo'], description: 'Shows some basic role info.', level: AccessLevels.Admin, channel: ChannelNames.admin, hideHelp: true },
            { id: CommandValues.serverinfo, cmds: ['!serverinfo'], description: 'Shows some basic server info.', level: AccessLevels.Admin, channel: ChannelNames.admin, hideHelp: true },
            {
                id: CommandValues.setBotActivity,
                cmds: ['!activity'],
                description: 'Sets bot activity.',
                level: AccessLevels.Admin,
                channel: ChannelNames.admin,
                hideHelp: true,
                parse: (text: string): ISetActivityArgs => CommandProcessor.parseSetActivity(text),
            },
            { id: CommandValues.wipeReception, cmds: ['!wipe'], description: `Wipes all messages from \`<#${DC.getId(ChannelNames.reception)}>\`.`, level: AccessLevels.Admin, channel: ChannelNames.admin, hideHelp: true },
            {
                id: CommandValues.zoo,
                cmds: ['!zoo', '!зоопарк'],
                description: `Использовать !зоопарк @имя, чтоб навесить роль Зоопарк, или !зоопарк @имя delete, чтоб ее убрать.`,
                channel: ChannelNames.general,
            },
            {
                id: CommandValues.os,
                cmds: ['!os', '!ос'],
                description: `Использовать !ос @имя, чтоб навесить роль ОС, или !ос @имя delete, чтоб ее убрать.`,
                channel: ChannelNames.general,
            },
            new PMCommand(),
            // <<<<<<<<<<Temporary command>>>>>>>>>>>
            // { id: CommandValues.importAwards, cmds: ['!import'], description: "Temporary command", level: AccessLevels.Admin, channel: ChannelNames.admin, hideHelp: true },
        ];
    }

    /**
     * Gets help response rich text.
     */
    helpResponse(level: AccessLevels, tag?: CommandTag): { commands: ICommand[], message: Discord.MessageEmbed} {
        // Filter commands by level and visibility
        let commands = this.commands.filter(c => !c.hideHelp && (!c.level || c.level <= level));
        
        // Filter commands by tag
        if (tag) {
            commands = commands.filter(c => c.tag === tag);
        } else {
            commands = commands.filter(c => !c.tag);
        }

        let embed = new Discord.MessageEmbed()
            .setColor(embedColor)
            .setTitle('Помощь, угу...')
            .setThumbnail("https://media.discordapp.net/attachments/595920342141370381/596022380628017173/O7_12.png");

        commands.forEach(c => {
            embed = embed.addField(c.cmds.join("/"), '`' + c.description + '`');
        });

        return { commands: commands, message: embed };
    }

    /**
     * Parses text command and parameterizes it.
     * @param text 
     */
    parse(text: string): ICommand {
        if (text && text.trim().length === 0) {
            return null;
        }

        if (!text.includes("!")) {
            return null;
        }

        const split = text.split(" "); // Split the message up in to pieces for each space
        const primaryCommand = split[0].toLowerCase(); // The first word directly after the exclamation is the command
        
        const command = this.getCommand(primaryCommand.trim());
        if (!command) {
            return null;
        }

        if (command.parse) {
            command.args = command.parse(text);
        } else {
            command.args = Helpers.removeDuplicateSpaces(text.trim()).split(" ").slice(1); // All other words are arguments/parameters/options for the command
        }

        command.text = text.replace(/^!\S+[ ]?/, '');

        if (!command.level) {
            command.level = AccessLevels.Everyone;
        }

        return command;
    }

    private getCommand(name: string): ICommand {
        return this.commands.find(c => c.cmds.findIndex(x => x === name) !== -1);
    }

    static parseBan(text: string): IBanArgs {
        const rgx = /^!\S+[ ]+<@![0-9]{18}>[ ]+([0-9.]{1,5})$/.exec(text);
        if (rgx && rgx.length > 1) {
            const hours = parseFloat(rgx[1]);
            if (isNaN(hours)) {
                return null;
            } else {
                return {
                    hours: +hours,
                    isPerm: false,
                };
            }
        }

        const banPermRgx = /^!\S+[ ]+perm[ ]+<@![0-9]{18}>([ ]+(.*)|[ ]*)$/.exec(text);
        if (banPermRgx && banPermRgx.length > 0) {
            const reason = banPermRgx[2];
            return {
                isPerm: true,
                reason: reason,
            };
        }

        return null;
    }

    static parseNewfags(text: string): INewfagArgs {
        const split = Helpers.removeDuplicateSpaces(text).split(' ');
        if (split.length === 2) {
            const numberOfDays = parseInt(split[1]);
            if (!isNaN(numberOfDays)) {
                return { days: +numberOfDays };
            }
        }

        return null;
    }

    static parseKick(text: string): IKickArgs {
        const match = /^!\S+[ ]+<@![0-9]{18}>[ ]+(.+)$/.exec(text);
        if (!match) {
            return null;
        }

        let reason = match[1];
        if (reason) {
            reason = reason.trim();
            if (reason.length > 0) {
                return {
                    reason: reason,
                };
            }
        }

        return null;
    }

    static parseSetActivity(text: string): ISetActivityArgs {
        const match = new RegExp(`^!\\S+[ ]+(${activityTypes.join('|')})+[ ]+(.+)$`, 'i').exec(text);
        if (!match) {
            return null;
        }

        if (match.length === 3) {
            const activity = match[1].toUpperCase();
            const message = match[2];
            return {
                activity: <ActivityType>activity,
                message: message,
            };
        }

        return null;
    }

    static parseYoutubeUrl(text: string): string {
        let match = /^!\S+[ ]+(https:\/\/www\.youtube\.com\/watch\?v=[^&]{11,})&?\S*/.exec(text);
        if (match && match.length === 2) {
            return match[1];
        }

        match = /^!\S+[ ]+(https:\/\/youtu\.be\/[^&]{11,})&?\S*/.exec(text);
        if (match && match.length === 2) {
            return match[1];
        }

        return null;
    }

    static cutoffCommandText(text: string): string {
        return text.replace(/^!\S+[ ]?/, '');
    }
}
