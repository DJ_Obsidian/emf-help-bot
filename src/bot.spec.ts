import { AccessLevels } from "./access-levels";
import { Arks } from "./arks";
import { Awards } from "./awards";
import { BgsClient } from "./bgs-client";
import EmfBot from "./bot";
import { ChannelNames } from "./channel-names";
import { IBanCommand, IKickCommand } from "./command";
import { CommandProcessor } from "./command-processor";
import { CommandValues } from "./command-values";
import { getArksFake, getAwardsFake, getBgsClientFake, getCommandProcFake, getCooldownManagerFake, getDbContextFake, getDbContextProviderFake, getFileHelperFake, getLoggerFake, getMusicAutoDisconnectInvoker, getMusicPlayerFake, getServerFake, getSettingsHelperFake, getTaskProcessorFake, getTaskSchedulerFake } from "./common-spec/common.spec";
import { ICooldown } from "./cooldown";
import { CooldownManager } from "./cooldown-manager";
import { DbContextProvider } from "./db/context";
import { IRole } from "./db/entities/role";
import { IRoleUser } from "./db/entities/role-user";
import { IUser } from "./db/entities/user";
import { DC } from "./discord-channels";
import { DiscordServer } from "./discord-server";
import FileHelper from "./file-helper";
import Helpers from "./helpers";
import Logger from "./logger";
import LongCooldownManager from "./long-cooldown-manager";
import { MusicAutoDisconnectTimedInvoker } from "./music/music-auto-disconnect-invoker";
import { MusicPlayer } from "./music/music-player";
import RssFeedGetter from "./rss/rss-feed-getter";
import { SettingsHelper } from "./settings-helper";
import { TaskNames } from "./task-names";
import { TaskProcessor } from "./task-processor";
import { TaskScheduler } from "./task-scheduler";

describe("bot", () => {

    function getBot(
        server: DiscordServer, 
        commandProc: CommandProcessor,
        taskProcessor: TaskProcessor,
        bgsClient: BgsClient,
        cooldownManager: CooldownManager,
        dbContextProvider: DbContextProvider,
        longCooldownManager?: LongCooldownManager,
        arks?: Arks,
        awards?: Awards,
        settingsHelper?: SettingsHelper,
        taskScheduler?: TaskScheduler,
        logger?: Logger,
        fileHelper?: FileHelper,
        musicPlayer?: MusicPlayer,
        musicAutoDisconnectInvoker?: MusicAutoDisconnectTimedInvoker,
        rss?: RssFeedGetter): EmfBot {
            return new EmfBot(
                server,
                commandProc,
                taskScheduler || getTaskSchedulerFake(),
                taskProcessor,
                bgsClient,
                cooldownManager,
                dbContextProvider,
                longCooldownManager || <LongCooldownManager>{},
                awards || <Awards>{},
                arks || <Arks>{},
                logger || getLoggerFake(),
                settingsHelper || getSettingsHelperFake(),
                fileHelper || getFileHelperFake(),
                musicPlayer || getMusicPlayerFake(),
                musicAutoDisconnectInvoker || getMusicAutoDisconnectInvoker(),
                rss || jasmine.createSpyObj("RssFeedGetter", ["getFeeds"]));
        }

    describe("guildMemberAddTask", () => {
        describe("when a new member is added", () => {

            const dbContextFake = getDbContextFake();
            const serverFake = getServerFake();
            const taskProcessorFake = getTaskProcessorFake();
            const bot = getBot(serverFake, <CommandProcessor>{}, taskProcessorFake, getBgsClientFake(), getCooldownManagerFake(), getDbContextProviderFake());

            const expectedUserId = "userid 1";
            const expectedUserTag = "user tag 1";
            let sentMessage: string;

            let isBotSpy: jasmine.Spy;
            let userRepoGetByIdSpy: jasmine.Spy;
            let sendMessageSpy: jasmine.Spy;
            let addTaskSpy: jasmine.Spy;

            beforeAll(async done => {
                isBotSpy = spyOn(serverFake, 'isBot').and.callThrough().and.resolveTo(false);
                userRepoGetByIdSpy = spyOn(dbContextFake.userRepo, 'getById')
                    .and.callThrough()
                    .and.resolveTo();
                sendMessageSpy = spyOn(serverFake, 'sendMessage')
                    .and.callThrough()
                    .and.callFake((msg: any, channelId: any) => {
                        sentMessage = msg;
                        return new Promise((resolve, _) => resolve(null));
                    });
                addTaskSpy = spyOn(taskProcessorFake, 'addTask').and.callThrough();

                // Act
                
                await bot.guildMemberAddTask(expectedUserId, expectedUserTag, dbContextFake);

                done();
            });

            it("should check if member is bot", () => {
                expect(isBotSpy).toHaveBeenCalledOnceWith(expectedUserId);
            })

            it("should call getByid to try get user", () => {
                expect(userRepoGetByIdSpy).toHaveBeenCalledOnceWith(expectedUserId);
            });

            it("should send message", () => {
                expect(sendMessageSpy).toHaveBeenCalledTimes(1);
                expect(sentMessage).toBeTruthy();
            });

            it("should call addTask to add user to DB", () => {
                expect(addTaskSpy).toHaveBeenCalledTimes(2);
                expect(addTaskSpy).toHaveBeenCalledWith({
                    name: TaskNames.addUser,
                    data: {
                        userid: expectedUserId,
                        usernametag: expectedUserTag,
                    },
                });
                expect(addTaskSpy).toHaveBeenCalledWith({
                    name: TaskNames.addStrangerRoleToNewUser,
                    data: {
                        userid: expectedUserId,
                        usernametag: expectedUserTag,
                    },
                });
            });
        });
    });

    describe("guildMemberRemove", () => {

        const taskProcessorFake = getTaskProcessorFake();
        const serverFake = getServerFake();
        const bot = getBot(serverFake, <CommandProcessor>{}, taskProcessorFake, getBgsClientFake(), getCooldownManagerFake(), getDbContextProviderFake());

        let addTaskSpy: jasmine.Spy;
        let isUserBannedSpy: jasmine.Spy;
        let getKickAuditLogSpy: jasmine.Spy;
        let sendMessageSpy: jasmine.Spy;

        describe("when user is banned", () => {

            beforeAll(async done => {
                addTaskSpy = spyOn(taskProcessorFake, 'addTask').and.callThrough();
                isUserBannedSpy = spyOn(serverFake, "isUserBanned").and.callThrough()
                    .and.resolveTo(true);
                getKickAuditLogSpy = spyOn(serverFake, "getKickAuditLog").and.callThrough();
                sendMessageSpy = spyOn(serverFake, "sendMessage").and.callThrough();
    
                // Act
                await bot.guildMemberRemove({
                    memberId: "user 1",
                    memberName: 'user name',
                });
    
                done();
            });
    
            it("should call addTask", () => {
                expect(addTaskSpy).toHaveBeenCalledOnceWith({
                    name: TaskNames.softDeleteUser,
                    data: {
                        userid: 'user 1',
                        usernametag: 'user name',
                    }
                });
            });

            it("should not post F response", () => {
                expect(sendMessageSpy).not.toHaveBeenCalled();
                expect(getKickAuditLogSpy).not.toHaveBeenCalled();
            });
        });

        describe("when user was kicked recently", () => {

            beforeAll(async done => {
                addTaskSpy = spyOn(taskProcessorFake, 'addTask').and.callThrough();
                isUserBannedSpy = spyOn(serverFake, "isUserBanned").and.callThrough()
                    .and.resolveTo(false);
                getKickAuditLogSpy = spyOn(serverFake, "getKickAuditLog").and.callThrough()
                    .and.resolveTo({createdAt: new Date()});
                sendMessageSpy = spyOn(serverFake, "sendMessage").and.callThrough();
    
                // Act
                await bot.guildMemberRemove({
                    memberId: "user 1",
                    memberName: 'user name',
                });
    
                done();
            });
    
            it("should call addTask", () => {
                expect(addTaskSpy).toHaveBeenCalledOnceWith({
                    name: TaskNames.softDeleteUser,
                    data: {
                        userid: 'user 1',
                        usernametag: 'user name',
                    }
                });
            });

            it("should not post F response", () => {
                expect(sendMessageSpy).not.toHaveBeenCalled();
            });
        });

        describe("when user was not kicked recently", () => {

            beforeAll(async done => {
                addTaskSpy = spyOn(taskProcessorFake, 'addTask').and.callThrough();
                isUserBannedSpy = spyOn(serverFake, "isUserBanned").and.callThrough()
                    .and.resolveTo(false);
                getKickAuditLogSpy = spyOn(serverFake, "getKickAuditLog").and.callThrough()
                    .and.resolveTo({createdAt: Helpers.date(new Date()).addSeconds(-61)});
                sendMessageSpy = spyOn(serverFake, "sendMessage").and.callThrough();
    
                // Act
                await bot.guildMemberRemove({
                    memberId: "user 1",
                    memberName: 'user name',
                });
    
                done();
            });
    
            it("should call addTask", () => {
                expect(addTaskSpy).toHaveBeenCalledOnceWith({
                    name: TaskNames.softDeleteUser,
                    data: {
                        userid: 'user 1',
                        usernametag: 'user name',
                    }
                });
            });

            it("should post F response", () => {
                expect(sendMessageSpy).toHaveBeenCalledTimes(1);
            });
        });

        describe("when user left on their own just now", () => {

            beforeAll(async done => {
                addTaskSpy = spyOn(taskProcessorFake, 'addTask').and.callThrough();
                isUserBannedSpy = spyOn(serverFake, "isUserBanned").and.callThrough()
                    .and.resolveTo(false);
                getKickAuditLogSpy = spyOn(serverFake, "getKickAuditLog").and.callThrough()
                    .and.resolveTo(undefined);
                sendMessageSpy = spyOn(serverFake, "sendMessage").and.callThrough();
    
                // Act
                await bot.guildMemberRemove({
                    memberId: "user 1",
                    memberName: 'user name',
                });
    
                done();
            });
    
            it("should call addTask", () => {
                expect(addTaskSpy).toHaveBeenCalledOnceWith({
                    name: TaskNames.softDeleteUser,
                    data: {
                        userid: 'user 1',
                        usernametag: 'user name',
                    }
                });
            });

            it("should post F response", () => {
                expect(sendMessageSpy).toHaveBeenCalledTimes(1);
            });
        });
    });

    describe("message", () => {

        [
            { commandLevel: AccessLevels.Everyone, userLevel: AccessLevels.Banned },
            { commandLevel: AccessLevels.Leader, userLevel: AccessLevels.Banned },
            { commandLevel: AccessLevels.Admin, userLevel: AccessLevels.Banned },
            { commandLevel: AccessLevels.Admin, userLevel: AccessLevels.Everyone },
            { commandLevel: AccessLevels.Admin, userLevel: AccessLevels.Leader },
            { commandLevel: AccessLevels.Leader, userLevel: AccessLevels.Everyone },
        ].forEach(x => {
            
            describe("when user level is lower than the command level", () => {
    
                const settingsHelper = getSettingsHelperFake();
                settingsHelper.settings.osRoleId = "123";
                const commandProcFake = getCommandProcFake();
                const commandCdManagerFake = getCooldownManagerFake();
                const optionsFake: any = {
                    authorId: "userid",
                    message: 'message!',
                    deleteMessage: (s: number): Promise<any> => { return null; },
                    getHighestUserLevel: () => { return null; },
                    send: (m: string, s: number): Promise<any> => { return null; },
                    hasRole: (): boolean => false,
                };
    
                let hasRoleSpy: jasmine.Spy;
                let getHighestLevelSpy: jasmine.Spy;
                let parseSpy: jasmine.Spy;
                let dcGetNameSpy: jasmine.Spy;
                let deleteMessageSpy: jasmine.Spy;
                let sendSpy: jasmine.Spy;
    
                beforeAll(async done => {
                    hasRoleSpy = spyOn(optionsFake, "hasRole").and.callThrough()
                        .and.returnValue(true);
                    parseSpy = spyOn(commandProcFake, 'parse').and.returnValue({
                        level: x.commandLevel,
                        channel: ChannelNames.general,
                        osOnly: true,
                    });
                    getHighestLevelSpy = spyOn(optionsFake, 'getHighestUserLevel').and.callThrough()
                        .and.returnValue(x.userLevel);
                    spyOn(commandCdManagerFake, 'has').and.returnValue(false);
                    dcGetNameSpy = spyOn(DC, 'getName').and.callThrough();
                    deleteMessageSpy = spyOn(optionsFake, 'deleteMessage').and.callThrough().and.resolveTo();
                    sendSpy = spyOn(optionsFake, 'send').and.callThrough().and.resolveTo();

                    const bot = getBot(
                        getServerFake(),
                        commandProcFake,
                        getTaskProcessorFake(),
                        <any>{},
                        commandCdManagerFake,
                        <any>{}, <any>{}, <any>{}, <any>{},
                        settingsHelper);
    
                    await bot.message(optionsFake, null);
    
                    done();
                });

                it("should call hasRole", () => {
                    expect(hasRoleSpy).toHaveBeenCalledOnceWith("123");
                });
    
                it("should call getHighestLevel", () => {
                    expect(getHighestLevelSpy).toHaveBeenCalledTimes(1);
                });

                it("should call deleteMessage", () => {
                    expect(deleteMessageSpy).toHaveBeenCalledTimes(1);
                });

                it("should call send", () => {
                    expect(sendSpy).toHaveBeenCalledTimes(1);
                })

                it("should not call DC.getName", () => {
                    expect(dcGetNameSpy).not.toHaveBeenCalled();
                });
            });
        });

        describe("when user is not OS", () => {
            const commandProcFake = getCommandProcFake();
            const commandCdManagerFake = getCooldownManagerFake();
            const optionsFake: any = {
                authorId: "userid",
                message: 'message!',
                deleteMessage: (s: number): Promise<any> => { return null; },
                getHighestUserLevel: () => { return null; },
                send: (m: string, s: number): Promise<any> => { return null; },
                hasRole: (): boolean => false,
            };
            const settingsHelper = getSettingsHelperFake();
            settingsHelper.settings.osRoleId = "123";

            let hasRoleSpy: jasmine.Spy;
            let getHighestLevelSpy: jasmine.Spy;
            let parseSpy: jasmine.Spy;
            let dcGetNameSpy: jasmine.Spy;
            let deleteMessageSpy: jasmine.Spy;
            let sendSpy: jasmine.Spy;

            beforeAll(async done => {
                hasRoleSpy = spyOn(optionsFake, "hasRole").and.callThrough()
                    .and.returnValue(false);
                parseSpy = spyOn(commandProcFake, 'parse').and.returnValue({
                    level: 0,
                    channel: ChannelNames.general,
                    osOnly: true,
                });
                getHighestLevelSpy = spyOn(optionsFake, 'getHighestUserLevel').and.callThrough();
                spyOn(commandCdManagerFake, 'has').and.returnValue(false);
                dcGetNameSpy = spyOn(DC, 'getName').and.callThrough();
                deleteMessageSpy = spyOn(optionsFake, 'deleteMessage').and.callThrough().and.resolveTo();
                sendSpy = spyOn(optionsFake, 'send').and.callThrough().and.resolveTo();

                const bot = getBot(getServerFake(), commandProcFake, getTaskProcessorFake(), <any>{}, commandCdManagerFake,
                    <any>{}, <any>{}, <any>{}, <any>{}, settingsHelper);

                await bot.message(optionsFake, null);

                done();
            });

            it("should call hasRole", () => {
                expect(hasRoleSpy).toHaveBeenCalledOnceWith("123");
            });

            it("should not call getHighestLevel", () => {
                expect(getHighestLevelSpy).not.toHaveBeenCalled();
            });

            it("should call deleteMessage", () => {
                expect(deleteMessageSpy).toHaveBeenCalledTimes(1);
            });

            it("should call send", () => {
                expect(sendSpy).toHaveBeenCalledTimes(1);
            });

            it("should not call DC.getName", () => {
                expect(dcGetNameSpy).not.toHaveBeenCalled();
            });
        });

        describe("when not allowed in this channel", () => {

            [ 
                { cmd: '!pook', command: { id: CommandValues.pook, channel: ChannelNames.nsfw } }, 
                { cmd: '!пук', command: { id: CommandValues.pook, channel: ChannelNames.nsfw } },
                { cmd: '!ark', command: { id: CommandValues.ark, channel: ChannelNames.nsfw } },
                { cmd: '!арк', command: { id: CommandValues.ark, channel: ChannelNames.nsfw } },
                { cmd: '!o7', command: { id: CommandValues.o7, channel: ChannelNames.bgsInfo } },
                { cmd: '!o7', command: { id: CommandValues.o7, channel: ChannelNames.music } },
            ].forEach(x => {

                describe(`should block command ${x.cmd}`, () => {

                    const cooldownManagerFake = getCooldownManagerFake()
                    const discordServerFake = getServerFake();
                    const commandProcFake = getCommandProcFake();
                    const dbContextFake = getDbContextFake();
        
                    DC.channels([
                        { id: ChannelNames.admin, channelid: `${ChannelNames.admin}`, description: "" },
                        { id: ChannelNames.nsfw, channelid: `${ChannelNames.nsfw}`, description: "" },
                        { id: ChannelNames.general, channelid: `${ChannelNames.general}`, description: "" },
                        { id: ChannelNames.serverInfo, channelid: "3", description: "" },
                        { id: ChannelNames.serverRules, channelid: "4", description: "" },
                        { id: ChannelNames.bgsInfo, channelid: "5", description: "" },
                        { id: ChannelNames.awards, channelid: "7", description: "" },
                        { id: ChannelNames.reception, channelid: "8", description: "" },
                        { id: ChannelNames.music, channelid: "9", description: "" },
                    ]);

                    const optionsFake: any = {
                        authorId: "1",
                        channelId: `1`,
                        channel: x.command.channel,
                        message: x.cmd,
                        send: (m: string, d: number): Promise<void> => { return null },
                        getHighestUserLevel: (): AccessLevels => { return null },
                        deleteMessage: () => {},
                    };

                    let cdHasSpy: jasmine.Spy;
                    let cdAddSpy: jasmine.Spy;
                    let parseSpy: jasmine.Spy;
                    let getHighestUserLevelSpy: jasmine.Spy;
                    let sendSpy: jasmine.Spy;
                    let deleteMessageSpy: jasmine.Spy;

                    let actualCooldown: any;
                    let actualSentMessage: string;
        
                    beforeAll(async done => {
                        cdHasSpy = spyOn(cooldownManagerFake, 'has').and.callThrough().and.returnValue(false);
                        cdAddSpy = spyOn(cooldownManagerFake, 'add').and.callThrough()
                            .and.callFake(cd => actualCooldown = cd);

                        parseSpy = spyOn(commandProcFake, 'parse').and.callThrough()
                            .and.returnValue({
                                id: x.command.id,
                                channel: x.command.channel,
                            });
                        getHighestUserLevelSpy = spyOn(optionsFake, 'getHighestUserLevel').and.callThrough();
                        sendSpy = spyOn(optionsFake, 'send').and.callThrough()
                            .and.callFake((msg: string) => {
                                actualSentMessage = msg;
                                return Promise.resolve(null);
                            });
                        deleteMessageSpy = spyOn(optionsFake, 'deleteMessage').and.callThrough();

                        const bot = getBot(discordServerFake, commandProcFake, getTaskProcessorFake(), <any>{}, cooldownManagerFake, getDbContextProviderFake());
        
                        // Act
                        await bot.message(optionsFake, dbContextFake);
        
                        done();
                    });

                    it("should add cooldown", () => {
                        expect(cdAddSpy).toHaveBeenCalledTimes(1);
                        expect(actualCooldown.seconds).toEqual(5);
                        expect(actualCooldown.userid).toBeTruthy();
                    });

                    it("should call send", () => {
                        expect(sendSpy).toHaveBeenCalledTimes(1);
                    });

                    it("should send message 'Команда работает только...'", () => {
                        expect(actualSentMessage).toMatch(/^Команда работает только/);
                    });

                    it("should call getHighestUserLevel", () => {
                        expect(getHighestUserLevelSpy).toHaveBeenCalledTimes(1);
                    });

                    it("should call parse", () => {
                        expect(parseSpy).toHaveBeenCalledTimes(1);
                    });

                    it("should delete message", () => {
                        expect(deleteMessageSpy).toHaveBeenCalledTimes(1);
                    });
                });

            });
        });
    });

    describe("ban", () => {

        describe("if old member not yet banned", () => {

            describe("valid arguments specified", () => {
                const mentions = [ { userid: 'userid', usernametag: 'user' } ];
                const optionsFake: any = {
                    ban: (): Promise<any> => { return null },
                    mentions: (): any[] => { return mentions },
                    send: (m: string, d: number): Promise<any> => { return null },
                    deleteMessage: (s:number): Promise<any> => { return null },
                };
    
                const dbContextFake:any = getDbContextFake();
                const taskProcessorFake:any = getTaskProcessorFake();
    
                let getByIdSpy: jasmine.Spy;
                let insertSpy: jasmine.Spy;
                let banSpy: jasmine.Spy;
                let sendSpy: jasmine.Spy;
                let addTaskSpy: jasmine.Spy;
                let deleteMessageSpy: jasmine.Spy;
    
                let actualBanUserTask: any;
    
                beforeAll(async done => {
                    getByIdSpy = spyOn(dbContextFake.userRepo, "getById").and.callThrough()
                        .and.callFake(() => Promise.resolve(null));
                    insertSpy = spyOn(dbContextFake.userRepo, 'insert').and.callThrough()
                        .and.callFake(() => Promise.resolve({
                            userid: mentions[0].userid,
                            usernametag: mentions[0].usernametag,
                        }));
                    banSpy = spyOn(optionsFake, 'ban').and.callThrough().and.callFake(() => Promise.resolve(mentions[0].userid));
                    sendSpy = spyOn(optionsFake, 'send').and.callThrough().and.callFake(() => Promise.resolve());
                    addTaskSpy = spyOn(taskProcessorFake, 'addTask').and.callThrough()
                        .and.callFake(task => actualBanUserTask = task);
                    deleteMessageSpy = spyOn(optionsFake, 'deleteMessage').and.callThrough()
                        .and.callFake(() => Promise.resolve());
    
                    // Act
                    const bot = getBot(getServerFake(), getCommandProcFake(), taskProcessorFake, <any>{}, getCooldownManagerFake(), getDbContextProviderFake());
                    await bot.ban(<IBanCommand>{
                        id: CommandValues.ban,
                        args: { hours: 10 },
                    },
                    optionsFake,
                    dbContextFake);
                    done();
                });
    
                it("should call getById", () => {
                    expect(getByIdSpy).toHaveBeenCalledWith(mentions[0].userid);
                    expect(getByIdSpy).toHaveBeenCalledTimes(1);
                });
    
                it("should call insert", () => {
                    expect(insertSpy).toHaveBeenCalledTimes(1);
                });
    
                it("should call ban", () => {
                    expect(banSpy).toHaveBeenCalledTimes(1);
                });
    
                it("should call addTask", () => {
                    expect(addTaskSpy).toHaveBeenCalledTimes(1);
                    expect(addTaskSpy).toHaveBeenCalledWith(actualBanUserTask);
                    expect((<Date>actualBanUserTask.data.banUntil).getTime()).toBeGreaterThan(Helpers.utcDate().getTime());
                    expect(actualBanUserTask.data.userid).toEqual('userid');
                });

                it("should call send", () => {
                    expect(sendSpy).toHaveBeenCalledTimes(1);
                    expect(sendSpy).toHaveBeenCalledWith(`<@userid> забанен на 10 часов.`, 10);
                });
    
                it("should call deleteMessage", () => {
                    expect(deleteMessageSpy).toHaveBeenCalledTimes(1);
                });
            });

            [
                { hours: 1000000000000, expectedResponseMessage: 'Ты что его на всю жизнь хочешь забанить???' },
            ].forEach(x => {
                describe("hours specified not a valid value", () => {
                    const mentions = [ { userid: 'userid', usernametag: 'user' } ];
                    const optionsFake: any = {
                        ban: (): Promise<any> => { return null },
                        mentions: (): any[] => { return mentions },
                        send: (m: string, d: number): Promise<any> => { return null },
                        deleteMessage: (s:number): Promise<any> => { return null },
                    };
        
                    const dbContextFake:any = getDbContextFake();
                    const taskProcessorFake:any = getTaskProcessorFake();
        
                    let getByIdSpy: jasmine.Spy;
                    let insertSpy: jasmine.Spy;
                    let banSpy: jasmine.Spy;
                    let sendSpy: jasmine.Spy;
                    let addTaskSpy: jasmine.Spy;
                    let deleteMessageSpy: jasmine.Spy;
        
                    beforeAll(async done => {
                        getByIdSpy = spyOn(dbContextFake.userRepo, "getById").and.callThrough()
                            .and.callFake(() => Promise.resolve({
                                userid: 'userid',
                                usernametag: 'user',
                            }));
                        insertSpy = spyOn(dbContextFake.userRepo, 'insert').and.callThrough();
                        banSpy = spyOn(optionsFake, 'ban').and.callThrough();
                        sendSpy = spyOn(optionsFake, 'send').and.callThrough().and.callFake(() => Promise.resolve());
                        addTaskSpy = spyOn(taskProcessorFake, 'addTask').and.callThrough();
                        deleteMessageSpy = spyOn(optionsFake, 'deleteMessage').and.callThrough()
                            .and.callFake(() => Promise.resolve());
        
                        // Act
                        const bot = getBot(getServerFake(), getCommandProcFake(), taskProcessorFake, <any>{}, getCooldownManagerFake(), getDbContextProviderFake());
                        await bot.ban(<IBanCommand>{
                            id: CommandValues.ban,
                            args: { hours: x.hours, isPerm: false },
                        },
                        optionsFake,
                        dbContextFake);
                        done();
                    });
    
                    it("should not call insert", () => {
                        expect(insertSpy).not.toHaveBeenCalled();
                    });
    
                    it("should not call ban", () => {
                        expect(banSpy).not.toHaveBeenCalled();
                    });
    
                    it("should not call addTask", () => {
                        expect(addTaskSpy).not.toHaveBeenCalled();
                    });
    
                    it("should call send", () => {
                        expect(sendSpy).toHaveBeenCalledWith(x.expectedResponseMessage, 10);
                    });
    
                    it("should call deleteMessage", () => {
                        expect(deleteMessageSpy).toHaveBeenCalledTimes(1);
                    });
                });
            });
        });
    
        describe("if member has already been banned", () => {
            // TODO: Лень
        });
    });

    describe("unban", () => {

        describe("when valid arguments specified", () => {

            const mentions = [ { userid: 'userid', usernametag: 'user' } ];
            const optionsFake: any = {
                mentions: (): any[] => { return mentions },
                send: (m: string, d: number): Promise<any> => { return null },
                deleteMessage: (s:number): Promise<any> => { return null },
                isBanned: (userid: string): Promise<any> => { return null },
                unban: (): Promise<any> => { return null },
            };

            const taskProcessorFake:any = getTaskProcessorFake();

            let unbanSpy: jasmine.Spy;
            let sendSpy: jasmine.Spy;
            let addTaskSpy: jasmine.Spy;
            let deleteMessageSpy: jasmine.Spy;
            let isBannedSpy: jasmine.Spy;

            let actualUnbanUserTask: any;

            beforeAll(async done => {
                isBannedSpy = spyOn(optionsFake, 'isBanned').and.resolveTo(true);
                unbanSpy = spyOn(optionsFake, 'unban').and.callThrough().and.callFake(() => Promise.resolve());
                sendSpy = spyOn(optionsFake, 'send').and.callThrough().and.callFake(() => Promise.resolve());
                addTaskSpy = spyOn(taskProcessorFake, 'addTask').and.callThrough().and
                    .callFake(task => actualUnbanUserTask = task);
                deleteMessageSpy = spyOn(optionsFake, 'deleteMessage').and.callThrough()
                    .and.callFake(() => Promise.resolve());

                const bot = getBot(getServerFake(), getCommandProcFake(), taskProcessorFake, <any>{}, getCooldownManagerFake(), getDbContextProviderFake());
                await bot.unban(optionsFake);
                done();
            });

            it("should call isBanned", () => {
                expect(isBannedSpy).toHaveBeenCalledOnceWith('userid');
            });

            it("should call unban", () => {
                expect(unbanSpy).toHaveBeenCalledTimes(1);
            });

            it("should call send", () => {
                expect(sendSpy).toHaveBeenCalledOnceWith('<@userid> разбанен.', 10);
            });

            it("should call addTask", () => {
                expect(addTaskSpy).toHaveBeenCalledTimes(1);
                expect(actualUnbanUserTask.name).toEqual(TaskNames.unBanUser);
                expect(actualUnbanUserTask.data.userid).toEqual('userid');
            });

            it("should call deleteMessage", () => {
                expect(deleteMessageSpy).toHaveBeenCalledTimes(1);
            });
        });

    });

    describe("unbanPending", () => {

        const dbContextProviderFake = getDbContextProviderFake();
        const dbContextFake = getDbContextFake();
        const serverFake = getServerFake();

        let executeSpy: jasmine.Spy;
        let getExpiredBannedUsersSpy: jasmine.Spy;
        let serverUnbanUserSpy: jasmine.Spy;
        let dbUnbanUserSpy: jasmine.Spy;

        const user1 = {
            userid: '1',
            usernametag: 'name 1',
        };

        const user2 = {
            userid: '2',
            usernametag: 'name 2',
        };

        beforeAll(async done => {
            executeSpy = spyOn(dbContextProviderFake, 'execute')
                .and.callFake(callback => {
                    callback(dbContextFake).then();
                });
            getExpiredBannedUsersSpy = spyOn(dbContextFake.userRepo, 'getExpiredBannedUsers').and.returnValues([user1, user2], []);
            serverUnbanUserSpy = spyOn(serverFake, 'unbanUser').and.callThrough()
                .and.resolveTo();
            dbUnbanUserSpy = spyOn(dbContextFake.userRepo, 'unbanUser').and.callThrough()
                .and.resolveTo();

            const bot = getBot(serverFake, getCommandProcFake(), getTaskProcessorFake(), <any>{}, getCooldownManagerFake(), dbContextProviderFake);

            // Act
            await bot.unbanPending();

            done();
        });

        it("should call unbanUser on server", () => {
            expect(serverUnbanUserSpy).toHaveBeenCalledTimes(2);
            expect(serverUnbanUserSpy).toHaveBeenCalledWith(user1.userid);
            expect(serverUnbanUserSpy).toHaveBeenCalledWith(user2.userid);
        });

        it("should call unbanUser in db", () => {
            expect(dbUnbanUserSpy).toHaveBeenCalledTimes(2);
            expect(dbUnbanUserSpy).toHaveBeenCalledWith(user1.userid);
            expect(dbUnbanUserSpy).toHaveBeenCalledWith(user2.userid);
        });
    });

    describe("kick", () => {

        describe("when command is valid", () => {

            [
                { authorid: 'not userid', response: '<@userid> , пошёл нахуй с этого сервера' },
                { authorid: 'userid', response: '<@userid> , кикнул сам себя. Можешь не возвращаться' },
            ].forEach(x => {
                describe("and", () => {
                    const mentions = [ { userid: 'userid', usernametag: 'user' } ];
                    const optionsFake: any = {
                        authorId: x.authorid,
                        mentions: (): any[] => { return mentions },
                        send: (m: string, d?: number): Promise<any> => { return null },
                        kick: (userid: string, reason?: string): Promise<any> => { return null },
                        kickable: (userid: string): boolean => { return true }
                    };
        
                    let kickableSpy: jasmine.Spy;
                    let kickSpy: jasmine.Spy;
                    let sendSpy: jasmine.Spy;
        
                    beforeAll(async done => {
                        kickableSpy = spyOn(optionsFake, 'kickable').and.callThrough();
                        kickSpy = spyOn(optionsFake, 'kick').and.callThrough().and.resolveTo();
                        sendSpy = spyOn(optionsFake, 'send').and.callThrough().and.resolveTo();
        
                        const bot = getBot(getServerFake(), <any>{}, getTaskProcessorFake(), <any>{}, <any>{}, <any>{});
        
                        await bot.kick(<IKickCommand>{
                            args: {reason: 'some reason'},
                        },
                        optionsFake);
        
                        done();
                    });

                    it("should call kickable", () => {
                        expect(kickableSpy).toHaveBeenCalledTimes(1);
                    });
    
                    it("should call kick", () => {
                        expect(kickSpy).toHaveBeenCalledOnceWith('userid', 'some reason');
                    });
    
                    it("should call send", () => {
                        expect(sendSpy).toHaveBeenCalledOnceWith(x.response);
                    });
    
                });

            });

        });

        describe("when user not kickable", () => {
            // TODO
        });

        describe("when no mentions", () => {
            // TODO
        });

    });

    describe("prune", () => {

        describe("when command is valid", () => {

            const cooldownManagerFake = getCooldownManagerFake();
            const optionsFake: any = {
                deleteMessage: (): Promise<any> => { return null; },
                send: (text: string, deleteAfter?: number): Promise<any> => { return null; },
                deleteMessages: (n: number): Promise<any> => { return null; },
            };

            const cooldownFake: any = { delete: (): void => { return;} };

            let deleteMessageSpy: jasmine.Spy;
            let sendSpy: jasmine.Spy;
            let hasSpy: jasmine.Spy;
            let addSpy: jasmine.Spy;
            let deleteMessagesSpy: jasmine.Spy;
            let deleteCooldownSpy: jasmine.Spy;

            let actualCooldown: ICooldown;

            beforeAll(async done => {
                deleteMessageSpy = spyOn(optionsFake, 'deleteMessage').and.callThrough()
                    .and.callFake(() => Promise.resolve());
                hasSpy = spyOn(cooldownManagerFake, 'has').and.callThrough().and.returnValue(false);
                addSpy = spyOn(cooldownManagerFake, 'add').and.callThrough()
                    .and.callFake(cd => {
                        actualCooldown = cd;
                        return cooldownFake;
                    });
                deleteMessagesSpy = spyOn(optionsFake, 'deleteMessages').and.callThrough()
                    .and.callFake(() => Promise.resolve());
                deleteCooldownSpy = spyOn(cooldownFake, 'delete').and.callThrough();
                sendSpy = spyOn(optionsFake, 'send').and.callThrough().and.callFake(() => Promise.resolve());

                // Act
                const bot = getBot(getServerFake(), getCommandProcFake(), getTaskProcessorFake(), getBgsClientFake(), cooldownManagerFake, getDbContextProviderFake());
                bot.prune({
                    id: CommandValues.prune,
                    args: ["10"],
                    description: '',
                    cmds: [],
                },
                optionsFake);
                done();
            });

            it("should call deleteMessage", () => {
                expect(deleteMessageSpy).toHaveBeenCalledTimes(1);
            });

            it("should call has", () => {
                expect(hasSpy).toHaveBeenCalledTimes(1);
            });

            it("should call deleteMessages with 10", () => {
                expect(deleteMessagesSpy).toHaveBeenCalledOnceWith(10);
            });

            it("should delete cooldown", () => {
                expect(deleteCooldownSpy).toHaveBeenCalledTimes(1);
            });

            it("should call send", () => {
                expect(sendSpy).toHaveBeenCalledTimes(1);
            });
        });

        [
            { args: ['a'], expectedResponse: "Ты чё, ебан?" },
            { args: [''], expectedResponse: "Ты чё, ебан?" },
            { args: [undefined], expectedResponse: "Ты чё, ебан?" },
            { args: [null], expectedResponse: "Ты чё, ебан?" },
            { args: ['101'], expectedResponse: "Слишком многого просишь." },
            { args: [101], expectedResponse: "Слишком многого просишь." },
        ].forEach(x => {

            describe("when command invalid", () => {
    
                const cooldownManagerFake = getCooldownManagerFake();
                const optionsFake: any = {
                    deleteMessage: (): Promise<any> => { return null; },
                    send: (text: string, deleteAfter?: number): Promise<any> => { return null; },
                    deleteMessages: (n: number): Promise<any> => { return null; },
                };
    
                let deleteMessageSpy: jasmine.Spy;
                let sendSpy: jasmine.Spy;
                let deleteMessagesSpy: jasmine.Spy;
    
                beforeAll(async done => {
                    deleteMessageSpy = spyOn(optionsFake, 'deleteMessage').and.callThrough()
                        .and.callFake(() => Promise.resolve());
                    deleteMessagesSpy = spyOn(optionsFake, 'deleteMessages').and.callThrough();
                    sendSpy = spyOn(optionsFake, 'send').and.callThrough().and.callFake(() => Promise.resolve());

                    // Act
                    const bot = getBot(getServerFake(), getCommandProcFake(), getTaskProcessorFake(), getBgsClientFake(), cooldownManagerFake, getDbContextProviderFake());
                    bot.prune({
                        id: CommandValues.prune,
                        args: x.args,
                        description: '',
                        cmds: [],
                    },
                    optionsFake);
                    done();
                });

                it("should call deleteMessage", () => {
                    expect(deleteMessageSpy).toHaveBeenCalledTimes(1);
                });

                it("should not call deleteMessages", () => {
                    expect(deleteMessagesSpy).not.toHaveBeenCalled();
                });

                it("should call send with appropriate response", () => {
                    expect(sendSpy).toHaveBeenCalledOnceWith(x.expectedResponse, 10);
                });
            });

        });

    });

    describe("syncUserRolesTask", () => {

        describe("when users is NULL", () => {
            const dbContextFake = getDbContextFake();
            const serverFake = getServerFake();
            const taskProcessorFake = getTaskProcessorFake();
            const bot = getBot(serverFake, getCommandProcFake(), taskProcessorFake, <any>{}, getCooldownManagerFake(), getDbContextProviderFake(), <any>{}, <any>{});

            let getServerUsersSpy: jasmine.Spy;
            let getMemberRolesSpy: jasmine.Spy;
            let addTaskSpy: jasmine.Spy;
            let insertSpy: jasmine.Spy;
            let insertManyRoleUsersSpy: jasmine.Spy;
            
            const expectedUsers = [
                {
                    id: 1,
                    name: 'name 1',
                },
                {
                    id: 2,
                    name: 'name 2',
                },
            ];

            beforeAll(async done => {
                getServerUsersSpy = spyOn(serverFake, "getServerUsers").and.callThrough()
                    .and.resolveTo(expectedUsers);
                getMemberRolesSpy = spyOn(serverFake, "getMemberRoles").and.callThrough();
                addTaskSpy = spyOn(taskProcessorFake, "addTask").and.callThrough();
                insertSpy = spyOn(dbContextFake.roleRepo, 'insert').and.callThrough();
                insertManyRoleUsersSpy = spyOn(dbContextFake.roleRepo, 'insertManyRoleUsers')
                    .and.callThrough();

                // Act
                await bot.syncUserRolesTask(null, dbContextFake);
                done();
            });

            it("should add a task with users", () => {
                expect(addTaskSpy).toHaveBeenCalledOnceWith({
                    name: TaskNames.syncUserRoles,
                    data: {
                        users: expectedUsers,
                    },
                });
            });

            it("should not call getMemberRoles", () => {
                expect(getMemberRolesSpy).not.toHaveBeenCalled();
            });

            it("sgould not call insert", () => {
                expect(insertSpy).not.toHaveBeenCalled();
            });

            it("should not call insertManyRoleUsers", () => {
                expect(insertManyRoleUsersSpy).not.toHaveBeenCalled();
            });
        });

        describe("when users is not NULL and not empty but no roles", () => {
            const dbContextFake = getDbContextFake();
            const serverFake = getServerFake();
            const taskProcessorFake = getTaskProcessorFake();
            const bot = getBot(serverFake, getCommandProcFake(), taskProcessorFake, <any>{}, getCooldownManagerFake(), getDbContextProviderFake(), <any>{}, <any>{});

            let getServerUsersSpy: jasmine.Spy;
            let getMemberRolesSpy: jasmine.Spy;
            let addTaskSpy: jasmine.Spy;
            let insertSpy: jasmine.Spy;
            let insertManyRoleUsersSpy: jasmine.Spy;
            
            const expectedRoles = [
                {
                    id: 1,
                    name: 'role 1',
                },
                {
                    id: 2,
                    name: 'role 2',
                },
            ];
            const expectedUsers = [
                {
                    id: 1,
                    name: 'name 1',
                },
                {
                    id: 2,
                    name: 'name 2',
                },
            ];

            beforeAll(async done => {
                getServerUsersSpy = spyOn(serverFake, "getServerUsers").and.callThrough()
                    .and.resolveTo(expectedUsers);
                getMemberRolesSpy = spyOn(serverFake, "getMemberRoles").and.callThrough()
                    .and.resolveTo(expectedRoles);
                addTaskSpy = spyOn(taskProcessorFake, "addTask").and.callThrough();
                insertSpy = spyOn(dbContextFake.roleRepo, 'insert').and.callThrough();
                insertManyRoleUsersSpy = spyOn(dbContextFake.roleRepo, 'insertManyRoleUsers')
                    .and.callThrough();

                // Act
                await bot.syncUserRolesTask(<any>expectedUsers, dbContextFake);
                done();
            });

            it("should not call getServerUsers", () => {
                expect(getServerUsersSpy).not.toHaveBeenCalled();
            });

            it("should call addTask with user and their roles", () => {
                expect(addTaskSpy).toHaveBeenCalledTimes(2);
                expect(addTaskSpy).toHaveBeenCalledWith(
                    {
                        name: TaskNames.syncUserRoles,
                        data: {
                            users: [
                                {
                                    id: 1,
                                    name: 'name 1',
                                    roles: expectedRoles,
                                },
                            ],
                        },
                    });
                expect(addTaskSpy).toHaveBeenCalledWith(
                    {
                        name: TaskNames.syncUserRoles,
                        data: {
                            users: [
                                {
                                    id: 2,
                                    name: 'name 2',
                                    roles: expectedRoles,
                                },
                            ],
                        },
                    });
            });

            it("should call getMemberRoles", () => {
                expect(getMemberRolesSpy).toHaveBeenCalledTimes(2);
                expect(getMemberRolesSpy).toHaveBeenCalledWith(expectedUsers[0].id);
                expect(getMemberRolesSpy).toHaveBeenCalledWith(expectedUsers[1].id);
            });

            it("sgould not call insert", () => {
                expect(insertSpy).not.toHaveBeenCalled();
            });

            it("should not call insertManyRoleUsers", () => {
                expect(insertManyRoleUsersSpy).not.toHaveBeenCalled();
            });
        });

        describe("when users is not NULL and not empty and with roles", () => {
            const dbContextFake = getDbContextFake();
            const serverFake = getServerFake();
            const taskProcessorFake = getTaskProcessorFake();
            const bot = getBot(serverFake, getCommandProcFake(), taskProcessorFake, <any>{}, getCooldownManagerFake(), getDbContextProviderFake(), <any>{}, <any>{});

            let getServerUsersSpy: jasmine.Spy;
            let getMemberRolesSpy: jasmine.Spy;
            let addTaskSpy: jasmine.Spy;
            let insertSpy: jasmine.Spy;
            let insertManyRoleUsersSpy: jasmine.Spy;
            let getUserRolesSpy: jasmine.Spy;
            let deleteManyRoleUsersSpy: jasmine.Spy;
            
            const expectedRoles = [
                {
                    id: '1',
                    name: 'role 1',
                },
                {
                    id: '2',
                    name: 'role 2',
                },
            ];
            const expectedUsers = [
                {
                    id: '1',
                    name: 'name 1',
                    roles: expectedRoles,
                },
            ];

            const expectedUserRoles = [
                {
                    roleid: '1',
                },
                {
                    roleid: '2',
                },
                {
                    roleid: '3',
                },
            ];

            beforeAll(async done => {
                getServerUsersSpy = spyOn(serverFake, "getServerUsers").and.callThrough();
                getMemberRolesSpy = spyOn(serverFake, "getMemberRoles").and.callThrough();
                addTaskSpy = spyOn(taskProcessorFake, "addTask").and.callThrough();
                insertSpy = spyOn(dbContextFake.userRepo, 'insert').and.callThrough();
                insertManyRoleUsersSpy = spyOn(dbContextFake.roleRepo, 'insertManyRoleUsers')
                    .and.callThrough();
                getUserRolesSpy = spyOn(dbContextFake.roleRepo, 'getUserRoles').and.callThrough()
                    .and.resolveTo(expectedUserRoles);
                deleteManyRoleUsersSpy = spyOn(dbContextFake.roleRepo, 'deleteManyRoleUsers')
                    .and.callThrough();

                // Act
                await bot.syncUserRolesTask(expectedUsers, dbContextFake);
                done();
            });

            it("should not call getServerUsers", () => {
                expect(getServerUsersSpy).not.toHaveBeenCalled();
            });

            it("should not call addTask", () => {
                expect(addTaskSpy).toHaveBeenCalledTimes(0);
            });

            it("should not call getMemberRoles", () => {
                expect(getMemberRolesSpy).toHaveBeenCalledTimes(0);
            });

            it("should call insert", () => {
                expect(insertSpy).toHaveBeenCalledOnceWith({
                    userid: expectedUsers[0].id,
                    usernametag: expectedUsers[0].name,
                });
            });

            it("should call insertManyRoleUsers", () => {
                expect(insertManyRoleUsersSpy).toHaveBeenCalledTimes(1);
            });

            it("should delete role from db", () => {
                expect(deleteManyRoleUsersSpy).toHaveBeenCalledOnceWith([
                    {
                        userid: '1',
                        roleid: '3',
                    },
                ]);
            });
        });

    });

    describe("sendMembersToTheZoo", () => {

        describe("when user has not yet been exiled", () => {

            const server = getServerFake();
            const dbContextProvider = getDbContextProviderFake();
            const dbContext = getDbContextFake();
            const settingsHelper = getSettingsHelperFake();
            settingsHelper.settings = {
                allowedInactivityDays: 10,
                zooRoleId: '12',
                zooKeeperRoleId: '13',
                zooExemptionRoleIds: ['exemption role id'],
            };
            const bot = getBot(server, getCommandProcFake(), getTaskProcessorFake(), getBgsClientFake(), getCooldownManagerFake(),
                dbContextProvider, getCooldownManagerFake(), getArksFake(), getAwardsFake(), settingsHelper);
    
            let getInactiveUsersSpy: jasmine.Spy;
            let setRolesToMemberSpy: jasmine.Spy;
            let getRoleSpy: jasmine.Spy;
            let getMemberRolesSpy: jasmine.Spy;
            let removeRolesFromMemberSpy: jasmine.Spy;
            let isBotSpy: jasmine.Spy;
            let userExistsSpy: jasmine.Spy;
            let deleteSpy: jasmine.Spy;
    
            let roles = {};
            roles[AccessLevels.Leader] = "role1";
            roles[AccessLevels.Admin] = "role2";
            roles[AccessLevels.Fuhrer] = "role3";
    
            let users: IUser[] = [
                { userid: 'user 1', usernametag: 'user name' },
                { userid: 'bot', usernametag: 'some bot' },
            ];
    
            beforeAll(async done => {
                getRoleSpy = spyOn(server, 'getRole').and.callThrough().and.resolveTo(true);
                spyOn(dbContextProvider, 'execute').and.callThrough()
                    .and.callFake(callback => callback(dbContext).then());
                getInactiveUsersSpy = spyOn(dbContext.userRepo, 'getInactiveUsers').and.callThrough()
                    .and.resolveTo(users);
                spyOn(server, 'getRoleIdForAccessLevel').and.callThrough()
                    .and.callFake((level: AccessLevels) => roles[level]);
                spyOn(server, 'userHasRole').and.callThrough().and.resolveTo(false);
                setRolesToMemberSpy = spyOn(server, 'setRolesToMember').and.callThrough().and.resolveTo();
                getMemberRolesSpy = spyOn(server, 'getMemberRoles').and.callThrough().and.resolveTo([{id: "role4", name: "role name"}]);
                removeRolesFromMemberSpy = spyOn(server, 'removeRolesFromMember').and.callThrough();
                isBotSpy = spyOn(server, 'isBot').and.callThrough()
                    .and.callFake(userid => Promise.resolve(userid === 'bot'));
                userExistsSpy = spyOn(server, 'userExists').and.callThrough()
                    .and.resolveTo(true);
                deleteSpy = spyOn(dbContext.userRepo, 'delete').and.callThrough();

                // Act
                await bot.sendMembersToTheZoo();
                done();
            });
    
            it("should call getInactiveUsers", () => {
                expect(getInactiveUsersSpy).toHaveBeenCalledOnceWith(10, ["12", "13", "role1", "role2", "role3", "exemption role id"]);
            });

            it("should add role zoo to user", () => {
                expect(setRolesToMemberSpy).toHaveBeenCalledOnceWith('user 1', ['12']);
            });

            it("should call getRole", () => {
                expect(getRoleSpy).toHaveBeenCalledWith('12');
                expect(getRoleSpy).toHaveBeenCalledWith('13');
            });

            it("should call getMemberRoles", () => {
                expect(getMemberRolesSpy).toHaveBeenCalledOnceWith('user 1');
            });

            it("should call removeRolesFromMember", () => {
                expect(removeRolesFromMemberSpy).toHaveBeenCalledOnceWith('user 1', ["role4"]);
            });

            it("should call isBot for each user", () => {
                expect(isBotSpy).toHaveBeenCalledTimes(2);
            });

            it("should not delete any users", () => {
                expect(deleteSpy).not.toHaveBeenCalled();
            });
        });

        describe("when user has already been exiled", () => {

            const server = getServerFake();
            const dbContextProvider = getDbContextProviderFake();
            const dbContext = getDbContextFake();
            const settingsHelper = getSettingsHelperFake();
            settingsHelper.settings = {
                allowedInactivityDays: 10,
                zooRoleId: '12',
                zooKeeperRoleId: '13',
                zooExemptionRoleIds: ['exemption role id'],
            };
            const bot = getBot(server, getCommandProcFake(), getTaskProcessorFake(), getBgsClientFake(), getCooldownManagerFake(),
                dbContextProvider, getCooldownManagerFake(), getArksFake(), getAwardsFake(), settingsHelper);
    
            let getInactiveUsersSpy: jasmine.Spy;
            let setRolesToMemberSpy: jasmine.Spy;
            let getRoleSpy: jasmine.Spy;
            let isBotSpy: jasmine.Spy;
            let userExistsSpy: jasmine.Spy;
            let deleteSpy: jasmine.Spy;
            let insertManyRoleUsersSpy: jasmine.Spy;
    
            let roles = {};
            roles[AccessLevels.Leader] = "role1";
            roles[AccessLevels.Admin] = "role2";
            roles[AccessLevels.Fuhrer] = "role3";
    
            let users: IUser[] = [
                { userid: 'user 1', usernametag: 'user name' },
            ];
    
            beforeAll(async done => {
                getRoleSpy = spyOn(server, 'getRole').and.callThrough().and.resolveTo(true);
                spyOn(dbContextProvider, 'execute').and.callThrough()
                    .and.callFake(callback => callback(dbContext).then());
                getInactiveUsersSpy = spyOn(dbContext.userRepo, 'getInactiveUsers').and.callThrough()
                    .and.resolveTo(users);
                insertManyRoleUsersSpy = spyOn(dbContext.roleRepo, 'insertManyRoleUsers').and.callThrough()
                    .and.resolveTo();
                spyOn(server, 'getRoleIdForAccessLevel').and.callThrough()
                    .and.callFake((level: AccessLevels) => roles[level]);
                spyOn(server, 'userHasRole').and.callThrough().and.resolveTo(true);
                setRolesToMemberSpy = spyOn(server, 'setRolesToMember').and.callThrough().and.resolveTo();
                isBotSpy = spyOn(server, 'isBot').and.callThrough().and.resolveTo(false);
                userExistsSpy = spyOn(server, 'userExists').and.callThrough()
                    .and.resolveTo(true);
                deleteSpy = spyOn(dbContext.userRepo, 'delete').and.callThrough();
                
                // Act
                await bot.sendMembersToTheZoo();
                done();
            });
    
            it("should call getInactiveUsers", () => {
                expect(getInactiveUsersSpy).toHaveBeenCalledOnceWith(10, ["12", "13", "role1", "role2", "role3", "exemption role id"]);
            });

            it("should not add role zoo to user", () => {
                expect(setRolesToMemberSpy).not.toHaveBeenCalled();
            });

            it("should call getRole", () => {
                expect(getRoleSpy).toHaveBeenCalledWith('12');
                expect(getRoleSpy).toHaveBeenCalledWith('13');
            });

            it("should not delete any users", () => {
                expect(deleteSpy).not.toHaveBeenCalled();
            });

            it("should insert zoo role in DB", () => {
                expect(insertManyRoleUsersSpy).toHaveBeenCalledTimes(1);
            })
        });
    });

    describe("userUpdateTask", () => {

        [
            { isRemoved: true, },
            { isRemoved: false, },
        ].forEach(t => {
            describe(`when role ZOO is ${t.isRemoved ? 'removed' : 'not removed'}`, () => {
    
                const server = getServerFake();
                const dbContextProvider = getDbContextProviderFake();
                const dbContext = getDbContextFake();
                const settingsHelper = getSettingsHelperFake();
                const taskProcessor = getTaskProcessorFake();
                settingsHelper.settings.zooRoleId = 'zoo';
    
                const bot = getBot(server, getCommandProcFake(), taskProcessor, getBgsClientFake(), getCooldownManagerFake(),
                    dbContextProvider, getCooldownManagerFake(), getArksFake(), getAwardsFake(), settingsHelper);
    
                const user: IUser = {
                    userid: '1',
                    usernametag: 'user name',
                };
                const rolesAdded: IRole[] = [
                    { roleid: 'role 1', name: 'role name 1', color: '' },
                ];
                const rolesRemoved: IRole[] = [
                    { roleid: 'role 2', name: 'role name 2', color: '' },
                ];
                if (t.isRemoved) {
                    rolesRemoved.push({ roleid: 'zoo', name: 'зоопарк', color: '' });
                }
    
                let isBotSpy: jasmine.Spy;
                let insertSpy: jasmine.Spy;
                let updateSpy: jasmine.Spy;
                let insertManyRoleUsersSpy: jasmine.Spy;
                let deleteManyRoleUsersSpy: jasmine.Spy;
                let addTaskSpy: jasmine.Spy;
                let userHasAnyRoleOfSpy: jasmine.Spy;
                let getUserPersonalRoleSpy: jasmine.Spy;
    
                let actualAddedRoleUsers: IRoleUser[];
                let actualRemovedRoleUsers: IRoleUser[];
    
                beforeAll(async done => {
                    isBotSpy = spyOn(server, 'isBot').and.callThrough().and.resolveTo(false);
                    insertSpy = spyOn(dbContext.userRepo, 'insert').and.callThrough().and.resolveTo();
                    updateSpy = spyOn(dbContext.userRepo, 'update').and.callThrough().and.resolveTo();
                    insertManyRoleUsersSpy = spyOn(dbContext.roleRepo, 'insertManyRoleUsers').and.callThrough()
                        .and.callFake(r => {
                            actualAddedRoleUsers = r;
                            return Promise.resolve();
                        });
                    deleteManyRoleUsersSpy = spyOn(dbContext.roleRepo, 'deleteManyRoleUsers').and.callThrough()
                        .and.callFake(r => {
                            actualRemovedRoleUsers = r;
                            return Promise.resolve();
                        })
                    addTaskSpy = spyOn(taskProcessor, 'addTask').and.callThrough();
                    userHasAnyRoleOfSpy = spyOn(server, 'userHasAnyRoleOf').and.callThrough()
                        .and.resolveTo(false);
                    getUserPersonalRoleSpy = spyOn(dbContext.roleRepo, 'getUserPersonalRole').and.callThrough()
                        .and.resolveTo(null);
    
                    // Act
                    await bot.userUpdateTask(user, rolesAdded, rolesRemoved, dbContext);
                    done();
                });

                it("should check if member is bot", () => {
                    expect(isBotSpy).toHaveBeenCalledOnceWith(user.userid);
                });
    
                it("should call insert", () => {
                    expect(insertSpy).toHaveBeenCalledOnceWith(user);
                });
    
                it("should call update", () => {
                    expect(updateSpy).toHaveBeenCalledOnceWith(user);
                });
    
                it("should call insertManyRoleUsers", () => {
                    expect(insertManyRoleUsersSpy).toHaveBeenCalledTimes(1);
                    expect(actualAddedRoleUsers.length).toEqual(1);
                    expect(actualAddedRoleUsers[0].roleid).toEqual('role 1');
                    expect(actualAddedRoleUsers[0].userid).toEqual('1');
                    expect(actualAddedRoleUsers[0].created).toBeTruthy();
                });
    
                it("should call deleteManyRoleUsers", () => {
                    expect(deleteManyRoleUsersSpy).toHaveBeenCalledTimes(1);
                    expect(actualRemovedRoleUsers.length).toEqual(rolesRemoved.length);
                    expect(actualRemovedRoleUsers[0].roleid).toEqual('role 2');
                    expect(actualRemovedRoleUsers[0].userid).toEqual('1');
                    if (t.isRemoved) {
                        expect(actualRemovedRoleUsers[1].roleid).toEqual('zoo');
                        expect(actualRemovedRoleUsers[1].userid).toEqual('1');
                    }
                });

                it("should call getUserPersonalRole", () => {
                    expect(getUserPersonalRoleSpy).toHaveBeenCalledOnceWith('1');
                });
    
                if (t.isRemoved) {
                    it("should call addTask", () => {
                        expect(addTaskSpy).toHaveBeenCalledOnceWith({
                            name: TaskNames.updateUserPresence,
                            data: {
                                userid: '1',
                                usernametag: 'user name',
                            },
                        });
                    });
                } else {
                    it("should not call addTask", () => {
                        expect(addTaskSpy).not.toHaveBeenCalled()
                    });
                }
            });
        });

        describe("when zoo is added but the user is a leader", () => {

            const server = getServerFake();
            const dbContextProvider = getDbContextProviderFake();
            const dbContext = getDbContextFake();
            const settingsHelper = getSettingsHelperFake();
            const taskProcessor = getTaskProcessorFake();
            settingsHelper.settings.zooRoleId = 'zoo';

            const bot = getBot(server, getCommandProcFake(), taskProcessor, getBgsClientFake(), getCooldownManagerFake(),
                dbContextProvider, getCooldownManagerFake(), getArksFake(), getAwardsFake(), settingsHelper);

            let isBotSpy: jasmine.Spy;
            let insertSpy: jasmine.Spy;
            let updateSpy: jasmine.Spy;
            let insertManyRoleUsersSpy: jasmine.Spy;
            let deleteManyRoleUsersSpy: jasmine.Spy;
            let addTaskSpy: jasmine.Spy;
            let userHasAnyRoleOfSpy: jasmine.Spy;
            let removeRolesFromMemberSpy: jasmine.Spy;

            let actualAddedRoleUsers: IRoleUser[];
            let actualRemovedRoleUsers: IRoleUser[];

            const user: IUser = {
                userid: '1',
                usernametag: 'name 1',
            };

            const rolesAdded: IRole[] = [
                {
                    roleid: 'zoo',
                    name: 'role 1',
                    color: '',
                },
            ];

            beforeAll(async done => {
                isBotSpy = spyOn(server, 'isBot').and.callThrough().and.resolveTo(false);
                insertSpy = spyOn(dbContext.userRepo, 'insert').and.callThrough().and.resolveTo();
                updateSpy = spyOn(dbContext.userRepo, 'update').and.callThrough().and.resolveTo();
                insertManyRoleUsersSpy = spyOn(dbContext.roleRepo, 'insertManyRoleUsers').and.callThrough()
                    .and.callFake(r => {
                        actualAddedRoleUsers = r;
                        return Promise.resolve();
                    });
                deleteManyRoleUsersSpy = spyOn(dbContext.roleRepo, 'deleteManyRoleUsers').and.callThrough()
                    .and.callFake(r => {
                        actualRemovedRoleUsers = r;
                        return Promise.resolve();
                    })
                addTaskSpy = spyOn(taskProcessor, 'addTask').and.callThrough();
                userHasAnyRoleOfSpy = spyOn(server, 'userHasAnyRoleOf').and.callThrough()
                    .and.resolveTo(true);
                removeRolesFromMemberSpy = spyOn(server, 'removeRolesFromMember').and.callThrough().and.resolveTo();

                // Act
                await bot.userUpdateTask(user, rolesAdded, [], dbContext);

                done();
            });

            it("should check if member is bot", () => {
                expect(isBotSpy).toHaveBeenCalledOnceWith(user.userid);
            });

            it("should not add role to DB", () => {
                expect(actualAddedRoleUsers.length).toEqual(0);
            });

            it("should remove role from member", () => {
                expect(removeRolesFromMemberSpy).toHaveBeenCalledOnceWith("1", ['zoo']);
            });

            it("should not remove role from DB", () => {
                expect(actualRemovedRoleUsers).toBeFalsy();
            });

            it("should call insert", () => {
                expect(insertSpy).toHaveBeenCalledOnceWith(user);
            });

            it("should call update", () => {
                expect(updateSpy).toHaveBeenCalledOnceWith(user);
            });
        });

        describe("when a leader role is added", () => {

            const server = getServerFake();
            const dbContextProvider = getDbContextProviderFake();
            const dbContext = getDbContextFake();
            const settingsHelper = getSettingsHelperFake();
            const taskProcessor = getTaskProcessorFake();
            settingsHelper.settings.zooRoleId = 'zoo';

            const bot = getBot(server, getCommandProcFake(), taskProcessor, getBgsClientFake(), getCooldownManagerFake(),
                dbContextProvider, getCooldownManagerFake(), getArksFake(), getAwardsFake(), settingsHelper);

            // TODO
        });
    });

    describe("removeZooMembersFromServerTask", () => {

        describe("when no users is provided", () => {
            
            const server = getServerFake();
            const dbContextProvider = getDbContextProviderFake();
            const dbContext = getDbContextFake();
            const settingsHelper = getSettingsHelperFake();
            const taskProcessor = getTaskProcessorFake();
            settingsHelper.settings.zooRoleId = 'zoo';
            settingsHelper.settings.allowedZooInactivityDays = 15;

            const bot = getBot(server, getCommandProcFake(), taskProcessor, getBgsClientFake(), getCooldownManagerFake(),
                dbContextProvider, getCooldownManagerFake(), getArksFake(), getAwardsFake(), settingsHelper);

            let executeSpy: jasmine.Spy;
            let getUsersWithRoleAfterDaysSpy: jasmine.Spy;
            let addTaskSpy: jasmine.Spy;

            const expectedUsers: IUser[] = [
                {
                    userid: '1',
                    usernametag: 'user 1',
                },
                {
                    userid: '2',
                    usernametag: 'user 2',
                },
            ];

            beforeAll(async done => {
                executeSpy = spyOn(dbContextProvider, 'execute').and.callThrough()
                    .and.callFake(callback => {
                        callback(dbContext);
                        return Promise.resolve();
                    });
                getUsersWithRoleAfterDaysSpy = spyOn(dbContext.userRepo, 'getUsersWithRoleAfterDays')
                    .and.callThrough().and.resolveTo(expectedUsers);
                addTaskSpy = spyOn(taskProcessor, 'addTask').and.callThrough();

                await bot.removeZooMembersFromServerTask();
                done();
            });

            it("should call addTask", () => {
                expect(addTaskSpy).toHaveBeenCalledOnceWith({
                    name: TaskNames.removeZooUsers,
                    data: {
                        users: [
                            {
                                user: {
                                    userid: '1',
                                    usernametag: 'user 1',
                                },
                                isRemoved: false,
                            },
                            {
                                user: {
                                    userid: '2',
                                    usernametag: 'user 2',
                                },
                                isRemoved: false,
                            },
                        ],
                    }
                });
            });

            it("should call getUsersWithRoleAfterDays with correct arguments", () => {
                expect(getUsersWithRoleAfterDaysSpy).toHaveBeenCalledOnceWith('zoo', 15);
            });
        });

        describe("when users are provided", () => {
            const server = getServerFake();
            const dbContextProvider = getDbContextProviderFake();
            const dbContext = getDbContextFake();
            const taskProcessor = getTaskProcessorFake();
            const settingsHelper = getSettingsHelperFake();
            settingsHelper.settings = {
                zooRoleId: 'zoo',
            };

            const bot = getBot(server, getCommandProcFake(), taskProcessor, getBgsClientFake(), getCooldownManagerFake(),
                dbContextProvider, getCooldownManagerFake(), getArksFake(), getAwardsFake(), settingsHelper);

            let executeSpy: jasmine.Spy;
            let getUsersWithRoleAfterDaysSpy: jasmine.Spy;
            let addTaskSpy: jasmine.Spy;
            let kickUserSpy: jasmine.Spy;
            let userHasRoleSpy: jasmine.Spy;

            const expectedUsers: {user: IUser, isRemoved: boolean}[] = [
                {
                    user: {
                        userid: '1',
                        usernametag: 'user 1',
                    },
                    isRemoved: false,
                },
                {
                    user: {
                        userid: '2',
                        usernametag: 'user 2',
                    },
                    isRemoved: true,
                },
                {
                    user: {
                        userid: '3',
                        usernametag: 'user 3',
                    },
                    isRemoved: false,
                },
            ];

            beforeAll(async done => {
                executeSpy = spyOn(dbContextProvider, 'execute').and.callThrough();
                getUsersWithRoleAfterDaysSpy = spyOn(dbContext.userRepo, 'getUsersWithRoleAfterDays').and.callThrough();
                addTaskSpy = spyOn(taskProcessor, 'addTask').and.callThrough();
                kickUserSpy = spyOn(server, 'kickUser').and.callThrough()
                    .and.resolveTo({exists: true});
                userHasRoleSpy = spyOn(server, 'userHasRole').and.callThrough()
                    .and.resolveTo(true);

                await bot.removeZooMembersFromServerTask(expectedUsers);
                done();
            });

            it("should call userHasRole", () => {
                expect(userHasRoleSpy).toHaveBeenCalledOnceWith("1", "zoo");
            });

            it("should call kickUser once", () => {
                expect(kickUserSpy).toHaveBeenCalledOnceWith("1", `Был удален за долгое присутсвие в зоопарке.`);
            });

            it("should call addTask removeZooUsers", () => {
                expect(addTaskSpy).toHaveBeenCalledTimes(2);
                expect(addTaskSpy).toHaveBeenCalledWith(
                    {
                        name: TaskNames.removeZooUsers,
                        data: {
                            users: [
                                {
                                    user: {
                                        userid: '1',
                                        usernametag: 'user 1',
                                    },
                                    isRemoved: true,
                                },
                                {
                                    user: {
                                        userid: '2',
                                        usernametag: 'user 2',
                                    },
                                    isRemoved: true,
                                },
                                {
                                    user: {
                                        userid: '3',
                                        usernametag: 'user 3',
                                    },
                                    isRemoved: false,
                                },
                            ],
                        }
                    }
                );
                expect(addTaskSpy).toHaveBeenCalledWith(
                    {
                        name: TaskNames.notifyAboutKick,
                        data: {
                            userid: '1',
                            usernametag: 'user 1',
                        },
                    }
                );
            });

            it("should not call getUsersWithRoleAfterDays", () => {
                expect(getUsersWithRoleAfterDaysSpy).not.toHaveBeenCalled();
            });

            it("should not call execute", () => {
                expect(executeSpy).not.toHaveBeenCalled();
            });
        });

        describe("when users are removed", () => {
            const server = getServerFake();
            const taskProcessor = getTaskProcessorFake();

            let getMembersByRoleSpy: jasmine.Spy;
            let addTaskSpy: jasmine.Spy;

            const expectedUsers: {user: IUser, isRemoved: boolean}[] = [
                {
                    user: {
                        userid: '1',
                        usernametag: 'user 1',
                    },
                    isRemoved: true,
                },
                {
                    user: {
                        userid: '2',
                        usernametag: 'user 2',
                    },
                    isRemoved: true,
                },
                {
                    user: {
                        userid: '3',
                        usernametag: 'user 3',
                    },
                    isRemoved: true,
                },
            ];

            const expectedLeaders = [
                { id: '1', name: 'leader 1' },
                { id: '3', name: 'admin 1' },
            ];

            const settingsHelper = getSettingsHelperFake();
            settingsHelper.settings = {
                zooRoleId: 'zoo',
                zooKeeperRoleId: 'zoo keeper',
            };

            const bot = getBot(
                server,
                getCommandProcFake(),
                taskProcessor,
                getBgsClientFake(),
                getCooldownManagerFake(),
                getDbContextProviderFake(),
                null,
                null,
                null,
                settingsHelper);

            beforeAll(async done => {
                getMembersByRoleSpy = spyOn(server, 'getMembersByRole')
                    .and.callThrough().and.resolveTo(expectedLeaders);
                addTaskSpy = spyOn(taskProcessor, 'addTask').and.callThrough();

                // Act
                await bot.removeZooMembersFromServerTask(expectedUsers);

                done();
            });

            it("should call method getMembersByRoleSpy", () => {
                expect(getMembersByRoleSpy).toHaveBeenCalledOnceWith('zoo keeper');
            });

            it("should call addTask", () => {
                expect(addTaskSpy).toHaveBeenCalledTimes(2);
                expect(addTaskSpy).toHaveBeenCalledWith({
                    name: TaskNames.sendRemovedUsersReport,
                    data: {
                        users: [
                            {
                                userid: '1',
                                usernametag: 'user 1',
                            },
                            {
                                userid: '2',
                                usernametag: 'user 2',
                            },
                            {
                                userid: '3',
                                usernametag: 'user 3',
                            },
                        ],
                        recipientid: '1',
                    },
                });
                expect(addTaskSpy).toHaveBeenCalledWith({
                    name: TaskNames.sendRemovedUsersReport,
                    data: {
                        users: [
                            {
                                userid: '1',
                                usernametag: 'user 1',
                            },
                            {
                                userid: '2',
                                usernametag: 'user 2',
                            },
                            {
                                userid: '3',
                                usernametag: 'user 3',
                            },
                        ],
                        recipientid: '3',
                    },
                });
            });
        });
    });

    describe("restartProcesses", () => {

        const taskProcessor = getTaskProcessorFake();
        const logger = getLoggerFake();
        const taskScheduler = getTaskSchedulerFake();

        const bot = getBot(getServerFake(), getCommandProcFake(), taskProcessor, getBgsClientFake(),
            getCooldownManagerFake(), getDbContextProviderFake(), getCooldownManagerFake(),
            getArksFake(), getAwardsFake(), getSettingsHelperFake(), taskScheduler, logger);

        let stopSpy: jasmine.Spy;
        let startSpy: jasmine.Spy;
        let addTaskSpy: jasmine.Spy;

        const executions: string[] = [];

        beforeAll(done => {
            stopSpy = spyOn(taskScheduler, 'stop').and.callThrough();
            startSpy = spyOn(taskScheduler, 'start').and.callThrough();
            addTaskSpy = spyOn(taskProcessor, 'addTask').and.callThrough();
            bot.restartProcesses().then(() => done());
        });

        it(`should add task ${TaskNames[TaskNames.loadCooldowns]}`, () => {
            expect(addTaskSpy).toHaveBeenCalledOnceWith({
                name: TaskNames.loadCooldowns,
            });
        });

        it('should call stop', () => {
            expect(stopSpy).toHaveBeenCalledTimes(1);
        });

        it('should call start', () => {
            expect(startSpy).toHaveBeenCalledTimes(1);
        })
    });

    describe("zoo", () => {

        [
            { test: "when user is an Officer", isAdmin: false, roles: [{ roleid: "1" }] },
            { test: "when user is Admin", isAdmin: true, roles: [{ roleid: "3" }] },
        ].forEach(x => {
            
            describe(x.test, () => {
                
                describe("when adding zoo role to user", () => {
        
                    const settingsHelper = getSettingsHelperFake();
                    settingsHelper.settings.officerRoleId = "1";
                    settingsHelper.settings.zooRoleId = "2";
    
                    const serverFake = getServerFake();
            
                    let mentionsSpy: jasmine.Spy;
                    let getRolesSpy: jasmine.Spy;
                    let isAdminSpy: jasmine.Spy;
                    let userHasAnyRoleOfSpy: jasmine.Spy;
                    let setRolesToMemberSpy: jasmine.Spy;
                    let removeRolesFromMemberSpy: jasmine.Spy;
            
                    const options: any = {
                        mentions: () => [],
                        deleteMessage: () => null,
                        send: () => null,
                        getRoles: () => null,
                        isAdmin: () => null,
                        like: () => Promise.resolve(null),
                    };
            
                    const mentions = [{ userid: "user 1" }];
            
                    const bot = getBot(serverFake, getCommandProcFake(), getTaskProcessorFake(), getBgsClientFake(),
                        getCooldownManagerFake(), getDbContextProviderFake(), getCooldownManagerFake(),
                        getArksFake(), getAwardsFake(), settingsHelper, getTaskSchedulerFake(), getLoggerFake());
            
                    beforeAll(async done => {
                        mentionsSpy = spyOn(options, "mentions").and.callThrough().and.returnValue(mentions);
                        getRolesSpy = spyOn(options, "getRoles").and.callThrough().and.returnValue(x.roles);
                        isAdminSpy = spyOn(options, "isAdmin").and.callThrough().and.returnValue(x.isAdmin);
                        userHasAnyRoleOfSpy = spyOn(serverFake, "userHasAnyRoleOf").and.callThrough().and.resolveTo(false);
                        setRolesToMemberSpy = spyOn(serverFake, "setRolesToMember").and.callThrough();
                        removeRolesFromMemberSpy = spyOn(serverFake, "removeRolesFromMember").and.callThrough();
    
                        // Act
                        await bot.zoo(<any>{
                            args: ["<@!123>"],
                        },
                        options);
            
                        done();
                    });
    
                    it("should call setRolesToMember", () => {
                        expect(setRolesToMemberSpy).toHaveBeenCalledOnceWith(
                            "user 1",
                            ["2"] // zoo role id
                        );
                    });

                    it("should not call removeRolesFromMember", () => {
                        expect(removeRolesFromMemberSpy).not.toHaveBeenCalled();
                    });
                });

                describe("when removing zoo role from user", () => {
        
                    const settingsHelper = getSettingsHelperFake();
                    settingsHelper.settings.officerRoleId = "1";
                    settingsHelper.settings.zooRoleId = "2";
    
                    const serverFake = getServerFake();
            
                    let mentionsSpy: jasmine.Spy;
                    let getRolesSpy: jasmine.Spy;
                    let isAdminSpy: jasmine.Spy;
                    let userHasAnyRoleOfSpy: jasmine.Spy;
                    let setRolesToMemberSpy: jasmine.Spy;
                    let removeRolesFromMemberSpy: jasmine.Spy;
            
                    const options: any = {
                        mentions: () => [],
                        deleteMessage: () => null,
                        send: () => null,
                        getRoles: () => null,
                        isAdmin: () => null,
                        like: () => Promise.resolve(null),
                    };
            
                    const mentions = [{ userid: "user 1" }];
            
                    const bot = getBot(serverFake, getCommandProcFake(), getTaskProcessorFake(), getBgsClientFake(),
                        getCooldownManagerFake(), getDbContextProviderFake(), getCooldownManagerFake(),
                        getArksFake(), getAwardsFake(), settingsHelper, getTaskSchedulerFake(), getLoggerFake());
            
                    beforeAll(async done => {
                        mentionsSpy = spyOn(options, "mentions").and.callThrough().and.returnValue(mentions);
                        getRolesSpy = spyOn(options, "getRoles").and.callThrough().and.returnValue(x.roles);
                        isAdminSpy = spyOn(options, "isAdmin").and.callThrough().and.returnValue(x.isAdmin);
                        userHasAnyRoleOfSpy = spyOn(serverFake, "userHasAnyRoleOf").and.callThrough().and.resolveTo(false);
                        setRolesToMemberSpy = spyOn(serverFake, "setRolesToMember").and.callThrough();
                        removeRolesFromMemberSpy = spyOn(serverFake, "removeRolesFromMember").and.callThrough();
    
                        // Act
                        await bot.zoo(<any>{
                            args: ["<@!123>", "delete"],
                        },
                        options);
            
                        done();
                    });
    
                    it("should call removeRolesFromMember", () => {
                        expect(removeRolesFromMemberSpy).toHaveBeenCalledOnceWith(
                            "user 1",
                            ["2"] // zoo role id
                        );
                    });

                    it("should not call setRolesToMember", () => {
                        expect(setRolesToMemberSpy).not.toHaveBeenCalled();
                    });
                });
    
            });
        });

        describe("when user is not Officer and not admin", () => {
            [
                { test: "when add zoo role to user", args: ["<@!123>"] },
                { test: "when removing zoo role from user", args: ["<@!123>", "delete"] },
            ].forEach(x => {
                describe(x.test, () => {
            
                    const settingsHelper = getSettingsHelperFake();
                    settingsHelper.settings.officerRoleId = "1";
                    settingsHelper.settings.zooRoleId = "2";
    
                    const serverFake = getServerFake();
            
                    let mentionsSpy: jasmine.Spy;
                    let getRolesSpy: jasmine.Spy;
                    let isAdminSpy: jasmine.Spy;
                    let sendSpy: jasmine.Spy;
                    let setRolesToMemberSpy: jasmine.Spy;
                    let removeRolesFromMemberSpy: jasmine.Spy;
            
                    const options: any = {
                        mentions: () => [],
                        deleteMessage: () => null,
                        send: () => null,
                        getRoles: () => null,
                        isAdmin: () => null,
                        like: () => Promise.resolve(null),
                    };
            
                    const mentions = [{ userid: "user 1" }];
            
                    const bot = getBot(serverFake, getCommandProcFake(), getTaskProcessorFake(), getBgsClientFake(),
                        getCooldownManagerFake(), getDbContextProviderFake(), getCooldownManagerFake(),
                        getArksFake(), getAwardsFake(), settingsHelper, getTaskSchedulerFake(), getLoggerFake());
            
                    beforeAll(async done => {
                        mentionsSpy = spyOn(options, "mentions").and.callThrough().and.returnValue(mentions);
                        getRolesSpy = spyOn(options, "getRoles").and.callThrough().and.returnValue([]);
                        isAdminSpy = spyOn(options, "isAdmin").and.callThrough().and.returnValue(false);
                        setRolesToMemberSpy = spyOn(serverFake, "setRolesToMember").and.callThrough();
                        removeRolesFromMemberSpy = spyOn(serverFake, "removeRolesFromMember").and.callThrough();
                        sendSpy = spyOn(options, "send").and.callThrough();
    
                        // Act
                        await bot.zoo(<any>{
                            args: x.args,
                        },
                        options);
            
                        done();
                    });
    
                    it("should not call removeRolesFromMember", () => {
                        expect(removeRolesFromMemberSpy).not.toHaveBeenCalled();
                    });
    
                    it("should not call setRolesToMember", () => {
                        expect(setRolesToMemberSpy).not.toHaveBeenCalled();
                    });

                    it("should call send", () => {
                        expect(sendSpy).toHaveBeenCalledTimes(1);
                    });
                });

            });
        });

        describe("when user is Officer or admin but the user is also", () => {

            const settingsHelper = getSettingsHelperFake();
            settingsHelper.settings.officerRoleId = "4";
            settingsHelper.settings.zooRoleId = "5";

            const serverFake = getServerFake();
    
            let mentionsSpy: jasmine.Spy;
            let getRolesSpy: jasmine.Spy;
            let isAdminSpy: jasmine.Spy;
            let sendSpy: jasmine.Spy;
            let userHasAnyRoleOfSpy: jasmine.Spy;
            let setRolesToMemberSpy: jasmine.Spy;
            let removeRolesFromMemberSpy: jasmine.Spy;
            let getRoleIdForAccessLevelSpy: jasmine.Spy;
    
            const options: any = {
                mentions: () => [],
                deleteMessage: () => null,
                send: () => null,
                getRoles: () => null,
                isAdmin: () => null,
                like: () => Promise.resolve(null),
            };
    
            const mentions = [{ userid: "user 1" }];
    
            const bot = getBot(serverFake, getCommandProcFake(), getTaskProcessorFake(), getBgsClientFake(),
                getCooldownManagerFake(), getDbContextProviderFake(), getCooldownManagerFake(),
                getArksFake(), getAwardsFake(), settingsHelper, getTaskSchedulerFake(), getLoggerFake());
    
            beforeAll(async done => {
                mentionsSpy = spyOn(options, "mentions").and.callThrough().and.returnValue(mentions);
                getRolesSpy = spyOn(options, "getRoles").and.callThrough().and.returnValue([{roleid: "4"}]);
                isAdminSpy = spyOn(options, "isAdmin").and.callThrough().and.returnValue(false);
                setRolesToMemberSpy = spyOn(serverFake, "setRolesToMember").and.callThrough();
                removeRolesFromMemberSpy = spyOn(serverFake, "removeRolesFromMember").and.callThrough();
                userHasAnyRoleOfSpy = spyOn(serverFake, "userHasAnyRoleOf").and.callThrough().and.resolveTo(true);
                getRoleIdForAccessLevelSpy = spyOn(serverFake, "getRoleIdForAccessLevel").and.callThrough()
                    .and.callFake((al: AccessLevels) => al.toString());
                sendSpy = spyOn(options, "send").and.callThrough();

                // Act
                await bot.zoo(<any>{
                    args: [],
                },
                options);
    
                done();
            });

            it("should not call removeRolesFromMember", () => {
                expect(removeRolesFromMemberSpy).not.toHaveBeenCalled();
            });

            it("should not call setRolesToMember", () => {
                expect(setRolesToMemberSpy).not.toHaveBeenCalled();
            });

            it("should call send", () => {
                expect(sendSpy).toHaveBeenCalledTimes(1);
            });

            it("should call userHasAnyRoleOf", () => {
                expect(userHasAnyRoleOfSpy).toHaveBeenCalledOnceWith(
                    "user 1",
                    ["1", "2", "3", "4"])
            });
        });
    });

    describe("checkHeartbeat", () => {

        describe("when no activity for longer than 10 mins", () => {
            const taskSchedulerSpy = getTaskSchedulerFake({lastActivitySecondsAgo: 601});
            const taskProcessorSpy = getTaskProcessorFake();

            let stopSpy: jasmine.Spy;
            let startSpy: jasmine.Spy;
            let addTaskSpy: jasmine.Spy;

            const bot = getBot(getServerFake(), getCommandProcFake(), taskProcessorSpy, getBgsClientFake(),
                getCooldownManagerFake(), getDbContextProviderFake(), undefined, undefined, undefined, undefined, taskSchedulerSpy);
    
            beforeEach((done: DoneFn) => {
                stopSpy = spyOn(taskSchedulerSpy, "stop").and.callThrough().and.resolveTo();
                startSpy = spyOn(taskSchedulerSpy, "start").and.callThrough().and.resolveTo();
                addTaskSpy = spyOn(taskProcessorSpy, "addTask").and.callThrough();

                bot.checkHeartbeat().then(() => done());
                bot.checkHeartbeat();
            });
            
            it("should restart once", () => {
                expect(stopSpy).toHaveBeenCalledTimes(1);
                expect(startSpy).toHaveBeenCalledTimes(1);
                expect(addTaskSpy).toHaveBeenCalledTimes(1);
            });
        });

        describe("when there was activity", () => {
            const taskSchedulerSpy = getTaskSchedulerFake({lastActivitySecondsAgo: 599});
            const taskProcessorSpy = getTaskProcessorFake();

            let stopSpy: jasmine.Spy;
            let startSpy: jasmine.Spy;
            let addTaskSpy: jasmine.Spy;

            const bot = getBot(getServerFake(), getCommandProcFake(), taskProcessorSpy, getBgsClientFake(),
                getCooldownManagerFake(), getDbContextProviderFake(), undefined, undefined, undefined, undefined, taskSchedulerSpy);
    
            beforeEach((done: DoneFn) => {
                stopSpy = spyOn(taskSchedulerSpy, "stop").and.callThrough().and.resolveTo();
                startSpy = spyOn(taskSchedulerSpy, "start").and.callThrough().and.resolveTo();
                addTaskSpy = spyOn(taskProcessorSpy, "addTask").and.callThrough();

                bot.checkHeartbeat().then(() => done());
                bot.checkHeartbeat();
            });
            
            it("should not restart", () => {
                expect(stopSpy).not.toHaveBeenCalled();
                expect(startSpy).not.toHaveBeenCalled();
                expect(addTaskSpy).not.toHaveBeenCalled();
            });
        });

    });
});