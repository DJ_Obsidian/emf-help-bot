import { AccessLevels } from "../../access-levels";
import EmfBot from "../../bot";
import { ICommand } from "../../command";
import { CommandValues } from "../../command-values";
import { IDiscordChannelMessageEventOptions } from "../../discord-event";


export class HelpCommand implements ICommand {

    id = CommandValues.help;
    cmds = ['!help', '!помощь'];
    description = 'Вывести это сообщение, конечно же, или музыки: !помощь music.';

    args: string[];

    userLevel: AccessLevels;

    async execute(bot: EmfBot, options: IDiscordChannelMessageEventOptions): Promise<void> {
        const tag = (this.args && this.args.length > 0) ? this.args[0].toLowerCase() : undefined;

        await options.deleteMessage(bot.shortMsgDeleteTime);
        await options.dm(bot.commandProc.helpResponse(this.userLevel, tag).message);
    }
}
