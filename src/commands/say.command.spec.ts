import { SayCommand } from "./say.command";

describe("SayCommand", () => {
    describe("parse", () => {
        [
            { text: '!say <#636297013864169500> я бот', expected: { channelid: '636297013864169500', message: 'я бот' } },
            { text: '!say <#636297013864169500> я бот в <#636297013864169500>', expected: { channelid: '636297013864169500', message: 'я бот в <#636297013864169500>' } },
            { text: '!скажи    <#636297013864169500>     я бот', expected: { channelid: '636297013864169500', message: 'я бот' } },
            { text: '!скажи я бот', expected: null },
            { text: '!say <#636297013864169500>', expected: null },
            { text: '!say <#636297013864169500>     ', expected: null },
        ].forEach(x => {
            it(`should parse ${x.text} into ${x.expected ? JSON.stringify(x.expected) : 'null'}`, () => {
                expect(new SayCommand().parse(x.text)).toEqual(x.expected);
            });
        });
    });
});