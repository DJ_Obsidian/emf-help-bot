import { AccessLevels } from "../access-levels";
import EmfBot from "../bot";
import { ChannelNames } from "../channel-names";
import { ICommand } from "../command";
import { CommandValues } from "../command-values";
import { IDiscordChannelMessageEventOptions } from "../discord-event";
import Helpers from "../helpers";

interface IPMCommandArgs {
    userid: string;
    message: string;
}

export class PMCommand implements ICommand {

    id = CommandValues.pm;

    cmds = ["!pm"];

    description = 'Послать личное сообщение от лица Мику любому пользователю Дискорда. Использовать !pm userid текст.'

    level = AccessLevels.Admin;

    osOnly = false;

    text?: string;

    args?: IPMCommandArgs;

    channel = ChannelNames.admin;

    hideHelp = false;

    tag = "ADMIN";

    parse(text: string): IPMCommandArgs {
        text = Helpers.removeDuplicateSpaces(text);
        const rgx = /^!\S+[ ]+([0-9]{18})[ ]+(.+)$/.exec(text);
        if (!rgx || rgx.length === 0) {
            return null;
        }

        const userid = rgx[1];
        const message = rgx[2];

        if (userid && message && userid.trim() !== '' && message.trim() !== '') {
            return {
                userid: rgx[1],
                message: rgx[2],
            };
        } else {
            return null;
        }
    }
    
    async execute(bot: EmfBot, options: IDiscordChannelMessageEventOptions): Promise<void> {
        await options.deleteMessage();
        await bot.server.sendPMToNonMember(this.args.message, this.args.userid);
    }
}
