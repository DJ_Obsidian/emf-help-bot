import { AwardCommand } from "./award.command";

describe("AwardCommand", () => {
    describe("parse", () => {
        [
            { text: '!award <@!319251213679476737> medal description', expectedArgs: {medal: 'medal', description: 'description'} },
            { text: '!award <@!319251213679476737> защита <<описание>>', expectedArgs: {medal: 'защита', description: '<<описание>>'} },
            { text: '!award <@!319251213679476737> медаль1 A fully fledged sentence. Followed by another.', expectedArgs: {medal: 'медаль1', description: 'A fully fledged sentence. Followed by another.'} },
            { text: '!award   <@!319251213679476737>   защита    A fully fledged sentence. Followed by another.', expectedArgs: {medal: 'защита', description: 'A fully fledged sentence. Followed by another.'} },
            { text: '!award <@!319251213679476737> medal', expectedArgs: {medal: 'medal', description: ''} },
            { text: '!award <@!319251213679476737>', expectedArgs: null },
            { text: '!award', expectedArgs: null },
            { text: 'sh', expectedArgs: null },
        ].forEach(x => {
            it(`should return ${x.expectedArgs ? JSON.stringify(x.expectedArgs) : 'null' } for ${x.text}`, () => {
                expect(new AwardCommand().parse(x.text)).toEqual(x.expectedArgs);
            });
        })
    });
});