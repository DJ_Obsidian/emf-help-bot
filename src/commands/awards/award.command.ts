import { AccessLevels } from "../../access-levels";
import EmfBot from "../../bot";
import { ChannelNames } from "../../channel-names";
import { ICommand } from "../../command";
import { IAwardArgs } from "../../command-args";
import { CommandValues } from "../../command-values";
import { IDiscordChannelMessageEventOptions } from "../../discord-event";
import Helpers from "../../helpers";
import { TaskNames } from "../../task-names";


export class AwardCommand implements ICommand {

    id = CommandValues.award;
    cmds = ['!награди', '!award'];
    description = 'Выдает награду участнику. Использовать: !награди <Кого> <Тип_Награды> <Описание>';
    level = AccessLevels.Leader;

    instructions = 'Чекни !помощь, как пользоваться. Не забывай ставить пробелы.';

    args: IAwardArgs;

    parse(text: string): IAwardArgs {
        text = Helpers.removeDuplicateSpaces(text);
        const rgx = /^!\S+[ ]+<@![0-9]{18}>[ ]+(\S+)[ ]*(.*)$/.exec(text);
        if (!rgx || rgx.length === 0) {
            return null;
        }

        return {
            medal: rgx[1],
            description: rgx[2],
        };
    }

    async execute(bot: EmfBot, options: IDiscordChannelMessageEventOptions): Promise<void> {
        const mentions = options.mentions();

        if (mentions.length !== 1) {
            options.send('Чего хочешь? Mention того кого наградить.', bot.shortMsgDeleteTime);
            options.deleteMessage(bot.shortMsgDeleteTime);
            return;
        }

        if (!this.args.description) {
            options.send('Забыл описание.', bot.shortMsgDeleteTime);
            options.deleteMessage(bot.shortMsgDeleteTime);
            return;
        }

        if (this.args.description.length > 500) {
            options.send('Давай описание покороче.', bot.shortMsgDeleteTime);
            options.deleteMessage(bot.shortMsgDeleteTime);
            return;
        }

        const response = await bot.awards.awardMedalResponse(mentions[0].userid, this.args.medal, this.args.description);
        if (response) {
            options.deleteMessage();
            await bot.server.sendMessage(response, ChannelNames.awards);
            bot.taskProcessor.addTask({
                name: TaskNames.addUserAward,
                data: {
                    medal: this.args.medal,
                    userid: mentions[0].userid,
                    description: this.args.description,
                },
            });
        } else {
            options.deleteMessage(bot.shortMsgDeleteTime);
            await options.send('Такой награды нет. Вот тебе список:\n' + bot.awards.medals.map(m => m.name).join('\n'), bot.shortMsgDeleteTime);
        }
    }
}