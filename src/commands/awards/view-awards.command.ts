import EmfBot from "../../bot";
import { ICommand } from "../../command";
import { CommandValues } from "../../command-values";
import { DbContext } from "../../db/context";
import { IDiscordChannelMessageEventOptions } from "../../discord-event";


export class ViewAwardCommand implements ICommand {

    id = CommandValues.awards;
    cmds = ['!награды', '!awards'];
    description = 'Показывает количество твоих наград. Можно упомянуть человека и посмотреть количество у него.';

    async execute(bot: EmfBot, options: IDiscordChannelMessageEventOptions, db: DbContext): Promise<void> {
        options.deleteMessage(bot.shortMsgDeleteTime);

        let userid = options.authorId;
        let userDisplayName = options.authorDisplayName;
        const mention = options.mentions().length > 0 ? options.mentions()[0] : null;
        if (mention) {
            userid = mention.userid;
            userDisplayName = mention.usernametag;
        }

        const message = await bot.awards.getAwardsMessage(userid, userDisplayName, db);
        options.send(message, 120);
    }
}