import EmfBot from "../../bot";
import { ICommand } from "../../command";
import { CommandValues } from "../../command-values";
import { DbContext } from "../../db/context";
import { IDiscordChannelMessageEventOptions } from "../../discord-event";


export class PersonalRoleNameCommand implements ICommand {

    id = CommandValues.personalRoleName;
    cmds = ['!название','!name'];
    description = 'Делает твою персональную роль еще более персональной, меняет её название.';

    text?: string;

    private readonly _textLength = 35;

    get instructions(): string {
        return `Посмотри в помощи, как использовать команду. Имя должно не превышать длину из ${this._textLength} букв.`
    }

    async execute(bot: EmfBot, options: IDiscordChannelMessageEventOptions, db: DbContext): Promise<void> {
        if (this.text) {
            const name = this.text.trim();
            if (name.length > 0 && name.length <= this._textLength) {
                const roleUser = await db.roleRepo.getUserPersonalRole(options.authorId);
                if (roleUser) {
                    await options.changeRoleName(roleUser.roleid, name);
                    await options.deleteMessage();
                } else {
                    options.send(`Не могу найти твою персональную роль.`, bot.shortMsgDeleteTime);
                    
                }

                return;
            }
        }
    }
}
