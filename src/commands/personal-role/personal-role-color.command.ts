import EmfBot from "../../bot";
import { ICommand } from "../../command";
import { IPersonalRoleArgs } from "../../command-args";
import { CommandValues } from "../../command-values";
import { DbContext } from "../../db/context";
import { IDiscordChannelMessageEventOptions } from "../../discord-event";


export class PersonalRoleColorCommand implements ICommand {

    id = CommandValues.personalRoleColor;
    cmds = ['!цвет','!color'];
    description = 'Задает цвет для твоей роли! Цвет пиши в HEX.';

    instructions = `Посмотри в помощи, как использовать команду. Цвет должен начинаться с симболом #, например #FFFFFF.`;

    args: IPersonalRoleArgs;

    async execute(bot: EmfBot, options: IDiscordChannelMessageEventOptions, db: DbContext): Promise<void> {
        const roleUser = await db.roleRepo.getUserPersonalRole(options.authorId);
        if (roleUser) {
            await options.changeRoleColor(roleUser.roleid, this.args.color);
            await options.like();
            await options.deleteMessage(bot.shortMsgDeleteTime);
        } else {
            options.send(`Не могу найти твою персональную роль.`, bot.shortMsgDeleteTime);
        }
    }

    parse(text: string): IPersonalRoleArgs {
        const rgx = /^!\S+[ ]+(#[0-9a-fA-f]{3,6})$/.exec(text);
        if (rgx && rgx.length === 2) {
            return {
                color: rgx[1],
                delete: false,
                name: '',
            };
        }

        return null;
    }
}