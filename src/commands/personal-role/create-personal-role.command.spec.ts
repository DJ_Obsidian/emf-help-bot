import { getSettingsHelperFake, getServerFake, getCommandProcFake, getTaskProcessorFake, getBgsClientFake, getCooldownManagerFake, getDbContextProviderFake, getArksFake, getAwardsFake, getTaskSchedulerFake, getLoggerFake } from "../../common-spec/common.spec";
import { CreatePersonalRoleCommand } from "./create-personal-role.command";


describe("CreatePersonalRoleCommand", () => {
    describe("isRolePersonal", () => {

        const command = new CreatePersonalRoleCommand();
        const settingsHelper = getSettingsHelperFake();
        settingsHelper.settings.botReservedRoleNames = [
            "role 1",
        ];

        const bot = {
            settingsHelper: settingsHelper,
        };

        [
            { rolename: 'maxsaz888', usernametag: 'maxsaz888', expected: true },
            { rolename: 'maxsaz888', usernametag: '[OAS] maxsaz888', expected: true },
            { rolename: 'max_saz', usernametag: '[OAS] maxsaz888', expected: true },
            { rolename: 'max', usernametag: '[OAS] maxsaz888', expected: true },
            { rolename: 'ma', usernametag: '[OAS] maxsaz888', expected: false },
            { rolename: 'role 1', usernametag: '[OAS] maxsaz888', expected: false },
        ].forEach(x => {
            it(`${x.rolename} should ${x.expected ? '' : 'NOT '}be deemed as personal role for user ${x.usernametag}`, () => {
                expect(command.isRolePersonal(<any>bot, x.rolename, x.usernametag)).toEqual(x.expected);
            });
        });
    });
});