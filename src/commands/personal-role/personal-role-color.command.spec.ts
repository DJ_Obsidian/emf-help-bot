import { PersonalRoleColorCommand } from "./personal-role-color.command";


describe("PersonalRoleCommand", () => {


    describe("parse", () => {
        [
            { text: "!цвет #123fff", expected: { color: '#123fff', delete: false, name: '' } },
            { text: "!цвет #fff", expected: { color: '#fff', delete: false, name: '' } },
            { text: "!цвет #000000", expected: { color: '#000000', delete: false, name: '' } },
            { text: "#000000", expected: null },
            { text: "!цвет", expected: null },
            { text: "", expected: null },
        ].forEach(x => {
            it(`should parse ${x.text} into ${x.expected ? JSON.stringify(x.expected) : 'null'}`, () => {
                expect(new PersonalRoleColorCommand().parse(x.text)).toEqual(x.expected);
            });
        });
    });
});
