import EmfBot from "../../bot";
import { ICommand } from "../../command";
import { IPersonalRoleArgs } from "../../command-args";
import { CommandValues } from "../../command-values";
import { DbContext } from "../../db/context";
import { IDiscordChannelMessageEventOptions } from "../../discord-event";
import Helpers from "../../helpers";


export class CreatePersonalRoleCommand implements ICommand {
    id = CommandValues.personalRole;
    cmds = ['!роль','!role'];
    description = 'Создать себе персональную роль. Используй с аргументом *delete* чтобы удалить свою роль.';
    
    args: IPersonalRoleArgs;

    /**
     * Adds a personal role to the user.
     * @param command 
     * @param options 
     */
    async execute(bot: EmfBot, options: IDiscordChannelMessageEventOptions, db: DbContext): Promise<void> {
        const personalRole = await db.roleRepo.getUserPersonalRole(options.authorId);
        if (!this.args.delete) {
            // Creating role
            if (personalRole) {
                options.send("У тебя уже есть роль. Для удаления используй команду с аргументом delete", bot.shortMsgDeleteTime);
            } else {
                const userServerRoles = options.getRoles();
                let personalRoleId: string;
                for (const role of userServerRoles) {
                    if (this.isRolePersonal(bot, role.name, options.authorDisplayName)) {
                        bot.logger.log(`Existing role assigned to user ${options.authorId} (${options.authorDisplayName}) looks like it's a personal role ${role.name}`);
                        personalRoleId = role.roleid;
                        break;
                    }
                }

                if (!personalRoleId) {
                    await options.createRole(options.authorDisplayName, bot.settingsHelper.settings.personalRoleStartingPos, false);
                } else {
                    await db.roleRepo.deleteManyRoleUsers([{ roleid: personalRoleId, userid: options.authorId }]); // Must remove the role from the user first
                    await db.roleRepo.insertManyRoleUsers([
                        { roleid: personalRoleId, userid: options.authorId, isPersonalRole: true, created: new Date() },
                    ]);
                }

                await options.like();
            }
        } else {
            // Deleting role
            if (personalRole) {
                await bot.server.deleteRoleFromGuild(personalRole.roleid); // This is the member's personal role, so delete it from the server.
                await options.like();
            } else {
                // User does not have a personal role Miku is aware of.
                options.send("У тебя нет персональной роли, о которой я знаю.", bot.shortMsgDeleteTime);
            }
        }

        await options.deleteMessage(bot.shortMsgDeleteTime);
    }

    isRolePersonal(bot: EmfBot, rolename: string, usernametag: string): boolean {
        if (bot.settingsHelper.settings.botReservedRoleNames.includes(rolename)) {
            return false;
        }

        if (rolename.length < 3) {
            return false;
        }

        function compare(a: string, b: string): boolean {
            return new RegExp(`${a}`, 'i').test(b) || new RegExp(`${b}`, 'i').test(a);
        }

        let isMatch = compare(usernametag, rolename);

        if (!isMatch) {
            rolename = rolename.replace('_', '');
            rolename = rolename.replace(' ', '');
            usernametag = usernametag.replace('_', '');
            usernametag = usernametag.replace(' ', '');
            isMatch = compare(usernametag, rolename);
        }

        return isMatch;
    }
    
    parse(text: string): IPersonalRoleArgs {
        const split = Helpers.removeDuplicateSpaces(text).split(' ');
        if (split.length === 2) {
            if (split[1] === 'delete') {
                return {
                    color: '',
                    name: '',
                    delete: true,
                };
            }
        }

        return {
            color: '',
            name: '',
            delete: false,
        };
    }
}