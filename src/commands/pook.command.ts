import EmfBot from "../bot";
import { ChannelNames } from "../channel-names";
import { ICommand } from "../command";
import { CommandValues } from "../command-values";
import { DC } from "../discord-channels";
import { IDiscordChannelMessageEventOptions } from "../discord-event";
import Helpers from "../helpers";


export class PookCommand implements ICommand {

    id = CommandValues.pook;
    cmds = ['!пук', '!ger'];
    description = `Пук случайному человеку. Работает только в \`<#${DC.getId(ChannelNames.nsfw)}>\`. КД - сутки.`;
    channel = ChannelNames.nsfw;

    async execute(bot: EmfBot, options: IDiscordChannelMessageEventOptions): Promise<void> {

        if (bot.longCooldownManager.cooldowns.filter(c => c.command === CommandValues.pook).length === 500) {
            options.send(`Проветривается помещение. Пока нельзя!`, bot.shortMsgDeleteTime);
            options.deleteMessage();
            return;
        }

        const pookPhrases = ["пернул в ухо", "насрал в рот", "пернул в ротешник", "пустил шептуна за щеку","запустил струю подливы в ротешник"];
        const sheptoonPhrases = ["пустил шептуна","выдал звучную трель","громогласно перданул","жоплодирует!", "обосрался с подливой"];

        if (bot.longCooldownManager.has(options.memberId, CommandValues.pook)) {
            const secondsLeft = bot.longCooldownManager.getSecondsLeft(options.authorId, CommandValues.pook);
            bot.logger.log(`${options.authorDisplayName} has ${secondsLeft} seconds left to pook again.`);
            options.send(`Заряжаем жопу, осталось ${Helpers.formatDuration(secondsLeft * 1000)}`, bot.shortMsgDeleteTime);
            options.deleteMessage();
            return;
        }

        options.deleteMessage(bot.shortMsgDeleteTime);
        const isSheptoon = Helpers.doYouFeelLucky(10);
        if (!isSheptoon) {
            const member = options.randomMember();
            if (member) {
                await options.send(`<@${options.authorId}> ${Helpers.getRandomPhrase(pookPhrases)} ${member.userid !== options.authorId ? `<@${member.userid}>` : 'сам себе'}`);
            }
        } else {
            await options.send(`<@${options.authorId}> ${Helpers.getRandomPhrase(sheptoonPhrases)}`);
        }

        bot.longCooldownManager.add({
            userid: options.authorId,
            seconds: 24 * 60 * 60, // 24 hours
            command: CommandValues.pook,
        });
    }
}
