import EmfBot from "../bot";
import { ICommand } from "../command";
import { CommandValues } from "../command-values";
import { IDiscordChannelMessageEventOptions } from "../discord-event";


export class ViewAvatarCommand implements ICommand {

    id = CommandValues.avatar;
    cmds = ['!аватар', '!avatar'];
    description = 'Показывает твой аватар, либо аватар указанного пользователя.';

    args = [];

    async execute(bot: EmfBot, options: IDiscordChannelMessageEventOptions): Promise<void> {
        const mentions = options.mentions();
        const mention = mentions.length > 0 ? mentions[0] : undefined;
        const message = await options.getAvatar(mention ? mention.userid : undefined);
        options.deleteMessage(bot.shortMsgDeleteTime)
        if (message) {
            options.send(message, 120);
        } else {
            options.send('Ты это, скажи чей аватар показать то.', bot.shortMsgDeleteTime);
        }
    }
}