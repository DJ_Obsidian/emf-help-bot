import EmfBot from "../../bot";
import { ICommand } from "../../command";
import { CommandValues } from "../../command-values";
import { DbContext } from "../../db/context";
import { IDiscordMessageEventOptions } from "../../discord-event";


export class MyArkCommand implements ICommand {

    id = CommandValues.myark;
    cmds = ['!майарк', '!myark'];
    description = 'Показывает твою коллекцию дивочек из Arknights КД - 10 минут. Если указать имя, то покажет тебе картинку этого персонажа (пмр. !майарк Kroos).';

    text?: string;

    private async myArks(bot: EmfBot, options: IDiscordMessageEventOptions, db: DbContext): Promise<void> {
        const message = await bot.arks.myArkKnightsMessage(options.authorId, options.authorDisplayName, db);
        await options.dm(message);
        await options.deleteMessage();
    }

    async execute(bot: EmfBot, options: IDiscordMessageEventOptions, db: DbContext): Promise<void> {
        if (this.text && this.text.trim() !== '') {
            if (!bot.arks.isArkNameValid(this.text)) {
                await options.send(`${this.text}? Такого персонажа нет!`, bot.shortMsgDeleteTime);
                return;
            }

            const message = await bot.arks.myArkKnightByName(options.authorId, this.text, db);
            if (message) {
                await options.dm(message);
                await options.deleteMessage();
            } else {
                await options.send('У тебя нет такого персонажа', bot.shortMsgDeleteTime);
            }
        } else {
            if (bot.cooldownManager.has(options.authorId, CommandValues.myark)) {
                await options.send('Идет откад. Подожди.', bot.shortMsgDeleteTime);
                await options.deleteMessage();
                return;
            }

            await this.myArks(bot, options, db);
            bot.cooldownManager.add({
                userid: options.authorId,
                command: CommandValues.myark,
                seconds: 60 * 10, // 10 minutes
            });
        }
    }
}