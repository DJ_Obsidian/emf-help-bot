import EmfBot from "../../bot";
import { ChannelNames } from "../../channel-names";
import { ICommand } from "../../command";
import { CommandValues } from "../../command-values";
import { DbContext } from "../../db/context";
import { DC } from "../../discord-channels";
import { IDiscordChannelMessageEventOptions } from "../../discord-event";
import Helpers from "../../helpers";
import { TaskNames } from "../../task-names";


export class ArkCommand implements ICommand {

    id = CommandValues.ark;
    cmds = ['!арк', '!ark'];
    description = `Крутит случайную девочку \`||и не только||\` из Arknights. Работает только в \`<#${DC.getId(ChannelNames.nsfw)}>\`. КД - 4 часов.`;
    channel = ChannelNames.nsfw;

    async execute(bot: EmfBot, options: IDiscordChannelMessageEventOptions, db: DbContext): Promise<void> {
        if (bot.longCooldownManager.has(options.authorId, CommandValues.ark)) {
            options.send(
                `Копим орундум, осталось ${Helpers.formatDuration(bot.longCooldownManager.getSecondsLeft(options.authorId, CommandValues.ark) * 1000)}`,
                bot.shortMsgDeleteTime);
            options.deleteMessage(bot.shortMsgDeleteTime);
            return;
        }

        const message = await bot.arks.rollMessage(options.authorId, options.authorDisplayName, db);
        bot.logger.log(`User ${options.authorDisplayName}-${options.authorId} got Ark ${message.arkName}.`)
        await options.send(message.message);

        bot.taskProcessor.addTask({
            name: TaskNames.updateUserArkDrop,
            data: message.drop,
        });

        bot.longCooldownManager.add({
            userid: options.authorId,
            seconds: 4 * 60 * 60, // 4 hours
            command: CommandValues.ark,
        });
    }
}
