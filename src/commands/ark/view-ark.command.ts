import { AccessLevels } from "../../access-levels";
import EmfBot from "../../bot";
import { ChannelNames } from "../../channel-names";
import { ICommand } from "../../command";
import { CommandValues } from "../../command-values";
import { DbContext } from "../../db/context";
import { IDiscordChannelMessageEventOptions } from "../../discord-event";


export class ViewArkCommand implements ICommand {

    id = CommandValues.viewark;
    cmds = ['!viewark'];
    description = 'Показывает любую дивочку по имени. Использовать: viewark <имя>';
    level = AccessLevels.Admin;
    channel = ChannelNames.admin;

    text?: string;

    /**
     * Lets you view any ark knight by name. This is an admin command.
     */
     async execute(bot: EmfBot, options: IDiscordChannelMessageEventOptions, db: DbContext): Promise<void> {
        if (!this.text) {
            return;
        }

        const message = await bot.arks.viewArkMessageName(this.text, db);
        if (message) {
            await options.send(message);
        } else {
            await options.error();
        }

        await options.deleteMessage();
    }
}