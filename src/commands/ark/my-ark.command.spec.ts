import { CommandValues } from "../../command-values";
import { getDbContextFake, getCooldownManagerFake, getArksFake } from "../../common-spec/common.spec";
import { MyArkCommand } from "./my-ark.command";


describe("MyArkCommand", () => {

    describe("if no name specified", () => {

        const dbContextFake = getDbContextFake();
        const cooldownManager = getCooldownManagerFake();
        const optionsFake: any = {
            authorId: 'user',
            authorDisplayName: 'display name',

            deleteMessage: (): Promise<any> => { return null },
            send: (msg: string): Promise<any> => { return null },
            dm: (msq: string): Promise<any> => { return null },
        };
        const arksFake = getArksFake();

        let isArkNameValidSpy: jasmine.Spy;
        let sendSpy: jasmine.Spy;
        let myArkKnightByNameSpy: jasmine.Spy;
        let myArkKnightsMessageSpy: jasmine.Spy;
        let dmSpy: jasmine.Spy;
        let deleteMessageSpy: jasmine.Spy;
        let addSpy: jasmine.Spy;

        let myArkCommand = new MyArkCommand();
        myArkCommand.text = '',

        beforeAll(async done => {
            myArkKnightsMessageSpy = spyOn(arksFake, 'myArkKnightsMessage').and.callThrough();
            isArkNameValidSpy = spyOn(arksFake, 'isArkNameValid').and.callThrough();
            sendSpy = spyOn(optionsFake, 'send').and.callThrough().and.resolveTo();
            dmSpy = spyOn(optionsFake, 'dm').and.callThrough().and.resolveTo();
            myArkKnightByNameSpy = spyOn(arksFake, 'myArkKnightByName').and.callThrough();
            deleteMessageSpy = spyOn(optionsFake, 'deleteMessage').and.callThrough();
            spyOn(cooldownManager, 'has').and.callThrough().and.returnValue(false);
            addSpy = spyOn(cooldownManager, 'add').and.callThrough();

            let bot = { arks: arksFake, shortMsDeleteTime: 10, cooldownManager: cooldownManager, };

            // Act
            await myArkCommand.execute(
                <any>bot,
                optionsFake,
                dbContextFake);
            
            done();
        });

        it("should call myArkKnightsMessage", () => {
            expect(myArkKnightsMessageSpy).toHaveBeenCalledOnceWith('user', 'display name', dbContextFake);
        });

        it("should not use 'send' to send message", () => {
            expect(sendSpy).not.toHaveBeenCalled();
        });

        it("should dm", () => {
            expect(dmSpy).toHaveBeenCalledTimes(1);
        });

        it("should not call myArkKnightByName", () => {
            expect(myArkKnightByNameSpy).not.toHaveBeenCalled();
        });

        it("should not call isArkNameValid", () => {
            expect(isArkNameValidSpy).not.toHaveBeenCalled();
        });

        it("should call deleteMessage", () => {
            expect(deleteMessageSpy).toHaveBeenCalledTimes(1);
        });

        it("should call add to add cooldown", () => {
            expect(addSpy).toHaveBeenCalledOnceWith({
                userid: 'user',
                command: CommandValues.myark,
                seconds: 60 * 10,
            });
        });
    });

    describe("if name specified and character exists", () => {

        const dbContextFake = getDbContextFake();
        const cooldownManager = getCooldownManagerFake();
        const optionsFake: any = {
            authorId: 'user',
            authorDisplayName: 'display name',

            deleteMessage: (): Promise<any> => { return null },
            send: (msg: string): Promise<any> => { return null },
            dm: (msq: string): Promise<any> => { return null },
        };
        const arksFake = getArksFake();

        let isArkNameValidSpy: jasmine.Spy;
        let sendSpy: jasmine.Spy;
        let myArkKnightByNameSpy: jasmine.Spy;
        let myArkKnightsMessageSpy: jasmine.Spy;
        let dmSpy: jasmine.Spy;
        let deleteMessageSpy: jasmine.Spy;

        let myArkCommand = new MyArkCommand();
        myArkCommand.text = "ArkName";

        beforeAll(async done => {
            myArkKnightsMessageSpy = spyOn(arksFake, 'myArkKnightsMessage').and.callThrough();
            isArkNameValidSpy = spyOn(arksFake, 'isArkNameValid').and.callThrough()
                .and.returnValue(true);
            sendSpy = spyOn(optionsFake, 'send').and.callThrough().and.resolveTo();
            dmSpy = spyOn(optionsFake, 'dm').and.callThrough().and.resolveTo();
            myArkKnightByNameSpy = spyOn(arksFake, 'myArkKnightByName').and.callThrough()
                .and.resolveTo('message');
            deleteMessageSpy = spyOn(optionsFake, 'deleteMessage').and.callThrough();

            let bot = { arks: arksFake, shortMsDeleteTime: 10, cooldownManager: cooldownManager, };

            // Act
            await myArkCommand.execute(
                <any>bot,
                optionsFake,
                dbContextFake);
            
            done();
        });

        it("should not call myArkKnightsMessage", () => {
            expect(myArkKnightsMessageSpy).not.toHaveBeenCalled();
        });

        it("should not use 'send' to send message", () => {
            expect(sendSpy).not.toHaveBeenCalled();
        });

        it("should dm", () => {
            expect(dmSpy).toHaveBeenCalledOnceWith('message');
        });

        it("should call myArkKnightByName", () => {
            expect(myArkKnightByNameSpy).toHaveBeenCalledWith('user', 'ArkName', dbContextFake);
        });

        it("should call deleteMessage", () => {
            expect(deleteMessageSpy).toHaveBeenCalledTimes(1);
        });
    });

    describe("if no such character exists", () => {

        const dbContextFake = getDbContextFake();
        const optionsFake: any = {
            deleteMessage: (): Promise<any> => { return null },
            send: (msg: string): Promise<any> => { return null },
            dm: (msq: string): Promise<any> => { return null },
        };
        const arksFake = getArksFake();

        let isArkNameValidSpy: jasmine.Spy;
        let sendSpy: jasmine.Spy;
        let myArkKnightByNameSpy: jasmine.Spy;

        let myArkCommand = new MyArkCommand();
        myArkCommand.text = "SomeName";

        beforeAll(async done => {
            isArkNameValidSpy = spyOn(arksFake, 'isArkNameValid').and.callThrough()
                .and.returnValue(false);
            sendSpy = spyOn(optionsFake, 'send').and.callThrough().and.resolveTo();
            myArkKnightByNameSpy = spyOn(arksFake, 'myArkKnightByName').and.callThrough();

            let bot = { arks: arksFake, shortMsDeleteTime: 10 };

            // Act
            await myArkCommand.execute(
                <any>bot,
                optionsFake,
                dbContextFake);
            
            done();
        });

        it("should send message", () => {
            expect(sendSpy).toHaveBeenCalledTimes(1);
        });

        it("should not call myArkKnightByName", () => {
            expect(myArkKnightByNameSpy).not.toHaveBeenCalled();
        });

    });
});