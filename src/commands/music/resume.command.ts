import EmfBot from "../../bot";
import { ChannelNames } from "../../channel-names";
import { ICommand, tags } from "../../command";
import { CommandValues } from "../../command-values";
import { IDiscordChannelMessageEventOptions } from "../../discord-event";

export class ResumeCommand implements ICommand {

    id = CommandValues.resume;
    cmds = ['!resume','!продолжить'];
    description = 'Продолжает играть текущую дорожку, раннее поставленную на паузу.';
    channel = ChannelNames.music;
    osOnly = true;
    tag = tags.MUSIC;

    async execute(bot: EmfBot, options: IDiscordChannelMessageEventOptions): Promise<void> {
        await bot.musicPlayer.resume(options.voiceChannelId);
        await options.like();
    }
}