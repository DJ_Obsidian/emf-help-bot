import EmfBot from "../../bot";
import { ChannelNames } from "../../channel-names";
import { ICommand, tags } from "../../command";
import { IPlaylistArgs } from "../../command-args";
import { CommandProcessor } from "../../command-processor";
import { CommandValues } from "../../command-values";
import { IDiscordChannelMessageEventOptions } from "../../discord-event";


export class PlaylistCommand implements ICommand {

    id = CommandValues.playlist;
    cmds = ['!playlist', '!плэйлист'];
    description = `Показывает список песен, сохраненных в плэйлисте.\n` + 
    `Добавляет Ютуб песню, напр. !плэйлист <ссылка>.\n` + 
    `Удаляет песню из списка плэйлист если использовать delete <n>, где n - ID песни в списке.\n` + 
    `Удаляет весь список если использовать параметр clear.\n`;
    channel = ChannelNames.music;
    osOnly = true;
    tag = tags.MUSIC;

    args?: IPlaylistArgs;
    
    parse(text: string): IPlaylistArgs {
        const r = CommandProcessor.cutoffCommandText(text);
        if (!r || r.trim() === '') {
            return {
                show: true,
            };
        }

        const url = CommandProcessor.parseYoutubeUrl(text);
        if (url) {
            return {
                url: url,
            };
        } else {
            const match = /^!\S+[ ]+(delete)+[ ]+([0-9]{1,2})$/.exec(text);
            if (match && match.length === 3) {
                return {
                    deleteIndex: parseInt(match[2]),
                };
            } else {
                if (text.includes('clear')) {
                    return {
                        clear: true,
                    };
                }
            }
        }

        return null;
    }

    async execute(bot: EmfBot, options: IDiscordChannelMessageEventOptions): Promise<void> {
        if (this.args.deleteIndex) {
            await bot.musicPlayer.deleteFromPlaylist(options.authorId, this.args.deleteIndex);
            await options.like();
        } else if (this.args.url) {
            const result = await bot.musicPlayer.addToPlaylist(this.args.url, options.authorId);
            if (result.successful) {
                await options.like();
                await options.send(`\`+ ${result.title}\``);
            } else {
                if (result.full) {
                    await options.send(`Плэйлист полон - только до ${bot.musicPlayer.playlistItemLimit}-и.`, bot.shortMsgDeleteTime);
                } else {
                    await options.error();
                }
            }
        } else if (this.args.clear === true) {
            await bot.musicPlayer.deletePlaylist(options.authorId);
            await options.like();
        } else if (this.args.show) {
            const message = await bot.musicPlayer.playlist(options.authorId);
            await options.dm(message);
        } else {
            await options.error();
        }

        await options.deleteMessage(bot.shortMsgDeleteTime);
    }
}