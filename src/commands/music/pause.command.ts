import EmfBot from "../../bot";
import { ChannelNames } from "../../channel-names";
import { ICommand, tags } from "../../command";
import { CommandValues } from "../../command-values";
import { IDiscordChannelMessageEventOptions } from "../../discord-event";


export class PauseCommand implements ICommand {

    id = CommandValues.pause;
    cmds = ['!pause','!пауза'];
    description: 'Ставит текущую родожку на паузу.';
    channel = ChannelNames.music;
    osOnly = true;
    tag = tags.MUSIC;

    async execute(bot: EmfBot, options: IDiscordChannelMessageEventOptions): Promise<void> {
        await bot.musicPlayer.pause(options.voiceChannelId);
        await options.like();
    }
}