import { IPlayArgs } from "../../command-args";
import { getMusicPlayerFake } from "../../common-spec/common.spec";
import { PlayCommand } from "./play.command";

describe("PlayCommand", () => {

    describe("parse", () => {
        [
            { text: "!play", expected: { playPl: true } },
            { text: "!play search query", expected: { text: 'search query' } },
        ].forEach(x => {
            it(`should parse ${x.text} into ${x.expected ? JSON.stringify(x.expected) : 'null'}`, () => {
                expect(new PlayCommand().parse(x.text)).toEqual(<IPlayArgs>x.expected);
            });
        });
    });

    describe("execute", () => {

        [
            {successful:true},
            {successful:false},
        ].forEach(x => {
            describe(`when url is specified and successful is ${x.successful}`, () => {
                const musicPlayer = getMusicPlayerFake();
    
                let command: PlayCommand;
        
                let bot: any;
        
                const options = {
                    authorId: "2",
                    voiceChannelId: "1",
                    send: (): void => {},
                    like: (): void => {},
                    error: (): void => {},
                };
        
                const commandArgs = {
                    url: "url",
                };
    
                let playSpy: jasmine.Spy;
                let likeSpy: jasmine.Spy;
                let errorSpy: jasmine.Spy;
                let sendSpy: jasmine.Spy;
        
                beforeAll(async done => {
                    command = new PlayCommand();
                    command.args = commandArgs;
                    bot = { musicPlayer: musicPlayer },
    
                    playSpy = spyOn(musicPlayer, "play").and.callThrough()
                        .and.resolveTo({successful:x.successful});
                    likeSpy = spyOn(options, "like").and.callThrough().and.resolveTo();
                    errorSpy = spyOn(options, "error").and.callThrough();
                    sendSpy = spyOn(options, "send").and.callThrough().and.resolveTo();
    
                    await command.execute(bot, <any>options);
    
                    done();
                });
    
                it("should call play", () => {
                    expect(playSpy).toHaveBeenCalledOnceWith("url", options, "2");
                });
    
                if (x.successful) {
                    it("should call like", () => {
                        expect(likeSpy).toHaveBeenCalledTimes(1);
                    });
        
                    it("should not call error", () => {
                        expect(errorSpy).not.toHaveBeenCalled();
                    });
                } else {
                    it("should call error", () => {
                        expect(errorSpy).toHaveBeenCalledTimes(1);
                    });
                    it("should not call like", () => {
                        expect(likeSpy).toHaveBeenCalledTimes(0);
                    });
                    it("should call send", () => {
                        expect(sendSpy).toHaveBeenCalledTimes(0);
                    });
                }
            });
        });
    });
});
