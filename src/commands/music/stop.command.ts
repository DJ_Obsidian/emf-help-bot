import EmfBot from "../../bot";
import { ChannelNames } from "../../channel-names";
import { ICommand, tags } from "../../command";
import { CommandValues } from "../../command-values"
import { IDiscordChannelMessageEventOptions } from "../../discord-event";

export class StopCommand implements ICommand {

    id = CommandValues.stop;
    cmds = ['!stop','!стоп'];
    description = 'Останавливает текущую сессию и удаляет все дорожки.';
    channel = ChannelNames.music;
    osOnly = true;
    tag = tags.MUSIC;

    async execute(bot: EmfBot, options: IDiscordChannelMessageEventOptions): Promise<void> {
        await bot.musicPlayer.stop(options.voiceChannelId);
        await options.like();
    }
}