import EmfBot from "../../bot";
import { ChannelNames } from "../../channel-names";
import { ICommand, tags } from "../../command";
import { CommandValues } from "../../command-values";
import { IDiscordChannelMessageEventOptions } from "../../discord-event";


export class PlayingCommand implements ICommand {

    id = CommandValues.playing;
    cmds = ['!playing'];
    description = 'Показывает, что сейчас играет.';
    channel = ChannelNames.music;
    osOnly = true;
    tag = tags.MUSIC;

    async execute(bot: EmfBot, options: IDiscordChannelMessageEventOptions): Promise<void> {
        const message = await bot.musicPlayer.playing(options.voiceChannelId);
        if (message) {
            await options.send(message);
        } else {
            await options.error();
        }
    }
}