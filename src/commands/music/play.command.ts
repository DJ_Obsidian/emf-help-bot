import EmfBot from "../../bot";
import { ChannelNames } from "../../channel-names";
import { ICommand, tags } from "../../command";
import { IPlayArgs } from "../../command-args";
import { CommandProcessor } from "../../command-processor";
import { CommandValues } from "../../command-values";
import { IDiscordChannelMessageEventOptions } from "../../discord-event";


export class PlayCommand implements ICommand {
    id = CommandValues.play;

    cmds = ['!play','!играть','!играй'];

    description = 'Играть дорожку по ссылке. Или играть свой персональный плэйлист если вызвать команду без параметров.';
    
    channel = ChannelNames.music;
    
    osOnly = true;
    
    tag = tags.MUSIC;
    
    hideHelp = false;
    
    text?: string;

    args?: IPlayArgs;
    
    async execute(bot: EmfBot, options: IDiscordChannelMessageEventOptions): Promise<void> {
        if (options.voiceChannelId) {
            if (this.args.url) {
                const result = await bot.musicPlayer.play(this.args.url, options, options.authorId);
                if (result.successful) {
                    await options.like();
                    return;
                }
            } else {
                if (this.args.playPl) {
                    const result = await bot.musicPlayer.playlistPlay(options.authorId, options);
                    if (result.successful) {
                        await options.like();
                        return;
                    }
                } else if (this.args.text && this.args.text.trim().length > 0) {
                    const result = await bot.musicPlayer.play({text: this.args.text}, options, options.authorId);
                    if (result.successful) {
                        await options.like();
                        return;
                    }
                }
            }
        }

        await options.error();
    }

    parse(text: string): IPlayArgs {
        const r = CommandProcessor.cutoffCommandText(text);
        if (!r || r.trim() === '') {
            return {
                playPl: true,
            };
        }

        const url = CommandProcessor.parseYoutubeUrl(text);
        if (url) {
            return {
                url: url,
            };
        } else {
            return {
                text: r,
            };
        }
    }
}