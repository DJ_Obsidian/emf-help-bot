import { IPlaylistArgs } from "../../command-args";
import { PlaylistCommand } from "./playlist.command";


describe("PlaylistCommand", () => {

    describe("parse", () => {
        [
            { text: "!playlist https://www.youtube.com/watch?v=rPM0XL-ccVYddawd", expected: { url: 'https://www.youtube.com/watch?v=rPM0XL-ccVYddawd' } },
            { text: "!playlist https://youtu.be/qsFgue24Vbo", expected: { url: 'https://youtu.be/qsFgue24Vbo' } },
            { text: "!playlist https://www.youtube.com/watch?v=rPM0XL-ccVYddawd&addddd=123&dawd", expected: { url: 'https://www.youtube.com/watch?v=rPM0XL-ccVYddawd' } },
            { text: "!playlist https://www.youtube.com/watch", expected: null },
            { text: "!playlist", expected: { show: true } },
            { text: "!playlist somegibberish", expected: null },
            { text: "!playlist delete 2", expected: { deleteIndex: 2 } },
            { text: "!playlist delete 23", expected: { deleteIndex: 23 } },
            { text: "!playlist delete 234", expected: null },
            { text: "!playlist delete", expected: null },
            { text: "!playlist 23", expected: null },
            { text: "!playlist clear", expected: { clear: true } },
        ].forEach(x => {
            it(`should parse ${x.text} into ${x.expected ? JSON.stringify(x.expected) : 'null'}`, () => {
                expect(new PlaylistCommand().parse(x.text)).toEqual(<IPlaylistArgs>x.expected);
            });
        });
    });
});