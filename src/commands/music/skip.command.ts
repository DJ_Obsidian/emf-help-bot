import EmfBot from "../../bot";
import { ChannelNames } from "../../channel-names";
import { ICommand, tags } from "../../command";
import { CommandValues } from "../../command-values";
import { IDiscordChannelMessageEventOptions } from "../../discord-event";


export class SkipCommand implements ICommand {
    id = CommandValues.skip;
    cmds = ['!skip'];
    description = 'Скипает текущую дорожку.';
    channel = ChannelNames.music;
    osOnly = true;
    tag = tags.MUSIC;

    async execute(bot: EmfBot, options: IDiscordChannelMessageEventOptions): Promise<void> {
        const result = await bot.musicPlayer.skip(options.voiceChannelId);
        if (result) {
            if (result.skipped) {
                await options.send(bot.musicPlayer.getTrackMessage(result.skipped, false));
            }

            if (!result.next) {
                await options.send(`\`<<end>>\``);
            }
        }

        await options.like();
    }
}