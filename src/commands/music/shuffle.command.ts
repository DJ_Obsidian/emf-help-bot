import EmfBot from "../../bot";
import { ChannelNames } from "../../channel-names";
import { ICommand, tags } from "../../command";
import { CommandValues } from "../../command-values";
import { IDiscordChannelMessageEventOptions } from "../../discord-event";


export class ShuffleCommand implements ICommand {

    id = CommandValues.shuffle;
    cmds = ['!shuffle'];
    description = `Iграет дорожки в разброс.`;
    channel = ChannelNames.music;
    osOnly = true;
    tag = tags.MUSIC;

    async execute(bot: EmfBot, options: IDiscordChannelMessageEventOptions): Promise<void> {
        const result = await bot.musicPlayer.playlistPlay(options.authorId, options, true);
        if (result.successful) {
            await options.like();
        } else {
            await options.error();
        }
    }
}