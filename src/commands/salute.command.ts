import EmfBot from "../bot";
import { ChannelNames } from "../channel-names";
import { ICommand } from "../command";
import { ISayArgs } from "../command-args";
import { CommandValues } from "../command-values";
import { IDiscordChannelMessageEventOptions } from "../discord-event";
import { getRandomSalute } from "../salute-images";


export class SaluteCommand implements ICommand {
    
    id = CommandValues.o7;
    cmds = ['!o7','!о7','!07'];
    description = 'Приветствует командиров. Можно указать, кого приветствуем. КД - 30 минут.';
    channel = [ChannelNames.reception, ChannelNames.general, ChannelNames.nsfw];

    args: ISayArgs;

    async execute(bot: EmfBot, options: IDiscordChannelMessageEventOptions): Promise<void> {
        if (!options.isAdmin() && bot.cooldownManager.has(options.authorId, this.id)) {
            options.deleteMessage();
            options.send("Подожди еще немного, нельзя отдавать честь так часто.", bot.shortMsgDeleteTime);
            return;
        }

        const mentions = options.mentions();
        const saluteMessage = getRandomSalute(options.authorDisplayName, mentions.length > 0 ? mentions[0].userid : null);
        await options.send(saluteMessage);

        bot.cooldownManager.add({
            userid: options.authorId,
            command: this.id,
            seconds: bot.saluteCoolDownTime,
        });
    }
}