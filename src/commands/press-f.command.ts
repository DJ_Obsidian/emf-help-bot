import EmfBot from "../bot";
import { ICommand } from "../command";
import { CommandValues } from "../command-values";
import { IDiscordChannelMessageEventOptions } from "../discord-event";
import { getPressFResponse } from "../press-f-response";


export class PressFCommand implements ICommand {

    id = CommandValues.rip;
    cmds = ['!ф', '!f', '!рип', '!rip'];
    description = 'Отдает честь за почивших героев. Можно указать кого чтим. КД - 30 минут.';

    async execute(bot: EmfBot, options: IDiscordChannelMessageEventOptions): Promise<void> {
        if (!options.isAdmin() && bot.cooldownManager.has(options.authorId, CommandValues.rip)) {
            options.deleteMessage(bot.shortMsgDeleteTime);
            options.send("Подожди еще немного, нельзя отдавать честь так часто.", bot.shortMsgDeleteTime);
            return;
        }

        const mentions = options.mentions();
        let userid: string;
        if (mentions.length > 0) {
            userid = mentions[0].userid;
        }

        options.deleteMessage(bot.shortMsgDeleteTime);

        const message: unknown = getPressFResponse(options.authorDisplayName, userid);
        await options.send(message);

        bot.cooldownManager.add({
            userid: options.authorId,
            command: CommandValues.rip,
            seconds: bot.pressFCoolDownTime,
        });
    }
}