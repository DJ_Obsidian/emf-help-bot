import { AccessLevels } from "../access-levels";
import EmfBot from "../bot";
import { ChannelNames } from "../channel-names";
import { ICommand } from "../command";
import { ISayArgs } from "../command-args";
import { CommandValues } from "../command-values";
import { DC } from "../discord-channels";
import { IDiscordChannelMessageEventOptions } from "../discord-event";
import Helpers from "../helpers";


export class SayCommand implements ICommand {

    id = CommandValues.say;
    cmds = ['!say', '!скажи'];
    description = `Говорит устами Мику то, что пишешь. Работает только в \`<#${DC.getId(ChannelNames.admin)}>\` Использовать !скажи #<канал> <текст>.`;
    level = AccessLevels.Leader;
    channel = ChannelNames.admin;
    
    args: ISayArgs;

    parse(text: string): ISayArgs {
        text = Helpers.removeDuplicateSpaces(text);
        const rgx = /^!\S+[ ]+<#([0-9]{18})>[ ]+(.+)$/.exec(text);
        if (!rgx || rgx.length === 0) {
            return null;
        }

        const channelid = rgx[1];
        const message = rgx[2];

        if (channelid && message && channelid.trim() !== '' && message.trim() !== '') {
            return {
                channelid: rgx[1],
                message: rgx[2],
            };
        } else {
            return null;
        }
    }

    async execute(bot: EmfBot, options: IDiscordChannelMessageEventOptions): Promise<void> {
        if (!this.args) {
            options.deleteMessage(10);
            options.send('Неправильное использование команды. Посмотри в !помощь.', bot.shortMsgDeleteTime);
            return;
        }

        if (!this.args.message) {
            options.send('Где сообщение?', bot.shortMsgDeleteTime);
            return;
        }

        await options.deleteMessage();
        await bot.server.sendMessage(this.args.message, this.args.channelid);
    }
}