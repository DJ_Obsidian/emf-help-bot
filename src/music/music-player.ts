import * as Discord from 'discord.js'
import { EventEmitter } from 'events';
import { embedColor } from '../colors';
import { DbContextProvider } from '../db/context';
import { IPlaylistItem } from '../db/entities/playlist-item';
import { IStreamable } from '../discord-event';
import Helpers from '../helpers';
import Logger from '../logger';
import { ISessionItem, MusicPlaySession } from './music-play-session';
import { IMusicInfo, IMusicStreamer } from './streamer';

export class PlayResult {
    invalid = false;
    full = false;
    failure = false;
    unavailable = false;
    title = '';
    index = 0;

    get successful(): boolean {
        return !this.invalid && !this.full && !this.failure && !this.unavailable;
    }
}

export interface ISearchQuery {
    text: string;
}

export class MusicPlayer extends EventEmitter {

    private _sessions: MusicPlaySession[] = [];

    playSessionLimit = 1;

    playlistItemLimit = 50;

    playSessionItemLimit = 200;

    constructor(private _streamer: IMusicStreamer, private dbContextProvider: DbContextProvider, private _logger: Logger) {
        super();
    }

    private onStart(s: ISessionItem): void {
        this._logger.log(`<<Started playing ${s.info.title} - ${s.info.lengthSeconds} seconds>>`);
        this.emit("start", s);
    }

    private onStop(s: ISessionItem): void {
        this._logger.log(`<<Ended stream session of ${s.info.title}>>`);
    }

    private onEnd(s: MusicPlaySession): void {
        this._logger.log(`<<Ended play session ${s.voiceChannelId}>>`);
        this.emit("end", s);
    }

    private onFinish(s: ISessionItem): void {
        this._logger.log(`<<Finished playing ${s.info.title}>>`);
    }

    private onError(s: ISessionItem, error: Error): void {
        this._logger.log(`<<Stream session of ${s.info.title} threw an error>>`);
        this._logger.error(error);
    }

    private onPause(s: ISessionItem): void {
        this._logger.log(`<<Paused ${s.info.title}>>`);
        this.emit("pause", s);
    }

    private onResume(s: ISessionItem): void {
        this._logger.log(`<<Resumed ${s.info.title}>>`);
        this.emit("resume", s);
    }

    private onQueued(s: ISessionItem): void {
        this._logger.log(`<<Queued ${s.info.title}>>`);
        this.emit("queue", s);
    }

    private getSession(streamable: IStreamable): MusicPlaySession {
        let session: MusicPlaySession;
        session = this._sessions.find(x => x.voiceChannelId == streamable.voiceChannelId);
        if (!session) {
            // Another session
            if (this._sessions.length < this.playSessionLimit) {
                session = new MusicPlaySession(streamable, this._streamer);
                session.on("start", (s: ISessionItem) => this.onStart(s));
                session.on("stop", (s: ISessionItem) => this.onStop(s));
                session.on("end", (s: MusicPlaySession) => this.onEnd(s));
                session.on("finish", (s: ISessionItem) => this.onFinish(s));
                session.on("error", (s: ISessionItem, error: Error) => this.onError(s, error));
                session.on("pause", (s: ISessionItem) => this.onPause(s));
                session.on("resume", (s: ISessionItem) => this.onResume(s));
                this._sessions.push(session);
            }
        }

        return session;
    }

    private getSessionByChannel(voiceChannelId: string): MusicPlaySession {
        return this._sessions.find(x => x.voiceChannelId === voiceChannelId);
    }

    /**
     * Plays the audio from the url or enqueues it if there is another one currently
     * playing.
     * @param urlOrText 
     * @param streamable 
     */
    async play(urlOrText: string | ISearchQuery, streamable: IStreamable, userid: string): Promise<PlayResult>{
        const result = new PlayResult();

        const session = this.getSession(streamable);
        if (!session) {
            // Another session not allowed
            result.full = true;
            return result;
        }

        // Check if under play session limit
        if (session.items.length > this.playSessionItemLimit) {
            result.full = true;
            return result;
        }

        let info: IMusicInfo;
        if (typeof(urlOrText) === 'string') {
            const url = urlOrText.toString();
            if (!await this._streamer.validate(url)) {
                result.invalid = true;
                return result;
            }
    
            info = await this._streamer.getInfo(url);
            if (!info) {
                result.unavailable = true;
                return result;
            }
        } else {
            const searchQuery = <ISearchQuery>urlOrText;
            info = await this._streamer.search(searchQuery.text);
            if (!info) {
                result.failure = true;
                return result;
            }
        }

        result.title = info.title;

        const item = session.queue({
            lengthSeconds: info.lengthSeconds,
            title: info.title,
            url: info.url,
        },
        userid);

        session.start();

        result.index = item.index;
        if (session.current && result.index != session.current.index) {
            this.onQueued(item);
        }

        return result;
    }

    async playlistPlay(userid: string, streamable: IStreamable, shuffle = false): Promise<PlayResult> {
        const result = new PlayResult();

        const session = this.getSession(streamable);
        if (!session || session.playing) {
            // Another session not allowed
            result.full = true;
            return result;
        }

        const playlist = await this.dbContextProvider.execute(db => db.playlistItemRepo.getPlaylist(userid));
        this.loadPlaylist(playlist, session, userid, shuffle);

        session.start();

        return result;
    }

    private loadPlaylist(playlist: IPlaylistItem[], session: MusicPlaySession, userid: string, shuffle = false): IPlaylistItem[] {
        if (shuffle) {
            Helpers.shuffle(playlist);
        }

        for (const item of playlist) {
            session.queue({
                lengthSeconds: item.duration,
                title: item.title,
                url: item.url,
            },
            userid);
        }

        return playlist;
    }

    private useSession<T>(channelId: string, callback: (s: MusicPlaySession) => Promise<T>): Promise<T> {
        const session = this.getSessionByChannel(channelId);
        if (session) {
            return callback(session);
        }
    }

    pause(channelId: string): Promise<void> {
        return this.useSession(channelId, session => session.current.streamSession.pause());
    }

    resume(channelId: string): Promise<void> {
        return this.useSession(channelId, session => session.current.streamSession.resume());
    }

    stop(channelId: string): Promise<ISessionItem> {
        return this.useSession(channelId, session => {
            const current = session.current;
            if (session.current) {
                session.end();
            }

            return Promise.resolve(current);
        });
    }

    skip(channelId: string): Promise<{skipped: ISessionItem; next: ISessionItem;}> {
        return this.useSession(channelId, async session => {
            const skipped = session.current;
            await session.current.streamSession.end(); // Must end the stream first in order to skip
            return {
                skipped: skipped,
                next: session.next(),
            };
        });
    }

    getCurrent(channelId: string): Promise<ISessionItem> {
        return this.useSession(channelId, session => Promise.resolve(session.current));
    }

    playing(voiceChannelId: string): Promise<unknown> {
        const session = this.getSessionByChannel(voiceChannelId);
        if (session) {
            const message = new Discord.MessageEmbed();
            message.setColor(embedColor);
            let text = '';
            let passed = false;

            const sliced = Helpers.sliceAround(session.items, x => x.index === session.current.index, 3, 5);
            for (const item of sliced) {
                let pre = '';
                if (item.index === session.current.index) {
                    passed = true;
                    pre = '+ -->';
                } else {
                    if (!passed) {
                        pre = '-    ';
                    } else {
                        pre = '     ';
                    }
                }

                text += `${pre}${item.index} ${item.info.title} -- ${item.info.lengthSeconds}\n`;
            }

            if (sliced.length > 0) {
                text = '```diff\n' + text + '```';
            } else {
                text = 'Нет текущих дорожек.'
            }

            message.setDescription(text);

            return Promise.resolve(message);
        }

        return Promise.resolve();
    }

    async isPlaying(channelId: string): Promise<boolean> {
        const current = await this.getCurrent(channelId);
        return !!current;
    }

    async addToPlaylist(url: string, userid: string): Promise<PlayResult> {
        const result = new PlayResult();
        const size = await this.dbContextProvider.execute(db => db.playlistItemRepo.getPlaylistSize(userid));
        if (size >= this.playlistItemLimit) {
            result.full = true;
            return result;
        }

        if (!await this._streamer.validate(url)) {
            result.invalid = true;
            return result;
        }

        const info = await this._streamer.getInfo(url);
        if (!info) {
            result.unavailable = true;
            return result;
        }

        result.title = info.title;

        const item: IPlaylistItem = {
            title: info.title,
            url: url,
            userid: userid,
            duration: info.lengthSeconds,
            created: new Date(),
        };

        await this.dbContextProvider.execute(db => db.playlistItemRepo.insert(item));
        return result;
    }

    async playlist(userid: string): Promise<unknown> {
        const playlist = await this.dbContextProvider.execute(db => db.playlistItemRepo.getPlaylist(userid));
        const message = new Discord.MessageEmbed();
        message.setColor(embedColor);
        let text = '';
        if (playlist.length > 0) {
            for (const item of playlist) {
                text += `${item.row_num} <<${item.title}>> - ${item.duration}\n`;
            }
        } else {
            text = 'У тебя нет ничего.';
        }
        text = '```' + text + '```';
        message.setDescription(text);

        return message;
    }

    async deleteFromPlaylist(userid: string, index: number): Promise<void> {
        await this.dbContextProvider.execute(async db => {
            const itemId = await db.playlistItemRepo.getItemIdByRowNumber(userid, index);
            if (itemId) {
                await db.playlistItemRepo.deleteItem(itemId);
            }
        });
    }

    async deletePlaylist(userid: string): Promise<void> {
        await this.dbContextProvider.execute(db => db.playlistItemRepo.delete(userid));
    }

    getTrackMessage(item: ISessionItem, added = true): unknown {
        const text = `${added ? '+ -->' : '-    '} ${item.index} ${item.info.title} -- ${item.info.lengthSeconds}\n`;
        return '```diff\n' + text + '```';
    }

    getTrackQueuedMessage(item: ISessionItem): unknown {
        const text = `+     ${item.index} ${item.info.title} -- ${item.info.lengthSeconds}\n`;
        return '```diff\n' + text + '```';
    }

    /**
     * Ends stream session. Deletes the session.
     * @param voiceChannelId 
     * @param suppress suppresses the 'end' event
     */
    endSession(voiceChannelId: string, suppress?: boolean): void {
        const session = this.getSessionByChannel(voiceChannelId);
        if (session) {
            session.end(suppress);
        }

        Helpers.removeItem(this._sessions, x => x.voiceChannelId === voiceChannelId);
    }

    disconnect(voiceChannelId: string): void {
        this.useSession(voiceChannelId, x => Promise.resolve(x.disconnect()));
    }

    toString(): string {
        if (this._sessions.length > 0) {
            let result = 'Music sessions currently active:\n'
            for (const session of this._sessions) {
                const pre = `${session.voiceChannelName}(${session.voiceChannelId})`;
                if (session.current) {
                    const title = session.current.info.title;
                    const duration = session.current.info.lengthSeconds.toString();
                    result += ` -- ${pre} playing ${title} ${duration}\n`;
                } else {
                    result += ` -- ${pre} not playing anything\n`;
                }
            }

            return result;
        }
    }
}