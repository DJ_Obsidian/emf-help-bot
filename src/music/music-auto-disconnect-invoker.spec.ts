import { getMusicPlayerFake, getSettingsHelperFake } from "../common-spec/common.spec";
import { MusicAutoDisconnectTimedInvoker } from "./music-auto-disconnect-invoker";


describe("MusicAutoDisconnectTimedInvoker", () => {

    describe("initAutoDisconnectTimer", () => {

        const musicPlayer = getMusicPlayerFake();

        const invoker = new MusicAutoDisconnectTimedInvoker(musicPlayer, getSettingsHelperFake());

        let disconnectSpy: jasmine.Spy;

        beforeAll(done => {
            invoker.idleTimeMillis = 10;
            disconnectSpy = spyOn(musicPlayer, "disconnect").and.callThrough()
                .and.callFake(() => done());

            invoker.initAutoDisconnectTimer("2");
        });

        it("should call disconnect", () => {
            expect(disconnectSpy).toHaveBeenCalledOnceWith("2");
        });

        it("should delete timer", () => {
            expect(invoker.timers.length).toEqual(0);
        });
    });

    describe("clearAutoDisconnectTimer", () => {

        const musicPlayer = getMusicPlayerFake();

        const invoker = new MusicAutoDisconnectTimedInvoker(musicPlayer, getSettingsHelperFake());

        let disconnectSpy: jasmine.Spy;

        beforeAll(done => {
            invoker.idleTimeMillis = 1000;
            disconnectSpy = spyOn(musicPlayer, "disconnect").and.callThrough();

            invoker.initAutoDisconnectTimer("1");
            invoker.initAutoDisconnectTimer("2");
            invoker.clearAutoDisconnectTimer("2");
            done();
        });

        it("should not call disconnect", () => {
            expect(disconnectSpy).not.toHaveBeenCalled();
        });

        it("should delete the timer", () => {
            expect(invoker.timers.findIndex(x => x.voiceChannelId === "2")).toEqual(-1);
            expect(invoker.timers.findIndex(x => x.voiceChannelId === "1")).not.toEqual(-1);
        });
    });

});