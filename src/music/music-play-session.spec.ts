import { EventEmitter } from "events";
import { Readable } from "stream";
import { IStreamable, IStreamSession } from "../discord-event";
import { ISessionItem, MusicPlaySession } from "./music-play-session";
import { IMusicStreamer } from "./streamer";

class TestStreamSession extends EventEmitter implements IStreamSession {
    
    constructor(private destroyCounter: {times: number}) {
        super();
        setTimeout(() => this.emit("start"), 50);
        setTimeout(() => this.emit("finish"), 50); // Finish stream
    }
    pause(): Promise<void> {
        return Promise.resolve();
    }
    resume(): Promise<void> {
        return Promise.resolve();
    }
    end(): Promise<void> {
        return Promise.resolve();
    }
    destroy(): void {
        this.destroyCounter.times++;
    }
}

describe("MusicPlaySession", () => {

    const streamable: IStreamable = {
        stream: () => null,
        voiceChannelId: "",
        voiceChannelName: "",
        disconnect: () => null,
    };

    const streamer: IMusicStreamer = {
        getStream: () => null,
        validate: () => null,
        getInfo: () => null,
        search: () => null,
    };

    describe("play", () => {

        describe("when playing first track", () => {
            
            const musicPlaySession = new MusicPlaySession(streamable, streamer);

            const testReadable = {
                destroy: (): void => {},
            };

            const streamSessionDestroyCounter = {
                times: 0,
            };

            let started: ISessionItem;
            let finished: ISessionItem;

            let getStreamSpy: jasmine.Spy;
            let streamSpy: jasmine.Spy;
            let readableDestroySpy: jasmine.Spy;

            beforeAll(done => {
                getStreamSpy = spyOn(streamer, "getStream").and.callThrough()
                    .and.returnValue(<any>testReadable);
                streamSpy = spyOn(streamable, "stream").and.callThrough()
                    .and.callFake(() => Promise.resolve(new TestStreamSession(streamSessionDestroyCounter)));
                readableDestroySpy = spyOn(testReadable, 'destroy').and.callThrough();
                musicPlaySession.queue({
                    lengthSeconds: 10,
                    title: "test",
                    url: "https",
                },
                "user 1");
                musicPlaySession.on("start", s => started = s);
                musicPlaySession.on("finish", s => finished = s);
                musicPlaySession.on("end", () => done());
                musicPlaySession.start();
            });

            it("should be playing", () => {
                expect(musicPlaySession.playing).toBeFalse();
            });

            it("current should be null", () => {
                expect(musicPlaySession.current).toBeFalsy();
            });

            it("should have started", () => {
                expect(started).toBeTruthy();
            });

            it("should have finished", () => {
                expect(finished).toBeTruthy();
            });

            it("should have called stream", () => {
                expect(streamSpy).toHaveBeenCalledOnceWith(testReadable);
            });

            it("should call destroy on stream readable", () => {
                expect(readableDestroySpy).toHaveBeenCalledTimes(1);
            });

            it("should call destroy on stream session", () => {
                expect(streamSessionDestroyCounter.times).toEqual(1);
            });
        });

        describe("when playing consecutive tracks", () => {
            
            const musicPlaySession = new MusicPlaySession(streamable, streamer);

            const testReadable = {
                destroy: (): void => {},
            };

            const streamSessionDestroyCounter = {
                times: 0,
            };

            let started: ISessionItem[] = [];
            let finished: ISessionItem[] = [];

            let getStreamSpy: jasmine.Spy;
            let streamSpy: jasmine.Spy;
            let readableDestroySpy: jasmine.Spy;

            beforeAll(done => {
                getStreamSpy = spyOn(streamer, "getStream").and.callThrough()
                    .and.returnValue(<any>testReadable);
                streamSpy = spyOn(streamable, "stream").and.callThrough()
                    .and.callFake(() => Promise.resolve(new TestStreamSession(streamSessionDestroyCounter)));
                readableDestroySpy = spyOn(testReadable, 'destroy').and.callThrough();
                musicPlaySession.queue({
                    lengthSeconds: 10,
                    title: "test 1",
                    url: "https",
                },
                "user 1");
                musicPlaySession.queue({
                    lengthSeconds: 10,
                    title: "test 2",
                    url: "https",
                },
                "user 2");
                musicPlaySession.on("start", s => started.push(s));
                musicPlaySession.on("finish", s => finished.push(s));
                musicPlaySession.on("end", () => done());
                musicPlaySession.start();
            });

            it("should be playing", () => {
                expect(musicPlaySession.playing).toBeFalse();
            });

            it("current should be null", () => {
                expect(musicPlaySession.current).toBeFalsy();
            });

            it("should have started", () => {
                expect(started.length).toEqual(2);
            });

            it("should have finished", () => {
                expect(finished.length).toEqual(2);
            });

            it("should have called stream", () => {
                expect(streamSpy).toHaveBeenCalledTimes(2);
            });

            it("should call destroy on stream readable", () => {
                expect(readableDestroySpy).toHaveBeenCalledTimes(2);
            });

            it("should call destroy on stream session", () => {
                expect(streamSessionDestroyCounter.times).toEqual(2);
            });
        });
    });
    
});