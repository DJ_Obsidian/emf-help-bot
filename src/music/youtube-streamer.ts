import { Readable } from "stream";
import { IMusicInfo, IMusicStreamer } from "./streamer";
import * as ytdl from 'ytdl-core'
// eslint-disable-next-line @typescript-eslint/no-var-requires
const ytSearch = require('yt-search');

export class YoutubeStreamer implements IMusicStreamer {

    getStream(url: string): Readable {
        return ytdl(url, { filter : 'audioonly', dlChunkSize: 250000 });
    } 

    validate(url: string): Promise<boolean> {
        return Promise.resolve(ytdl.validateURL(url));
    }

    async getInfo(url: string): Promise<IMusicInfo> {
        try {
            const info = await ytdl.getInfo(url);
            return {
                title: info.videoDetails.title,
                lengthSeconds: parseInt(info.videoDetails.lengthSeconds),
                url: url,
            };
        } catch (error) {
            console.error(error);
            // Video unavailable
            return null;
        }
    }

    async search(query: string): Promise<IMusicInfo> {
        const result = await ytSearch(query);
        const video = result.videos.length > 0 ? result.videos[0] : null;

        if (video) {
            return {
                title: video.title,
                lengthSeconds: video.duration.seconds,
                url: video.url,
            };
        } else {
            return null;
        }
    }
}