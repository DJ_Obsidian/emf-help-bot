import { Readable } from "stream";


export interface IMusicInfo {
    title:string;
    lengthSeconds:number;
    url:string;
}

export interface IMusicStreamer {
    getStream(url: string): Readable;
    validate(url: string): Promise<boolean>;
    getInfo(url: string): Promise<IMusicInfo>;
    search(query: string): Promise<IMusicInfo>;
}