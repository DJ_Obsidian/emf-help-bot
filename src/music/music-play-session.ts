import { EventEmitter } from 'events';
import { Readable } from 'stream';
import { IStreamSession, IStreamable } from "../discord-event";
import { IMusicInfo, IMusicStreamer } from './streamer';

export interface ISessionItem {
    url: string;
    index: number;
    info: IMusicInfo;
    readable?: Readable;
    streamSession?: IStreamSession;
    session: MusicPlaySession;
    userid: string;
    error?: unknown;
}

export class MusicPlaySession extends EventEmitter {

    private _queueList: ISessionItem[] = [];
    private _current: ISessionItem;
    private _playing = false;

    get current(): ISessionItem {
        return this._current;
    }

    get playing(): boolean {
        return this._playing;
    }

    get voiceChannelId(): string {
        return this._streamable.voiceChannelId;
    }

    get voiceChannelName(): string {
        return this._streamable.voiceChannelName;
    }

    get items(): ISessionItem[] {
        return this._queueList;
    }

    constructor(private _streamable: IStreamable, private _streamer: IMusicStreamer) {
        super();
    }

    private handleError(current: ISessionItem, error: Error): void {
        current.error = error;
        // Error during streaming?
        this._playing = false;
        this.emit("error", current, error);
        this.next(); // Play next
    }

    start(): void {
        if (!this._current) {
            this.next();
        }
    }

    private onTrackStart(): void {
        this._playing = true;
        this.emit("start", this._current);
    }

    private onTrackFinish(): void {
        this._playing = false;
        this.destroySessionItem();
        this.emit("finish", this._current);
        this.next(); // Play next
    }

    private onError(error: Error): void {
        this.handleError(this._current, error);
    }

    private onTrackStop(): void {
        this._playing = false;
        this.destroySessionItem();
        this.emit("stop", this._current);
    }

    private onTrackPause(): void {
        this.emit("pause", this._current);
    }

    private onTrackResume(): void {
        this.emit("resume", this._current);
    }

    private destroySessionItem(): void {
        if (this._current) {
            if (this._current.streamSession) {
                this._current.streamSession.destroy();
            }

            if (this._current.readable) {
                this._current.readable.destroy();
            }
        }
    }
    
    next(): ISessionItem {
        if (this._playing) { // Cannot skip while playing
            return;
        }

        this._current = this.getNextItem();
        if (this._current) {
            setImmediate(() => {
                const readable = this._streamer.getStream(this._current.url);
                this._current.readable = readable;
                this._streamable.stream(readable).then(
                    (s: IStreamSession) => {
                        this._current.streamSession = s;
                        s.on("start", () => this.onTrackStart());
                        s.on("finish", () => this.onTrackFinish());// When track is has finished playing
                        s.on("error", (error: Error) => this.onError(error));
                        s.on("end", () => this.onTrackStop()); // When track is stopped
                        s.on("pause", () => this.onTrackPause());
                        s.on("resume", () => this.onTrackResume());
                    }
                ).catch(error => this.onError(error));
            });
        } else {
            this._playing = false;
            this.clear();
            this.emit("end", this);
        }

        return this._current;
    }

    queue(track: IMusicInfo, userid: string): ISessionItem {
        const sessionItem = {
            url: track.url,
            info: track,
            index: this._queueList.length,
            session: this,
            userid: userid,
        };

        this._queueList.push(sessionItem);
        return sessionItem;
    }

    private getNextItem(): ISessionItem {
        return this._queueList.find(x => x.index === (this._current ? this._current.index + 1 : 0));
    }

    /**
     * Destroys any streams and clears the list.
     */
    clear(): void {
        if (this._playing) {
            return;
        }

        this._queueList = [];
        this._current = null;
    }

    /**
     * Ends session by ending the current stream and clearing the list of current tracks.
     * Also emits an event 'end'.
     */
    end(suppress?: boolean): void {
        this._playing = false;
        if (this._current && this._current.streamSession) {
            this._current.streamSession.destroy(); // End current stream instance
        }
        
        this.clear();

        if (suppress !== true) {
            this.emit("end", this);
        }
    }

    /**
     * Disconnect bot from voice channel.
     */
    disconnect(): void {
        this._streamable.disconnect();
    }
}