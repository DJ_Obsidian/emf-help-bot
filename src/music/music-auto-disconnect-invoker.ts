import Helpers from "../helpers";
import { SettingsHelper } from "../settings-helper";
import { ISessionItem, MusicPlaySession } from "./music-play-session";
import { MusicPlayer } from "./music-player";


export class MusicAutoDisconnectTimedInvoker {

    private _timers: {[voiceChannelId: string] : NodeJS.Timeout; } = {};

    get timers(): {voiceChannelId: string}[] {
        return Object.keys(this._timers).map(x => <{voiceChannelId: string}>{voiceChannelId: x});
    }
    
    idleTimeMillis = 1000 * 60 * 5; // 5 Mins

    constructor(private musicPlayer: MusicPlayer, private settingsHelper: SettingsHelper) {
        this.musicPlayer.on("end", (s: MusicPlaySession) => this.onSessionEnd(s));
        this.musicPlayer.on("start", (s: ISessionItem) => this.onSessionStart(s.session));
        this.musicPlayer.on("pause", (s: ISessionItem) => this.onSessionPause(s.session));
        this.musicPlayer.on("resume", (s: ISessionItem) => this.onSessionResume(s.session));
        this.musicPlayer.on("stop", (s: ISessionItem) => this.onSessionStop(s.session));
    }

    private onSessionEnd(session: MusicPlaySession): void {
        this.initAutoDisconnectTimer(session.voiceChannelId);
    }
    
    private onSessionStart(session: MusicPlaySession): void {
        this.clearAutoDisconnectTimer(session.voiceChannelId);
    }
    
    private onSessionPause(session: MusicPlaySession): void {
        this.initAutoDisconnectTimer(session.voiceChannelId);
    }
    
    private onSessionResume(session: MusicPlaySession): void {
        this.clearAutoDisconnectTimer(session.voiceChannelId);
    }
    
    private onSessionStop(session: MusicPlaySession): void {
        this.initAutoDisconnectTimer(session.voiceChannelId);
    }

    private disconnectFromVoiceChannel(voiceChannelId: string): void { //todo: add log to say that the bot left voice channel due to inactivity
        this.musicPlayer.disconnect(voiceChannelId);
        delete this._timers[voiceChannelId];
    }

    initAutoDisconnectTimer(voiceChannelId: string): void {
        this.clearAutoDisconnectTimer(voiceChannelId);
        if (!Helpers.isProduction) {
            console.log(`Music play session idle countdown started for channel ${voiceChannelId}.`);
        }

        this._timers[voiceChannelId] = setTimeout((id: string) => this.disconnectFromVoiceChannel(id), this.idleTimeMillis, voiceChannelId);
    }
    
    clearAutoDisconnectTimer(voiceChannelId: string): void {
        if (!Helpers.isProduction) {
            console.log(`Music play session idle countdown cleared for channel ${voiceChannelId}.`);
        }

        clearTimeout(this._timers[voiceChannelId]);
        delete this._timers[voiceChannelId];
    }

    toString(): string {
        const voiceChannelIds = Object.keys(this._timers);
        if (voiceChannelIds.length > 0) {
            let result = 'Current music session auto-shutdown timers for channels:\n';
            for (const key of voiceChannelIds) {
                result += ` -- ${key}\n`;
            }
    
            return result;
        } else {
            return null;
        }
    }

    destroy(): void {
        for (const timeout of Object.values(this._timers)) {
            clearTimeout(timeout);
        }

        this._timers = {};
    }
}