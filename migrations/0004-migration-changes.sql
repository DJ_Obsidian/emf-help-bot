-- Cooldowns -  remove foreign key to Users
alter table Cooldowns drop constraint command; -- Remove unique constraint
SELECT CONCAT(
    'ALTER TABLE `Cooldowns` DROP FOREIGN KEY `',
    constraint_name,
    '`'
) INTO @sqlst
FROM information_schema.key_column_usage
WHERE table_name = 'Cooldowns'
    AND column_name = 'userid'
    and referenced_table_name = 'users'
    and referenced_column_name = 'userid';
PREPARE stmt FROM @sqlst;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
SET @sqlst = NULL;

-- ArkUsers - remove foreign key to users
SELECT CONCAT(
    'ALTER TABLE `ArkUsers` DROP FOREIGN KEY `',
    constraint_name,
    '`'
) INTO @sqlst
FROM information_schema.key_column_usage
WHERE table_name = 'ArkUsers'
    AND column_name = 'userid'
    and referenced_table_name = 'users'
    and referenced_column_name = 'userid';
PREPARE stmt FROM @sqlst;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;
SET @sqlst = NULL;

alter table ArkUsers drop constraint userid; -- Remove unique constraint

-- Fix Users
alter table Users drop primary key;
alter table Users add column id int primary key NOT NULL AUTO_INCREMENT;
alter table Users add unique key UniqueUser (userid); -- add unique constraint

SET SQL_SAFE_UPDATES = 0;

-- Fix Cooldowns
alter table Cooldowns add column user_id INT NOT NULL; -- add new column for FK
update Cooldowns c -- Update user id's
join Users u on u.userid = c.userid
set c.user_id = u.id;
alter table Cooldowns add unique key UniqueCooldown (command, channel, user_id); -- add unique constraint

alter table Cooldowns -- add foreign key
add foreign key (user_id) references Users(id) ON DELETE CASCADE;

alter table Cooldowns
drop column userid;

-- Fix ArkUsers
alter table ArkUsers add column user_id INT NOT NULL; -- add new column for FK
update ArkUsers c -- Update user id's
join Users u on u.userid = c.userid
set c.user_id = u.id;
alter table ArkUsers add unique key UniqueArkUser (user_id, ark_knightid); -- add unique constraint

alter table ArkUsers -- add foreign key
add foreign key (user_id) references Users(id) ON DELETE CASCADE;

alter table ArkUsers
drop column userid;
