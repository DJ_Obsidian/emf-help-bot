create table Pooks (
	userid varchar(50) primary key NOT NULL,
    pooked datetime NOT NULL,
    FOREIGN KEY (userid) REFERENCES Users(userid)
);
