
create table ArkProfessions (
	id int primary key NOT NULL,
    name varchar(25) NOT NULL,
    unique key (name)
);

create table ArkPositions (
	id int primary key NOT NULL,
    name varchar(25) NOT NULL,
    unique key (name)
);

create table ArkTags (
	id int primary key NOT NULL,
    name varchar(25) NOT NULL,
    unique key (name)
);

create table ArkKnights (
	id int primary key NOT NULL,
	characterid varchar(50) NOT NULL,
    name varchar(100) NOT NULL,
    rarity int NOT NULL,
    itemusage longtext NULL,
    description longtext NULL,
    itemdesc longtext NULL,
    positionid int NOT NULL,
    professionid int NOT NULL,
    unique key (characterid),
    unique key (name),
    FOREIGN KEY (positionid) REFERENCES ArkPositions(id),
    FOREIGN KEY (professionid) REFERENCES ArkProfessions(id)
);

create table ArkKnightTags (
	ark_knightid int NOT NULL,
    ark_tagid int NOT NULL,
    index index_arkknight_arktag (ark_knightid, ark_tagid),
	FOREIGN KEY (ark_knightid) REFERENCES ArkKnights(id),
    FOREIGN KEY (ark_tagid) REFERENCES ArkTags(id)
);

create table ArkUsers (
	userid varchar(50) NOT NULL,
    ark_knightid int NOT NULL,
    times int NOT NULL,
    unique key (userid, ark_knightid),
    FOREIGN KEY (userid) REFERENCES Users(userid),
    FOREIGN KEY (ark_knightid) REFERENCES ArkKnights(id)
);

alter table Users add arkgrd5misses int NULL;

create table Cooldowns (
	id int primary key NOT NULL AUTO_INCREMENT,
	command int NOT NULL,
    userid varchar(50) NOT NULL,
    channel int NOT NULL,
    created datetime NOT NULL,
    hours int NOT NULL,
    FOREIGN KEY (userid) REFERENCES Users(userid),
    FOREIGN KEY (channel) REFERENCES Channels(id),
    unique key (command, userid, channel)
);

drop table Pooks;

insert into Channels (id, channelid, description) values (-1, '', 'unspecified');