
alter table Users
add column lastpresent datetime NOT NULL DEFAULT NOW();

alter table Users
add column deleted datetime NULL;

create table Roles (
	id int primary key NOT NULL auto_increment,
    roleid varchar(50) NOT NULL,
    name varchar(100) NOT NULL,
    unique key (roleid)
);

create table RoleUsers (
	role_id int NOT NULL,
    user_id int NOT NULL,
    created datetime NOT NULL default NOW(),
    unique key (role_id, user_id),
    FOREIGN KEY (user_id) REFERENCES Users(id) ON DELETE CASCADE,
    FOREIGN KEY (role_id) REFERENCES Roles(id) ON DELETE CASCADE
);

drop table Settings;
