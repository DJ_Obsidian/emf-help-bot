
alter table Roles
add column color varchar(7) NOT NULL;

alter table RoleUsers
add column personal_role_id int NULL,
add foreign key (personal_role_id) references Roles(id);

alter table RoleUsers add unique key PersonalRole (personal_role_id);