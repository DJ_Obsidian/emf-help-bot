
insert into Channels (id, channelid, description) values (7, '793483231835979776', 'awards');

create table Awards (
	id int primary key NOT NULL,
    name varchar(25) NOT NULL,
    file varchar(25) NOT NULL,
    unique key (name)
);

insert into Awards (id, name, file) values (1, 'орден', 'orden.png');
insert into Awards (id, name, file) values (2, 'медаль1', 'medal1.png');
insert into Awards (id, name, file) values (3, 'медаль2', 'medal2.png');
insert into Awards (id, name, file) values (4, 'медаль3', 'medal3.png');
insert into Awards (id, name, file) values (5, 'защита', 'protection.png');
insert into Awards (id, name, file) values (6, 'отвага', 'valor.png');
insert into Awards (id, name, file) values (7, 'победа', 'victory.png');
insert into Awards (id, name, file) values (8, 'охота', 'hunt.png');

create table AwardUsers (
    award_id int NOT NULL,
	user_id int NOT NULL,
    description varchar(500) NOT NULL,
    created datetime NOT NULL,
	FOREIGN KEY (user_id) REFERENCES Users(id) ON DELETE CASCADE,
    FOREIGN KEY (award_id) REFERENCES Awards(id)
);