
insert into Channels (id, channelid, description) values (9, '827950280925642839', 'music');

create table PlaylistItems (
	id int primary key NOT NULL auto_increment,
    user_id int NOT NULL,
    title varchar(500) NOT NULL,
    duration int NOT NULL,
    url varchar(250) NOT NULL,
    created datetime NOT NULL default NOW(),
    FOREIGN KEY (user_id) REFERENCES Users(id) ON DELETE CASCADE
);