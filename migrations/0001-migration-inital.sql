create table __migrations (
	id int primary key NOT NULL,
    name varchar(300) NOT NULL,
    date datetime NOT NULL
);

create table Settings (
	id int primary key NOT NULL,
    value varchar(300) NOT NULL
);
insert into Settings (id, value) values (1, 'NjU3NjMyOTg5NjMwNTYyMzM1.Xf0Ccg.cHsiCwzLSv44qUtZgRPmlSo16s0');

create table Users (
	userid varchar(50) primary key NOT NULL,
    usernametag varchar(100) NOT NULL,
    banneduntil datetime NULL
);

create table AccessLevels (
	id int primary key NOT NULL,
	roleid varchar(50) NOT NULL,
    name varchar(50) NOT NULL,
    unique key (id, roleid)
);
insert into AccessLevels (id, roleid, name) values (-1, '635934919167705098', 'Banned'); -- server ban role
insert into AccessLevels (id, roleid, name) values (1, '800169493841051668', 'Leader');
insert into AccessLevels (id, roleid, name) values (2, '630867889527717888', 'Admin');
insert into AccessLevels (id, roleid, name) values (3, '800421967064596500', 'Fuhrer');

create table Channels (
	id int primary key NOT NULL,
    channelid varchar(50) NOT NULL,
    description varchar(100) NOT NULL,
    index (channelid)
);
insert into Channels (id, channelid, description) values (1, '630848078810841101', 'general');
insert into Channels (id, channelid, description) values (2, '636297013864169500', 'nsfw');
insert into Channels (id, channelid, description) values (3, '773601744495312947', 'server-info');
insert into Channels (id, channelid, description) values (4, '773601597077979157', 'server-rules');
insert into Channels (id, channelid, description) values (5, '773601770412834829', 'bgs-info');
insert into Channels (id, channelid, description) values (6, '630870808935530516', 'admin');
    
-- Retrospectively added
insert into Channels (id, channelid, description) values (8, '818580161301446706', 'reception');