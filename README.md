# EMF help bot

Discord bot for EMF server. Бот для дискорд сервера ЕМФ

Как настроить MariaDB:

1) Скачать и установить сервер
2) Скачать и установить DBeaver
3) Запустить сервер и открыть редактор в DBeaver
4) В редакторе, запустить эти команды:

CREATE DATABASE emfbot;
CREATE USER 'bot'@'localhost' IDENTIFIED VIA mysql_native_password using PASSWORD('secret1');
GRANT ALL ON emfbot.* TO 'bot'@'localhost';
FLUSH PRIVILEGES;

DEV - Как начать разработку.

1) Для начала надо запустить:
    
    npm start


2) Далее запустить тесты (MySQL должен быть настроен сперва и надо запустить бот хотя бы раз, чтоб установить SQL скрипты):
    
    npm test


3) Чтобы выпустить версию бота на живой сервер, использовать команду: 
    
    npm run-script prod

Новый файл app.js будет в папке dist. Его и устанавливаем.


Проверять на потребление памяти, нет ли утечек:

1) Запустить бота командой node --inspect ./dist/dev/app.js
2) Открыть chrome
3) Вписать в ссылку chrome://inspect/#devices
4) Нажать на F12
5) Нажать на trace


MySQL cyrillic compatibility fix:
ALTER DATABASE <database> CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
ALTER TABLE <table_name> CONVERT TO CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;


Common errors:
---- Error: Cannot find module '/home/container/node_modules/@discordjs/opus/prebuild/node-v72-napi-v3-linux-x64-musl-1.1.24/opus.node'
	Solution:
		- Download file from this link  https://github.com/discordjs/opus/releases/download/v0.5.0/opus-v0.5.0-node-v72-napi-v3-linux-x64-musl-1.1.24.tar.gz
		- Extract opus.node file and paste it in the missing directory above on the server. This is likely due to the server running on linux and the npm package is unable to download it
		for some reason.
