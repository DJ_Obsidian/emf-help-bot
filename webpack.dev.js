/* eslint-disable no-undef */
/* eslint-disable @typescript-eslint/no-var-requires */

const path = require('path');

const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');

__dirname = path.join(__dirname, "/");
const outputDir = __dirname + 'dist/dev/';

module.exports = merge(common, {
    mode: 'development',
    watch: true,
    devtool: 'source-map',
    output: {
        filename: 'app.js',
        path: outputDir
    },
});