/* eslint-disable no-undef */
/* eslint-disable @typescript-eslint/no-var-requires */

const path = require('path');
const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');

__dirname = path.join(__dirname, "/");
const outputDir = __dirname + 'dist/prod/';

module.exports = merge(common, {
    mode: 'production',
    output: {
        filename: 'app.js',
        path: outputDir
    },
});